import React from 'react'
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import deepFreeze from 'deep-freeze'
import { fetchMock } from "jest-fetch-mock";
import { mockPlayer, mockPlayers } from '../mocks/PlayerMocks'
global.React = React
//global.fetch = fetchMock
global._demoUserCredentials = deepFreeze({
	email:'deo@y.z',
	password:'dddddd'
})
global._demoUser = deepFreeze({
	coach: "5d6ec4f42f11be0838fa5533",
	coachesFollowed: [],
	created: "2019-09-03T19:54:28.278Z",
	email: "deo@y.z",
	firstName: "Deo",
	groups: ["5d6ec790945a4023c00e37d6"],
	groupsFollowed: [],
	isSystemAdmin: false,
	mainGroup: "5d6ec790945a4023c00e37d6",
	playersFollowed: [],
	surname: "O",
	username: "deo",
	_id: "5d6ec4f42f11be0838fa5534"
})
global._testUser = deepFreeze({
	userName:'User1UserName',
	firstName:'User1FirstName',
	surname:'User1Surname',
	_id:'User1Id',
	groups:[],
	player:mockPlayer
})
global._testPlayers = deepFreeze(mockPlayers)

global.alert = jest.fn();

Enzyme.configure({ adapter: new Adapter() });

