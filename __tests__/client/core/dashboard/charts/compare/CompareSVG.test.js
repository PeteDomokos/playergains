import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import { mockItem } from '../../../../../../mocks/DisplayItemMocks'
import CompareSVG from '../../../../../../client/core/dashboard/charts/compare/CompareSVG.js'

describe('<CompareSVG/>', () =>{
	describe('render methods', () =>{
		test('should render correctly', () =>{
			//normal case
			const wrapper = shallow(<CompareSVG item={mockItem}/>)
			//need to mock out svgContainer and its getBoundingClient() method and attach to this
			//two svgs based on screen size which one has display:none
			expect(wrapper.find('svg').length).toBe(1)
		}),
		test('initSelectorsAndChartValues', () =>{

		}),
		test('updateChartState', () =>{

		}),
		test('handleShowOptionsButtonClick', () =>{

		}),
		test('handleOptionButtonClick', () =>{

		}),
		test('handleSuboptionButtonClick', () =>{

		}),
		test('runAnimation', () =>{

		}),
		test('handleDemoClick', () =>{

		}),
		test('moveAnimationOn', () =>{

		}),
		test('runAnimation', () =>{

		})
	})
})
