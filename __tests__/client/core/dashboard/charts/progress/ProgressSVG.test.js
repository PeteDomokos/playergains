import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import { mockItem } from '../../../../../../mocks/DisplayItemMocks'
import ProgressSVG from '../../../../../../client/core/dashboard/charts/progress/ProgressSVG.js'

describe('<ProgressSVG/>', () =>{
	describe('render methods', () =>{
		test('should render correctly', () =>{
			//normal case
			const wrapper = shallow(<ProgressSVG item={mockItem}/>)
			//need to mock out svgContainer and its getBoundingClient() method and attach to this
			expect(wrapper.find('svg').length).toBe(1)
		}),
		test('initChartValues', () =>{

		}),
		test('handleShowOptionsButtonClick', () =>{

		}),
		test('handleOptionButtonClick', () =>{

		}),
		test('handleSuboptionButtonClick', () =>{

		}),
		test('handlePlayerClick', () =>{

		}),
		test('handleAddPlayer', () =>{

		}),
		test('handleRemovePlayer', () =>{

		}),
		test('onAnimationChange', () =>{

		}),
		test('updateState', () =>{

		})
	})
})
