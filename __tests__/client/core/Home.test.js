import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import auth from '../../../client/auth/auth-helper'
import Home from '../../../client/core/Home.js'
import { demoUser, mockUser } from '../../../mocks/UserMocks'
import * as apiAuth from '../../../client/auth/api-auth'

jest.mock('../../../client/user/Profile') 

describe('<Home/>', () =>{
	describe('render methods', () =>{
		test('should render correctly when user not logged in', () =>{
			auth.isAuthenticated = jest.fn(() => false)
			const wrapper = shallow(<Home/>)
			expect(wrapper.find('section').length).toBe(3)
			expect(wrapper.find('div').length).toBe(12)
			expect(wrapper.find('h2').length).toBe(2)
			expect(wrapper.find('p').length).toBe(4)
			expect(wrapper.find('button').length).toBe(2)
		})
		test('should render correctly when user is logged in', () =>{
			auth.isAuthenticated = jest.fn(() => {
				return {user:_testUser}
			})
			const wrapper = shallow(<Home/>)
			expect(wrapper.find('section').length).toBe(1)
			expect(wrapper.find('div').length).toBe(14)
			expect(wrapper.find('button').length).toBe(6)
		})
	})
	describe('demoSignin', () =>{
		//the user that we are expecting to be signed in for demo 
		const demoUser = {
		      email:'deo@y.z',
		      password:'dddddd'
		    }
		beforeEach(() =>{

		})
		test('calls signin() with demo user object no matter what section is clicked', () =>{
			//mock module not being found so doing it manually here
		    auth.isAuthenticated = jest.fn(() => {
				return {user:_demoUser}
			})
			auth.authenticate = jest.fn(() => {
				return {user:_demoUserCredentials}
			})
			apiAuth.signin = jest.fn(() => Promise.resolve({user:_demoUser}));
			const homeComponent = new Home()
			const home = homeComponent.demoSignin('any-section')
			expect(apiAuth.signin).toHaveBeenCalledWith(_demoUserCredentials)
		})
		//note - need done because auth.autheticate takes a cb
		test('if signin() is successful, and section is user-home, pushes empty string to history ', async () =>{
			//mock module not being found so doing it manually here
		    auth.isAuthenticated = jest.fn(() => {
				return {user:_demoUser}
			})
			auth.authenticate = jest.fn((jwt, cb) => {
				cb()
			})
			apiAuth.signin = jest.fn(() => Promise.resolve({user:_demoUser}));
			const mockHistory = {
				push:jest.fn((path) =>{
				})
			}
			const props = {history:mockHistory}
			const homeComponent = new Home(props)

			//this works but return promise syntax wasnt.????
			const home = await homeComponent.demoSignin('user-home')

			expect(homeComponent.props.history.push).toHaveBeenCalled()
			expect(homeComponent.props.history.push).toHaveBeenCalledWith('/')
		})
		test('if signin is successful, and section name is any string except user-home, pushes section name to history ', async () =>{
			//mock module not being found so doing it manually here
		    auth.isAuthenticated = jest.fn(() => {
				return {user:_demoUser}
			})
			auth.authenticate = jest.fn((jwt, cb) => {
				cb()
			})
			apiAuth.signin = jest.fn(() => Promise.resolve({user:_demoUser}));
			const mockHistory = {
				push:jest.fn((path) =>{
				})
			}
			const props = {history:mockHistory}
			const homeComponent = new Home(props)

			//this works but return promise syntax wasnt.????
			const home = await homeComponent.demoSignin('dashboard')

			expect(homeComponent.props.history.push).toHaveBeenCalled()
			expect(homeComponent.props.history.push).toHaveBeenCalledWith('/dashboard')


		})
		test('if signin is not successful, alerts user of the server error ', () =>{
			//mock module not being found so doing it manually here
		    auth.isAuthenticated = jest.fn(() => {
				return false
			})
			apiAuth.signin = jest.fn(() => Promise.resolve( {error:'sign in error'}));
			const homeComponent = new Home()
			homeComponent.demoSignin('any-section')
			//expect(alert).toHaveBeenCalled()
		})
	})	
})
