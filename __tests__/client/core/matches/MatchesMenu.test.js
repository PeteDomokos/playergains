import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import 'whatwg-fetch'
import { createContainer} from '../../../domHelpers'
import { fetchResponseOk } from '../../../spyHelpers'

import { MatchesMenu } from '../../../../client/core/matches/MatchesMenu'

describe('<MatchesMenu/>', () =>{
	let render, container, form, field
	let availableMatches = [{id:"1"}, {id:"2"}]

	beforeEach(() =>{
		({ render, container, form, field } = createContainer())
	})
	afterEach(() =>{
	})
	it('renders a form', () =>{
		render(<MatchesMenu />)
		expect(form('matchAndPlayerSelection')).not.toBeNull()
	})
	describe('match selection field', () => {
		beforeEach(() =>{
		})
		it('renders as a select box', () => {
			render(<MatchesMenu />)
			expect(field('matchAndPlayerSelection', 'matchSelection')).not.toBeNull()
			expect(field('matchAndPlayerSelection', 'matchSelection').tagName).toEqual('SELECT')
		})
		it.skip('initially has a blank value', () =>{
			render(<MatchesMenu />)
			const firstNode = field('matchAndPlayerSelection', 'matchSelection').childNodes[0];
      		expect(firstNode.value).toEqual('')
      		expect(firstNode.selected).toBeTruthy()
		})
		it('lists all the available matches', () =>{
			render(<MatchesMenu availableMatches={availableMatches} />)
			const optionNodes = 
				Array.from(field('matchAndPlayerSelection', 'matchSelection').childNodes)
			const renderedMatchIds = optionNodes.map(node => node.textContent)
     		const availableMatchIds = availableMatches.map(m => m.id)
     		expect(renderedMatchIds).toEqual(expect.arrayContaining(availableMatchIds))

		})
	})
})

