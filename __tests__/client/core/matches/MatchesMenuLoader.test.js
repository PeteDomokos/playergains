import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import 'whatwg-fetch'
import { createContainer} from '../../../domHelpers'
import { fetchResponseOk } from '../../../spyHelpers'

import { MatchesMenuLoader } from '../../../../client/core/matches/MatchesMenuLoader'
import * as MatchesMenuExports from '../../../../client/core/matches/MatchesMenu'

describe('<MatchesMenuLoader/>', () =>{
	let renderAndWait, container
	let availableMatches = [{id:1}, {id:2}]
	beforeEach(() =>{
		({ renderAndWait, container } = createContainer())
		jest
			.spyOn(window, 'fetch')
			.mockReturnValue(fetchResponseOk(availableMatches))
		jest
			.spyOn(MatchesMenuExports, 'MatchesMenu')
			.mockReturnValue(null)
	})
	afterEach(() =>{
		window.fetch.mockRestore()
		MatchesMenuExports.MatchesMenu.mockRestore()
	})
	it('fetches data when component is mounted', async () =>{
		await renderAndWait(<MatchesMenuLoader/>)

		expect(window.fetch).toHaveBeenCalledWith('/matches', //removed . before /s
			expect.objectContaining({
				method:'GET',
				credentials:'same-origin',
				headers:{'Content-Type':'application/json'}
			})
		)
	})
	it('initially passes no data to Matches Menu', async () =>{
		await renderAndWait(<MatchesMenuLoader/>)
		expect(MatchesMenuExports.MatchesMenu).toHaveBeenCalledWith({
			availableMatches:[]
		}, expect.anything())
	})
	it('passes availableMatches to MatchesMenu when fetched on mount', async () =>{
		await renderAndWait(<MatchesMenuLoader/>)
		expect(MatchesMenuExports.MatchesMenu).toHaveBeenLastCalledWith({
			availableMatches
		}, expect.anything())
	})
})

