/**
 * @jest-environment node
 */
import User from '../../../server/models/user.model'
import { createAndReturnUser, list } from '../../../server/controllers/user.controller'

const mockRequest = (sessionData, body) => ({
  session: { data: sessionData },
  body
})

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res)
  res.json = jest.fn().mockReturnValue(res)
  return res
}
const fields = {
	username:'Meadboy',
	firstName:'Denilson',
	surname:'Carvalho',
	email:'dc@y.z',
	player:{},
	coach:{}
}
const files ={}

const users = {}

describe("createAndReturnUser", () => {
	beforeAll(() =>{ 
	})
	it("creates and saves user when provided with all required fields", () => {
		const res = mockResponse()
		//this is wrong!!!!!!!!!!!! should be user.save
		User.save = jest.fn()
		createAndReturnUser(res, fields, files)

		expect(User.save).toHaveBeenCalledWith(expect.objectContaining({username:'Meadboy'}), expect.anything())
	})
	it("returns saved user to client when provided with all required fields", () => {
		const res = mockResponse();
		//save() takes  - instance to save and ... cb (err, val) => {...}
		//todo - make this async
		User.save = jest.fn().mockImplementation((instance, cb) => cb(null, instance))
		createAndReturnUser(res, fields, files)

		expect(res.status).toHaveBeenCalledWith(200)
		expect(res.json).toHaveBeenCalledWith(expect.objectContaining({username:'Meadboy'}))
	})
	it("returns an error to the client if user cannot be saved", () => {
		const res = mockResponse();
		//save() takes  - instance to save and ... cb (err, val) => {...}
		//todo - make this async
		User.save = jest.fn().mockImplementation((instance, cb) => cb('saving error', null))
		createAndReturnUser(res, fields, files)

		expect(res.status).toHaveBeenCalledWith(400)
		expect(res.json).toHaveBeenCalledWith(expect.objectContaining({error:'saving error'}))
	})
})

describe("list", () => {
	let req, res, queryResult
	beforeEach(() =>{
		req = mockRequest({})
		res = mockResponse()
		queryResult = ["user1", "user2", "user3"]
		User.find = jest.fn().mockImplementation(cb => cb(null, queryResult))
	})
	it("requests all users from the server", () => {
	    list(req, res)
		expect(User.find).toHaveBeenCalled()
	})
	it("returns all users to the client", () => {
	    list(req, res)
		expect(res.status).toHaveBeenCalledWith(200)
		expect(res.json).toHaveBeenCalledWith(queryResult)
	})
})
