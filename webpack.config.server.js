const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const CURRENT_WORKING_DIR = process.cwd()

const config = {
    name: "server",
    entry: [ path.join(CURRENT_WORKING_DIR , './server/server.js') ],
    target: "node",
    output: {
        path: path.join(CURRENT_WORKING_DIR , '/dist/'),
        filename: "server.generated.js",
        publicPath: '/dist/', //http://localhost:3000
        libraryTarget: "commonjs2"
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [ 'babel-loader' ]
            },
            {
                test: /\.(ttf|eot|svg|gif|jpg|png)(\?[\s\S]+)?$/,
                use: 'file-loader'
            },
            {
                test: /\.css$/,
                //exclude: [
                //path.resolve(__dirname, "node_modules/bootstrap"),
                //],
                //TODO - EXCLUDE FILES FROM WIHIN THE GLOBAL STYLES FOLDER, AND MAKE A GLOBAL STYLES FOLDER FOR GLOBAL FILES, INC BOOTSTRAP
                use:['style-loader',
                      {
                       loader:'css-loader',
                       //{modules:true} enables locally scoped css (ie css modules) by default
                       //Q) does this option need to be set for style-loader etc too?
                       options:{ modules: false }
                      }, 
                      {
                       loader:'postcss-loader',
                       options:{ plugin:() => [require('autoprefixer')] }
                      }
                    ]
            }
        ]
    }

}

module.exports = config
