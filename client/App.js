import { hot } from 'react-hot-loader'
import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import MainRouter from './MainRouter'
//redux dependencies
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import {  user, storedItems, available, dashboard, asyncProcesses, dialogs } from './Reducers'
import C from './constants/ConstantsForStore'
import { InitialState } from './constants/InitialStateForStore'
const middleware = applyMiddleware(thunk, createLogger())

const store = createStore(combineReducers(
    { user, storedItems, available, dashboard, asyncProcesses, dialogs }), InitialState, middleware)

  /**
  * Desc ...
  **/
class App extends Component {
  /**
  * Desc ...
  * @return {any} props value
  **/
  render() {
    return (
    	<BrowserRouter>
    		<Provider store={store}>
	        	<MainRouter/>
	        </Provider>
	    </BrowserRouter>
      )
  }
}

export default hot(module)(App)
