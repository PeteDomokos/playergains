export const createGroup = (group, credentials) => {
   console.log("client api-group create:",group)
  return fetch('/api/groups/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + credentials.t
      },
      body: group
    })
    .then((response) => {
      return response.json()
    }).catch((err) => console.log(err))
}

export const listGroups = () => {
  console.log("api-group listGroups...")
  return fetch('/api/groups/', {
    method: 'GET',
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const listUserAdminGroups = (params) => {
  console.log("api-group listGroups...")
  return fetch('/api/groups/by-admin-user/'+params.userId, {
    method: 'GET',
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const readGroup = (params, credentials) => {
  console.log("api-group readGroup")
  return fetch('/api/groups/' + params.id, {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const updateGroup = (params, credentials, group) => {
  console.log("api-group updateGroup")
  return fetch('/api/groups/' + params.id, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    },
    body: group
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}
//todo - merge with update
export const updateGroupFields = (params, credentials, fields) => {
  console.log("api-group updateGroupFields fields", fields)
  return fetch('/api/groups/update-fields/' + params.id, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    },
    body: JSON.stringify(fields)
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const removeGroup = (params, credentials) => {
  console.log("api-group remove")
  return fetch('/api/groups/' + params.id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    console.log("response received")
    return response.json()
  }).catch((err) => console.log(err))
}
export const addPlayer = (params, credentials) => {
  console.log("api-group addPlayer")
  return fetch('/api/groups/' +params.groupId +'/' +params.playerId, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const removePlayer = (params, credentials) => {
  console.log("api-group removePlayer")
  return fetch('/api/groups/' +params.groupId +'/' +params.playerId, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

export const removeAllGroups = () => {
  return fetch('/api/groups/remove-all', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

