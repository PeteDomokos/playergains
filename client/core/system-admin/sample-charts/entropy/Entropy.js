import * as d3 from 'd3'
import React, { useEffect, useState }  from 'react';
import { renderPieChart } from './Pie'
import { renderEntropyNameChart } from './EntropyNameChart'

const containerSt = {border:'solid', width:'100%'}

//helper 
const formatCSV = (d, stringColumns) =>{
	const newD = {}
	Object.keys(d).forEach(key =>{
			newD[key.trim()] = d[key];
	})
    return newD
}

export const Entropy = () =>{
	const [data, setData] = useState('')
	useEffect(() => {
		console.log('loading Entropy Data...')
		//todo - load from server
		const url = 'premier_scorers.csv'
		const stringColumns = []
		d3.csv(url, function(d) {
		    return d
		}).then(function(data) {
			console.log('data', data)
			const teams = data.columns
				.filter(head => head.includes('Player'))
				.map(head => head.replace(' Player', ''))
				.map(team =>{
					const scorers = data
						.map(row =>{
							return {
								name:row[team+' Player'],
								goals:row['Goals '+team]
							}
						}).filter(row => row.goals);

					const top5Scorers = scorers.filter(
						row => row.name !== 'total' && row.name !== 'others')

					return {
						name:team,
						key:team.replace(" ",""),
						scorers:scorers.filter(row => row.name !== 'total'),
						total:scorers.find(row => row.name === 'total').goals,
						top5Scorers:top5Scorers,
						top5Total:d3.sum(top5Scorers, d => d.goals)
					}
				});

			const teamsWithEntropies = teams.map(team =>{
				const ent = (-1) * team.scorers
				//const ent = (-1) * team.top5Scorers
					.map(scorer =>
						(scorer.goals / team.total) * Math.log(scorer.goals / team.total))
					.reduce((a,b) => a + b, 0)
				return{
					...team,
					entropy:ent
				}
			})
			console.log('teams', teamsWithEntropies)
			//setData
			setData(teamsWithEntropies)
		});
	}, [])

	return(
		<div className='entropy' style={containerSt}>
			{!data ?
				<div>loading data...</div>
				:
				<EntropyCharts data={data}/>}
		</div>
	);
}

const entropyContainerSt = {
	height:'100%', display:'flex', justifyContent:'space-around', flexWrap:'wrap', 
	border:'solid', borderColor:'red'
}

const EntropyCharts =({data}) =>{
	//order the data
	//TODO - USE FUNCTIONAL STYLE
	data.sort((d1,d2) => d2.entropy - d1.entropy)
	//todo - make the scale dependent on values
	const min = d3.min(data, d => d.entropy)
	const max = d3.max(data, d => d.entropy)
	var rotScale = d3.scaleLinear().domain([min, max]).range([75, 0])
	return(
		<div style={entropyContainerSt}>
			{data.map(item =>
				<EntropyChart data={item} width={200} rotScale={rotScale} key={item.name} />)}
		</div>
	)
}

const EntropyChart = ({data, width, rotScale, displayPie}) =>{
    const contSt = {width:width, height:width, border:'solid', 
    	border:'yellow', backgroundColor:'yellow', margin:'1vw'};
	useEffect(() => {
        if(displayPie){
            renderPieChart(data, {width:width, containerClass:'chart-'+data.key});
        }else{
            renderEntropyNameChart(data, 
            	{width:width, containerClass:'chart-'+data.key, rotScale:rotScale});
        }
	}, [])
	return(
    	<div className={"chart-"+data.key} style={contSt}></div>
	);
}