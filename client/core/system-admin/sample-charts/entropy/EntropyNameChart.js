import * as d3 from 'd3'
import React, { useEffect }  from 'react';


export const renderEntropyNameChart = function(data, options){
    const opt = options ? options : {}

	//DATA HELPERS
    function dataHelper() {
    };

    //MAIN CHART FUNCTION

    //SET UP
    //create chart with settings
    //TODO - REFACTOR SO IM NOT SETTING DEFAULTS HERE AND IN THE CHART FUNC
    //WHILST STILL ENABLING ME TO CHAIN HERE
    const chart = entropyNameChart()
        .width(opt.width || 200)
        .height(opt.width || 200)
        .rotScale(opt.rotScale || d3.scaleLinear().domain([0, 1]).range([75, 0]));

    // Create the selection, bind the data and call the chart.
    const containerRef = 
        opt.containerId ? '#'+opt.containerId : 
        opt.containerClass ? '.'+opt.containerClass : '#chart'

   	d3.select(containerRef).selectAll('div.dataset-item')
        //if we have multiple datasets, we can pass them all in here
        //to get multiple versions of the same chart for different data
        //BUT WE WOULDNT HAVE THAT WHEN USING REACT, AS WE USE MAP INSTEAD TO RENDER
        //EACH ONE SEPARATELY
        .data([data])
        .enter()
        .append('div')
        .attr('class', 'dataset-item')
        .style('border', 'solid 1px blue')
        .style('width', '100%')
        //.style('margin', '4px')
        //style('padding', '4px')
        //.style('background-color', '#eeeeec')
        .call(chart);
	
};

/**
later - height of each team in simulation can be based on the
 total number of goals scored instead

 this chart can be used with players, where we look at any distribution - could be
 the ditribution of each players top twenty sprints in the match - in terms of 
 which 15 min interval they came in. or anything like that.
 **/
function entropyNameChart() {

    // Chart Variables.
    var width = 500,
        height = 400,
        margin = {top: 10, right: 10, bottom: 10, left: 10};

    // Default Accessors
    var rotScale = d3.scaleLinear().domain([0, 1]).range([75, 0])

    function chart(selection) {
        //selection.each only runs once as selection is only 1 array of dates
        selection.each(function(data) {
            const chartWidth = width - margin.left - margin.right
            const chartHeight = height - margin.top - margin.bottom

            var div = d3.select(this),
                svg = div.selectAll('svg').data([data]);
            // SVG Initialization.
            svg.enter().append('svg').call(chart.svgInit);
            //main chart g
            var g = div.selectAll('svg').select('g.chartG')
            //computations
            //for now, not needed but later we will calculate entropy

            //scales and axes
            //create a linear scale for rotating
            //note - for now, scale includes all entropies up to 1
            //later, we will truncate to just the range of values we have,
            //we only go to 75 so that all names are readable (ie none vertical)
            //render components
            g.append('g')
                .attr('class', 'nameG')
                .attr('transform', 'translate(' +chartWidth/2 +',' +chartHeight/2 +')')
 
            g.select('g.nameG')
                .append('text')
                    .attr('transform', 'rotate(' +rotScale(data.entropy) +')')
                    .style('text-anchor','middle')
                    .style('font-size', '20px')
                    .text(data.name)
                    //note - rotation angle from horizontal start must be entropy*90
                    //       using translate - rotate(angle)

            //enter, update, exit
            

        });
    }

    // Initialize the SVG Element
    chart.svgInit = function(svg) {
        // Set the SVG size
        svg
            .attr('width', width)
            .attr('height', height);

        // Create and translate the container group
        var g = svg.append('g')
            .attr('class', 'chartG')
            .attr('transform', 'translate(' + [margin.top, margin.left] + ')');

        // Add a background rectangle
        g.append('rect')
            .attr('width', width - margin.left - margin.right)
            .attr('height', height - margin.top - margin.bottom)
            .attr('fill', 'white');
    };

    // Api
    // Width
    chart.width = function(value) {
        if (!arguments.length) { return width; }
        width = value;
        return chart;
    };

    // Height
    chart.height = function(value) {
        if (!arguments.length) { return height; }
        height = value;
        return chart;
    };

    // Margin
    chart.margin = function(value) {
        if (!arguments.length) { return margin; }
        margin = value;
        return chart;
    };
    // scale
    chart.rotScale = function(value) {
        if (!arguments.length) { return rotScale; }
        rotScale = value;
        return chart;
    };
    return chart;
};

