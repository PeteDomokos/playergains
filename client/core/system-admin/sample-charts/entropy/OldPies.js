import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'
import D3Helpers from '../D3Helpers'
import ChartComponentRenderer from '../ChartComponentRenderer'
import DisplayItemHelpers from '../DisplayItemHelpers'
import StatHelpers from '../StatHelpers'

/*

*/
/**
* 
*
**/
const Pies = {
	render(svgSelector, Lengths, data, titleLines, actorNames, statNames, secKey){
		let { wSF, hSF, width, height, marginTop, chartHeight, marginBottom, marginLeft, chartWidth, marginRight} = Lengths
		//dimensions
		//chartWidth = (chartWidth -(20*actorNames.length))/actorNames.length
		let pieDiameter
		let gapBetweenCharts
		//change the margins - more chart space than normal, and even more if 3 pies
		marginTop = height*0.35
		marginBottom = height*0.15
		chartHeight = height*0.5
		if(actorNames.length == 3){
			marginLeft = width*0.1
			chartWidth = width*0.8
			marginRight = width*0.1
		}else{
			marginLeft = width*0.15
			chartWidth = width*0.8
			marginRight = width*0.15
		}
		//pie sizing
		let shiftRightRequired = 0
		switch(actorNames.length){
			case 3:{
				console.log("3 actors......")
				pieDiameter = Math.min(chartWidth*0.3, chartHeight)
				gapBetweenCharts = chartWidth*0.05
				break
			}
			default:{ //2
				pieDiameter = chartWidth*0.45
				if(chartHeight < pieDiameter){
					let diffEachPie = pieDiameter -chartHeight
					let totalDiff = diffEachPie*actorNames.length
					//set the value that pies need to be shifted right to keep them centred
					shiftRightRequired = totalDiff*0.5
					//reduce the diameter
					pieDiameter = chartHeight
				}
				gapBetweenCharts = chartWidth*0.1
				break
			}
		}
		let pieRadius = pieDiameter*0.5

		/*
		use seckey to get datakeys
		let dataKeys = 
		d3.select("#controls").selectAll("button.keys").
		   data(dataKeys).enter().
		   append("button").
		   on("click", buttonClick).
		   html(d => d)

		function buttonClick(dataKey){
			//must update the data then call update for each pieChart
			let pieChart = d3.pie()
			pieChart.value(d => d.buttonKeys.find(buttonKey => buttonKey.key == dataKey).value)
			update(pieChart, dataArr)
		}
		*/
		//colours
		let priKeys = statNames  //temp - will change param to priKeys so its general
		let fillScale = d3.scaleOrdinal().
			domain(priKeys).
			range(["red", "green", "blue", "orange", "pink","brown"])
		//legend(colour-based) - use old Lengths values for best placement in top left //{...updatedLengths, marginTop:Lengths.marginTop}
		let updatedLengths = {wSF:wSF, hSF:hSF, marginTop:marginTop, chartHeight:chartHeight, marginBottom:marginBottom, marginLeft:marginLeft, chartWidth:chartWidth, marginRight:marginRight}
		ChartComponentRenderer.legend('pies', svgSelector, fillScale, statNames.length, Lengths, "topLeft")

		//shift title
		//todo - impl this by shifting it rather than replacing it, or change the title func in the helper object!!!!!
		//this is the best way to save myself deleting stuff, just make the renderer funcs more flexble and complex
		d3.select(svgSelector).selectAll("text.svg-title").remove()
		let fakeLargerWidth = width*1.2
		ChartComponentRenderer.title(titleLines, svgSelector, {...Lengths, width:fakeLargerWidth})
		/*
		add sentence about 'none' - could add to controls space if no buttons
		d3.select(svgSelector).append("text").
			attr("transform", 'translate(' +(marginLeft*0.8) +', ' +(chartHeight+marginTop+marginBottom-30) +')').
			style("font-size", 14).
			text("*none* means: ball lost due to poor control")
		d3.select(svgSelector).append("text").
			attr("transform", 'translate(' +(marginLeft*0.8) +', ' +(chartHeight+marginTop+marginBottom-15) +')').
			style("font-size", 14).
			text("or being tackled before attempting any action.")
		*/
		//arc function
		let newArc = d3.arc()
		newArc.innerRadius(0).outerRadius(pieRadius)

		let nestedData = d3.nest().
							key(d => d.player).
							sortKeys((x,y) => actorNames.indexOf(x) - actorNames.indexOf(y)).
							key(d => d.typ).
							entries(data)
		//set default pieChart values
		nestedData.forEach((actorData,i) => {
			let pieChart = d3.pie()
			pieChart.value(d => d.values.length).
					 sort(null)

			//check and sort data
			let nests = actorData.values  //each value is {key:statName/priKey, values:Array[Action]}
			let sortedNests = priKeys.map(priKey => {
				let nest = nests.find(nest => nest.key == priKey)
				if(nest)
					return nest
				else
					return {key:priKey, values:[]}
			})
			let sortedActorData = {key:actorData.key, values:sortedNests}

			//call update
			update(pieChart, sortedActorData, i)
		})
		
		//iterate through data and check each nest exists  - add an empty one if not, and order them
		//warning -shit as it needs access to statNames!
		let myComparator = (x,y) =>{
			return priKeys.indexOf(y) -  priKeys.indexOf(x)
		}
		function update(pieChart, data, i){
			//configure the dataArr with pie layout
			let pieData = pieChart(data.values)
			//todo - deal with case of no actions for one of the keys in nesting, and then make sure I order the keys
			//todo - add line to eliminate ordering of the slices (see d3 book on pie charts)

			d3.select(svgSelector).append("g").
				attr("class", "pieG"+i).
				attr("transform", "translate(" +(marginLeft +shiftRightRequired +pieRadius +i*(pieDiameter+gapBetweenCharts)) +"," +(marginTop+pieRadius) +")").
				selectAll("path").
				data(pieData).enter().
				append("path").
				attr("d", newArc).//newArc takes each d
				style("fill", d => fillScale(d.data.key)).
				style("stroke", "black").
				style("stroke-width", 2)

			d3.select(svgSelector).select("g.pieG"+i).
				append("text").
				attr("x", 0).
				attr("y", pieRadius*1.3).
				style("text-anchor", "middle").
				style("font-size", DisplayItemHelpers.fontSize("subLabel", wSF, hSF, 7)). //note use 7 so font same for all
				text(StatHelpers.uNameToPlayerFirstName(data.key))
		}
	}
}
export default Pies