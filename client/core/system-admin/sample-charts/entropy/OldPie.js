import * as d3 from 'd3'
import D3Helpers from '../D3Helpers'
import ChartComponentRenderer from '../ChartComponentRenderer'
import DisplayItemHelpers from '../DisplayItemHelpers'
import StatHelpers from '../StatHelpers'
/*

*/
/**
* This impl integrates the keys with the values into a new dataArr
*
**/
const Pie = {
	render(svgSelector, Lengths, dataArr){
		//dimensions
		let  { wSF, hSF, marginTop, chartHeight, marginBottom, marginLeft, chartWidth, marginRight } = Lengths
		//todo - change so it receives all pllayers data so it can enact the buttons using buttonKeys
		//use first d to get button subkeys
		let dataKeys = dataArr[0].buttonKeys.map(buttonKey => buttonKey.key)
		//create buttons for changing the data channel
		//ChartComponentRenderer.buttons
		d3.select("#controls").selectAll("button.keys").
		   data(dataKeys).enter().
		   append("button").
		   on("click", buttonClick).
		   html(d => d)

		//create func that receives the 'dataKey' ie the datapoint of button clicked from the array the was passed into buttons
		function buttonClick(dataKey){
			//update the pieChart if required
			let pieChart = d3.pie()
			pieChart.value(d => d.buttonKeys.find(buttonKey => buttonKey.key == dataKey).value)
			//update dataArr if required

			//call update func with updated pieChart func and dataArr
			update(pieChart, dataArr)
		}

		//colours
		let priKeys = dataArr.map(d => d.priKey)
		let fillScale = d3.scaleOrdinal().
			domain(priKeys).
			range(["red", "green", "blue", "orange", "pink","brown"])

		//legend(colour-based)
		ChartComponentRenderer.legend(svgSelector, fillScale, Lengths, "topLeft")

		//arc function
		let newArc = d3.arc()
		newArc.innerRadius(0).outerRadius(50)

		//set default pieChart values
		let initPieChart = d3.pie()
		initPieChart.value(d => d.buttonKeys[0].value)
		update(initPieChart, dataArr)

		function update(pieChart, dataArr){
		//configure the dataArr with pie layout
			let pieArray = pieChart(dataArr)

			d3.select(svgSelector).append("g").
				attr("transform", "translate(200,100)").
				selectAll("path").
				data(pieArray).enter().
				append("path").
				attr("d", newArc).//newArc takes each d
				style("fill", d => fillScale(d.data.priKey)).
				style("stroke", "black").
				style("stroke-width", 2)
		}
	}
}
export default Pie