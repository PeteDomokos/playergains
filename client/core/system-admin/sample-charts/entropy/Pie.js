import * as d3 from 'd3'
import React, { useEffect }  from 'react';

export const renderPieChart = function(data, options){
    const opt = options ? options : {}
	//DATA HELPERS
    function dataHelper() {
    };

    const chart = entropyNameChart()
        .width(opt.width || 200)
        .height(opt.width || 200)

    const containerRef = 
        opt.containerId ? '#'+opt.containerId : 
        opt.containerClass ? '.'+opt.containerClass : '#chart'

    d3.select(containerRef).selectAll('div.dataset-item')
        .data([data])
        .enter()
        .append('div')
        .attr('class', 'dataset-item')
        .style('border', 'solid 1px blue')
        .style('width', '100%')
        .call(chart);
}

//MAIN CHART FUNCTION
function pieChart(){

    // Chart Variables.
    var width = 600,
        height = 400,
        margin = {top: 10, right: 10, bottom: 10, left: 10};

    // Default Accessors
    //eg var value = function(d) { return d.date; };

    function chart(selection) {
    	//selection.each only runs once as selection is only 1 array of dates
        selection.each(function(data) {
            var div = d3.select(this),
                svg = div.selectAll('svg').data([data]);

            // SVG Initialization.
            svg.enter().append('svg').call(chart.svgInit);
            //main chart g
            var chartG = div.selectAll('svg').select('g.chartG')

            //computations

            //scales and axes

            //render components
            //enter, update, exit 
             g.append('g')
                .attr('class', 'nameG')
                .attr('transform', 'translate(' +chartWidth/2 +',' +chartHeight/2 +')')
 
            g.select('g.nameG')
                .append('text')
                    .style('text-anchor','middle')
                    .style('font-size', '20px')
                    .text('pie')
            

        });
    }

    // Initialize the SVG Element
    chart.svgInit = function(svg) {
        // Set the SVG size
        svg
            .attr('width', width)
            .attr('height', height);

        // Create and translate the container group
        var g = svg.append('g')
            .attr('class', 'chartG')
            .attr('transform', 'translate(' + [margin.top, margin.left] + ')');

        // Add a background rectangle
        g.append('rect')
            .attr('width', width - margin.left - margin.right)
            .attr('height', height - margin.top - margin.bottom)
            .attr('fill', 'white');
    };

    // Api
    // Width
    chart.width = function(value) {
        if (!arguments.length) { return width; }
        width = value;
        return chart;
    };

    // Height
    chart.height = function(value) {
        if (!arguments.length) { return height; }
        height = value;
        return chart;
    };

    // Margin
    chart.margin = function(value) {
        if (!arguments.length) { return margin; }
        margin = value;
        return chart;
    };
    return chart;
};


