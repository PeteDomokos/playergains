import * as d3 from 'd3'
import React, { useEffect }  from 'react';

const exampleStyle = {
	margin:20, padding:10, border: 'solid 1px yellow'
}

export const Barcode = () =>{
	useEffect(() => {
		console.log('rendering barcode chart...');
		renderChart()
	}, [])
	return(
	<div className='barcode' style={{border:'solid', borderColor:'red'}}>
		<button id="btn-update-09">Add data</button>
		<button id="btn-remove-09">Remove data</button>
		<div className="chart-example" style={exampleStyle} id="chart-09"></div>
	</div>
	);
}


function renderChart(){
	//HELPERS
	// Compute a random interval using an Exponential Distribution of
    // parameter lambda = (1 / avgSeconds).
    function randomInterval(avgSeconds) {
        return Math.floor(-Math.log(Math.random()) * 1000 * avgSeconds);
    };

    // Create or extend an array of increasing dates by adding a random
    // time interval using an exponential distribution.
    function addData(data, numItems, avgSeconds) {
        // Compute the most recent time in the data array. If the array is
        // empty, uses the current time.
        var n = data.length,
            t = (n > 0) ? data[n - 1].date : new Date();

        // Append items with increasing times in the data array.
        for (var k = 0; k < numItems - 1; k += 1) {
            t = new Date(t.getTime() + randomInterval(avgSeconds));
            data.push({date: t});
        }

        return data;
    }

    //MAIN CHART FUNCTION
	var barcodeChart9 = function() {

        // Chart Variables.
        var width = 570,
            height = 30,
            margin = {top: 5, right: 5, bottom: 5, left: 5};

        // Default Date Accessor.
        var value = function(d) { return d.date; };

        // Default time interval.
        var timeInterval = d3.timeDay;
        //this function is only called by the user after they set the settings
        function chart(selection) {
        	console.log('chart called...sel', selection)
        	//selection.each only runs once as selection is only 1 array of dates
            selection.each(function(data) {
            	/*
            	//BASIC EG
            	// Bind the dataset to the svg selection.
                var div = d3.select(this),
                    svg = div.selectAll('svg').data([data]);

                // SVG Initialization.
                svg.enter().append('svg').call(chart.svgInit);

                // Compute the horizontal scale.
                var xScale = d3.scaleTime()
                    .domain(d3.extent(data, function(d) { return d.date; }))
                    .range([0, width - margin.left - margin.right]);

                // Select the chart group
                var g = div.selectAll('svg').select('g.chart-content');

                // Bind the data to the bars selection.
                var bars = g.selectAll('line')
                    .data(data, function(d) { return d.date; });

                // Append the bars on enter and set its attributes.
                bars.enter().append('line')
                    .attr('x1', function(d) { return xScale(d.date); })
                    .attr('x2', function(d) { return xScale(d.date); })
                    .attr('y1', 0)
                    .attr('y2', height - margin.top - margin.bottom)
                    .attr('stroke', '#000')
                    .attr('stroke-opacity', 0.5);

				*/
				//COMPLEX EG
				//data is an array of dates
            	console.log('sel.each called data...', data)

                // Bind the dataset to the svg selection.
                //i think we need to use data not datum as we need the join
                //again we are only making 1 svg for the whole array of dates here
                //so we wrap data in an array.
                var div = d3.select(this),
                    svg = div.selectAll('svg').data([data]);

                // SVG Initialization.
                //note - width and height have already been passed in before 
                //chart function is called, unless user wants to use defaults
                svg.enter().append('svg').call(chart.svgInit);

                // Select the chart group and the lines in that group
                //WARNING - BOOK DOESNT WORK - NEED TO RESELECT SVG HERE
                var g = div.selectAll('svg').select('g.chart-content'),
                    lines = g.selectAll('line');

                // Compute the most recent date from the dataset
                var lastDate = d3.max(data, value);

                // Replace the lastDate with the most recent date of the
                // dataset before the update, if the selection is not empty.
                //ie if there has been previous data added
                //This means previously entered dates will be kept in their old positions,
                //and newly added ones will be added off the chart (to the right)
                //and we use opacity to hide them
                //and then we immediately transition to the new positions
                //It doesnt happen if no previous data, ofcourse
                lastDate = lines.empty() ? lastDate : d3.max(lines.data(), value);

                // Compute the date of the lastDate minus the time interval.
                //here, it is 1 unit (eg day or hour or week) before the last date
                var firstDate = timeInterval.offset(lastDate, -1);
                // Compute the horizontal scale with the previous dataset values.
                var xScale = d3.scaleTime()
               		//.domain(d3.extent(data, function(d) { return d.date; }))
                    .domain([firstDate, lastDate])
                    .range([0, width - margin.left - margin.right]);

                // Bind the updated data to the bars.
                var bars = g.selectAll('line').data(data, value);
                // Create the bars on enter and set their position.
                bars.enter().append('line')
                    .attr('x1', function(d) { 
                    	console.log('entering line')
                    	return xScale(value(d)); })
                    .attr('x2', function(d) { return xScale(value(d)); })
                    .attr('y1', 0)
                    .attr('y2', height - margin.top - margin.bottom)
                    .attr('stroke', '#000')
                    .attr('stroke-opacity', 0.5);
                console.log('entered bars', bars)

                // Update the scale with the new data
                lastDate = d3.max(data, value);
                firstDate = timeInterval.offset(lastDate, -1);
                xScale.domain([firstDate, lastDate]);

                // Update the position of the bars, with the updated scale.
                bars.transition()
                    .duration(300)
                    .attr('x1', function(d) { return xScale(value(d)); })
                    .attr('x2', function(d) { return xScale(value(d)); });

                // Remove the bars that don't have corresponding data items.
                bars.exit().transition()
                    .duration(300)
                    .attr('stroke-opacity', 0)
                    .remove();
            });
        }

        // Initialize the SVG Element
        chart.svgInit = function(svg) {
            // Set the SVG size
            svg
                .attr('width', width)
                .attr('height', height);

            // Create and translate the container group
            var g = svg.append('g')
                .attr('class', 'chart-content')
                .attr('transform', 'translate(' + [margin.top, margin.left] + ')');

            // Add a background rectangle
            g.append('rect')
                .attr('width', width - margin.left - margin.right)
                .attr('height', height - margin.top - margin.bottom)
                .attr('fill', 'pink');
        };

        // Accessor Methods

        // Width
        chart.width = function(value) {
            if (!arguments.length) { return width; }
            width = value;
            return chart;
        };

        // Height
        chart.height = function(value) {
            if (!arguments.length) { return height; }
            height = value;
            return chart;
        };

        // Margin
        chart.margin = function(value) {
            if (!arguments.length) { return margin; }
            margin = value;
            return chart;
        };

        // Date Accessor
        chart.value = function(accessorFunction) {
            if (!arguments.length) { return value; }
            value = accessorFunction;
            return chart;
        };

        // Time Interval
        chart.timeInterval = function(value) {
            if (!arguments.length) { return timeInterval; }
            timeInterval = value;
            return chart;
        };

        return chart;
    };


    //SET UP
    var data09 = addData([], 150, 3 * 60);
    console.log('data', data09)
    const barcode09 = barcodeChart9();
    // Create the selection, bind the data and call the chart.
   	d3.select('#chart-09').selectAll('div.data-item')
   	//we wrap the array in another array, so we only have one div for the array
   	//but if we had two arrays of data then we would haev two separate 
   	//barcode charts in two divs
        .data([data09])
        .enter()
        .append('div')
        .attr('class', 'data-item')
        .style('border', 'solid 1px blue')
        .style('margin', '4px')
        .style('padding', '4px')
        .style('background-color', '#eeeeec')
        .call(barcode09);

    // Create the callback for the add data button.
    d3.select('#btn-update-09')
        .on('click', function() {
            // Adds 30 data items to the array.
            data09 = addData(data09, 30, 3 * 60);

            // Rebinds the updated dataset.
            d3.select('#chart-09').selectAll('div.data-item')
                .data([data09])
                .call(barcode09);
        });

    // Binds a callback to the remove button
    d3.select('#btn-remove-09')
        .on('click', function() {
            // Remove 10 items, if there are more than ten in the array.
            if (data09.length > 10) {
                data09 = data09.slice(10, data09.length);
            }

            // Rebinds the updated dataset.
            d3.select('#chart-09').selectAll('div.data-item')
                .data([data09])
                .call(barcode09);
        });
	
};

