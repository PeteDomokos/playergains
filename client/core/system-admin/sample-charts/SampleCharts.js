import React, { useState }  from 'react'
import { Link, Redirect } from 'react-router-dom'
//material-ui
import Button from '@material-ui/core/Button'
//children
import { Entropy } from './entropy/Entropy'
import { Barcode } from './Barcode'
import { Slider } from './Slider'

const listStyle = {margin:20, display:'flex'}
const listItemStyle = {margin:10}
export const SampleCharts = () =>{
  	const [selection, setSelection] = useState('entropy')//('')
	return(
		<div style={{backgroundColor:'white'}}>
			{/**<div style={listStyle}>
				<Button  variant="contained" style={listItemStyle} onClick={() => setSelection('entropy')}>
					Entropy
				</Button>
				<Button  variant="contained" style={listItemStyle} onClick={() => setSelection('barcode')}>
					Barcode
				</Button>
				<Button  variant="contained" style={listItemStyle} onClick={() => setSelection('slider')}>
					Slider
				</Button>
			</div>**/}
			{(selection === 'entropy') &&<Entropy/>}
			{(selection === 'barcode') &&<Barcode/>}
			{(selection === 'slider') &&<Slider/>}
		</div>
		)
}