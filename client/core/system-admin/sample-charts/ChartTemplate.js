import * as d3 from 'd3'
import React, { useEffect }  from 'react';

export const ChartComponent = () =>{
	useEffect(() => {
		console.log('rendering chart...');
		renderChart()
	}, [])
	return(
    	<div className="chart" id="chart"></div>
	);
}


function renderChart(){
	//DATA HELPERS
    function dataHelper() {
    };

    //MAIN CHART FUNCTION
	var chartTypeChart = function() {

        // Chart Variables.
        var width = 600,
            height = 400,
            margin = {top: 10, right: 10, bottom: 10, left: 10};

        // Default Accessors
        //eg var value = function(d) { return d.date; };

        function chart(selection) {
        	//selection.each only runs once as selection is only 1 array of dates
            selection.each(function(data) {
                var div = d3.select(this),
                    svg = div.selectAll('svg').data([data]);

                // SVG Initialization.
                svg.enter().append('svg').call(chart.svgInit);
                //main chart g
                var chartG = div.selectAll('svg').select('g.chartG')

                //computations

                //scales and axes

                //render components
                //enter, update, exit 
                

            });
        }

        // Initialize the SVG Element
        chart.svgInit = function(svg) {
            // Set the SVG size
            svg
                .attr('width', width)
                .attr('height', height);

            // Create and translate the container group
            var g = svg.append('g')
                .attr('class', 'chartG')
                .attr('transform', 'translate(' + [margin.top, margin.left] + ')');

            // Add a background rectangle
            g.append('rect')
                .attr('width', width - margin.left - margin.right)
                .attr('height', height - margin.top - margin.bottom)
                .attr('fill', 'pink');
        };

        // Api
        // Width
        chart.width = function(value) {
            if (!arguments.length) { return width; }
            width = value;
            return chart;
        };

        // Height
        chart.height = function(value) {
            if (!arguments.length) { return height; }
            height = value;
            return chart;
        };

        // Margin
        chart.margin = function(value) {
            if (!arguments.length) { return margin; }
            margin = value;
            return chart;
        };
        return chart;
    };


    //SET UP
    //pull data
    var data = [a,b,c]
    //create chart
    const chart = chartTypeChart();
    // Create the selection, bind the data and call the chart.
   	d3.select('#chart').selectAll('div.dataset-item')
        //if we have multiple datasets, we can pass them all in here
        //to get multiple versions of the same chart for different data
        .data([data])
        .enter()
        .append('div')
        .attr('class', 'dataset-item')
        .style('border', 'solid 1px yellow')
        //.style('margin', '4px')
        //.style('padding', '4px')
        //.style('background-color', '#eeeeec')
        .call(chart);
	
};

