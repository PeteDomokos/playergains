import * as d3 from 'd3'
import React, { useEffect }  from 'react';

export const Slider = () =>{
	useEffect(() => {
		console.log('rendering chart...');
		renderSliderChart()
	}, [])
	return(
	<div className='slider'></div>
	);
}

//basic circle in a div
function renderCircleChart(){
	const width = 600;
	const height = 150;
	const svg = d3.select('div.slider').append('svg')
		.attr('width', width)
		.attr('height', height);

	const circle = svg.append('circle')
		.attr('cx', width/2)
		.attr('cy', height/2)
		.attr('r', 30)
		.attr('fill', '#555')

	const drag = d3.drag().on('drag', dragListener);

	circle.call(drag);

	function dragListener(d){
		const newCx = +d3.select(this).attr('cx') +d3.event.dx;
		const newCy = +d3.select(this).attr('cy') +d3.event.dy;
		d3.select(this)
			.attr('cx', newCx)
			.attr('cy', newCy);
	}
};

function renderSliderChart(){
	// Figure properties.
    var width = 600,
        height = 40,
        margin = 10;

    // Valid range and initial value.
    var initValue = 35,
        domain = [0, 100];

    // Create the svg element and set its dimensions.
    var svg = d3.select('.slider').append('svg')
        .attr('width', width + 2 * margin)
        .attr('height', height + 3 * margin);

     // Create a color scale with the same range that the slider
    var cScale = d3.scaleLinear()
        .domain(domain)
        .range(['#edd400', '#a40000']);

    // Add a background to the svg element.
    var rectangle = svg.append('rect')
        .attr('x', margin)
        .attr('y', 2 * margin)
        .attr('width', width)
        .attr('height', height)
        .attr('fill', cScale(initValue));


    // Create and configure the slider control.
    var slider = sliderControl()
        .width(width)
        .domain(domain)
        .onSlide(function(selection) {
	        selection.each(function(d) {
	            rectangle.attr('fill', cScale(d));
        	});
   		});

    // Create a container for the slider and translate it
    // to align it vertically.
    var gSlider = svg.selectAll('g')
        .data([initValue])
        .enter()
        .append('g')
        .attr('transform', 'translate(' + [margin, margin] + ')')
        .call(slider);

}

//slider
function sliderControl(){
	//slider attributes
	var width = 600;
	//Default domain.
    var domain = [0, 100];
    //Default slider callback.
    var onSlide = function(selection) {};

	//charting function
	function chart(selection){
		selection.each(function(data){
			//container group
			const group = d3.select(this)
			//slider contents
			//lines
			group.selectAll('line')
				.data([data])
				.enter()
					.append('line')
						.call(chart.initLine);

			//handler
			const handle = group.selectAll('circle')
				.data([data])
				.enter()
					.append('circle')
						.call(chart.initHandle);
			//scale
			const posScale = d3.scaleLinear()
				.domain(domain)
				.range([0, width]);

			//init position the handler
			handle.attr('cx', d => posScale(d));

			//drag
			const drag = d3.drag().on('drag', moveHandle);
			handle.call(drag);

			function moveHandle(d){
				const newCx = +d3.select(this).attr('cx') +d3.event.dx;
				//update pos and rebind data if its within range
				if(0 < newCx && newCx < width){
					d3.select(this)
						.data([posScale.invert(newCx)])
						.attr('cx', newCx)
						.call(onSlide);
				}
			}
	
		});
	}
	//Set the initial attributes of the line.
	chart.initLine = function(selection){
		selection
			.attr('x1', 2)
			.attr('x2', width - 4)
			.attr('stroke', '#777')
			.attr('stroke-width', 4)
			.attr('stroke-linecap', 'round');
	}
	//Set the initial attributes of the handle.
    chart.initHandle = function(selection) {
        selection
            .attr('cx', function(d) { return width / 2; })
            .attr('r', 6)
            .attr('fill', '#aaa')
            .attr('stroke', '#222')
            .attr('stroke-width', 2);
    };

	//accessor methods
	//Width
    chart.width = function(value) {
        if (!arguments.length) { return width; }
        width = value;
        return chart;
    };

    //Domain
    chart.domain = function(value) {
        if (!arguments.length) { return domain; }
        domain = value;
        return chart;
    };

    //Slide callback function
    chart.onSlide = function(onSlideFunction) {
        if (!arguments.length) { return onSlide; }
        onSlide = onSlideFunction;
        return chart;
    };

    return chart;
}