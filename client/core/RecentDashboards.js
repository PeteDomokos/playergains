import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link }from 'react-router-dom'
//children
import Paper from '@material-ui/core/Paper'
import { List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
//icons
import ArrowForward from '@material-ui/icons/ArrowForward'
//styles
import {withStyles} from '@material-ui/core/styles'


const styles = theme => ({
  root: theme.mixins.gutters({
    padding: theme.spacing(1),
    margin: theme.spacing(2),
    marginTop: 0,
    marginBottom: theme.spacing(6),
    minWidth:270,
    height:130
  }),
  header:{
    height:60,
    display:'flex',
    justifyContent:'space-between'
  },
  title: {
    margin: `${theme.spacing(0)}px 0 ${theme.spacing(0)}px`,
    color: theme.palette.openTitle,
    display:'flex',
    alignItems:'center'
  },
  actions:{
    margin: `${theme.spacing(0)}px 0 ${theme.spacing(0)}px`
  },
  item:{
    height:55
  }
})

//todo - pass in Dashboard objects instead (ie same as store.dashboard)

const RecentDashboards = ({groups, classes}) =>{
		const recentDashboards = groups && groups[0] ? [{
			group:groups[0],
			datasets:'All'
		}] : []

	return (
      <Paper className={classes.root} elevation={4}>
        <div className={classes.header} >
          <Typography type="title" className={classes.title}>
            Recent Dashboards
          </Typography>
          <div className={classes.actions}>
            {/**put link to all data renderActionButton(addGroupAction)**/}</div>
        </div>
        <List dense>
         {
            recentDashboards.map((dashboard,i) => {
              return( 
                <Link to={"/dashboard/group/" + dashboard.group._id} key={'dashboards'+i}>
                  <ListItem button className={classes.item}>
                    <ListItemText primary={dashboard.group.name} style={{marginRight:10}}/>
                    {/**<ListItemText primary={'Nr Datasets:' +dashboard.datasets} style={{marginRight:10}}/>**/}
                    <ListItemSecondaryAction>
                    <IconButton>
                        <ArrowForward/>
                    </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
               </Link>
              )
            })
          }
        </List>
      </Paper>
    )
}

export default withStyles(styles)(RecentDashboards)