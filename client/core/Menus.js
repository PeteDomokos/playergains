import React from 'react'
import {NavLink, Link, withRouter} from 'react-router-dom'
import { slide as ElasticMenu } from 'react-burger-menu'
import logo from '../assets/images/logo.png'
import { DashboardMenu } from './dashboard/DashboardMenu'
import SignoutContainer from '../auth/containers/SignoutContainer'
import auth from '../auth/auth-helper'

//inline styling for Navlink components
let activeSt = {color: '#ed4401', textDecoration:'none'}
let linkSt = {color: 'white', textDecoration:'none'}
/**
* Determines whether or not path is part of current path, and returns appropriate styling object
* @param {Object} history - this router's history object
* @param {Object} path - the path to be tested against the current pathname in history
* @returns {Object} - a styling object
**/
const isActive = (history, path) => {
  if (history.location.pathname.startsWith(path))
    return {color: '#ed4401'}
  return {color: 'white'}
}
/**
* Determines whether or not the home logo is to be displayed
* This depends on the path
* TODO - further implement different page scenarios 
* @param {Object} history - this router's history object
* @returns {boolean} - whether or not to dispay the home logo
**/
const showHomeLink = (history) =>{
  if(history.location.pathname.startsWith('/dashboard'))
    return true
  return false
}


/**
* Desc ...
* @param {boolean} elastic - flag to show whether the menu must be elastic or not
* @param {Object} history - this router's history object
* @returns {Object} The MainMenu component, whish is a list of links
**/
export const MainMenu = withRouter(({elastic, history}) =>{
  return(
  <ul className={'main-menu '+(elastic ? 'elastic':'non-elastic')}>
      {showHomeLink(history) && <li className='not-ls main-menu-item logo-cont'>
        <NavLink to="/">
            <img src={logo} />
        </NavLink>
      </li>}
      <li className='main-menu-item other'>
        <NavLink to="/about" activeStyle={activeSt} style={linkSt}>
           About
        </NavLink>
      </li>
      {/**<li className={'main-menu-item ' 
        +(history.location.pathname.startsWith('/dashboard') ? 'large':'other')}>
         <NavLink to="/dashboard"  activeStyle={activeSt} style={linkSt}>
           Dashboard
        </NavLink>
      </li>**/}
      <li className='main-menu-item other'>
        <NavLink to="/contact"  activeStyle={activeSt} style={linkSt}>
           Contact
        </NavLink>
      </li>
      {!auth.isAuthenticated()  && 
        <li className='main-menu-item other'>
          <NavLink to="/signup" activeStyle={activeSt} style={linkSt}>
             Sign up
          </NavLink>
        </li>
      }
        {auth.isAuthenticated() ?
          <SignoutContainer/>
          :
          <li className='main-menu-item other'>
            <NavLink to="/signin"  activeStyle={activeSt} style={linkSt}>
               Sign in
            </NavLink>
          </li>
        }
  </ul>
  )}
)
MainMenu.defaultProps = {
}
/**
* Desc ...
* @param {Object} history - this router's history object 
**/
export const AboutMenu = withRouter(({history}) =>(
  <nav className='about-menu'>
    <div className='left-items'>
      <NavLink to="/about">
        <button className='about-menu-item btn' style={isActive(history, "/about")}>
        Welcome</button>
      </NavLink>
      <NavLink to="/about/how-it-works">
        <button className='about-menu-item btn' style={isActive(history, "/about/how-it-works")}>
        How It Works</button>
      </NavLink>
    </div>
    <div className='right-items'>
      <NavLink to="/about/build-info">
        <button className='about-menu-item btn' style={isActive(history, "/about/build-info")}>
        How It Was Built</button>
      </NavLink>
      <NavLink to="/about/what-people-say">
        <button className='about-menu-item btn' style={isActive(history, "/about/what-people-say")}>
        What People Say
        </button>
      </NavLink>
    </div>
  </nav>

  ))
AboutMenu.defaultProps = {
}


/**
* Desc ...
* @param {string} menuName - 
* @param {Object} history - this router's history object
* @param {Object[]}  
* @param {number} 
**/
//for now, only submenu is for targets
export const SubMenu = withRouter(({menuName, history}) =>
  <DashboardMenu/>
)


