import React from 'react'
import { Link } from 'react-router-dom'

/**
* Simple component to render contact details
**/
//TODO - make this a higher-order component and pass content in from file
const Contact = () => 
	<div className='contact'>
		<div className='contact-content'>
			<h3 className='heading'>Contact Details</h3>
			<h4 className='subheading'>Telephone</h4>
			<p>07930 507 468 / 0207 5611 685</p>
			<h4 className='subheading'>Email</h4>
			<p>peterdomokos@switchplay.co.uk</p>
			<h4 className='subheading'>Office</h4>
			<p>Switchplay Learning Technologies Ltd</p>
			<p>64 Fonthill Road</p>
			<p>London</p>
			<p>N4 3HT</p>
		</div>
	</div>

export default Contact