import React from 'react'
import { AboutMenu } from './Menus'
import { Route }from 'react-router-dom'
import welcomeImage from '../assets/images/welcome-image.jpg'
/**
* Simple component with a submenu and subcomponents of content about the app
**/
const About =({ match }) =>
	<section className="about main-page">
		<AboutMenu/>
		<Route exact path="/about" component={Welcome} />
		<Route path="/about/how-it-works" component={HowItWorks} />
		<Route path="/about/build-info" component={BuildInfo} />
		<Route path="/about/what-people-say" component={WhatPeopleSay} />
	</section>

//TODO - make this a higher-order component and pass content in from file
const Welcome = () =>
	<section className='welcome'>
		<div className='welcome-img'>
			<img src={welcomeImage} />
		</div>
		<div className='text'>
			<p>
				Hello and welcome to Player Gains, brought to you by Switchplay.
			</p>
			<p>
				We develop web and mobile apps to aid the sports development and education of young sportsmen and women.
			</p>
			<p>
				Player Gains allows sports coaches and players to enter and track performance, fitness and skills data.
			</p>
			<p>
				Users can view their data from a variety of angles, such as monitoring progress over time, comparing data between players, and seeing how players compare to the expected standards for their age and level.
			</p>
			<div className='signature-area'>
				<h3 className='name'>Peter Domokos</h3>
				<h3>Director</h3>
				<h3>Switchplay</h3>
			</div>
		</div>
	</section>

//TODO - make this a higher-order component and pass content in from file
const HowItWorks = () =>
	<section className='how-it-works'>
		<h2 className='heading'>About The App</h2>
		<p>
			Users sign up (either as coaches or as players) and then create groups (eg teams or small groups of players within a team). They can also create subgroups from an existing group - for example, all the defenders in a team. 
		</p>
		
		<p>
			Users assign the relevant performance, skills and fitness tests to each group, depending on what criteria they want to measure. They can either select these tests from a range of pre-existing standardised tests in the system, or they can create their own custom tests.
		</p>
		<p>
			Users then start to enter data whenever a test is taken, or whenever data for a test is gathered from a match or a training session.
		</p>
		<p>
			Users go to the dashboard to view their data in different ways.
		</p>
		

		<h2 className='heading'>Standardised Tests</h2>
		<p>
			The standardised tests come with the additional benefit that players can see how they compare to the expected standards for their age and level, or how they compare to other players from outside of their group.
		</p>
	</section>

//TODO - make this a higher-order component and pass content in from file
const BuildInfo = () =>
	<section className='build-info'>
		<h2 className='heading'>Technologies</h2>
		<p>
			 
		</p>
		<h2 className='heading'>Design Methodology</h2>
		<p>
			 
		</p>
		<h2 className='heading'>Trials</h2>
		<p>
			
		</p>
		<h2 className='heading'>The Future</h2>
		<p>
		
		</p>
	</section>

//TODO - make this a higher-order component and pass content in from file
const WhatPeopleSay = () =>
	<section className='what-people-say'>
		<h2 className='heading'>Testimonials</h2>
	</section>

export default About