import React, {Component} from 'react'
import { Route }from 'react-router-dom'
import PropTypes from 'prop-types'
//linked and child components
import SimpleTable from '../../util/components/SimpleTable'
import SortingTable from '../../util/components/SortingTable'
//helpers
import { addWeeks } from '../../core/dashboard/charts/TimeHelpers'
import auth from '../../auth/auth-helper'

class Dataset extends Component{
	render() {
		const { groupId, groupAdmin, dataset, loadingDatapoints, players, update } = this.props
		//aut to update
		const jwt = auth.isAuthenticated()
	    //note - admin is an array of user objects with id, firstName and surname
	    const authToUpdate = jwt && groupAdmin && 
	      (groupAdmin.map(user => user._id).includes(jwt.user._id) || 
	        jwt.user.isSystemAdmin)

	    return (
	      <section className='dataset'>
	      		<h3 style={{margin:30, backgroundColor:'white'}}>Dataset</h3>
	      		<Datapoints 
	      			dataValueFormats={dataset.dataValueFormats} 
	      			datapoints={dataset.datapoints}
	      			players={players}
	      			handleUpdate= {authToUpdate ? ds => update(groupId, {...dataset, datapoints:ds}) : undefined} />
	      </section>
	    )
	}
}
Dataset.propTypes = {
}
Dataset.defaultProps = {
}

const Datapoints = ({dataValueFormats, datapoints, players, handleUpdate}) => {
	//helper
	const fullName = player => player.firstName + ' ' +player.surname

	const handleDelete = handleUpdate ? (ids) =>{
		console.log('handleDelete datapoints before', datapoints)
		console.log('rowIds to delete', ids)
		const remainingDatapoints = datapoints.filter(d => !ids.find(id => id === d._id))
		console.log('remainingDatapoints', remainingDatapoints)

		handleUpdate(remainingDatapoints)
	} : undefined

	const formattedDs = datapoints.map(d =>{
		var values = {}
		d.dataValues.forEach(dv => {
			values[dv.key] = dv.value
		})

		return {
			...d,
			...values,
			name:fullName(players.find(p => p._id === d.players[0])),
			date: new Date(d.eventDate),
			surface: d.surface || 'unknown'
		}
	})
	const generalHeadings = [
		{id: 'name', numeric: false, disablePadding: true, label:'Name'}, 
		{id: 'date', numeric: true, disablePadding: false, label: 'Date'}
	]
	//todo - make numeric 
	const valueHeadings = dataValueFormats.map(dvf => ({
		id:dvf.key, 
		numeric: (dvf.dataType === 'number' ? true : false),
		disablePadding: (dvf.dataType === 'number' ? false : true),
		label:dvf.name
	}))
	//todo - once filter is implemented, have these as collapsible info 
	//(see Material-ui Collapsibe-Table)
	const additionalHeadings = [
		{id:'surface', numeric: false, disablePadding: true, label:'Surface'}
	]
	//could add additonal headings on if space allows - this depends on
	//screen width and also number of dataValueFormats
	const headings = [...generalHeadings, ...valueHeadings]
	

	return(
		<div style={{margin:30, backgroundColor:'white'}}>
			{/**<SimpleTable rows={formattedDs} headings={headings}/>**/}
			<SortingTable title='Datapoints' rows={formattedDs} headings={headings} handleDelete={handleDelete}/>
		</div>
		)
}

export default Dataset
