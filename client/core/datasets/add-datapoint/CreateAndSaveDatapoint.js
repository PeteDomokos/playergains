import React, {Component, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
//other third party
import MomentUtils from '@date-io/moment'
import * as moment from 'moment';

//child components
import { DialogWrapper } from '../../../util/components/Dialog'
//import file
import { DatapointOptions } from '../../../constants/DatasetConstants'

//helpers
/*
todo - deal with decimals - not showing up in Textfield
*/
const defaultInit = userId => {
  return{
    players:[],
    dataValues:[],
    eventDate:Date.now(),
    notes:'',
    surface:'grass',
    location:'',
    createdBy:userId
  }
}

//note - dependancy - DatapointConstants - surfaces
export const CreateAndSaveDatapoint = 
  ({userId, init, availablePlayers, dataset, updating, onSubmit, resetStatus, history, classes}) =>{

  //note - datapoint stores playerId, then player is retrieved from availablePlayers, 
  // because otherwise Select comp doesnt recognise its the same player
  const [datapoint , setDatapoint] = useState(init ? init : defaultInit(userId))
  const selectedPlayers = datapoint.players.map(id => 
    availablePlayers.find(p => p._id === id))

  //note - if datasetSelected, then it will also contain other required properties
  const datasetSelected = dataset.category && dataset.name && dataset.nrParticipants
  const playersSelected = datapoint.players.length === dataset.nrParticipants 
  const dataValuesEntered = datasetSelected &&
    datapoint.dataValues.length === dataset.dataValueFormats.length

  const handleLocationChange = (event,i) => {
    setDatapoint({...datapoint, location:event.target.value})
  }
  const handleSurfaceChange = (event,i) => {
    setDatapoint({...datapoint, surface:event.target.value})
  }
  /**
  *    i will be undefined if only one player
  **/
  const handlePlayerChange = (event,i) => {
    const index = i ? i : 0
    const updatedPlayers = datapoint.players
    updatedPlayers[index] = event.target.value
    setDatapoint({...datapoint, players:updatedPlayers})
  }
  //todo - make an 'are you sure' dialog if target
  const handleDateChange = event => {
    const now = new Date()
    const date = new Date(event.target.value) 
    const _isTarget = now < date ? true : false
    console.log('now < date ? ', (now < date))
    setDatapoint({...datapoint, eventDate:date.getTime(), isTarget:_isTarget})
  }
  const handleDataValueChange = (event, format) => {
    const { value } = event.target
    //check entry can be converted to number
    const valueAsNumber = Number(value)
    if(isNaN(valueAsNumber))
      alert("Each value must be a number")
    else{
      //save value as string to preserve any decimal point
      const newDataValue = {name:format.name, key:format.key, value:value}
      const otherValues = datapoint.dataValues.filter(d => d.name !== format.name)
      const updatedValues = [...otherValues, newDataValue]
      console.log('updatedValues', updatedValues)
      setDatapoint({...datapoint, dataValues:updatedValues})
    }
  }
  const handleNotesChange = (event,i) => {
    setDatapoint({...datapoint, notes:event.target.value})
  }
  const formatValuesAndSubmit = () =>{
    //convert data values to numbers
    const dataValuesAsNumbers = datapoint.dataValues.map(d => {
      return {name:d.name, key:d.key, value:Number(d.value)}
    })
    console.log('dataValuesAsNumbers', dataValuesAsNumbers)
    setDatapoint({...datapoint, dataValues:dataValuesAsNumbers})
    onSubmit(datapoint)
  }

  const clearDataValues = () => {
    console.log('resetting dataValues...')
    setDatapoint({...datapoint, dataValues:[]})
  }
   let dialogButtonActions = [{label:'Return Home',
    onClick: () =>{
      resetStatus()
      history.push('/')
    }
  }]
  //add error and success options
  if(updating && updating.error && updating.error.type != 'demoUser'){
      dialogButtonActions.push({label:'Try again', onClick:onSubmit})
  }
  if(updating.complete){
    dialogButtonActions.push({
      label:'Add Another Datapoint', 
      onClick: () =>{
            clearDataValues()
            resetStatus()
      }
    })
  }

  //default surface options if file not available
  const surfaces = DatapointOptions.surfaces ? DatapointOptions.surfaces : 
    ['grass', 'synthetic', 'concrete', 'other']

  return(
    <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
      {datasetSelected &&
        <div className={classes.fields}>
          <Typography type="headline" component="h2" 
            className={classes.subtitle} style={{marginBottom:30}}>
            General Info
          </Typography>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <SelectEventDate 
              handleChange={handleDateChange} 
              selectedDate={datapoint.eventDate}
              classes={classes}/>
          </MuiPickersUtilsProvider>

          <SelectLocation
            location={datapoint.location}
            handleChange={handleLocationChange}
            classes={classes}/>

          <SelectSurface
            selectedSurface={datapoint.surface}
            surfaces={surfaces}
            handleChange={handleSurfaceChange}
            classes={classes}/>

          <EditNotes
              notes={datapoint.notes}
              handleChange={handleNotesChange}
              classes={classes}/>

          <SelectPlayers 
            selectedPlayers={selectedPlayers} 
            availablePlayers={availablePlayers} 
            nrParticipants={dataset.nrParticipants} 
            handleChange={handlePlayerChange}
            classes={classes}/>

            <AddValues 
              dataValues={datapoint.dataValues} 
              dataValueFormats={dataset.dataValueFormats} 
              handleChange={handleDataValueChange}
              classes={classes}/>
        </div>}

      {playersSelected && dataValuesEntered &&
          <Button color="primary" variant="contained" onClick={formatValuesAndSubmit} 
              className={classes.submit}>Save</Button>}

      {updating &&
        <DialogWrapper 
          open={true} 
          title='New Datapoint'
          mesg={updating.error ? updating.error.mesg : 
            updating.pending ?  'Saving Datapoint Please Wait...' :
            'New Data Entry successfully created'}
          buttons={dialogButtonActions} />}

    </div>
  )
}
//todo - impl this properly using a location component
const SelectLocation = ({location, handleChange, classes}) => {

  return(
    <TextField id="location" type="location" label="location" 
      className={classes.textField} value={location} 
    onChange={handleChange} margin="normal"/>
  )
}

SelectLocation.defaultProps = {
}

const SelectSurface = ({surfaces, selectedSurface, handleChange, classes}) => {
  return(
    <div className={classes.fieldWrapper}>
      <h4 className={classes.fieldName}>Surface</h4>
      <Select 
        value={selectedSurface? selectedSurface : ''}
        labelId="label" id='select-surface'
        onChange={handleChange}
        style={{minWidth:120}}>
        {DatapointOptions.surfaces.map(surface =>
          <MenuItem key={'surface-'+surface} value={surface}>
            {surface}</MenuItem>
        )}
      </Select>
    </div>
    )
}

SelectSurface.defaultProps = {
}

const EditNotes = ({notes, handleChange, classes}) => {

  return(
       <TextField id="notes" type="notes" label="notes" 
          className={classes.textField} value={notes} 
          onChange={handleChange} margin="normal"/>
    )
}

EditNotes.defaultProps = {
}

const SelectPlayer = ({title, selectedPlayer, availablePlayers, handleChange, i, classes}) => {
  return(
    <div style={{width:'100%'}}>
      <h4 className={classes.fieldName}>{title ? title : "Select a player"}</h4>
      <Select 
        value={selectedPlayer ? selectedPlayer._id : ''}
        labelId="label" id={'select-player'+(i ? i :'')} 
        onChange={event => handleChange(event, i)}
        style={{minWidth:120}}>
        {availablePlayers.map(player =>
          <MenuItem key={'player-'+player.firstName} value={player._id}>
            {player.firstName} {player.surname}</MenuItem>
        )}
      </Select>
    </div>
  )
}

SelectPlayer.defaultProps = {
  availablePlayers:[]
}

const SelectPlayers = ({selectedPlayers, availablePlayers, nrParticipants, handleChange, classes}) => {
  const playerIndex = nrParticipants ? [...Array(nrParticipants).keys()] :[]
  //helper to remove players that are already selected in another index
  const otherSelectedPlayers = i => selectedPlayers
    .filter(p => !selectedPlayers[i] || p._id !== selectedPlayers[i]._id)
  
  return(
    <div className={classes.fieldWrapper}>
      <Typography type="headline" component="h2" className={classes.subtitle}>
        Players
      </Typography>
      <div style={{display:'flex', justifyContent:'space-around'}}>
        {playerIndex.map(i =>{
          //filter players already selected elsewhere
          const availableAndNotSelected = availablePlayers
            .filter(p => !otherSelectedPlayers(i).find(player => player._id === p._id))
          return(
            <SelectPlayer 
              title={'Player '+(i+1)} selectedPlayer={selectedPlayers[i]}
              availablePlayers={availableAndNotSelected} handleChange={handleChange} 
              key={'players-'+i} i={i} classes={classes}/>
            )})}
      </div>
    </div>
    )
}

SelectPlayers.defaultProps = {
  availablePlayers:[]
}

//todo - move wrapper to root of app
/*
2020 18:37:55 GMT+0000 (Greenwich Mean Time)" does not conform 
to the required format.  The format is "yyyy-MM-ddThh:mm" 
followed by optional ":ss" or ":ss.SSS".*/
const SelectEventDate = ({handleChange, classes}) =>
  <div>
    <form className={classes.container} noValidate>
    <TextField
      className={classes.textField}
      id="datetime-local"
      label="Date and Time"
      type="datetime-local"
      onChange={handleChange}
      defaultValue={moment().format("YYYY-MM-DDTHH:mm")}
      className={classes.textField}
      InputLabelProps={{
        shrink: true,
      }}/>
    </form>
  </div>


const AddValues = ({dataValueFormats, dataValues, handleChange, classes}) => {

  return(
    <div className={classes.fieldWrapper}>
      <Typography type="headline" component="h2" className={classes.subtitle}>
        Values
      </Typography>
      {dataValueFormats.map((format,i) =>{
        const currentDataValue = dataValues.find(d => d.name === format.name)
        return(
            <TextField id={"value-"+i} label={format.name} 
              key={'format-'+format.name} className={classes.textField} 
              value={ currentDataValue ? currentDataValue.value : ''} 
              onChange={event => handleChange(event, format)} 
              margin="normal"/>)
      })}
    </div>
    )
}

AddValues.defaultProps = {
  dataValueFormats:[],
  values:[]
}



