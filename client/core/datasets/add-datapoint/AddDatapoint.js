import React, {Component, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Edit from '@material-ui/icons/Edit'
import {withStyles} from '@material-ui/core/styles'
//child components
import { SelectDataset } from '../add-dataset/SelectDataset'
import { CreateAndSaveDatapoint } from './CreateAndSaveDatapoint'
//helpers
import { sameKey } from '../DatasetHelpers'

const styles = theme => ({
  paper: {
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
    display:'flex',
    flexDirection:'column',
    alignItems:'center'
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    color: theme.palette.openTitle,
    fontSize:'22px'
  },
  subtitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    color: theme.palette.openTitle,
    fontSize:'18px'
  },
  fields:{
    border:'solid',
    backgroundColor:'#DBFFDB',
    display:'flex',
    flexDirection:'column',
    alignItems:'center'
  },
  fieldWrapper:{
    width:'90%'
  },
  fieldName: {
    textAlign:'left',
    marginTop: theme.spacing(3),
    color: theme.palette.openTitle,
    fontWeight:'lighter',
    fontSize:'14px'
  },
  textField: {
    width:'90%',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
    //width: '60vw'
  },
  numberTextField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    maxWidth: '120px'
  },
  datasetNameWrapper:{
    display:'flex',
    justifyContent:'center',
    marginBottom:theme.spacing(1)
  },
  datasetName:{
    display:'flex',
    flexDirection:'column',
    justifyContent:'center'
  },
  submit: {
    margin: 'auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  DatapointTypeArea: {
    margin: 'auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2)
  },
  additionalFields:{
  }
})

//init allows for container to pass in preselected dataset from url
const AddDatapoint = ({init, userId, group, updating, handleSubmit, resetStatus, history, classes}) => {
  const [dataset , setDataset] = useState(init ? init :{})

  const datasetSelected = dataset.category && dataset.name && dataset.nrParticipants

  const processSubmittedDataset = submittedDataset =>{
    const fullDataset = group.datasets.find(d => sameKey(d, submittedDataset))
    setDataset(fullDataset)
  }

  return(
    <Paper className={classes.paper}>
      <Typography 
        type="headline" component="h1" className={classes.title}>
        Add Test Datapoint
      </Typography>
      {datasetSelected ?
          <div className={classes.datasetNameWrapper}>
            <DatasetName dataset={dataset} classes={classes}/>
            <IconButton onClick={() => setDataset({})} 
              aria-label="Edit" color="primary" >
              <Edit/>
            </IconButton>
          </div>
        :
          <SelectDataset
            datasets={group.datasets}
            onSubmit={d => processSubmittedDataset(d)}
            classes={classes} />
      }
      
      <CreateAndSaveDatapoint
        userId={userId}
        availablePlayers={group.players} 
        dataset={dataset}
        updating={updating}
        resetStatus={resetStatus}
        onSubmit={d => handleSubmit(d, group._id, dataset._id)}
        history={history} 
        classes={classes} />

    </Paper>
  )
}


//todo - pass in datasets too, and use it to determine if there 
//is only a 1-player version. If so, then no need to show (1 player)
const DatasetName = ({dataset:{ category, name, nrParticipants }, classes}) =>
  <div className={classes.datasetName}>
    <h3 style={{fontSize:14, fontWeight:'bold'}}>{category}:
      <span style={{fontWeight:'normal'}}> {name}</span></h3>
    <h3 style={{fontSize:12}}>
      ({nrParticipants} {nrParticipants === 1 ? 'player' : 'players'})</h3>
  </div>


export default withStyles(styles)(AddDatapoint)



