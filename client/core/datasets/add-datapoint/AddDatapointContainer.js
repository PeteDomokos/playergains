import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addDatapoint } from '../../../actions/Groups'
import { resetStatus } from '../../../actions/Common'

import AddDatapoint from './AddDatapoint'

//todo - add a loader to check group is loaded
const mapStateToProps = (state, ownProps) => {
	console.log("state", state)
	const { groupId, datasetId } = ownProps.match.params
	const group = state.storedItems.groups.find(g => g._id === groupId)
	//note - group may need loading
	const initDataset = group && group.datasets ? group.datasets.find(d => d._id === datasetId) : undefined
	return({
		init:initDataset,
		userId:state.user._id,
		group:state.storedItems.groups.find(g => g._id === groupId),
		//if group loading, group will be undefined 
		updating:state.asyncProcesses.updating.dataset.datapoints,
		history:ownProps.history
	})
}
const mapDispatchToProps = dispatch => ({
	handleSubmit(datapoint, groupId, datasetId){
		console.log('cont..save datapoint', datapoint)
		dispatch(addDatapoint(datapoint, groupId, datasetId))
	},
	resetStatus(){
		dispatch(resetStatus('updating.dataset.datapoints'))
	}
})

//wrap all 4 sections in the same container for now.
const AddDatapointContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(AddDatapoint)

export default AddDatapointContainer

