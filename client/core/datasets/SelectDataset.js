import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import { SelectGroup } from './InputFields'
import { findUniqueCategories, findAvailableNames, 
  findAvailableNrParticipants } from './DatasetHelpers'

const styles = theme => ({

})
export const SelectDataset = ({selectedDataset, availableDatasets, handleDatasetChange}) =>{
  //console.log("SelectDataset selectedDataset", selectedDataset)
  //console.log("SelectDataset availableDatasets", availableDatasets)
  //all cats are available, even if name selected, because change in cat leads to reset
  const categories = findUniqueCategories(availableDatasets)
  //console.log("SelectDataset categories", categories)
  const names = findAvailableNames(selectedDataset, availableDatasets)
  //console.log("SelectDataset names", names)
  const nrParticipants = findAvailableNrParticipants(selectedDataset, availableDatasets)
 //console.log("SelectDataset nrParticipants", nrParticipants)
  return(
  <React.Fragment>
    <SelectCategory selectedCategory={selectedDataset.category}
      availableCategories={categories}
      handleChange={handleDatasetChange} />
    <SelectName selectedName={selectedDataset.name} 
      availableNames={names}
      handleChange={handleDatasetChange}/>
    <SelectNrParticipants nrParticipants={selectedDataset.nrParticipants}
      availableNrParticipants={nrParticipants}
      handleChange={handleDatasetChange}/>
  </React.Fragment>
  )}

SelectDataset.propTypes = {
}
SelectDataset.defaultProps = {
}


const SelectCategory = ({selectedCategory, availableCategories, handleChange}) =>{
  console.log('availableCategories', availableCategories)
  console.log('selectedCategory', selectedCategory)
  return(
    <div>
      <h4 style={{margin:5}}>Select a category</h4>
      <Select 
        value={availableCategories.includes(selectedCategory) ? selectedCategory : ''}
        labelId="label" id="select" onChange={handleChange('category')}
        style={{minWidth:120}}>
        {availableCategories.map(category =>
          <MenuItem value={category} key={category}>{category}</MenuItem>
        )}
      </Select>
    </div>)
  }

SelectCategory.defaultProps = {
  availableCategories:[]
}

const SelectName = ({selectedName, availableNames, handleChange}) =>{
   //console.log("SelectName selectedName:", selectedName)
    //console.log("SelectName availableNames:", availableNames)
    return(
    <div>
      <h4 style={{margin:5}}>Select a name</h4>
      <Select 
        value={availableNames.includes(selectedName) ? selectedName : ''}
        labelId="label" id="select" onChange={handleChange('name')}
        style={{minWidth:120}}>
        {availableNames.map(name =>
          <MenuItem key={name} value={name}>{name}</MenuItem>
        )}
      </Select>
    </div>
    )}
SelectName.defaultProps = {
  availableNames:[]
}

const SelectNrParticipants = ({nrParticipants, availableNrParticipants, handleChange}) =>{
  const backgroundColour = (number) =>{
    if(nrParticipants === number)
      return 'aqua'
    return 'auto'
  }
  return(
    <div style={{margin:30}}>
      <h4 style={{margin:5}}>Number of players in each test</h4>
      <Select labelId="label" id="select" value={nrParticipants} 
        onChange={handleChange('nrParticipants')} style={{minWidth:120}}>
         {availableNrParticipants.map(number =>
           <MenuItem value={number} key={number+1} 
            style={{backgroundColor:backgroundColour(number)}}>
            {number === 0 ? 'Any number' : number}</MenuItem>
         )}
      </Select>
    </div>
   )
}
SelectNrParticipants.defaultProps = {
  availableNrParticipants:[]
}
