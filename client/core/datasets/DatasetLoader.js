import React, { useEffect }  from 'react'
import Dataset from './Dataset'
/**
*
**/
//npt needed for now, but later we will not send datasets' datapoints with group, we will just send 
//all the other details. So then here we will get the datapoints too 
const DatasetLoader = ({groupId, groupAdmin, players, dataset, loadingDatapoints, onLoadDatapoints, update}) => {
	useEffect(() => {
		if(!dataset.datapoints){
			onLoadDatapoints(group._id, dataset._id)
		}
	}, [])
	return (
		<Dataset groupId={groupId} groupAdmin={groupAdmin} dataset={dataset} players={players} loadingDatapoints={loadingDatapoints} update={update} />
		)
}

export default DatasetLoader