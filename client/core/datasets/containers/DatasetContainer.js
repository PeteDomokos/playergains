import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchDatapoints } from '../../../actions/Dashboard'

import DatasetLoader from '../DatasetLoader'

import { updateDataset } from '../../../actions/Groups'


//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	const { groupId, datasetId } = ownProps.match.params
	const group = state.storedItems.groups.find(g => g._id === groupId)
	return({
		groupId:groupId,
		groupAdmin: group ? group.admin : undefined,
		players:group ? group.players : undefined,
		dataset:group ? group.datasets.find(d => d._id === datasetId) : undefined,
		loadingDatapoints: state.asyncProcesses.loading.datapoints,
		//if group loading, group.datasets etc will be undefined 
		loadingGroup:state.asyncProcesses.loading.group
	})
}
const mapDispatchToProps = dispatch => ({
	onLoadDatapoints(groupId, datasetId){
		dispatch(fetchDatapoints(groupId, datasetId))
	},
	update(groupId, dataset){
		
		console.log('update()...dataset: '+groupId, dataset)
		dispatch(updateDataset(groupId, dataset))
	}
})

//wrap all 4 sections in the same container for now.
const DatasetContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DatasetLoader)

export default DatasetContainer

