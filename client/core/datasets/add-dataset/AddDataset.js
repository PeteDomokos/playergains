import React, {Component, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormLabel from '@material-ui/core/FormLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
//child components
import { SelectDataset } from './SelectDataset'
import { AddStandardDataset } from './AddStandardDataset'
import { AddCustomDataset } from './AddCustomDataset'
import { DialogWrapper } from '../../../util/components/Dialog'
//helpers
import { standardDatasets } from '../../../constants/DatasetConstants' 
import { datasetAlreadyDefined, findDataset, visibilityValue } from '../DatasetHelpers'
//import auth from '../../../auth/auth-helper'

const styles = theme => ({
  paper: {
    width:'280px',
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '60vw'
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  datasetTypeArea: {
    margin: 'auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2)
  },
  additionalFields:{
  },
  submit:{
    margin:'20px'
  }
})

/*
TODO 
- DIALOG processes
.

*/

const initDataset = {
    isStandard:true,
    category:'',
    name:'',
    nrParticipants:'',
    visibleTo:['group-admin','group-coaches', 'group-players', 'all-users'],
}

const visibilityOptions = ['everyone', 'group members only', 
  'group coaches only', 'group admin only']

//todo - scroll up to visibility options when datasetSelected

const AddDataset = ({userId, group, handleSubmit, updating, resetStatus, history, classes}) => {
  console.log('standardDatasets', standardDatasets)
  //dataset type
  const [dataset, setDataset] = useState(initDataset)
  const { isStandard, category, name, nrParticipants, visibleTo } = dataset

  const datasetSelected = category && name && nrParticipants

  //handleChange takes an event or a direct value
  const handleChange = key => eventOrValue =>{
    console.log('key', key)
    //clear will reset everything, except visibility and isStandard
    if(key === 'clear'){
      setDataset({...initDataset, isStandard:isStandard, visibleTo:visibleTo})
    }else{
      const value = eventOrValue.target ? 
        eventOrValue.target.value : eventOrValue
      console.log('value', value)
      setDataset({...dataset, [key]:value})
    }
  }
  //process visibility value into array format
  const handleVisibilityChange = event =>{
    handleChange('visibleTo')(visibilityValue(event.target.value))
  }

  const processDatasetSubmitted = submittedDataset =>
    setDataset({...dataset, ...submittedDataset})

  const onSubmit = () =>{

    const storedDataset = findDataset(dataset, standardDatasets)

    //if(!isStandard)
    //check for duplicates and name clash with a standard dataset
    const allDatasetFields = {
        ...dataset,
        createdBy:userId,
        key:storedDataset.key,
        dataValueFormats:storedDataset.valueFormats
    }
    handleSubmit(allDatasetFields, group._id)
  }
  //todo - push to last location
  const onCancel = () => {
    history.push('/')
  }
  //todo - put reset status into error buttons too
  let dialogButtonActions = [{label:'Return Home',
    onClick: () =>{
      resetStatus()
      history.push('/')
    }
  }]
  //add error and success options
  if(updating && updating.error && updating.error.type != 'demoUser'){
      dialogButtonActions.push({label:'Try again', onClick:onSubmit})
  }
  if(updating.complete){
    dialogButtonActions.push({
        label:'Add Another Dataset', 
        onClick: () =>{
          resetStatus()
          handleChange('clear')()
        }
    })
  }

  return(
    <Paper className={classes.paper}>
      {updating ?
          <DialogWrapper 
          open={true} 
          title='New Dataset'
          mesg={updating.error ? updating.error.mesg : 
            updating.pending ?  'Saving Dataset Please Wait...' :
            'New Data Entry successfully created'}
          buttons={updating.pending ? [] : dialogButtonActions} />
        :
        <div>
          {isStandard ? 
            <SelectDataset
              init = {{category:category, name:name, nrParticipants:nrParticipants}}
              datasets={standardDatasets
                .filter(d => !datasetAlreadyDefined(d, group.datasets))}
              onSubmit={processDatasetSubmitted}
              classes={classes} />
            :
            <AddCustomDataset classes={classes} />
          }

          {datasetSelected &&
            <div>
              <RadioSelector
                  formLabel='Data visible to...'
                  options={visibilityOptions}
                  defaultValue='everyone'
                  handleChange={handleVisibilityChange} />

                <Button color="primary" variant="contained" onClick={onCancel} 
                  className={classes.submit}>Cancel</Button>
                <Button color="primary" variant="contained" onClick={onSubmit} 
                  className={classes.submit}>Save</Button>
            </div>
          }
          <div className={classes.datasetTypeArea}>
            <h4 style={{margin:10, marginBottom:0, fontWeight:'normal'}}>Dataset Type: 
              <span style={{fontWeight:'bold'}}>
                {isStandard ? 'Standard' : 'Custom'}</span></h4>
            <Button style={{margin:5}} color="primary" variant="contained" 
              onClick={() => handleChange('isStandard')(!dataset.isStandard)} 
              className={classes.submit}>
              Change</Button>
          </div>
        </div>
      }
    </Paper>
    )
}

//each option is either a value, or an object with a value (and optional label)
//defaultValue optional, otherwise first option value selected
const RadioSelector  = ({formLabel, options, defaultValue, handleChange}) => 
  <FormControl component="fieldset">
    <FormLabel component="legend">{formLabel}</FormLabel>
    <RadioGroup onChange={handleChange} 
      defaultValue={defaultValue ? defaultValue : options[0] ? 
        (options[0].value ? options[0].value : options[0]) : ''}
      aria-label={formLabel} name="customized-radios">
      {options.map((opt,i) =>
        <FormControlLabel 
          value={opt.value ? opt.value : opt} 
          control={<Radio />} 
          label={opt.label ? opt.label : opt.value ? opt.value : opt}
          key={formLabel +'option-'+i} /> )}
    </RadioGroup>
  </FormControl>

export default withStyles(styles)(AddDataset)



