import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addDataset } from '../../../actions/Groups'
import { resetStatus } from '../../../actions/Common'

import AddDataset from './AddDataset'

//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	console.log("state", state)
	const { groupId } = ownProps.match.params
	return({
		userId:state.user._id,
		group:state.storedItems.groups.find(g => g._id === groupId),
		//if group loading, group.datasets etc will be undefined 
		updating:state.asyncProcesses.updating.group.datasets,
		history:ownProps.history
	})
}
const mapDispatchToProps = dispatch => ({
	handleSubmit(dataset, groupId){
		dispatch(addDataset(dataset, groupId))
	},
	resetStatus(){
		dispatch(resetStatus('updating.group.datasets'))
	}
})

//wrap all 4 sections in the same container for now.
const AddDatasetContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(AddDataset)

export default AddDatasetContainer

