import React, {Component, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
//child components

//helpers
import { findAvailableNames, findAvailableNrParticipants, 
  findAvailableCategories} from '../DatasetHelpers'
import { onlyUnique } from '../../../util/helpers/ArrayManipulators'

//import auth from '../../../auth/auth-helper'


/*
  todo - fix bug where if only 1 available then neither of them can be changed
*/

const defaultInit = {
    category:'',
    name:'',
    nrParticipants:'',
}

/*

*/
export const SelectDataset = ({init, datasets, onSubmit, withSubmitButton, classes}) => {
  const [dataset , setDataset] = useState(init ? init : defaultInit)
  const { category, name, nrParticipants } = dataset
  console.log('SelectDataset dataset', dataset)

  //handleChange takes an event or a direct value
  const handleChange = key => eventOrValue =>{
    //clear will reset everything, except visibility and isStandard
    if(key === 'clear')
      setDataset(init ? init : defaultInit)
    else{
      const value = eventOrValue.target ? 
        eventOrValue.target.value : eventOrValue

      //if category change, reset name and nrParticipants
      if(key === 'category')
        setDataset({category:value, name:'', nrParticipants:''})
      else
        setDataset({...dataset, [key]:value})
    }
  }
  const allCategories = datasets.map(d => d.category).filter(onlyUnique)
  //filter categories based on name and nrParticipants selection
  const availableCategories = findAvailableCategories(
    {name:name, nrParticipants:nrParticipants}, datasets)
  //auto-select if only 1 option
  if(availableCategories.length === 1 && category !== availableCategories[0])
    handleChange('category')(availableCategories[0])

  //filter names based on category and nrParticipants selection
  const availableNames = findAvailableNames(
    {category:category, nrParticipants:nrParticipants}, datasets)
  //auto-select if only 1 option
  if(availableNames.length === 1 && name !== availableNames[0])
    handleChange('name')(availableNames[0])

  //filter nrParticipants based on category and name selection
  const availableNrParticipants = findAvailableNrParticipants(
    {category:category, name:name}, datasets)
  //auto-select if only 1 option
  if(availableNrParticipants.length === 1 && nrParticipants !== availableNrParticipants[0])
    handleChange('nrParticipants')(availableNrParticipants[0])

  return(
    <Card>
      <CardContent>
        <div>Select Dataset</div>
        <div style={{display:'flex', flexDirection:'column', justifyContent:'flex-start',
        margin:30}}>
          <DropdownSelector
            description='Category' 
            selected={category} 
            options={allCategories}
            handleChange={handleChange('category')} />
          <DropdownSelector
            description='Name' 
            selected={name} 
            options={availableNames}
            handleChange={handleChange('name')} />
          <DropdownSelector
            description='Participants' 
            selected={nrParticipants} 
            options={availableNrParticipants}
            handleChange={handleChange('nrParticipants')} />
        </div>
      </CardContent>
      <CardActions>
      {(category || name || nrParticipants) &&
        <Button 
          color="primary" variant="contained" className={classes.submit}
          onClick={() => handleChange('clear')()}>Clear</Button>}
      {(category && name && nrParticipants) &&
        <Button 
          color="primary" variant="contained" className={classes.submit}
          onClick={() => onSubmit(dataset)}>Choose</Button>}
      </CardActions>
    </Card>
    )
}

const DropdownSelector = ({description, selected, options, handleChange}) =>
    <div style={{display:'flex', flexDirection:'column', margin:20}}>
      <h4>Select {description}</h4>
      <Select value={selected}
         labelId="label" id={'select-' +description} 
         onChange={handleChange}>
        {options.map(option =>
          <MenuItem value={option} key={option}>{option}</MenuItem>
        )}
      </Select>
      {selected && options.length !== 1 &&
        <div style={{alignSelf:'flex-end', color:'red', marginTop:10}}
          onClick={() => handleChange('')}>X</div>}
    </div>







