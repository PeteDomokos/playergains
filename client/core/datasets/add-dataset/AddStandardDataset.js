import React, {Component, useState} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import { onlyUnique } from '../../../util/helpers/ArrayManipulators'
//material-ui
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
//child components

//helpers
import { findAvailableNames, findAvailableNrParticipants, 
  findAvailableCategories} from '../DatasetHelpers'
//import auth from '../../../auth/auth-helper'

export const AddStandardDataset = ({dataset, standardDatasets, handleChange, classes}) => {
  const { category, name, nrParticipants } = dataset

  //filter categories based on name and nrParticipants selection
  const availableCategories = findAvailableCategories(
    {name:name, nrParticipants:nrParticipants}, standardDatasets)
  //auto-select if only 1 option
  if(availableCategories.length === 1 && category !== availableCategories[0])
    handleChange('category')(availableCategories[0])

  //filter names based on category and nrParticipants selection
  const availableNames = findAvailableNames(
    {category:category, nrParticipants:nrParticipants}, standardDatasets)
  //auto-select if only 1 option
  if(availableNames.length === 1 && name !== availableNames[0])
    handleChange('name')(availableNames[0])

  //filter nrParticipants based on category and name selection
  const availableNrParticipants = findAvailableNrParticipants(
    {category:category, name:name}, standardDatasets)
  //auto-select if only 1 option
  if(availableNrParticipants.length === 1 && nrParticipants !== availableNrParticipants[0])
    handleChange('nrParticipants')(availableNrParticipants[0])

  return(
    <div>
      <div>Add Standard Dataset</div>
      <div style={{display:'flex', flexDirection:'column', justifyContent:'flex-start',
      margin:30}}>
        <DropdownSelector
          description='Category' 
          selected={category} 
          options={availableCategories}
          handleChange={handleChange('category')} />
        <DropdownSelector
          description='Name' 
          selected={name} 
          options={availableNames}
          handleChange={handleChange('name')} />
        <DropdownSelector
          description='Participants' 
          selected={nrParticipants} 
          options={availableNrParticipants}
          handleChange={handleChange('nrParticipants')} />
      </div>
    </div>
    )
}

const DropdownSelector = ({description, selected, options, handleChange}) =>
    <div style={{display:'flex', flexDirection:'column', margin:20}}>
      <h4>Select {description}</h4>
      <Select value={selected}
         labelId="label" id={'select-' +description} 
         onChange={handleChange}>
        {options.map(option =>
          <MenuItem value={option} key={option}>{option}</MenuItem>
        )}
      </Select>
      {selected && options.length !== 1 &&
        <div style={{alignSelf:'flex-end', color:'red', marginTop:10}}
          onClick={() => handleChange('')}>X</div>}
    </div>







