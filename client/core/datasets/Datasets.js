import React, {Component} from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//linked and child components
import DatasetsHome from './DatasetsHome'
import DatasetContainer from './containers/DatasetContainer'
import AddDatasetContainer from './add-dataset/AddDatasetContainer'
import AddDatapointContainer from './add-datapoint/AddDatapointContainer'
//helpers
import auth from '../../auth/auth-helper'

class Datasets extends Component{
	render() {
    const { group, datasetId } = this.props
	    return (
	      <section className='datasets'>
	      		<Switch>
		      		<Route path="/group/:groupId/datasets/new" 
		      			component={AddDatasetContainer} />
		      		<Route path="/group/:groupId/datasets/:datasetId/datapoints/new" 
		      			component={AddDatapointContainer} />
		      		<Route path="/group/:groupId/datasets/:datasetId" 
		      			component={DatasetContainer}/>
		      	</Switch> 
	      		<DatasetsHome datasets={group.datasets}/>
	      </section>
	    )
	}
}
Datasets.propTypes = {
}
Datasets.defaultProps = {
}

export default Datasets
