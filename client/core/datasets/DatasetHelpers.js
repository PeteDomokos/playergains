import { onlyUnique } from '../../util/helpers/ArrayManipulators'

/*
  if standard, then check sameKey. If custom, check same id
*/
export const areSame = datasets =>{
  //console.log("areSame datasets", datasets)
  //console.log("datasets[0].isStandard", datasets[0].isStandard)
  //console.log("datasets[1].isStandard", datasets[1].isStandard)
  //console.log("same type? ", datasets.filter(d => d.isStandard !== datasets[0].isStandard))
  //check all are of the same type (could be standard or custom)
  if(datasets.filter(d => d.isStandard !== datasets[0].isStandard).length !== 0){
    //console.log("not all same type so false")
    return false
  }
  if(datasets[0].isStandard){
    //console.log("all standard")
    //console.log("number that are same", datasets.filter(d => sameKey(d, datasets[0])).length)
    return datasets.filter(d => sameKey(d, datasets[0])).length === datasets.length
  }
  else{
    //console.log("all custom")
    //console.log("number that are same", datasets.filter(d => d._id === datasets[0]._id))
    return datasets.filter(d => d._id === datasets[0]._id).length === datasets.length
  }
}

export const categoryAndNameBothSet = dataset =>{
  return dataset.category && changed == 'name' || 
        dataset.name && changed == 'category'
}

export const customDatasetAlreadyDefined = (datasetToCheck, datasets) =>{
  return datasetAlreadyDefined(datasetToCheck, datasets.filter(d => !d.isStandard))
}

export const customDatasetsForGroup = (group) =>{
  if(!group || !group.datasets)
    return []
  return group.datasets.filter(d => !d.isStandard)
}

//validates and processes fields to return dataset
//todo - handle user creating a custom dataset with same identifiers as one they already have,
// custom or standard. Add (custom) and (standard) to names if req.
export const datasetFormProcessor = (userId, group, fields) =>{
  //if(datasetAlreadyDefined(fields, group.datasets)){
   // return{
     // error: 'Duplicate test. Change the category, name or number of participants.'}
  //}else
    return{
      category: fields.category,
      name: fields.name,
      nrParticipants:fields.nrParticipants,
      createdBy:userId,
      isStandard:fields.isStandard,
      visibleTo:fields.visibleTo,
      dataValueFormats:[
        //todo - impl options for these, and capitalise name and make units lower case
        {name:'Time', unit:'secs', dataType:'number'},
        {name:'Penalties', dataType:'number'}]
    }
}

export const datapointFormProcessor = (userId, fields) =>{
   const { players, dataValues, eventDate } = fields
   return {
      player: players.length == 1 ? players[0]._id : undefined,
      players:players.length > 1 ? players.map(p => p._id) : undefined,
      //in state, we store each value with a name too, but not needed in db
      dataValues:dataValues.map(dv => dv.value),
      eventDate:eventDate ? eventDate : undefined,
      createdBy:userId
    }
}

//note - names for standard sets cannot be used for custom sets,
//even if the standard ones are not defined in group, so no need to 
//check for standard/custom
export const datasetAlreadyDefined = (datasetToCheck, datasets) =>{
  if(!datasetToCheck || !datasets){
    return false
  }
  if(datasets.find(d =>
    d.category === datasetToCheck.category &&
    d.name === datasetToCheck.name &&
    d.nrParticipants === datasetToCheck.nrParticipants))
    return true
  return false
}

export const datasetNamePair = (dataset, datasets) =>{
  if(!datasets || !datasets.length)
     return [dataset.name,'']
  if(!dataset)
    return ['','']

  //check uniqueness
  const otherSets = datasets.filter(d => d._id != dataset._id)
  const categoryAndNameAreUnique = otherSets.find(d => 
      d.category === dataset.category && d.name === dataset.name) 
        ? false : true
  //create extra part of name in some cases
  let extraName = ''
  if(dataset.nrParticipants != 1)
    extraName = ' ('+dataset.nrParticipants +' players)'
  else if(!categoryAndNameAreUnique)
    extraName = ' (1 player)'

  //temp mock names
  let nameToReturn
  if(dataset.name === '5-metre Acceleration'){
    nameToReturn = '5m Max Acceleration Test'
  }else if(dataset.name === 'Best Acceleration'){
    nameToReturn = 'Top Acceleration in Match'
  }else{
    nameToReturn = dataset.name
  }
  return [nameToReturn, extraName]
}


export const editPlayers = (currentPlayers, player) =>{
  if(currentPlayers.map(p => p._id).includes(player._id)){
    console.log("removing")
    return currentPlayers.filter(p => p._id !== player._id)
  }
  else{
     console.log("adding")
    return [...currentPlayers, player]
  }
}

//WARNING - I HAVE ADDED THE FILTERS FOR NRPARTICIPANTS AND NAME BACK IN
export const findAvailableCategories = (selectedDataset, availableDatasets) =>{
  if(!Array.isArray(availableDatasets) || !selectedDataset)
    return []

  let { name, nrParticipants } = selectedDataset

  return availableDatasets
    .filter(d => !name || d.name === name)
    .filter(d => !nrParticipants || d.nrParticipants === nrParticipants)
    .map(d => d.category)
    .filter(onlyUnique)
}
export const findAvailableNames = (selectedDataset, availableDatasets) =>{
  //console.log("findAvailableNames selectedDataset", selectedDataset)
  //console.log("findAvailableNames availableDatasets", availableDatasets)
    if(!Array.isArray(availableDatasets) || !selectedDataset)
      return []
  //if cat != '', filter to sets with that cat
  //if nrParts != '', filter remaining sets to sets with that NrParts
  let { category, nrParticipants } = selectedDataset
  return availableDatasets
    .filter(d => !category || d.category === category)
    .filter(d => !nrParticipants || d.nrParticipants === nrParticipants)
    .map(d => d.name)
    .filter(onlyUnique)
}
/**
* @params  datasetFields:Object - the other chosen properties of the dataset signature
*          availableDatasets -  
*
**/
export const findAvailableNrParticipants = (datasetFields, availableDatasets) =>{
    if(!Array.isArray(availableDatasets) || !datasetFields)
    return []
  //if cat != '', filter to sets with that cat
  //if name != '', filter remaining sets to sets with that name
  let { category, name } = datasetFields
  //return 
  return availableDatasets
    .filter(d => !category || d.category === category)
    .filter(d => !name || d.name === name)
    .map(d => d.nrParticipants)
    .filter(onlyUnique)
    .sort((a,b) => a - b)
}


//returns the dataset from datasets - this may be a deeper version as datset may only 
//have the 3 required fields
export const findDataset = (dataset, datasets) =>{
  return datasets.find(d => 
    d.category === dataset.category &&
    d.name === dataset.name &&
    d.nrParticipants === dataset.nrParticipants)
}

/**
*   filters out any standard datasets that are already in group.datasets
**/
export const findAvailableStandardDatasets = (group, datasets) =>{
  //console.log("findAvailableStandardDatasets() group:", group)
  if(!group || !group.datasets ||!Array.isArray(group.datasets) || !datasets)
    return []
  let standardGroupDatasets = group.datasets.filter(d => d.isStandard)
  //console.log("standardGroupDatasets", standardGroupDatasets)
  //console.log("datasets to compare to group", datasets)
  //return those standardDatasets not in group already 
  let available = datasets.filter(d => {
    let inGroup = standardGroupDatasets.find(grpDataset =>
      grpDataset.name === d.name && 
      grpDataset.category === d.category && 
      grpDataset.nrParticipants === d.nrParticipants)
    return !inGroup
  })
  //console.log("available:", available)
  return available
}

export const findUniqueCategories = (datasets) =>{
  if(!datasets)
    return []
  return datasets.map(set => set.category)
                 .filter(onlyUnique)
}
/**
if standard, use key to identify sameness.
if custom, use _id 

@params  groups - an array of objects, each of which must simply have a datasets property

**/
export const isCommon = (dataset, groups) =>{
  if(dataset.isStandard){
    const commonToBoth = groups
                 .map(g => g.datasets)
                 .filter(datasets => datasets.find(d => sameKey(d, dataset)))
                 .length === groups.length
    return commonToBoth
  }else
    return groups.map(g => g.datasets)
                 .filter(datasets => datasets.find(d => d._id === dataset._id))
                 .length === groups.length
}

export const mergedDatasets = datasets => {
  const customSets = datasets.filter(d => !d.isStandard)
  const standardSets = datasets.filter(d => d.isStandard)
  let mergedStandard = []
  standardSets.forEach(dataset=>{
    //Only create a merged set if it hasnt been done already
    if(!mergedStandard.find(d => sameKey(d, dataset))){
      const mergedDataset = standardSets
        .filter(d => sameKey(d, dataset))
        .reduce((a,b) => {
          const aIds = a.ids ? a.ids : [a._id]
          const bIds = b.ids ? b.ids : [b._id]
          const newIds = [...aIds, ...bIds]
          return {...a, ...b, ids:newIds} })

      mergedStandard.push(mergedDataset)
    }
  })
  return [...customSets, ...mergedStandard]
}

export const namePairsForCategory = (category, datasets) =>{
  if(!category || !datasets.length)
    return ['','']
  return datasets.filter(dataset => dataset.category === category)
                 .map(dataset => datasetNamePair(dataset, datasets))
}

//merges standard datasets from different groups that have same key
export const groupDatasets = groupWithParents =>{
  //check if only group and not parents
  if(!Array.isArray(groupWithParents))
    return groupWithParents.datasets
  else{
    const datasets = groupWithParents.map(g => g.datasets).reduce((a,b) => [...a, ...b], [])
    return mergedDatasets(datasets)
  }
}

//groups may be an array of groups, or an array of arrays if parents are included
//merges standard datasets from different groups that have same key

export const playerDatasets = (player, groups) =>{
  const datasets = groups.map(g => groupDatasets(g)).reduce((a,b) => [...a, ...b], [])
  console.log("playerDatasets helper... datasets", datasets)
  const merged = mergedDatasets(datasets)
  console.log("playerDatasets helper... merged", merged)
  //!!!!!!!!!!!!!!!!!!!todo - mergedDatasets is returning undefined
  return merged
}

export const sameKey = (dataset1, dataset2) =>
  dataset1.category === dataset2.category &&
  dataset1.name === dataset2.name &&
  dataset1.nrParticipants === dataset2.nrParticipants

export const standardDatasetAlreadyDefined = (datasetToCheck, datasets) =>{
  return datasetAlreadyDefined(datasetToCheck, datasets.filter(d => d.isStandard))
}

export const standardDatasetsForGroup = group =>{
  if(!group || !group.datasets)
    return []
  return group.datasets.filter(d => d.isStandard)
}

/**
*
**/
export const visibilityValue = (value) => {
  switch(value){
    case 'everyone':{
      return ['group-admin', 'group-coaches', 'group-members', 'all-users']
    }
    case 'group members only':{
      return ['group-admin', 'group-coaches', 'group-members']
    }
    case 'group coaches only':{
      return ['group-admin', 'group-coaches']
    }
    case 'group admin only':{
      return ['group-admin']
    }
  }
}