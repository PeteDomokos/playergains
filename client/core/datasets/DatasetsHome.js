import React, {Component} from 'react'
import { Route, Link }from 'react-router-dom'
import PropTypes from 'prop-types'
//linked and child components
import DatasetsList from './DatasetsList'
//helpers
import auth from '../../auth/auth-helper'

class DatasetsHome extends Component{
	constructor(props){
    	super(props)
    	this.state = {
    		adminGroups:[],
    		playingGroups:[],
    		coachingGroups:[],
    		groupsFollowing:[],
    		loading:false
    	}
  	}
  	componentDidMount(){
  		let jwt = auth.isAuthenticated()
  		let userId = jwt.user ? jwt.user._id : ''
  	}
	render() {
		const emptyMyDatasetsMesg = this.state.loading ? 'Loading' : 'No datasets created yet'
		const emptyOtherDatasetsMesg = this.state.loading ? 'Loading' : 'No datasets available'
	    return (
	    	<div className='dataset-home'>

				<DatasetsList 
		  			title='My Datasets' 
		  			datasets={this.state.myDatasets} 
		  			emptyMesg={emptyMyDatasetsMesg}/>
		  		<DatasetsList 
		  			title='Other Datasets' 
		  			datasets={this.state.otherDatasets}
		  			emptyMesg={emptyOtherDatasetsMesg}/>
		  	</div>
		  	)
	}
}
DatasetsHome.propTypes = {
}
DatasetsHome.defaultProps = {
}

export default DatasetsHome
