import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormLabel from '@material-ui/core/FormLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
//helpers

export const SelectGroup = ({selectedGroup, availableGroups, handleChange}) =>
  <div>
    <h4 style={{margin:5}}>Select a group</h4>
    <Select value={selectedGroup ? selectedGroup : ''} 
      labelId="label" id="select" onChange={handleChange('group')}
      style={{minWidth:120}}>
      {availableGroups.map(group =>
        <MenuItem value={group} key={group.name}>
            {group.name}</MenuItem>
      )}
    </Select>
  </div>

SelectGroup.propTypes = {
}
SelectGroup.defaultProps = {
  availableGroups:[]
}


