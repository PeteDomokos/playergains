import React, {Component} from 'react'
import { Route, Link }from 'react-router-dom'
import PropTypes from 'prop-types'
//helpers
import { datasetNamePair } from './DatasetHelpers'

const DatasetsList = ({title, datasets, emptyMesg}) =>
	<div style={{margin:10, marginTop:20, marginBottom:20, backgroundColor:'white'}}>
		<h3 style={{margin:5, backgroundColor:'white'}}>{title}</h3>
		{datasets.length > 0 ?
			<div style={{margin:5}}>
				{datasets.map(dataset =>{
					let namePair = datasetNamePair(dataset, datasets)
					return(
						<div style={{margin:5, fontSize:16}} key={dataset._id}>
							{namePair[0]}<span style={{fontSize:12}}>{namePair[1]}</span>
						</div>
					)
				})}
			</div>
			:
			<div style={{margin:5}}>{emptyMesg}</div>}
	</div>
DatasetsList.propTypes = {
}
DatasetsList.defaultProps = {
	datasets:[]
}

export default DatasetsList
