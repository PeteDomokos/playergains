import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link, Redirect } from 'react-router-dom'
//material-ui
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import ArrowForward from '@material-ui/icons/ArrowForward'
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import CancelIcon from '@material-ui/icons/Cancel';
//video
import VideoPlayer from '../util/components/VideoPlayer'
//Child Components
import RecentDashboards from './RecentDashboards'
import UserProfile from './../user/UserProfile'
import UserGroups from '../groups/UserGroups'
import SystemAdmin from './system-admin/SystemAdmin'
import { SampleCharts } from './system-admin/sample-charts/SampleCharts'
//helpers
import auth from '../auth/auth-helper'
//Constants and Data
import homeSections from '../constants/HomeSections'
import { renderChart } from './d3_practice/analytic_methods/Chap2'

/**
* Displays a generic home page if no user is authenticated, 
* or the users personalised home page, with links to their players, groups and data, if they are.
* If user is a system admin user, then it also displays a series of additional buttons for systems-related processes
**/

//  TODO - SORT OUT HOEM ESP LINKS IN HOMESECTIONS SHOULD USE RENDERACTIONBUTTONS METHOD
class Home extends Component {
  constructor(props){
    super(props)
    this.handleSectionClick = this.handleSectionClick.bind(this)
    this.demoSignin = this.demoSignin.bind(this)
    this.handleSelectVideo = this.handleSelectVideo.bind(this)
    this.state = {
      selectedVideoURL:''
    }
  }
  /**
  * Uses History.push() to handle clicks on links to other sections
  * @param {string} section - the name of the active section, such as 'about', 'dashboard'
  **/
  //NOTE - for now, this just signs in user as demo sign in
  handleSectionClick(section){
    this.demoSignin(section)
    //this.props.history.push("/"+section)
  }
  /**
  * Calls signin using a demo users details, tehn pushes the section taht was clicked to history
  * @param {string} redirectPath - the name of the active section, such as 'about', 'dashboard'
  **/
  demoSignin(redirectTo){
    let demoUser = {
      //email:'deo@y.z',
      //password:'dddddd'
      email:'democoach@y.z',
      password:'dddddd'
    }
    this.props.onSignin(demoUser, this.props.history, redirectTo)
  }
  handleSelectVideo(url){
    if(!url || this.state.selectedVideoURL === url){
      this.setState({selectedVideoURL:''});
    }else{
      this.setState({selectedVideoURL:url});
    }
  }
  componentDidMount(){
    //renderChart()
  }
  render() {
    const { user, groups } = this.props
    //let userObj = auth.isAuthenticated()
    //let user = userObj ? userObj.user : undefined
    //console.log("Home user from auth", user)
    return (
      <section >
        {this.state.selectedVideoURL && 
          <div style={{width:'100%', display:'flex', justifyContent:'center'}}>
            <VideoPlayer url={this.state.selectedVideoURL} 
                       handleCancel={this.handleSelectVideo}
                       playing={true}/>
          </div>}
        {auth.isAuthenticated() && user._id ?
          <UserHome user={user} groups={groups} 
            handleSelectVideo={this.handleSelectVideo}/>
        :
          <NonUserHome handleSectionClick={this.handleSectionClick} 
            demoSignin={this.demoSignin} handleSelectVideo={this.handleSelectVideo}/>
        }
      </section>
    )
  }
}
Home.defaultProps = {
}

const UserHome = ({user, groups, handleSelectVideo}) => 
  <div className='user-home'>
   {user.isSystemAdmin && <SampleCharts/>}
    <div className='screen-fit'>
      <div className='row-1'>
        <div className='not-ms welcome-mesg'>
          Welcome back {user.username}!
        </div>
        <div className='profile-cont'>
          <UserProfile user={user}/>
        </div>
      </div>
      <div className='row-2'>
        <UserGroups user={user} groups={groups}/>
        <RecentDashboards user={user} groups={groups}/>
      </div>
    </div>
    {/**user.isSystemAdmin && <SystemAdmin/>**/}
  </div>

const NonUserHome =({handleSectionClick, demoSignin, handleSelectVideo}) => 
  <div className='home'>
    {/**<SampleCharts/>**/}
    <div className='intro-banner'>
      <div className='logo-player-gains'>PLAYER GAINS</div>
      <div className='motto'>
        <div>A data system for coaches and players</div>
      </div>

    </div>
    <div className='sections'>
      {homeSections.map((section,i) => 
        <section className={'section '} key={i}>
          <div className='banner' key={i} onClick={() => handleSectionClick(section.id)}>
              <h2 className='title'>{section.content.heading}</h2>
              <div className='mesg'>
                <p>{section.content.subheading[0]}</p>
                <p>{section.content.subheading[1]}</p>
              </div>
          </div>

          <div className='section-content'>
            <Button onClick={() => handleSelectVideo(section.content.videoURL)}>
              Video
            </Button>
            <Button onClick={() => demoSignin(section.id)}>
              Explore
            </Button> 
              {/**<IconButton onClick={() => 
                  handleSelectVideo(section.content.videoURL)}>
                <VideoLabelIcon/>
              </IconButton>
              <IconButton onClick={() => demoSignin(section.id)}>
                <ArrowForward/>
              </IconButton>**/}
          </div>

        </section>)
      }
    </div>
  </div>

Home.propTypes = {
}

export default Home
