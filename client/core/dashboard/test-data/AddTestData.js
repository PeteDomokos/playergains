import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'

import {withStyles} from 'material-ui/styles'
import Paper from 'material-ui/Paper'
import List, {ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle} from 'material-ui/Dialog'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Edit from 'material-ui-icons/Edit'
import Person from 'material-ui-icons/Person'
import Divider from 'material-ui/Divider'
import Card, {CardActions, CardContent} from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import FileUpload from 'material-ui-icons/FileUpload'
import Icon from 'material-ui/Icon'
import ArrowForward from 'material-ui-icons/ArrowForward'

import {create} from './api-data.js'
import {Link} from 'react-router-dom'
import auth from '../../../auth/auth-helper'
import { readGroup } from '../../../groups/api-group.js'
import { getLabel, createScoreDataObject } from '../charts/TestDataHelpers'

const styles = theme => ({
  card: {
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing.unit * 5,
    marginBottom: theme.spacing.unit * 5,
    paddingBottom: theme.spacing.unit * 2
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing.unit * 2,
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '60vw'
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing.unit * 2
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing.unit * 2
  },
  additionalFields:{
  }
})
/**
*   user can only add data for players that they are admin for - so in reality a coach should be 
*   admin for all of their players.
*
**/
class AddTestData extends Component {
  state = {
  	  group: {name:'No Group Selected', players:[]},
      player: '',
      date:'',
      cat:'',
      test:'',
      data:'',
      date:'',
      selecting:'',
      groups:[],
      //todo, make cats available dependent on grouo.players ie pass players to a function
      //which determines which cats and tests are done by at least one player
      cats:[{name:'skills', tests:['touch', 'switchplay', 'dribble']},
      		{name:'fitness', tests:['yoyo', 'shuttles', 'tTest']}
      ],
      open: false
  }
  //init - check user is authenticated - if so, set state to that user
  init = (groupId) => {
  	const jwt = auth.isAuthenticated()
  	console.log("jwt", jwt)

  	let reqGroupId = groupId ? groupId : jwt.user.mainGroup ? jwt.user.mainGroup : undefined
  	console.log("reqGroupId", reqGroupId)
  	if(reqGroupId){
  		readGroup({groupId: reqGroupId}, {t: jwt.token})
  			.then((result) => {
	      		if(result.error){
	       		 	this.setState({redirectToSignin: true})
	      		}else{
	       			console.log("Group returned", result)
	        		this.setState({group: result})
	      		}
	    	})
  	}else{
  		//get a list of players and put them in an adhoc group
  		//todo - get all players from database
  		//let players = []
  		//let reqGroup = {name:'All Players', players:[]}
  		//console.log("adhoc group created", reqGroup)
	    //this.setState({group: reqGroup})
	}
  }
  componentWillReceiveProps = (props) => {
  	//user may click add data from within a specific group, and this could be passed as props
    if(props.group)
      this.setState({group:props.group})
    else
      this.init()
  }
  componentDidMount = () => {
    if(this.props.group)
      this.setState({group:this.props.group})
    else
      this.init()

  	console.log("CDM group:", this.state.group)
  }
  handleChange = name => event => {
  	//if name = group, wipe existing players
  	if(Object.keys(this.state.data).includes(name)){
      //its a data entry
      let updatedData = {...this.state.data, [name]:event.target.value}
      console.log("updatedData", updatedData)
      this.setState({data:updatedData})
    }else
      this.setState({[name]: event.target.value})
  }

  clickSubmit = () => {
    const jwt = auth.isAuthenticated()
    console.log("user", jwt.user)
    if(!jwt.user)
      alert("You must be signed in to add new data")
    else if(!this.state.player)
      alert("You must select a player")
    else if(!this.state.cat)
      alert("You must select a category")
    else if(!this.state.test)
      alert("You must select a test")
    else if(!this.state.data)
      alert("You must enter the data for the test")
    else{
      /*must collect teh relevant test data here, in a testData object rwther than just score
      for now it wil only have 1 property but this could easily be More
      so in coaches object, it should contaun a list of his tests, and each test
    contains teh relevant criterie from teh schema*/

      const dataWrapper = {
      	date: this.state.date || undefined, //defaults to Now
        playerId: this.state.player._id || undefined,
        cat: this.state.cat.name || undefined,
        test: this.state.test,
        data:this.state.data
      }
      create(dataWrapper, {t: jwt.token}).then((result) => {
        if(!result)
          console.log("no result from server")
        else if (result.error) {
          alert(result.error)
          this.setState({error: result.error})
        } else {
          console.log("Data created!", result)
          this.setState({error: '', open: true, score:''})
        }
      })
    }
  }
  //if item == undefined or null, this will reset all to false
  setSelecting(item){
  	//edge cases
  	if(item == 'test' && !this.state.cat){
  		alert("select a category first")
  		this.setState({selecting:'cat'})
  	}else{
  		this.setState({selecting: item})
  	}
  }
  onSelect(type, selection){
  	switch(type){
  		case 'group':{
  			this.setState({group:selection, selecting:''})
  			break
  		}
  		case 'player':{
  			this.setState({player:selection, selecting:''})
  			break
  		}
  		case 'cat':{
  			this.setState({cat:selection, selecting:'', test:''})
  			break
  		}
  		case 'test':{
        //get score criteria
        let _data = createScoreDataObject(selection)
        console.log("data", _data)
        console.log("keys", Object.keys(_data))

  			this.setState({test:selection, selecting:'', data:_data})
  			break
  		}
  		case 'date':{
        //todo .................................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //new Date(this.state.user.created)).toDateString()
  			this.setState({date:selection, selecting:''})
  			break
  		}
  	}
  }
  render() {
    const {classes} = this.props
    return (<div>
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2" className={classes.title}>
            Add Data
          </Typography>

          <Paper className={classes.root} elevation={4}>
          	<div onClick={() => this.setSelecting('group')}>
	          	<h5>Group: <span style={{fontSize:'1rem'}}>{this.state.group ? this.state.group.name : 'none selected'}</span></h5>
		        <Typography type="title" className={classes.title}>
		          Edit
		        </Typography>
		    </div>
	        {this.state.selecting == 'group' &&
		        <List dense>
		         {this.state.groups.map((group, i) => {
		          return <div onClick={() => this.onSelect('group', group)} key={i}>
		                    <ListItem button>
		                      <ListItemAvatar>
		                        <Avatar>
		                          <Person/>
		                        </Avatar>
		                      </ListItemAvatar>
		                      <ListItemText primary={group.name}/>
		                      <ListItemSecondaryAction>
		                      <IconButton>
		                          <ArrowForward/>
		                      </IconButton>
		                      </ListItemSecondaryAction>
		                    </ListItem>
		                 </div>
		               })
		          }
		        </List>
		    }
	      </Paper>
	      <Paper className={classes.root} elevation={4}>
          	<div onClick={() => this.setSelecting('player')}>
	          	<h5>Player: <span style={{fontSize:'1rem'}}>{this.state.player ? this.state.player.firstName + ' ' +this.state.player.surname 
	          										 : 'none selected'}</span></h5>
		        <Typography type="title" className={classes.title}>
		          Edit
		        </Typography>
		    </div>
	        {this.state.selecting == 'player' &&
		        <List dense>
		         {this.state.group.players.map((player, i) => {
		          return <div onClick={() => this.onSelect('player', player)} key={i}>
		                    <ListItem button>
		                      <ListItemAvatar>
		                        <Avatar>
		                          <Person/>
		                        </Avatar>
		                      </ListItemAvatar>
		                      <ListItemText primary={player.firstName + ' '+player.surname}/>
		                      <ListItemSecondaryAction>
		                      <IconButton>
		                          <ArrowForward/>
		                      </IconButton>
		                      </ListItemSecondaryAction>
		                    </ListItem>
		                 </div>
		               })
		          }
		        </List>
		    }
	      </Paper>

	       <Paper className={classes.root} elevation={4}>
          	<div onClick={() => this.setSelecting('date')}>
	          	<h5>Date: <span style={{fontSize:'1rem'}}>{this.state.date ? this.state.date : 'Today\'s date'}</span></h5>
		        <Typography type="title" className={classes.title}>
		          Edit
		        </Typography>
		    </div>
	        {this.state.selecting == 'date' &&
		        <List dense>
		         date selector
		        </List>
		    }
	      </Paper>

	      <Paper className={classes.root} elevation={4}>
          	<div onClick={() => this.setSelecting('cat')}>
	          	<h5>Category: <span style={{fontSize:'1rem'}}>{this.state.cat ? getLabel(this.state.cat.name) : 'none selected'}</span></h5>
		        <Typography type="title" className={classes.title}>
		          Edit
		        </Typography>
		        </div>
	        {this.state.selecting == 'cat' &&
		        <List dense>
		         {this.state.cats.map((cat, i) => {
		          return <div onClick={() => this.onSelect('cat', cat)} key={i}>
		                    <ListItem button>
		                      <ListItemAvatar>
		                        <Avatar>
		                          <Person/>
		                        </Avatar>
		                      </ListItemAvatar>
		                      <ListItemText primary={getLabel(cat.name)}/>
		                      <ListItemSecondaryAction>
		                      <IconButton>
		                          <ArrowForward/>
		                      </IconButton>
		                      </ListItemSecondaryAction>
		                    </ListItem>
		                 </div>
		               })
		          }
		        </List>
		    }
	      </Paper>

	      <Paper className={classes.root} elevation={4}>
          	<div onClick={() => this.setSelecting('test')}>
	          	<h5>Test: <span style={{fontSize:'1rem'}}>{this.state.test ? getLabel(this.state.test) : 'none selected'}</span></h5>
		        <Typography type="title" className={classes.title}>
		          Edit
		        </Typography>
		    </div>
	        {this.state.selecting == 'test' &&
		        <List dense>
		         {this.state.cat.tests.map((test, i) => {
		          return <div onClick={() => this.onSelect('test', test)} key={i}>
		                    <ListItem button>
		                      <ListItemAvatar>
		                        <Avatar>
		                          <Person/>
		                        </Avatar>
		                      </ListItemAvatar>
		                      <ListItemText primary={test}/>
		                      <ListItemSecondaryAction>
		                      <IconButton>
		                          <ArrowForward/>
		                      </IconButton>
		                      </ListItemSecondaryAction>
		                    </ListItem>
		                 </div>
		               })}
		        </List>
		    }
	      </Paper>

        {this.state.test &&
          <div>
             {Object.keys(this.state.data).map((dataKey, i) => {
              return <div key={'data'+i}>
                        <TextField key={'data'+i} id={dataKey} label={dataKey} className={classes.textField} 
                          value={this.state.data[dataKey]} 
                          onChange={this.handleChange(dataKey)} margin="normal"/>
                     </div>
              })}
          </div>
           
	      }

        </CardContent>
        <CardActions>
          <Button color="primary" variant="raised" onClick={this.clickSubmit} className={classes.submit}>Save</Button>
        </CardActions>
      </Card>
      <Dialog open={this.state.open} disableBackdropClick={true}>
        <DialogTitle>New Data</DialogTitle>
        <DialogContent>
          <DialogContentText>
            New Data Entry successfully created.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => {this.setState({open:false})}} color="primary" autoFocus="autoFocus" variant="raised">
            Add More Data
          </Button>
          <Link to="/">
            <Button color="primary" autoFocus="autoFocus" variant="raised">
              Return Home
            </Button>
          </Link>
        </DialogActions>
      </Dialog>

      {/**<div className='create-player-button' 
        onClick={this.handleCreatePlayer} >create player</div>
      <div className='create-player-button' 
        onClick={this.handleListPlayers} >list players</div>**/}
    </div>)
  }
}

AddTestData.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(AddTestData)
