import React, {Component} from 'react'
import { Route }from 'react-router-dom'
import PropTypes from 'prop-types'

import AddTestData from './AddTestData'

class TestData extends Component {
  render() {
    return (
      <section className='data'>
      		<Route path="/dashboard/data/new" component={AddTestData}/> 
      </section>
    )
  }
}

TestData.propTypes = {
}

export default TestData
