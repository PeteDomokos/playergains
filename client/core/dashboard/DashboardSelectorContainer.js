import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { } from '../../actions/Dashboard'

import DashboardSelector from './DashboardSelector'


const mapStateToProps = state => {
	return({
		dashboard:state.dashboard,
	})
}
const mapDispatchToProps = dispatch => ({
})

//wrap all 4 sections in the same container for now.
const DashboardSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DashboardSelector)

export default DashboardSelectorContainer

