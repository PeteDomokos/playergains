import React, { useEffect }  from 'react'
import GroupsDashboard from './GroupsDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const GroupsDashboardLoader = 
	({dashboard, loading, setFilter, loadDatapoints}) => {
	console.log("DashboardLoader")
	useEffect(() => {
		//todo - check groups loaded, check players loaded, adn check datapoints loaded for datasets
		//in each group (atm datpoints are laoded when groups are loaded but this 
		//should be broken up)

	}, [])
	return (
		<GroupsDashboard dashboard={dashboard} setFilter={setFilter} loading={loading} />)
}

export default GroupsDashboardLoader

/*
	(
		<GroupsDashboard 
			user={user} group={group} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)

			*/