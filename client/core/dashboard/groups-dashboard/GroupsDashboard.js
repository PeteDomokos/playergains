import React, { useEffect }  from 'react'
import { merge } from '../../datasets/DatasetHelpers'
import { isACommonParentOf, isAParentOf, flattenedDatasets } 
	from '../../../groups/GroupHelpers'
/**
*
**/
const GroupsDashboard = ({dashboard, loading, setFilter}) => {
	console.log("GroupsDashboard dashboard", dashboard)
	//const parentDatasets = flattenedDatasets(groups.parents)

	/*
	Get one array of all datasets from all groups and all parents
	Remember each dataset has a groupId attached if required later
	Then we a) remove all custom sets
	        b) merge all standard sets with same signature
	        c) get a list of common parents, and get their custom sets
	*/
	/*const allParentDatasets = groups.parents
		.map(p => p.datasets)
		.reduce((sets1, sets2) => [...sets1, ...sets2], [])

	const allGroupsDatasets = groups.selected
		.map(g => g.datasets)
		.reduce((sets1,sets2) => [...sets1, ...sets2], [])

	const standardDatasets = [...allParentDatasets, ...allGroupsDatasets]
		.filter(d => d.isStandard)

	const mergedStandardDatasets = merge(standardDatasets)

	//C) retain parents custom sets (including grandparents etc)
	const commonParents = groups.parents
		.filter(p => isACommonParentOf(p, groups.selected, groups.parents))
	const customDatasetsFromCommonParents = commonParents
		.map(p => p.datasets)
		.reduce((sets1, sets2) => [...sets1, ...sets2], [])
		.filter(d => !d.isStandard)
	//put together and filter to ones selected
	const datasets = 
		[...mergedStandardDatasets, customDatasetsFromCommonParents]
			.filter(dataset => dashboard.datasets
				.find(d => d._id === dataset._id))
	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature*/

	return (
		<div style={{margin:50, backgrouncolor:'white'}}>
		Groups Dashboard
		</div>)
}

export default GroupsDashboard