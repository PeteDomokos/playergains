import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import GroupsDashboardLoader from './GroupsDashboardLoader'
import { getParents } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("GroupsDashboardContainer state", state)
	//groups will be set, even if players are selected
	const loadedDashboardGroups = state.dashboard.groups.map(arr =>
		arr.map(group => state.storedItems.groups.find(g => g._id === group._id)))
	const loadedDashboard = {...state.dashboard, groups:loadedDashboardGroups}
	return({
		dashboard: loadedDashboard,
		loading:state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	//options can be playerIds to filter what we request, and groupIds 
	//so we filter to only players in that group, as it may be a dataset from a parent
	loadDatapoints(datasets, options){
		dispatch(fetchDatapoints(datasets, options))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const GroupsDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupsDashboardLoader)

export default GroupsDashboardContainer