import React, { useEffect }  from 'react'
import PlayerDashboard from './PlayerDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const PlayerDashboardLoader = ({player, groups, dashboard, loading, setFilter, LoadDatapoints}) => {
	console.log("PlayerDashboardLoader")
	useEffect(() => {
		if(!loading){
			const datasets = flattenedDatasets(groups).filter(dataset => 
				dashboard.datasets.find(d => d._id === dataset._id))
			
			const unloadedDatasets = datasets.filter(d => !d.datapoints)
			if(unloadedDatasets.length > 0){
				console.log("Loader loading datapoints...")
				loadDatapoints(unloadedDatasets)
			}
		}
	}, [])
	return (
		<PlayerDashboard 
			player={player} groups={groups} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)
}

export default PlayerDashboardLoader