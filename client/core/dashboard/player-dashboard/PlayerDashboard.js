import React, { useEffect }  from 'react'
import { mergedDatasets } from '../../datasets/DatasetHelpers'
/**
*
**/
const PlayerDashboard = ({player, groups, dashboard, loading, setFilter}) => {
	console.log("PlayerDashboard groups", groups)
	//filter the datasets selected
	const allDatasets = [...groups.map(g => g.datasets)]
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	console.log("allDatasets", allDatasets)
	//merge standard sets
	//todo - check if we still need this now, and if so, we shouldnt need to separate custom out 
	const mergedStandard = mergedDatasets(allDatasets.filter(d => d.isStandard))
	const custom = allDatasets.filter(d => !d.isStandard)
	//put together and filter to ones selected
	const datasets = [...mergedStandard, ...custom]
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature

	return (
		<div style={{margin:30, backgrouncolor:'white'}}>
		Player Dashboard
		</div>)
}

export default PlayerDashboard