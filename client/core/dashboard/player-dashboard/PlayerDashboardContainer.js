import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import PlayerDashboardLoader from './PlayerDashboardLoader'
import { getParents, getPlayerGroups } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("PlayerDashboardContainer state", state)
	//params may be undefined
	const { playerId } = ownProps.match.params
	const player = state.storedItems.users.find(user => user._id === playerId)
	//all groups are already loaded from DatasetsLoader
	const playerGroups = getPlayerGroups(player, state.storedItems.groups) 
	//no need for parents, because if player plays for a subgroup, then
	//they must also play for all of its parents
	return({
		player: player,
		groups:playerGroups,
		dashboard: state.dashboard,
		loading: state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	loadGroup(groupId){
		dispatch(fetchGroup(groupId))
	},
	loadDatapoints(groupId, datasetId){
		dispatch(fetchDatapoints(groupId, datasetId))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const PlayerDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(PlayerDashboardLoader)

export default PlayerDashboardContainer