import React from 'react'
import {NavLink, Link, withRouter} from 'react-router-dom'

  /**
  * Desc ...
  **/
let activeSt = {color: '#ed4401', textDecoration:'none'}
  /**
  * Desc ...
  **/
let linkSt = {color: 'white', textDecoration:'none'}

/**
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
**/
export const PlayerDashboardMenu = withRouter(({onHide, history}) => (
  <ul className={'sub-menu dashboard-menu'/*+(show ? 'full-height':'zero-height')*/}>
    <li className='sub-menu-item'>
      <NavLink to="/dashboard/progress" activeStyle={activeSt} style={linkSt}>
        progress
      </NavLink>
    </li>
    <li className='sub-menu-item'>
      <NavLink to="/dashboard/compare" activeStyle={activeSt} style={linkSt}>
        compare
      </NavLink>
    </li>
    {/**<li className='sub-menu-item'>
      <NavLink to="/dashboard/tests" activeStyle={activeSt} style={linkSt}>
        see tests
      </NavLink>
    </li>**/}   
  </ul>
))

