import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { HashRouter, Route, Switch, Redirect, withRouter } from 'react-router-dom'
import { Links } from '../../util/LinksAndRoutes'
import { TestDataConstants } from '../../constants/TestDataConstants'
import { DashboardMenu } from './DashboardMenus'
import ProgressSVG from './charts/progress/ProgressSVG'
import CompareSVG from './charts/compare/CompareSVG'
import PlayerTests from './player-tests/PlayerTests'
import TestData from './test-data/TestData'
import { listPlayers } from '../../players/api-player'
import auth from '../../auth/auth-helper'
import {read} from '../../user/api-user.js'

/**
* Handles retrieval of players data list from server 
* Passes playersdata onto the required chart components 
**/
/*
TODO - put this into container and handle server data via store)
TODO - for large screen sizes, render more than one chart at a time ie a proper data dashboard
*/
export class UnwrappedDashboard extends Component{
  	constructor(props){
		super(props)
		this.state ={
			players:[],
			user:'',
			redirectToSignin: false
		}
	}
	/**
    * Retrieves the players from server, and sets in state  
    **/
	init = () => {
		console.log("dashboard init()...")
	   //check session storage for authenticated user
	   const jwt = auth.isAuthenticated()
	   if(!jwt || !jwt.user){
	   	this.setState({redirectToSignin: true})
	   }
	   else if (this.props.history.location.pathname == '/dashboard')
	   		this.setState({redirectToProgress: true})
	   	else{
	        listPlayers().then(data =>{
	        	if(data.error || !Array.isArray(data)) {
	        		console.log("dashboard init error:", data.error)
			      } else {
			      	console.log("dashboard init players", this.state.players)
			        this.setState({players: data})
			      }
	        })
		}
  	}
	componentDidMount(){
		this.init()
	}
	/**
    * Returns either a redirect to signin page, a redirect to itself with amended path, 
    * or a submenu component such as a chart
    **/
	render(){
		console.log("dashboard render()..players", this.state.players)
    	if (this.state.redirectToSignin)
     		return <Redirect to='/signin'/>
     	if (this.props.history.location.pathname == '/dashboard')
     		return <Redirect to='/dashboard/progress'/>
		let { user } = this.props 
		let _item = !this.state.players ? {} :
			{
				id:'dashboard-item', 
				players:this.state.players,
				...TestDataConstants
			}
		return(
			//todo - 1.make svgwrapper load at right height even while it waits fro players,
			//so that footer isnt shunted up the screen
			<div className='dashboard'>
				{this.state.players.length > 0 ?
					<div>
						<Switch>
							<Route path="/dashboard/data" component={TestData}/> 
							<Route path="/dashboard/compare" render={(props) => 
								<CompareSVG {...props} item={_item} />} />
							<Route path="/dashboard/progress" render={(props) => 
								<ProgressSVG  {...props} item={_item} />} />
							<Route path="/dashboard/player-tests" render={(props) => 
								<PlayerTests {...props} item={_item} />} />
							<Route render={(props) => 
								<ProgressSVG  {...props} item={_item} />} />
						</Switch>
					</div>
					:
					<div style={{color:'white', margin:30}}>
						loading players....
					</div>
				}
			</div>
		)
	}
}
UnwrappedDashboard.defaultProps = {
}
export const Dashboard = withRouter(UnwrappedDashboard)

//export default withRouter(Dashboard)
