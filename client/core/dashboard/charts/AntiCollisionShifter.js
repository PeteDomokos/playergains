import { sameDay } from './TimeHelpers'
//todo - think about class syntax instead
function AntiCollisionShifter(){
}

/**
*  defauklt shift is 5, ie assumes radius of circle / half rect width is 5
**/

//todo - impl option to start with existing chart values in the cache
AntiCollisionShifter.prototype.createShifts = function(datapoints, scales, initCache = []){
	const { x, y } = scales
	//helpers
	//note - need to add scales to this check too, when we will check if its
	//todo - check within a certain range, rather than exact equality, also days or weeks etc

	const checkSame = (p1, p2) => {
		//console.log('checkSame p1', p1)
		//console.log('checkSame p2', p2)
		//console.log('check?', p1.chartX == p2.chartX && p1.chartY == p2.chartY)
		return p1.chartX == p2.chartX && p1.chartY == p2.chartY
	}

	const checkPoints = (d, remainingPoints, cache) =>{

		const match = cache.find(datapoint => checkSame(d, datapoint))
		//find the nearest point with no match (right shifts allowed only atm)
		if(match){
			const shift = match.isLastActual ? 15 : 5
			//create new and increase shift value
			const shiftedPoint = {
				...d, 
				chartX:d.chartX+5, 
				chartY:d.chartY, 
				shift:d.shift ? d.shift+5 : 5
			}
			//check this shiftedPoint
			return checkPoints(shiftedPoint, remainingPoints, cache)
		}else{
			//point is fine - check if it was the last point
			if(remainingPoints.length > 0)
				return checkPoints(remainingPoints[0], remainingPoints.slice(1), [...cache, d])
			else
				return [...cache, d]
		}
	}

	if(!datapoints[0])
		return []
	else{
		const chartDatapoints = datapoints.map(d =>({
			...d, chartX:x(d.xValue), chartY:y(d.yValue)
		}))
		//apply shifts
		return checkPoints(chartDatapoints[0], chartDatapoints.slice(1), initCache)
	}
} 

export default AntiCollisionShifter

/*
AntiCollisionShifter.prototype.createShifts = function(datapoints, otherPlayersDatapoints){
	//note - inreality, need to test for same week if scale is going up in weeks?
	//or maybe same three days? depends on scale
	//assume one player per datapoint set for now
	const playerId = datapoints[0].players[0]
	//collision avoidance

	const datapointsWithShifts = datapoints.map(datapoint =>{
		//console.log('datapoint', datapoint)
		const nrPlayersWithSameValue = otherPlayersDatapoints
			.map(playerDs => playerDs.find(d => sameDay(d.xValue, datapoint.xValue)))
			.filter(d => d && d.yValue === datapoint.yValue)
			.length
		//move circle by 1 diameter up for each identical value
		const shiftDown = 5 * nrPlayersWithSameValue
		return {...datapoint, shiftDown: shiftDown}
	})
	return datapointsWithShifts
} 

export default AntiCollisionShifter

*/
