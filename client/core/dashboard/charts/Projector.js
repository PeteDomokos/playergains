import regression from 'regression'
import { addWeeks } from './TimeHelpers'

//todo - use class syntax instead
//todo - think about fact that datapoints are unlikely to be equally spaced out 
		//- so i need a way of adjusting them for the purposes of creating projections */
//todo - projector settings, takes 2 dates - from (ie from what point to take actual data, and to - also a date) These can both be entered in by user, in chart settings (under options)
function Projector(nrValues){
	this.nrValues = nrValues || 52
}
//create to prototype so it can create multiple charts without creating copies

//bounds is a Array[2] with lower and upper numbers, in either order


//todo - to make regression work with neg values: if any actual values are negative, 
//find the smallest neg number, add its abs value
//onto every point, then calc the regression, then subtract it back off
Projector.prototype.create = function(actualDatapoints, bounds){
	//sort by xvalue
	const sortedData = actualDatapoints.sort((d1,d2) => (d1.xValue - d2.xValue))

	let lowestPossY, highestPossY
	if(bounds && bounds.length == 2){
		lowestPossY = Math.min(...bounds)
		highestPossY = Math.max(...bounds)
	}else{
		const minY = Math.min(...actualDatapoints.map(d =>d .yValue))
		const maxY = Math.max(...actualDatapoints.map(d =>d .yValue))
		lowestPossY = minY - 0.5 * Math.abs(minY)
		highestPossY = maxY +0.5 * Math.abs(maxY)
	}
	if(actualDatapoints.length < 2)
		return []
	else{
		//get up to the last 4 entries (ie assume weeks for now)
		const latestEntries = actualDatapoints.slice(-4)
		const lastEntry = latestEntries[latestEntries.length -1]
		//format for regression func (assume weeks)
		const datapairs = latestEntries.map((d,i) => {
			return [i, d.yValue]
		})
		//adjust first value if 0 as it doesnt work
		const ds = datapairs.map(d =>{
			if(d[1] == 0)
				return [d[0], 0.001]
			else
				return d
		})
		//notes - for now, max is 100 but this will depend on zone thresholds
		const regressionFunc = regression.exponential(ds);
		const projectionArray = Array.from(new Array(this.nrValues), (val, n) => n+1)
		//cushions avoid projections touch the edge of chart
		const lowestWithCushion = lowestPossY + 0.01 * Math.abs(lowestPossY)
		const highestWithCushion = highestPossY - 0.01 * Math.abs(highestPossY)
		return projectionArray
			.map(n => regressionFunc.predict(n))
			.map(pair => {
				if(pair[1] > highestPossY){

					return [pair[0], highestWithCushion]
				}
				else if(pair[1] < lowestPossY)
					return [pair[0], lowestWithCushion]
				return pair
			})
			.map((pair,i) => {
				return {
					xValue:addWeeks(pair[0], lastEntry.xValue),
					yValue:pair[1],
					key:actualDatapoints[0].key,
					projected:true,
					players: lastEntry.players || undefined,
					//use lastEntry to ensure id doesnt clash with other dataset
					_id:'proj-'+i +'-'+lastEntry._id
				}
			})
	}
}

export default Projector