export const activeGroupsChanged = (optionGroups, prevOptionGroups) =>{
	const prevActiveGrps = prevOptionGroups
		.filter(grp => grp.active)
		.map(grp => grp.key)

	const newActiveGrps = optionGroups
		.filter(grp => grp.active)
		.map(grp => grp.key)

	const nrGrpsActivated = newActiveGrps
		.filter(gKey => !prevActiveGrps.includes(gKey)).length
	const nrGrpsDeactivated = prevActiveGrps
		.filter(gKey => !newActiveGrps.includes(gKey)).length
	
	return nrGrpsActivated != 0 || nrGrpsDeactivated != 0
	
}
export const findChanged = (optionGroups, prevOptionGroups) =>{
	//the order is import for the ifs
	if(chartChanged(optionGroups, prevOptionGroups))
		return 'chart'
	if(datasetsChanged(optionGroups, prevOptionGroups))
		return 'datasets'
	if(playersChanged(optionGroups, prevOptionGroups))
		return 'players'
	if(measureChanged(optionGroups, prevOptionGroups))
		return 'measure'
	return ''
}
export const chartChanged = (optionGroups, prevOptionGroups) =>{
	if(optionGroups.length == 0 || prevOptionGroups.length == 0)
		return false
	return findSelected('chart', prevOptionGroups).key !==
	       findSelected('chart', optionGroups).key
}

export const datasetsChanged = (optionGroups, prevOptionGroups) =>{
	if(optionGroups.length == 0 || prevOptionGroups.length == 0)
		return false

	const prevDatasetKeys = findSelected('datasets', prevOptionGroups)
		.map(dataset => dataset.key)
	const newDatasetKeys = findSelected('datasets', optionGroups)
		.map(dataset => dataset.key)

	if(prevDatasetKeys.length != newDatasetKeys.length)
		return true
	var changed = false
	prevDatasetKeys.forEach((datasetKey,i) =>{
		if(datasetKey !== newDatasetKeys[i]){
			changed = true
		}
	})
	return changed
}
/**
* Desc ...
* @constructor
* @param {string} 
**/

//todo - handle case of no init, then default
export const initialiseOptions = (group, charts, measures, init) => {
	console.log('group', group)
	console.log('init', init)
	//charts
	const initialisedCharts = charts.map(chart =>{
		if(init.chart.key === chart.key)
			return { ...chart, selected:true }
		else
			return chart
	})
	//players
	const initialisedPlayers = group.players.map(player =>{
		if(init.players && init.players.find(p => p._id === player._id))
			return {...player, selected:true, key:player._id, name:player.firstName}
		else
			return {...player, selected:false, key:player._id, name:player.firstName}
	})
	//datasets
	const selectedDatasetIds = init.datasets ? 
		init.datasets.map(d => d._id) : group.datasets.slice(0,1).map(d => d._id)
	const initialisedDatasets = group.datasets.map(dset =>{
		//check if a value is main Value (if not, use the first one)
		const noMainValue = dset.dataValueFormats.filter(value => value.isMain).length == 0
		//each dataset has a selected valueFormat, even if dataset is not selected 
		const initialisedValueFormats = dset.dataValueFormats.map((value, i) => {
			if(value.isMain || (noMainValue && i == 0)){
				return {...value, selected:true}
			}else{
				return {...value, selected:false}
			}
		})
		//temp mock names fro mock data
		console.log('dset name', dset.name)
		let name
		if(dset.name == '5-metre Acceleration'){
			name = 'Test 5M Acceleration'
		}else if(dset.name == 'Best Acceleration'){
			name = 'Match Top Acceleration'
		}else{
			name = dset.name
		}
		return {
			...dset,
			name:name,
			selected:selectedDatasetIds.includes(dset._id), 
			dataValueFormats:initialisedValueFormats
		}
	})
	//measures
	const initialisedMeasures = measures.map((measure,i) =>{
		if(init.measure && init.measure.key === measure.key)
			return {...measure, selected:true}
		if(!init.measure && i == 0)
			return {...measure, selected:true}
		else
			return {...measure, selected:false}
	})
	//dates 
	const startDate = init.dates && init.dates.start ? init.dates.start : undefined
	const endDate = init.dates && init.dates.end ? init.dates.end : undefined
	const initialisedDates = [
		{name:'Start', key:'start', value:startDate},
		{name:'End', key:'end', value:endDate}
		]


	//note - rethink - charts are not really needed as a group except for buttons
	return [
		{name:'Charts', key:'charts', options:initialisedCharts},
		{name:'Datasets', key:'datasets', options:initialisedDatasets},
		{name:'Players', key:'players', options:initialisedPlayers},
		{name:'Measures', key:'measures', options:initialisedMeasures},
		/*{name:'Dates', key:'dates', options:initialisedDates}*/
	]		
}
export const measureChanged = (optionGroups, prevOptionGroups) =>{
	if(optionGroups.length == 0 || prevOptionGroups.length == 0)
		return false
	const prevMeasure = findSelected('measure', prevOptionGroups)
	const newMeasure = findSelected('measure', optionGroups)
	return prevMeasure !== newMeasure
}
export const playersChanged = (optionGroups, prevOptionGroups) =>{
	if(optionGroups.length == 0 || prevOptionGroups.length == 0)
		return false
	const prevPlayers = findSelected('players', prevOptionGroups)
	const newPlayers = findSelected('players', optionGroups)
	const hasAddedPlayers = newPlayers.filter(player => 
		!prevPlayers.find(p => p._id == player._id)).length != 0

	const hasRemovedPlayers = prevPlayers.filter(player =>
		!newPlayers.find(p => p._id == player._id)).length != 0

	return hasAddedPlayers || hasRemovedPlayers
}
export const findSelected = (property, optionGroups) =>{
	//console.log('findselecetd... property', property)
	//console.log('optionGroups', optionGroups)
	if(!optionGroups || !optionGroups[0]){
		if(['datasets', 'players'].includes(property))
			return []
		else
			return {}
	}
	switch(property){
		case 'chart':
			return optionGroups
				.find(grp => grp.key == 'charts').options
				.find(chart => chart.selected)
		case 'datasets':
			return optionGroups
				.find(grp => grp.key == 'datasets').options
				.filter(dset => dset.selected)
		case 'measure':
			return optionGroups
				.find(grp => grp.key == 'measures').options
				.find(measure => measure.selected)
		case 'valueFormat':
			return optionGroups
				.find(grp => grp.key == 'value-formats').options
				.find(v => v.selected)
		case 'players':
			return optionGroups
				.find(grp => grp.key == 'players').options
				.filter(p => p.selected)
	}

}

export const sizesChanged = (sizes, prevSizes) =>{
	if(!sizes || !prevSizes)
		return false
	return sizes.width != prevSizes.width || sizes.height != prevSizes.height
}

