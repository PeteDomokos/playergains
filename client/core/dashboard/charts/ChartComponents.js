import * as d3 from 'd3'
import * as fc from 'd3fc'
import colorbrewer from 'colorbrewer'

const renderButton = (selection, handleClick, config) =>{
	const { className, size } = config
	const buttonG = selection.append('g')
		.attr('class', ('buttonG '+className ? className : ''))

	const width = size == 'small' ? 40 : size == 'large' ? 60 : 50
	const height = size == 'small' ? 17 : size == 'large' ? 25 : 20
	const textTransform = size == 'large' ? 4 : 3

	buttonG.append('rect')
		//make rect aligned centrally too 
		.attr('transform', 'translate(-'+(width * 0.5) +', 0)')
		.attr('width', width)
			.attr('height', height)
			.attr('rx', 2)
			.attr('ry', 2)
		.style('cursor', 'pointer')
		.on("click", button =>{
			handleClick()
		})

	buttonG.append('text')
		.attr('transform', 'translate(0 ,' +textTransform +')')
		.style('text-anchor','middle')
		.style('alignment-baseline', 'hanging')
		.style('cursor', 'pointer')
		.on("click", button =>{
			handleClick()
		})

}

export { renderButton }


	