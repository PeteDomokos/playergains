import * as d3 from 'd3'
import * as fc from 'd3fc'
import colorbrewer from 'colorbrewer'
import Projector from '../Projector'
import AntiCollisionShifter from '../AntiCollisionShifter'
import { datasetZones } from '../../../../constants/DatasetConstants'
import { findSelected } from '../OptionGroupHelpers'

/**
* Calls method to calculate and format data for the chart, 
* and then renders the components of the chart (via several subrender methods)
*/

//helper
//todo - replace myScatter with prevSizes
const sizesChanged = (sizes, myScatter) =>{
	//if chart not yet rendered, return false
	if(!myScatter.rendered())
		return false
	return sizes.width !== myScatter.sizes().width || sizes.height !== myScatter.sizes().height
}

//each d gets a key = dataset.key fro grouping purposes eg colouring
const formatPlayerDatapoints = (player, datasets) =>{
	return datasets
		.map(dset => {
			const mainValueKey = dset.dataValueFormats.find(dvf => dvf.isMain).key
			return dset.datapoints
				.filter(d => d.players[0] === player._id)
				.map(d =>({
					...d,
					key:dset.key,
					value:d.value || d.dataValues.find(dv => dv.key === mainValueKey).value
				}))
		})
		.reduce((a,b) => [...a, ...b], [])
}

const updateCharts = (svgs, myScatter, optionGroups, fillScales, sizes, changed) =>{
	//helper
	const selected = property => findSelected(property, optionGroups)
	const options = grpKey => optionGroups.find(g => g.key === grpKey).options
	const chart = selected('chart').key
	const players = selected('players').map(p =>({...p, key:p._id}))

	const keyItemsChanged = chart.includes('players') && changed == 'players' ||
			!chart.includes('players') && changed == 'datasets'
	const dataItemsChanged = (changed === 'datasets' && chart.includes('players')) ||
		(changed === 'players' && chart.includes('datasets'))

	const handleDatapointClick = (d, currentKeyItems) =>{
		if(currentKeyItems.length == 1)
			return
		const newKeyItems = currentKeyItems.map(it => {
			if(it.key === d.key)
				return { ...it, inFocus:!it.inFocus }
			return it
		})
		//reset if all players in focus
		const nrKeysInFocusNow = newKeyItems.filter(it => it.inFocus).length
		const updatedKeyItems = nrKeysInFocusNow != newKeyItems.length ? newKeyItems :
			newKeyItems.map(it => ({...it, inFocus:false}))

		myScatter.keyItems(updatedKeyItems)
	}

	const callChart = config =>{
		//prepare data
		var dataItems
		if(chart.includes('players'))
			dataItems = selected('datasets').map(dset =>({
				...dset,
				datapoints:dset.datapoints.map(d => ({...d, key:d.players[0]}))
			}))
		else
			dataItems = players.map(p =>({
				...p,
				key:p._id, 
				datapoints:formatPlayerDatapoints(p, options('datasets'))
			}))
		//render
		d3.selectAll('svg.svg').datum(function(){
			return dataItems.find(item => this.getAttribute('class').includes(item.key))
		}).call(myScatter, config)
	}

	const keyItems = chart.includes('players') ? players : selected('datasets')
	const fillScale = chart.includes('players') ? fillScales.players : fillScales.datasets

	if(!myScatter.rendered() || changed === 'chart'){
		console.log('RENDERING SCATTER...')
		//create chart
		myScatter
			.sizes(sizes)
			.keyItems(keyItems, fillScale)
			.measure(selected('measure'))
			.zones(datasetZones) //opt 2nd arg fillScale
			//todo - impl true option which uses default projector and anit-col
			.withProjections(chart.includes('scatter') ? new Projector() : false) 
			.withAntiCollision(new AntiCollisionShifter)
			.withTargetPaths(true)
			.on('customClick', handleDatapointClick)
		//wipe previous and render
		d3.selectAll('svg.svg').selectAll('*').remove()
		callChart()
	}
	else if(dataItemsChanged){
		console.log('UPDATING SCATTER DATA ITEMS...')
		//redo sizes incase scroll has appeared or gone
		const config = sizesChanged(sizes, myScatter) ? {sizes:sizes} : {}
		d3.selectAll('svg.svg').selectAll('*').remove()
		callChart(config)
	}
	else if(keyItemsChanged){
		console.log('UPDATING SCATTER KEYITEMS...')
		myScatter.keyItems(keyItems)
	}
	else if(changed === 'measure'){
		console.log('UPDATING SCATTER MEASURE...')
		myScatter.measure(selected('measure'))
	}
	else if(sizesChanged(sizes, myScatter)){
		console.log('UPDATING SCATTER size...')
		myScatter.sizes(sizes)
	}
}
export default updateCharts
	