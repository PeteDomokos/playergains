import * as d3 from 'd3'
import * as fc from 'd3fc'
import { addWeeks } from '../TimeHelpers'
import { onlyUnique } from '../../../../util/helpers/ArrayManipulators'

//todo - make radius a prop of d
const nestStepData = ds =>{
	const start = d3.min(ds, d => d.xValue)
	//todo - redo as recursive function and allow for weeks before start
	//helper
	const weeksAfterStart = (date, start) => {
		var count = 0
		var sameWeek = false
		while(!sameWeek){
			if(addWeeks(count+1, start).getTime() > date.getTime())
				sameWeek = true
			else
				count++
		}
		return count
	}
	//nest data into xValue steps (use weeks)
	const steps = d3.nest()
		.key(d => weeksAfterStart(d.xValue, start))
		.entries(ds)
		.sort((nest1, nest2) => nest1.key - nest2.key)

	//remove duplicate entries per step per player
	const filteredSteps = steps.map((step,i) =>{
		const keyNests = d3.nest().key(d => d.key).entries(step.values)
		const keyBestDs = keyNests.map(keyNest =>{
			//todo - use a best() func which uses d3 min or max depeding on numberOrder
			const max = d3.max(keyNest.values, d => d.yValue)
			return keyNest.values.find(d => d.yValue === max)
		})
		return { ...step, values:keyBestDs}
	})
	//fill out empty entries for existing players
	const fillSteps = steps =>{
		const fillStep = (i, step, prevSteps) =>{
			const prevKeys = prevSteps[i-1].values.map(d => d.key).filter(onlyUnique)
			const newKeys = step.values.map(d => d.key).filter(onlyUnique)
			const keys = [...prevKeys, ...newKeys].filter(onlyUnique)
			const filledValues = keys.map(key => {
				const value = step.values.find(d => d.key === key)
				return value || prevSteps[i-1].values.find(d => d.key === key)
			})
			if(i < steps.length-1){
				const stepsSoFar = [...prevSteps, {...step, values:filledValues}]
				return fillStep(i+1, steps[i+1], stepsSoFar)
			}else
				return [...prevSteps, {...step, values:filledValues}]
		}
		if(steps.length < 2)
			return steps
		//dont attempt to fill first step
		return fillStep(1, steps[1], [steps[0]])
	}

	const filledSteps = fillSteps(filteredSteps)
	return filledSteps
}

const updateAnimateBtn = (selection, sizes, animate) =>{
	selection.select('g.animate')
		.attr('transform', 
			`translate(${sizes.margin.left + 0.98 * sizes.chartWidth - 20} , 
					   ${sizes.margin.top +sizes.chartHeight + 0.1 * sizes.margin.bottom})`)

	selection.select('g.animate').select('text')
		.style("stroke", animate ? 'red' : 'black')
		.style("fill", animate ? 'red' : 'black')
		.style('font-size', 12)
		.style('stroke-width', 0.4)
		.text(animate ? 'End' : 'Play')

	selection.select('g.animate').select('rect')
		.style("fill", animate ? 'white' : 'aqua')

} 

const updateAnimationPauseBtn = (selection, sizes, paused) =>{

	selection.select('g.pause')
		.attr('transform', 
			`translate(${sizes.margin.left + 0.98 * sizes.chartWidth - 20 - 50} , 
					   ${sizes.margin.top +sizes.chartHeight + 0.1 * sizes.margin.bottom})`)

	selection.select('g.pause').select('text')
		.style("stroke", paused ? 'red' : 'black')
		.style("fill", paused ? 'red' : 'black')
		.style('font-size', 12)
		.style('stroke-width', 0.4)
		.text(paused ? 'Resume' : 'Pause')

	selection.select('g.pause').select('rect')
		.style("fill", paused ? 'white' : 'aqua')

}

const updateBackground = (scatterG, sizes) =>{
	//render
	if(scatterG.select('rect.backgroundRect').nodes().length == 0)
		scatterG.append("rect")
			.attr("class", "backgroundRect")
			.style("fill", "aqua")

	//update
	scatterG.select('rect.backgroundRect')
		.transition()
		.duration(500)
		.attr('width', sizes.chartWidth)
		.attr('height', sizes.chartHeight)
		.attr('x', sizes.margin.left)
		.attr('y', sizes.margin.top)
}
/**
*
*
**/
const updateClipPath = (selection, sizes) =>{
	selection.select('clipPath#clip').select('rect')
		.transition()
		.duration(500)
		.attr('width', sizes.chartWidth)
		.attr('height', sizes.chartHeight)
		.attr('x', sizes.margin.left)
		.attr('y', sizes.margin.top)
}
/**
* Applys a shaded rectangle over current week
*/

const updateCurrentWeekOverlay = (scatterG, sizes, x) =>{
	//1.REMOVE
	//scatterG.select('rect.currentWeek').remove()
	//2.APPEND
	const { margin, chartWidth, chartHeight } = sizes
	const bandwidth = 25
	//render if not yet rendered (cannot be done before update due to zones)
	if(scatterG.select('rect.currentWeek').nodes().length == 0)
		scatterG.append("rect")
			.attr("class", "currentWeek")
			.style("fill", "black")
			.style("opacity", 0.15)
	//update
	scatterG.select("rect.currentWeek")
		.attr('class', 'currentWeek')
		.attr("x", (x(new Date()) - bandwidth/2))
		.attr("y", margin.top)
		.attr("width", bandwidth)
		.attr("height", chartHeight)

}
const updateBeeSwarmDs = (beeSwarmG, dataKey, ds, keyItems, r, fillScale, dispatcher, transitions, focusChange) =>{
	//helpers
	const keyItemsInFocus = keyItems.filter(key => key.inFocus)
	const inFocus = d => {
		if(keyItemsInFocus.find(it => it.key == d.key))
			return true
		return false
	}
	//todo - remove key checks eevrywhere - just make the api ensure that all ds always have a key
	let circleFill = d =>{
		if(d.projected)
			return "none"
		if(keyItemsInFocus.length != 0 && !inFocus(d))
			return "grey"
		if(d.isTarget)
			return "black"
		if(fillScale && d.key)
			return fillScale(d.key)
		return 'red'
	}
	let circleStroke = (d, i) => {
		if(fillScale && d.key)
			return fillScale(d.key)
		return 'red'
	}
	//1. BIND
	const updatedChartG = beeSwarmG.selectAll(("g.datapointG-"+dataKey))
		.data(ds, d => d.key)
	//2. EXIT - old elements not present in new data
	updatedChartG.exit()
		.attr('class', 'exit')
		.remove()

	//3.UPDATE - ONLY old elements with NEW data bound to them
	updatedChartG
		.classed('enter', false)

	//4. ENTER - new elements present in new data

	let chartGEnter = updatedChartG.enter()
		.append("g")
			//todo - remove key checks - just make the api ensure that all ds always have a key
		  	.attr("class", d => "enter datapointG datapointG-"+(d.key ? d.key : '') +" datapointG-"+dataKey)

	chartGEnter
		.append("circle")
			//each actual d has a unique id from schema
			.attr("r", r)
		    .style("fill", d => circleFill(d))
		    .style('opacity', d => (keyItemsInFocus.length == 0 || inFocus(d) ? 1 : 0.4))
		    .style("stroke", (d,i) => {
		    	//console.log('entering d...')
		    	return circleStroke(d, i)
		    })
		    .style('stroke-width', 3)
		    .style('opacity', 0)
		  	.transition(d3.transition().duration(250).delay(500))
		  	.style('opacity', 1) 
	
	//photos - use beeSwarmG instead to target only those that are lastActual
	if(keyItems && keyItems[0] && (keyItems[0].photo || keyItems[0].surname)){
		beeSwarmG.selectAll("g.enter")
			.insert("image","text")
				.attr('class', 'lastActual')
	  			.attr("xlink:href", d => {
	  				//console.log('photo for player', d.players[0])
	  				return photoUrl(d)
	  			})
	  			.on('click', function(d){
			  		dispatcher.call('customClick', this, d, keyItems)
			  	})
				.attr("height", "30px")
				.attr("width", "30px")
				.attr("x", -15)
				.attr("y", -15)
				.style('cursor', 'pointer')
				.style('opacity', 0)
				.transition(d3.transition().duration(250).delay(500))
			  	.style('opacity', 1)
	}
	else{
		beeSwarmG.selectAll("g.enter")
			.append('text')
				.attr('transform', `translate(${0} , ${5})`)
				.style('text-anchor','middle')
				//.style('alignment-baseline', 'hanging')
				.style('font-size', 14)
			    .style('stroke-width', 1)
				.style('stroke', 'white')
				.style('fill', 'white')
				.style('cursor', 'pointer')
				.on("click", function(d){
			  		dispatcher.call('customClick', this, d, keyItems)
			  	})
				.text(d => keyItems.find(it => it.key === d.key).name.substring(0,3))
	} 

	function photoUrl(d){
		//console.log('photoUrl called for d....................', d)
		const keyItem = keyItems.find(it => it.key == d.key)
		//console.log('player', player)
		if(keyItem && keyItem.photo)
			return `/api/user/photo/${keyItem.key}?${new Date().getTime()}`
		return '/api/user/defaultphoto'
	}
	//5.MERGE-UPDATE - newly entered elements AND old elements with new data bound to them

}


/**
*
*
**/
const updateDataLines = (scatterG, datapoints, keyItems, scales, fillScale, transitions, focusChange) =>{
	const { x, y } = scales
	const keyItemsInFocus = keyItems.filter(it => it.inFocus)
	const inFocus = d => {
		if(keyItemsInFocus.find(it => it.key == d.key))
			return true
	}

	const strokeColour = d =>{
		if(keyItemsInFocus.length != 0 && !inFocus(d)){
			return "grey"
		}
		if(fillScale)
			return fillScale(d.key)
		return 'red'
	}

	const actualDs = datapoints.filter(d => !d.isTarget && !d.projected)

	const line = d3.line()
		.x(d => x(d.xValue) +(d.shift ? d.shift:0))
		.y(d => y(d.yValue))

	//1.BIND
	//data is an array of datapoint arrays, 1 per key
	const data = !keyItems ? actualDs :
		keyItems.map(it => actualDs.filter(d => d.key === it.key))
			   .filter(ds => ds[0])
	//note, each d here is actually an array of datapoints
	const updatedLines = scatterG.selectAll("path.dataLine")
		.data(data, (ds,i) => (ds[0] && ds[0].key ? ds[0].key : i))
	//2.EXIT
	updatedLines.exit().remove()
	//3.UPDATE
	//position
	updatedLines
		.attr('d',ds => {
				//curve
				const curve = ds.length < 4 ? d3.curveNatural : d3.curveBasis
				line.curve(curve)
				return line(ds)
			})
	//display
	updatedLines
		.transition(d3.transition().duration(500))
		.attr("stroke", ds => strokeColour(ds[0]))
		.style('opacity', ds => (keyItemsInFocus.length == 0 || inFocus(ds[0]) ? 1 : 0.4))
	//4.ENTER
	updatedLines.enter()
		.append("path")
			.attr("class", ds => 'dataLine dataLine-'+((ds[0] && ds[0].key) ? ds[0].key : ''))
			.attr("d", ds => {
				//curve
				const curve = ds.length < 4 ? d3.curveNatural : d3.curveBasis
				line.curve(curve)
				return line(ds)
			})
			.attr("fill", "none")
			.attr("stroke", ds => strokeColour(ds[0]))
			.attr("stroke-width", 2)
			.style('opacity', 0)
		  	//.transition(d3.transition().duration(250).delay(500))
		  	.style('opacity', 1)

}

/**
*  Renders (or re-renders) the circles for the test results and the projected results 
*  for each selected player each xKey entry (eg week)
*/
const updateScatterDs = (scatterG, datapoints, keyItems, scales, fillScale, dispatcher, transitions, focusChange) =>{
    const { x, y } = scales
	const actualDs = datapoints.filter(d => !d.isTarget && !d.projected)
	//console.log('updateDatapoints actual', actualDs)
	const keyItemsInFocus = keyItems.filter(it => it.inFocus)
	const inFocus = d => {
		if(keyItemsInFocus.find(it => it.key === d.key))
			return true
	}

	//helpers
	let circleFill = d =>{
		if(d.projected)
			return "none"
		if(keyItemsInFocus.length != 0 && !inFocus(d))
			return "grey"
		if(d.isTarget)
			return "black"
		if(fillScale)
			return fillScale(d.key)
		return 'red'
	}
	let circleStroke = (d, i) => {
		if(fillScale)
			return fillScale(d.key)
		return 'red'
	}

	const translation = d => {
		//console.log('d', d)
		return "translate(" +(x(d.xValue) +(d.shift ? d.shift:0)) +", "+y(d.yValue) +")"
	}

	//1. BIND  - new data to old elements 
	let updatedChartG = scatterG.selectAll("g.datapointG")
		.data(datapoints, d => d._id)
	//2. EXIT - old elements not present in new data
	updatedChartG.exit()
		.attr('class', 'exit')
		.remove()
	//3.UPDATE - ONLY old elements with NEW data bound to them
	//position
	updatedChartG
		.classed('enter', false)
		.attr("transform", (d,i) => translation(d))
	//display
	updatedChartG
		.transition(d3.transition().duration(500))
		.style("fill", d => circleFill(d))
		.style('opacity', 
			d => (keyItemsInFocus.length == 0 || inFocus(d) ? 1 : 0.4))
	
	//update onClick to include latest version of keyItems
	scatterG.selectAll("g.lastActual").selectAll("*")
  			.on('click', function(d){
		  		dispatcher.call('customClick', this, d, keyItems)
		  	})

	//4. ENTER - new elements present in new data

	let chartGEnter = updatedChartG.enter()
		.append("g")
		  	//add identifier for last actual to append photos later
		  	.attr("class", d => "enter datapointG datapointG-"+(d.key ? d.key : '')
		  		+(d.isLastActual ? ' lastActual':''))
		  	//dont need this transform if added to update selection
		  	.attr("transform", (d,i) => {
		  		//console.log('entering a d.......')
		  		return translation(d)
		  	})

	chartGEnter
		.append("circle")
			.attr('class', d => 'datapointCircle '+(d.isLastActual ? ' lastActual':''))
			//each actual d has a unique id from schema
			.attr("r", d => d.isLastActual ? 15 : 5)
		    .style("fill", d => circleFill(d))
		    .style('opacity', d => (keyItemsInFocus.length == 0 || inFocus(d) ? 1 : 0.4))
		    .style("stroke", (d,i) => circleStroke(d, i))
		    .on("mouseover", showInfo)
		    .on("mouseout", hideInfo)
		    .style('cursor', 'pointer')
		    .on('click', function(d){
		    	if(d.lastActual){
			  		dispatcher.call('customClick', this, d, keyItems)
		    	}else{
		    	}
		  	})
		    .style('opacity', 0)
		  	//.transition(d3.transition().duration(250).delay(500))
		  	.style('opacity', 1) 
	//photos - use scatterG instead to target only those that are lastActual
	//even if keys are not players, they may have a photo eg key could be teams
	//but if they are players, then we use default photo anyway
	if(keyItems && keyItems[0] && (keyItems[0].photo || keyItems[0].surname)){
		scatterG.selectAll("g.lastActual.enter")
			.insert("image","text")
				.attr('class', 'lastActual')
	  			.attr("xlink:href", d => {
	  				return photoUrl(d)
	  			})
	  			.on('click', function(d){
			  		dispatcher.call('customClick', this, d, keyItems)
			  	})
				.attr("height", "30px")
				.attr("width", "30px")
				.attr("x", -15)
				.attr("y", -15)
				.style('cursor', 'pointer')
				.style('opacity', 0)
				//.transition(d3.transition().duration(250).delay(500))
			  	.style('opacity', 1)
	}
	else{
		scatterG.selectAll("g.lastActual.enter")
			.append('text')
				.attr('transform', `translate(${0} , ${5})`)
				.style('text-anchor','middle')
				//.style('alignment-baseline', 'hanging')
				.style('font-size', 14)
			    .style('stroke-width', 1)
				.style('stroke', 'white')
				.style('fill', 'white')
				.style('cursor', 'pointer')
				.on("click", function(d){
			  		dispatcher.call('customClick', this, d, keyItems)
			  	})
				.text(d => keyItems.find(it => it.key === d.key).name.substring(0,3))
	}

	function photoUrl(d){
		const item = keyItems.find(it => it.key === d.key)
		if(item && item.photo)
			return `/api/user/photo/${item.key}?${new Date().getTime()}`
		return '/api/user/defaultphoto'
	}
	//5.MERGE-UPDATE - newly entered elements AND old elements with new data bound to them

}

/**
*
*
**/
//todo - if player is highlighted, then target line colours match player colour
const updateTargetLines = (scatterG, ds, scales, sizes, format, fillScale) =>{
	const { margin, chartWidth, chartHeight } = sizes
	const { x, y } = scales

	//helpers 


	//scales have margins built in
	const calcHorizTargetPath = d => {
		const scaledHoz = [
			[margin.left, y(d.yValue)],
			[margin.left + chartWidth, y(d.yValue)]
		]
		return {horizontal:scaledHoz}
	}
	const calcTargetPathsToPoint = d => {
		const scaledHoz = [
			[margin.left, y(d.yValue)],
			[x(d.xValue), y(d.yValue)]
		]
		const scaledVert = [
			[x(d.xValue), margin.top + chartHeight],
			[x(d.xValue), y(d.yValue)]
		]
		return {horizontal:scaledHoz, vertical:scaledVert}
	}

	const targets = ds.map(d => {
		if(format === 'toPoint')
			return {...d, paths:calcTargetPathsToPoint(d)}
		else
			return {...d, paths:calcHorizTargetPath(d)}
	})
	//HORIZONTAL
	//1.BIND
	const updatedHorizLines = scatterG.selectAll("path.horizTargetLine")
		.data(targets, d => d._id)
	//2.EXIT
	updatedHorizLines.exit().remove()
	//3.UPDATE
	updatedHorizLines
		//.transition(d3.transition().duration(500))
		.attr('d', targetD => d3.line()(targetD.paths.horizontal))
	//4.ENTER
	updatedHorizLines.enter()
		.append("path")
			.attr('class', 'targetLine horizTargetLine')
			.attr("d", targetD => d3.line()(targetD.paths.horizontal))
			.attr("fill", "none")
			.attr("stroke", d => fillScale(d.key))
			.style("stroke-dasharray", "5,5")
			.attr("stroke-width", 1)

	//VERTICAL
	if(targets[0] && targets[0].paths.vertical){
		//1.BIND
		const updatedVertLines = scatterG.selectAll("path.vertTargetLine")
			.data(targets, d => d._id)
		//2.EXIT
		updatedVertLines.exit().remove()
		//3.UPDATE
		updatedVertLines
			//.transition(d3.transition().duration(500))
			.attr('d', targetD => d3.line()(targetD.paths.vertical))
		//4.ENTER
		updatedVertLines.enter()
			.append("path")
				.attr('class', 'targetLine vertTargetLine')
				.attr("d", targetD => d3.line()(targetD.paths.vertical))
				.attr("fill", "none")
				.attr("stroke", "black")
				.style("stroke-dasharray", "5,5")
				.attr("stroke-width", 1)
	}
}

/**
*
*
**/
const updateXAxis = (selection, scale, sizes, tUpdate) =>{
	const axis = fc.axisLabelRotate(fc.axisBottom(scale))
		.ticks(13)
		.labelRotate(45)
		.decorate(sel =>{
			sel.enter().selectAll("path")
				.style("stroke-width", 1.5)
				.style("stroke", "white")

			sel.enter().selectAll("text")
				.style("stroke-width", 1.5)
				.style("stroke", "white")
		})

		selection.select('g.xAxisG')
			.transition(d3.transition().duration(500))
			.attr("transform", `translate(0, ${sizes.margin.top +sizes.chartHeight})`)
			//.style("font", "0.9rem times")

		selection.select('g.xAxisG').call(axis)

		selection.select('g.xAxisG').select(".domain")
			.style("stroke-width", 1.5)
			.style("stroke", "white")

}


/**
*
*
**/
//position, text, and font-size could change
const updateXLabel = (selection, sizes, tUpdate) =>{
	const horizontalShift = sizes.margin.left + 0.5 * sizes.chartWidth
	const verticalShift = sizes.chartHeight +sizes.margin.top +80
	selection.select('text.xLabel')
	.transition(d3.transition().duration(500))
		.attr("transform", 'translate(' +horizontalShift +', ' +verticalShift +')')
		.text('Date')
}

/**
*
*
**/
const updateYAxis = (selection, scale, sizes, tUpdate) =>{
	const axis = d3.axisLeft().scale(scale)
	selection.select("g.yAxisG")
		.attr("transform", 'translate(' +sizes.margin.left +',' +0 +')')
		//this trans delays zoom changes so cannot be here
		//.transition(d3.transition().duration(500))
		.call(axis)

	selection.select('g.yAxisG').selectAll("line")
		.style("stroke-width", 1.5)
		.style("stroke", "white")
	selection.select('g.yAxisG').selectAll("path")
		.style("stroke-width", 1.5)
		.style("stroke", "white")
	selection.select('g.yAxisG').selectAll("text")
		.style("stroke-width", 1.5)
		.style("stroke", "white")
}

/**
*
*
**/
const updateYLabel = (selection, sizes, name) =>{
	//adjust name temporarily for mock data
	let nameToShow
	if(name === 'Match Top Acceleration'){
		if(window.innerWidth > 500 && window.innerWidth <= 980){
			nameToShow = 'Match'
		}else{
			nameToShow = 'Top Acceleration in Match (m/s/s)'
		}
	}else if(name === 'Test 5M Acceleration'){
		if(window.innerWidth > 500 && window.innerWidth <= 980){
			nameToShow = 'Test'
		}else{
			nameToShow = '5m Max Acceleration Test Off-Pitch (m/s/s)'
		}
	}else{
		nameToShow = name
	}
	let horizontalShift, verticalShift
	if(window.innerWidth < 501){
		horizontalShift = sizes.margin.left
		verticalShift = 0.7 * sizes.margin.top
	}
	else if(window.innerWidth > 500 && window.innerWidth <= 980){
		//ms screen (landscape) so label along side
		horizontalShift = 0.1 * sizes.margin.left
		verticalShift = sizes.margin.top + 0.5 * sizes.chartHeight
	}else{
		//label along top (so units can go down the side, later)
		horizontalShift = sizes.margin.left
		verticalShift = 0.5 * sizes.margin.top
	}
	selection.select('text.yLabel')
		.attr("transform", 'translate(' +horizontalShift +', ' +verticalShift +')')
		.style('text-anchor','start')
		.text(nameToShow)
}


/**
*
*
**/
const updateZonePaths = (scatterG, zones, scales, sizes, fillScale, tUpdate) =>{
	const bandDecorator = sel => {
		//2.EXIT
		//may need to impl remove here, or is it implememted internally???

		//3.UPDATE - existing only
		//sel.selectAll('path')
		sel.selectAll('text.zoneName')
			.attr("transform", (d,i) => "translate(" +(sizes.chartWidth * 0.45) 
					+", " +0 +")")
			//for some reason, the ds are not the updated ones when percentages
			.text(d =>  zones.find(z => z.nr === d.nr).name)

		sel.selectAll('path')
			//.transition()
			//.duration(500)
			.attr('fill', d => fillScale(d.nr))
			//.style("opacity", d => d.hidden ? 0 : 1)

		//4.ENTER
		sel.enter().selectAll('path')
			//.transition()
			//.duration(500)
			.attr('fill', d => fillScale(d.nr))
			.style("opacity", d => d.hidden ? 0 : 1)

		sel.enter()
			.append('text')
				.attr('class', d => {
					//console.log('entering zone...')
					return 'zoneName zoneName-' +d.nr
				})
				//g.band is already translated to the centre of the band
				.attr("transform", (d,i) => "translate(" +(sizes.chartWidth * 0.4) 
					+", " +0 +")")
				//.transition()
				//.duration(500)
				.text(d => d.name)
				.style("text-anchor", "end")
				.style("font-size", 14)
				.style("stroke", "black")
				.style("opacity", d => d.hidden ? 0 : 0.4)
				.style("dominant-baseline", "central")

		//5.MERGE-UPDATE (existing and new)


	}

	//1.BIND
	const band = fc.annotationSvgBand().xScale(scales.x).yScale(scales.y)
		.decorate(bandDecorator)
	scatterG.datum(zones, z => z.nr)
			//this transition slows down zoom response of zones
			//.transition()
			//.duration(500)
			.call(band)
}

const showInfo = (datapoint) => {
	/*let scores = datapoint.tests.map(test => test.name +": " +test.score() +"\n")
	d3.select(svg).selectAll("g."+datapoint.uName).
		append("text").
		attr("class", datapoint.uName +"DescText").
		attr("y", 10).
		text("scores for " +datapoint.uName +"....")
		*/
}
const hideInfo = (datapoint) =>{
	/*
	d3.select(svg).selectAll("g." +datapoint.uName).
		select("text." +datapoint.uName +"DescText").remove()
		*/
}

export { nestStepData, updateAnimateBtn, updateAnimationPauseBtn, updateBackground, updateBeeSwarmDs, updateClipPath, updateDataLines, updateScatterDs, 
 updateXLabel, updateXAxis, updateYAxis,  updateYLabel, 
	updateZonePaths, updateTargetLines, updateCurrentWeekOverlay }