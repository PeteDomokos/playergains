import * as d3 from 'd3'
import * as fc from 'd3fc'
import colorbrewer from 'colorbrewer'
//general
import { addWeeks } from '../TimeHelpers'
import { onlyUnique } from '../../../../util/helpers/ArrayManipulators'
import DefaultProjector from '../Projector'
import DefaultAntiCollisionShifter from '../AntiCollisionShifter'
import { renderButton } from '../ChartComponents'
//scatter
import ScatterData from './ScatterData'
import { boundsChanged, calcDataBounds, calcScales, calcTranslateExtent, 
	calcZoneBounds } from './ChartHelpers'
import { updateAnimateBtn, updateAnimationPauseBtn, nestStepData, updateBackground, updateClipPath, updateBeeSwarmDs, updateScatterDs, updateDataLines, updateTargetLines, 
	updateXLabel, updateYLabel, updateXAxis, updateYAxis, updateZonePaths, updateCurrentWeekOverlay} from './UpdateChartComponents'
/**
* 
* 
*/

const percentageZones = [  
	{id:'no', name:'No Progress', nr:1, from:-1000, to:0}, 
	{id:'low', name:'Low Progress', nr:2, from:0, to:2.5}, 
	{id:'medium', name:'Medium Progress', nr:3, from:2.5, to:5}, 
	{id:'good', name:'Good Progress', nr:4, from:5, to:10}, 
	{id:'very-good', name:'Very Good Progress', nr:5, from:10, to:20}, 
	{id:'exceptional', name:'Exceptional Progress', nr:6, from:20, to:1000}
]
const weeklyPercentageZones = [  
	{id:'big-decline', name:'Big Decline', nr:1, from:-1000, to:-5}, 
	{id:'decline', name:'Decline', nr:2, from:-5, to:-2}, 
	{id:'none', name:'Not Much Change', nr:3, from:-2, to:2}, 
	{id:'good', name:'Progress', nr:4, from:2, to:5}, 
	{id:'very-good', name:'Good Progress', nr:5, from:5, to:10}, 
	{id:'exceptional', name:'Exceptional Progress', nr:6, from:10, to:1000}
]
//INIT HELPERS
const defaultZonesFillScale = nrZones => 
	d3.scaleQuantize()
		.domain([1, nrZones])
		.range(colorbrewer.Blues[nrZones])

const defaultKeysFillScale = keys => 
	d3.scaleOrdinal().
		domain(keyItems.map(it => it.key)).
		range(["blue", "red", "yellow", "orange", "navy","green"])

const initKeyItems = ds =>{
	if(!ds || !ds[0].key)
		return ''
	else
		return datapoints.map(d => d.key)
						 .filter(onlyUnique)
						 .map(key => { return { key:key} })
}
//check for selected, then check for main, then default to 1st available
const initValueFormat = dataItem => {
	if(!dataItem.dataValueFormats || !dataItem.dataValueFormats[0])
		return {key:'value', name:'Value'}
	else{
		const selected = dataItem.dataValueFormats.find(v => v.selected)
		if(selected)
			return selected
		else{
			const main = dataItem.dataValueFormats.find(v => v.isMain)
			if(main)
				return main
			return dataItem.dataValueFormats[0]
		}
	}
}
const valueFormatApplicable = (valueFormat, dataItem) =>{
	return dataItem.dataValueFormats.find(format => format.key == valueFormat.key)
}

const defaultZones = (lower, upper) =>{
	const range = Math.abs(upper - lower)
	const inc = range/6
	return Array.from(Array(6).keys())
		.map(n => ({ nr:n+1, from:n * inc, to:(n + 1) * inc, hidden:true }))
}

//SCATTER MAIN FUNCTION
/**
*  Receives (via api) and handles all chart settings. 
*  Passes the data and settings to the data processing object (ScatterData), telling it what processes are required.
*  Calls the component update functions (). 
*
**/
function scatter(){
	//flag
	var rendered = false
	var chartKey = {}
	//setings
	var sizes = {
		//default is MS sizes
		width:600, height:300, wSF:600/550, hSF:300/320, 
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	//general vars (same for all dataItem charts)
	var measure = {name:'Actual', key:'actual'}
	var keyItems, startDate, availableZones
	var projector
	var antiCollisionShifter = new DefaultAntiCollisionShifter()
	var withTargetPaths = false
	var fillScales = {}
	//specific vars to each dataItem chart
	var valueFormat = {}, data = {}, chartDs = {}, zones = {}, scales = {}
	var bounds = {}
	var requiredZones = {}
	var scatterG = {}
	//general update handlers
	var mainUpdate, updateSizes, updateStartDate, updateMeasure, updateKeyItems
	//secondary update functions
	var updateZonesAndZoneBounds, updateScales
	var updateElements, removeElements, updateZoneElements, updateTargetLineElements
	//zoom handler
	var zoom, zoomed
	//simulation
	var force = {}
	var updateSimulation
	
	//DISPATCHER
	const dispatcher = d3.dispatch('customClick')

	const transitions = { 
		enter:{dur:250, delay:750},
	    update:{dur:750, delay:0}
	}

	function my(selection, config){
		rendered = true
		if(config){
			//todo - use Object.keys to iterate
			if(config.sizes)
				sizes = config.sizes
		}
		//update keyItems to default if not set
		if(!keyItems){
			const allDsForAllDatasets = []
			selection.each(function(dataItem){
				allDsForAllDatasets.push(...dataItem.datapoints)
			})
			keyItems = initKeyItems(allDsForAllDatasets)
		}

		//UPDATE FUNCTIONS ----------------------------------------
		//these are for vars that are not specific to any dataItem

		updateSizes = function(){
			//selection only includes the last one rendered for some reason
			selection.each(function(dataItem){
				const dataKey = dataItem.key
				//sizes stuff that needs to happen if sizes updates
				updateClipPath(d3.select(this), sizes)
				updateZonesAndZoneBounds(dataKey)
				updateScales(dataKey)
				updateElements(d3.select(this), dataItem)
			})
		}
		updateStartDate = function(){
			selection.each(function(dataItem){
				const dataKey = dataItem.key
				//wipe percentages and projections
				data[dataKey] = data[dataKey]
					.removeProjections()
					.clearPercentages()
					.addPercentagesIfNeeded(valueFormat[dataKey].key, measure, keyItems)

				mainUpdate(d3.select(this), dataItem)
			})
		}
		updateMeasure = function(){
			//bug - %change -> scores, need to wipe previous formatting
			selection.each(function(dataItem){
				const dataKey = dataItem.key
				updateZonesAndZoneBounds(dataKey)
				//wipe projections, may need to add/remove targets, may add percents
				data[dataKey] = data[dataKey]
					.addOrRemoveTargets(keyItems.map(it => it.key), measure.key, dataItem.datapoints)
					.removeProjections()
					.addPercentagesIfNeeded(valueFormat[dataKey].key, measure.key, keyItems)
					.format(valueFormat[dataKey].key, measure.key, keyItems)
					.addProjections(projector, keyItems, bounds[dataKey].zones, measure.key)

				updateScales(dataKey)
				//must remove and re-enter points so they are on top of new zones
				removeElements(dataKey)
				updateElements(d3.select(this), dataItem)
			})
		}
		updateKeyItems = function(focusChange){
			selection.each(function(dataItem){
				const dataKey = dataItem.key
				if(focusChange){
				//only need to change opacity of rest of keyItems
					if(chartKey[dataKey].includes('scatter')){
						updateDataLines(scatterG[dataKey], chartDs[dataKey], keyItems, scales[dataKey], fillScales.keyItems, transitions)
						updateScatterDs(scatterG[dataKey], chartDs[dataKey], keyItems, scales[dataKey], fillScales.keyItems, dispatcher, transitions)
					}
				}
				else{
					//may need to remove targets or addpercents from new keyItems
					data[dataKey] = data[dataKey]
						.filterForKeys(keyItems.map(it => it.key), dataItem.datapoints)
						.addOrRemoveTargets(keyItems.map(it => it.key), measure.key, dataItem.datapoints)
						.assignAndFilterDate(startDate)
						.addPercentagesIfNeeded(valueFormat[dataKey].key, measure.key, keyItems)
						.format(valueFormat[dataKey].key, measure.key, keyItems)
						.addProjections(projector, keyItems, bounds[dataKey].zones, measure.key)

					if(boundsChanged())
						updateScales(dataKey)
					updateElements(d3.select(this), dataItem)
				}
			})
		}
		//main update
		mainUpdate = function(svg, dataItem){
			//console.log('mainUpdate dataItem', dataItem.key)
			const dataKey = dataItem.key
			updateZonesAndZoneBounds(dataKey)
			data[dataKey] = data[dataKey]
				.filterForKeys(keyItems.map(it => it.key), dataItem.datapoints) //pass all dataItem ds not just current ones
				.addOrRemoveTargets(keyItems.map(it => it.key), measure.key, dataItem.datapoints)
				.assignAndFilterDate(startDate)
				.addPercentagesIfNeeded(valueFormat[dataKey].key, measure.key, keyItems)
				.format(valueFormat[dataKey].key, measure.key, keyItems)
				.addProjections(projector, keyItems, bounds[dataKey].zones, measure.key)

			updateScales(dataKey)
			updateElements(svg, dataItem)
		}

		updateZonesAndZoneBounds = function(dataKey){
			if(measure.key === 'actual'){
				if(zones[dataKey] && zones[dataKey][valueFormat[dataKey].key])
					requiredZones[dataKey] = zones[dataKey][valueFormat[dataKey].key]
				else requiredZones[dataKey] = undefined
			}
			else{
				requiredZones[dataKey] = undefined
				//impl percentage zones here
			}
			bounds[dataKey] = { ...bounds[dataKey], ...calcZoneBounds(requiredZones[dataKey]) }
		}

		updateScales = function(dataKey){
			bounds[dataKey] = {
				...bounds[dataKey],
				//leave minX and maxX in, as they may be useful for simulation
				...calcDataBounds(data[dataKey].datapoints.filter(d => !d.projected))
			}
			const numberOrder = measure == 'actual' ? 
				valueFormat[dataKey].numberOrder : 'low-to-high'
			//calcScales returns a fake x-axis if chartKey == beeSwarm
			scales[dataKey] = calcScales(data[dataKey].datapoints, bounds[dataKey], numberOrder, sizes, chartKey[dataKey])
		}	

		updateElements = function(svg, dataItem){
			const dataKey = dataItem.key
			//helpers
			const updateScatterElements = () =>{
				if(antiCollisionShifter){
					const unshiftedData = data[dataKey].datapoints.map(d =>({ ...d, shift:0 }))
					chartDs[dataKey] = antiCollisionShifter.createShifts(unshiftedData, scales[dataKey])
				}else{
					chartDs[dataKey] = data[dataKey].datapoints
				}
				//remove x label - no need
				//updateXLabel(svg, sizes, transitions.update)
				updateYLabel(svg, sizes, dataItem.name, transitions.update)
				if(boundsChanged()){
					updateXAxis(svg, scales[dataKey].x, sizes, transitions.update)
					updateYAxis(svg, scales[dataKey].y, sizes, transitions.update)
					updateZoneElements(dataKey, scales[dataKey])
					//remove current week overlay - too cluttered
					//updateCurrentWeekOverlay(scatterG[dataKey], sizes, scales[dataKey].x)
				}
				if(withTargetPaths){
					updateTargetLineElements(dataKey, scales[dataKey])
				}

				updateDataLines(scatterG[dataKey], chartDs[dataKey], keyItems, scales[dataKey], fillScales.keyItems, transitions)
				updateScatterDs(scatterG[dataKey], chartDs[dataKey], keyItems, scales[dataKey], fillScales.keyItems, dispatcher, transitions)
			}
			const updateBeeSwarmElements = () =>{
				chartDs[dataKey] = data[dataKey].datapoints
				updateYLabel(svg, sizes, dataItem.name, transitions.update)
				if(boundsChanged()){
					updateYAxis(svg, scales[dataKey].y, sizes, transitions.update)
					updateZoneElements(dataKey, scales[dataKey])
				}
				if(withTargetPaths){
					//removed due to bug
					//updateTargetLineElements(dataKey, scales[dataKey])
				}
				updateSimulation(svg, dataItem)
			}

			updateBackground(scatterG[dataKey], sizes)
			//chart elements
			if(chartKey[dataKey].includes('scatter')){
				updateScatterElements()
			}else{
				updateBeeSwarmElements()
			}
			//zoom
			zoom = d3.zoom()
		      .scaleExtent([.5, 20])
		      //.translateExtent(scales[dataKey].translateExtent)
		      .extent([[0, 0], [sizes.chartWidth, sizes.chartHeight]])
		      .on("zoom", zoomed)

		   	scatterG[dataKey].call(zoom)
		}
		removeElements = function(dataKey){
			scatterG[dataKey].selectAll('*').remove()
		}
		//scales passed in as zoomed uses this method too
		updateZoneElements = function(dataKey, scales){
				var zonesToUse
				//only use actual score zones if keyItems are players
				if(measure.key === 'actual' && chartKey[dataKey].includes('players'))
					zonesToUse = requiredZones[dataKey]
				else if(measure.key === 'percent-change')
					zonesToUse = percentageZones
				else if(measure.key === 'percent-change-pw')
					zonesToUse = weeklyPercentageZones

				if(!zonesToUse){
					//create fake zones and hide them to stop zones exiting/re-entering
					const lower = scales.y.range()[0]
					const upper = scales.y.range()[1]
					const range = Math.abs(upper - lower)
					const inc = range/6
					zonesToUse = defaultZones(lower, upper)
				}
				if(!fillScales.zones)
					fillScales.zones = defaultZonesFillScale(zonesToUse.length)

				updateZonePaths(scatterG[dataKey], zonesToUse, scales, sizes, fillScales.zones, transitions.update)

		}
		//scales passed in as zoomed uses this method too
		updateTargetLineElements = function(dataKey, scales){
				console.log('updateTargetLineElements()...')
				var targets, lineType, fillScale
				if(measure.key === 'percent-change-pw'){
					targets = [{yValue:2}]
					lineType = 'horiz'
					fillScale = function(){return 'green'}
				}
				else if(chartKey[dataKey] === 'beeSwarm'){
					//todo - only last targ of each key
					targets = chartDs[dataKey].filter(d => d.isTarget)
					lineType = 'horiz'
					fillScale = fillScales.keyItems
				}else{
					targets = chartDs[dataKey].filter(d => d.isTarget)
					lineType = 'toPoint'
					fillScale = fillScales.keyItems
				}
				updateTargetLines(scatterG[dataKey], targets, scales, sizes, lineType, fillScale)
			}

		updateSimulation = function(svg, dataItem){
			const dataKey = dataItem.key
			//radius
			const r = 15
			//flags
			var animationRunning = false
			var paused = false
			//1st step will be complete at time = 0
			var stepsComplete = 1
			var stepsRemaining = 0
			//var manyBody = d3.forceManyBody().strength(10)
			force[dataKey] =d3.forceSimulation()
				.force("x", d3.forceX(sizes.margin.left + sizes.chartWidth * 0.5))
				//y a little stronger than default(0.1) to prioritise y pos
				.force("y", d3.forceY(d => scales[dataKey].y(d.yValue)).strength(0.2))
				.force("collision", d3.forceCollide(r))
				.velocityDecay(0.7) //up from 0.4 to stop oscillation
				.alphaDecay(0.001) 
				.on("tick", updateNetwork)

			function updateNetwork(){
				scatterG[dataKey].selectAll('g.datapointG-'+dataKey)
					.attr("transform", d => "translate(" +d.x +" , " +d.y +")")
			}
			//non-animated simulation
			updateBeeSwarmDs(scatterG[dataKey], dataKey, chartDs[dataKey], keyItems, r, fillScales.keyItems, dispatcher, transitions)
			force[dataKey].nodes(chartDs[dataKey])

			//animated simulation
			var interval = null
			const runAnimation = () =>{
				//vars
				var speed = 1500
				paused = false
				var nestedData = nestStepData(chartDs[dataKey])
				//first step data
				var stepData = nestedData[0].values
				//1st step (for nestedData[0])
				updateBeeSwarmDs(scatterG[dataKey], dataKey, stepData, keyItems, r,  fillScales.keyItems, dispatcher, transitions)
				force[dataKey].nodes(stepData)
				//helper
				const setForceInterval = () => {
					interval = setInterval(function(){
						console.log('step...')
						if(nestedData.length - stepsComplete === 0){
							setTimeout(() =>{
								//check it hasnt been manually ended
								if(animationRunning){
									endAnimation()
								}
							}, 3000)
						}else{
							stepData = nestedData[stepsComplete].values
							
							updateBeeSwarmDs(scatterG[dataKey], dataKey, stepData, keyItems, r, fillScales.keyItems, dispatcher, transitions)

							force[dataKey].stop()
							const currentNodes = force[dataKey].nodes()

							stepData.forEach(d =>{
								const currNodeForKey = currentNodes
									.find(n => n.key === d.key)
								if(currNodeForKey){
									d.x = currNodeForKey.x,
									d.y = currNodeForKey.y
								}
							})
							force[dataKey].alpha(1)
							force[dataKey].nodes(stepData)
							force[dataKey].restart()

							stepsComplete++
						}
					},speed)
				}

				setForceInterval()

				//pause button
				const handlePauseClick = () =>{
					if(!paused){
						force[dataKey].stop()
						clearInterval(interval)
						paused = true
					}else{
						setForceInterval()
						force[dataKey].restart()
						paused = false
					}
					updateAnimationPauseBtn(svg, sizes, paused)
				}
				renderButton(svg, handlePauseClick, {size:'small', className:'pause'})
				updateAnimationPauseBtn(svg, sizes, paused)
			}

			const endAnimation = () =>{
				clearInterval(interval)
				stepsComplete = 1
				animationRunning = false
				paused = false
				updateAnimateBtn(svg, sizes, false)
				//remove pause btn
				svg.select('g.pause').remove()
				mainUpdate(svg, dataItem)
			}
			const startAnimation = () =>{
				animationRunning = true
				updateAnimateBtn(svg, sizes, true)
				runAnimation()
			}

			//play button
			const handleAnimateClick = () =>{
				force[dataKey].stop()
				if(animationRunning){
					endAnimation()
				}
				else{
					startAnimation()
				}
			}
			renderButton(svg, handleAnimateClick, {size:'small', className:'animate'})
			updateAnimateBtn(svg, sizes, false)
		}


		selection.each(function(dataItem){
			const svg = d3.select(this)
			const dataKey = dataItem.key

			const className = this.getAttribute('class')
			if(className.includes('beeSwarm')){
				if(className.includes('players')){
					chartKey[dataKey] = 'beeSwarm-players'
				}
				else if(className.includes('datasets')){
					chartKey[dataKey] = 'beeSwarm-datasets'
				}
				else{
					chartKey[dataKey] = 'beeSwarm'
				}

			}else{
				if(className.includes('players')){
					chartKey[dataKey] = 'scatter-players'
				}
				else if(className.includes('datasets')){
					chartKey[dataKey] = 'scatter-datasets'
				}
				else{
					chartKey[dataKey] = 'scatter'
				}
			}

			valueFormat[dataKey] = initValueFormat(dataItem)

			//INITIAL DATA PROCESSING ----------------------------
			data[dataKey] = new ScatterData(dataItem.datapoints)

			//RENDERING PROCESSES 
			//initial axes and labels
			if(chartKey[dataKey].includes('scatter')){
				svg.append("g").attr("class", "axis xAxisG")
				svg.append('text')
					.attr('class', 'xLabel')
					.style("text-anchor", "middle")
					.style("stroke", "white")
					.style('fill', 'white')
			}

			svg.append("g").attr("class", "axis yAxisG")
				
			svg.append('text')
				.attr('class', 'yLabel')
				.style("text-anchor", "middle")
				.style("stroke", "white")
				.style('fill', 'white')

			//init clipPath G
			svg.append('defs').append('clipPath')
				.attr('id', 'clip')
				.append('rect')

			//scatterG
			scatterG[dataKey] = svg.append('g')
				.attr('class', 'clipPathG')
				.attr('clip-path', "url(#clip)")

			//init
			updateClipPath(svg, sizes)

		   	//ZOOM UPDATE FUNCTION  --------------------------------
		    zoomed = function(){
		    	//helpers
		    	const updateScatterElements = () =>{
		    		updateXAxis(selection, newX, sizes)
			    	updateYAxis(selection, newY,  sizes)
			    	updateScatterDs(scatterG[dataKey], chartDs[dataKey], keyItems, newScales, fillScales.keyItems, dispatcher, transitions)
					updateDataLines(scatterG[dataKey], chartDs[dataKey], keyItems, newScales, fillScales.keyItems)
					//remove current week overlay - too cluttered
					//updateCurrentWeekOverlay(scatterG[dataKey], sizes, newX)
		    	}
		    	const updateBeeSwarmElements = () =>{
		    		updateYAxis(selection, newY,  sizes)
		    		force[dataKey].stop()
					force[dataKey].alpha(1)
					force[dataKey].force("y", d3.forceY(d => newY(d.yValue)).strength(0.2))
					force[dataKey].restart()	
		    	}

		    	const dataKey = dataItem.key
		    	//new scales - must recalc transExt too so we pass it to calcScales 
		    	const numberOrder = measure == 'actual' ? 
					valueFormat[dataKey].numberOrder : 'low-to-high'

		    	//COMP - THIS NOT NEEDED
			   	const newX = d3.event.transform.rescaleX(scales[dataKey].x)
			    const newY = d3.event.transform.rescaleY(scales[dataKey].y)
			    const newScales = { x:newX, y:newY }
			    //const newTranslateExtent = calcTranslateExtent(chartDs[dataKey], bounds[dataKey], newScales, numberOrder, sizes.margin)
			    //newScales.translateExtent = newTranslateExtent
			    //update zoom itself
			   // zoom.translateExtent(newTranslateExtent)
			    //zones
				updateZoneElements(dataKey, newScales)
				if(withTargetPaths){
					updateTargetLineElements(dataKey, newScales)
				}
				//chart elements
				if(chartKey[dataKey].includes('scatter')){
					updateScatterElements()
				}else{
					updateBeeSwarmElements()
				}
		    }
		    mainUpdate(svg, dataItem)
		})
	}
	my.rendered = function(value){
		if(!arguments.length)
			return rendered
		rendered = value
		return my
	}
	my.sizes = function(value){
		if(!arguments.length)
			return sizes
		sizes = value
		if(typeof updateSizes === 'function')
			updateSizes()
		return my
	}
	my.startDate = function(value){
		if(!arguments.length)
			return startDate
		startDate = value
		if(typeof updateStartDate === 'function')
			updateStartDate()
		return my
	}
	my.measure = function(value){
		if(!arguments.length)
			return measure
		measure = value
		if(typeof updateMeasure === 'function')
			updateMeasure()
		return my
	}
	my.keyItems = function(value, fillScale){
		if(!arguments.length)
			return keyItems
		//helper
		const updateFillScale = () =>{
			if(fillScale){
				fillScales.keyItems = fillScale
			}
			else if(!fillScales.keyItems){
				fillScales.keyItems = defaultKeysFillScale(value)
			}
		}
		if(!keyItems){
			//setting
			keyItems = value
			updateFillScale()
		}
		else{
			//updating
			const currentNrInFocus = keyItems.filter(it => it.inFocus).length
			const newNrInFocus = value.filter(it => it.inFocus).length
			const focusChange = currentNrInFocus != newNrInFocus
			//set and update
			keyItems = value
			updateKeyItems(focusChange)
			updateFillScale()
		}
		return my
	}
	my.zones = function(value, fillScale){
		if(!arguments.length)
			return zones
		zones = value
		fillScales.zones = fillScale
		return my
	}
	//todo - if 
	my.withProjections = function(value){
		if(!arguments.length)
			return projector
		if(value == true)
			project = new DefaultProjector(52)
		projector = value

		return my
	}
	my.withAntiCollision = function(value){
		if(!arguments.length)
			return antiCollisionShifter
		if(value === false){
			antiCollisionShifter = null
		}
		else{
			antiCollisionShifter = value
		}
		return my
	}
	my.withTargetPaths = function(flag){
		if(!arguments.length)
			return withTargetPaths
		withTargetPaths = flag

		return my
	}
	my.on = function(){
		//attach extra arguments 
		let value = dispatcher.on.apply(dispatcher, arguments)
		return value === dispatcher ? my : value
	}
	return my
}

export default scatter