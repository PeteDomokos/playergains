import * as d3 from 'd3'
import { addWeeks } from '../TimeHelpers'
/**
* Returns an object containing the size information for the d3 components to use  
* @param {number} 
* @param {number} 
**/
//optional 3rd param to specify x or y
export const boundsChanged = () =>{
	//const prevBounds = Object.create(oldBounds[key])
	//const newDataBounds = calcDataBounds(datapoints.filter(d => !d.projected))
	//todo - finish this
	return true
}
/**
* Returns an object containing the size information for the d3 components to use  
* @param {number} 
* @param {number} 
**/
export const calcChartSizes = (width, height, chart) => {
	let margin
	if(window.innerWidth < 501)
		margin = {top:height *0.19, bottom:height *0.18, left:width *0.11, right:width*0.05}
	else if(window.innerWidth < 980){
		const topMult = chart === 'progress' ? 0.03 : 0.08
		margin = {top:height * topMult, bottom:height *0.27, left:width *0.2, right:width*0.1}
	}
	else
		margin = {top:height *0.15, bottom:height *0.23, left:width *0.17, right:width*0.08}
	return {
		height:height,
		width:width,
		//no hSF or wSF atm
		chartHeight:height - margin.top - margin.bottom,
		chartWidth:width - margin.left - margin.right,
		margin:margin
	}
}
export const calcDataBounds = datapoints => {
	//bounds
	let minX, maxX, minY, maxY
	if(datapoints){
		minX = d3.min(datapoints, d => d.xValue)
		maxX = d3.max(datapoints, d => d.xValue)
		minY = d3.min(datapoints, d => d.yValue)
		maxY = d3.max(datapoints, d => d.yValue)
	//bounds
	}
	return { minX, maxX, minY, maxY }			
}

export const calcScales = (ds, bounds, yNumberOrder, sizes, chartType) => {
	const { chartWidth, chartHeight, margin } = sizes
	//last actual data
	let actualDs, lastActualDate
	if(ds){
		actualDs = ds.filter(d => !d.projected && !d.isTarget)
		lastActualDate = actualDs.length != 0 ? actualDs[actualDs.length - 1].xValue : undefined
	}
	//bounds
	const { minX, maxX, minY, maxY, worstPossY, bestPossY, zones } = bounds

	const scaleBounds = {
		...calcXScaleBounds(bounds, lastActualDate), 
		...calcYScaleBounds(bounds, yNumberOrder)}
	const { lowerX, upperX, lowerY, upperY } = scaleBounds

	//scales
	var x
	if(chartType.includes('beeSwarm')){
		//fake scale just for zone dimension purposes
		x = d3.scaleLinear()
			.domain([0, 1])
			.range([margin.left, margin.left+ chartWidth])
	}
	else{
		x = d3.scaleTime()
			.domain([lowerX, upperX])
			.range([margin.left, chartWidth + margin.left])
	}

	const y = d3.scaleLinear()
			.domain([lowerY, upperY])
			.range([chartHeight + margin.top, margin.top])

	const translateExtent = calcTransExt(lastActualDate, x, y, scaleBounds, bounds, margin)
	
	return {
		x:x,
		y:y,
		translateExtent:translateExtent
	}
}

export const calcTranslateExtent = (ds, bounds, scales, yNumberOrder, margin) =>{
	const { x, y } = scales
	let actualDs, lastActualDate
	if(ds){
		actualDs = ds.filter(d => !d.projected && !d.isTarget)
		lastActualDate = actualDs.length != 0 ? actualDs[actualDs.length - 1].xValue : undefined
	}
	//bounds
	const { minX, maxX, minY, maxY, worstPossY, bestPossY, zones } = bounds

	const scaleBounds = {
		...calcXScaleBounds(bounds, lastActualDate), 
		...calcYScaleBounds(bounds, yNumberOrder)}
	const { lowerX, upperX, lowerY, upperY } = scaleBounds
	return calcTransExt(lastActualDate, x, y, scaleBounds, bounds, margin)
}

//todo - learn how to calc TE lowerXBound properly - atm the upper and lowers must be equal
// - is that always the case? how then to elimate unwanted axes ticks?
//yBounds then need to be something like 
//lower --- yScale.range()[0] + [Math.max(Math.abs(yScale(lowerY), yScale(upperY))) 
//upper --- yScale.range()[1] - [Math.max(Math.abs(yScale(lowerY), yScale(upperY)))
//(remember its reversed on yAxis so lower is plus) 
const calcTransExt = (lastActualDate, xScale, yScale, scaleBounds, bounds, margin) => {
	const { lowerX, upperX, lowerY, upperY } = scaleBounds
	const { bestPossY, worstPossY } = bounds

	const now = new Date()
	//no data - in this case, lower and upper Ys are set by zones, or 0 to 100
	if(!lastActualDate){
		//console.log('case a: no actual');
		return [
			[xScale(addWeeks(-1, now)), yScale(upperY)],
			[xScale(addWeeks(260, now)), yScale(lowerY)]
		]
	}
	else if(!bestPossY){
		//console.log('case b: no zone bounds');
		return [
			[xScale(lowerX), yScale(upperY)],
			[xScale(addWeeks(156, lastActualDate)), yScale(lowerY)]
		]
	}
	else if(bestPossY > worstPossY){
		//console.log('case c: low-to-high zones bounds');
		return [
			[xScale(addWeeks(-156, lowerX)), yScale(bestPossY)],
			[xScale(addWeeks(156, lastActualDate)), yScale(worstPossY)]
		]
	}
	else {
		//console.log('case c: high-to-low zones bounds');
		return [
			[xScale(lowerX), yScale(worstPossY)],
			[xScale(addWeeks(156, lastActualDate)), yScale(bestPossY)]
		]
	}
}

export const calcXScaleBounds = (bounds, lastActualDate) => {
	//bounds
	const { minX, maxX } = bounds
	
	//The possYs will be undefined if no chart zone
	//helper vars
	const now = new Date()
	let lowerX, upperX

	if(!lastActualDate){
		lowerX = now
		upperX = addWeeks(52, now)
	}else{
		lowerX = addWeeks(-1, minX) //addWeeks(-6, now)
		upperX = addWeeks(8, lastActualDate) //addWeeks(6, now)
	}
	return {lowerX:lowerX, upperX:upperX}
}
export const calcYScaleBounds = (bounds, yNumberOrder) =>{
	//bounds
	const { minY, maxY, worstPossY, bestPossY, zones } = bounds
	let lowerY, upperY

	var zoneRange
	if(bestPossY || bestPossY === 0)
		zoneRange = Math.abs(bestPossY - worstPossY)
	else{
		//ensure the range will be at least double the highest score
		const highestAbs = Math.max(Math.abs(minY), Math.abs(maxY))
		zoneRange = (highestAbs * 2) != 0 ? highestAbs * 3 : 100
	}

	//y limits depend on numberOrder
	//note- doesnt work for negative scores as it is atm
	if(minY || minY === 0){
		if(yNumberOrder == 'high-to-low'){
			lowerY = maxY +(0.1 * zoneRange)
			upperY = minY -(0.1 * zoneRange)

			if(bestPossY && bestPossY < lowerY)
				lowerY = worstPossY
			if(worstPossY && worstPossY > upperY)
				upperY = bestPossY

		}else{
			lowerY = minY -(0.1 * zoneRange)
			upperY = maxY +(0.1 * zoneRange)

			if(worstPossY && worstPossY > lowerY)
				lowerY = worstPossY
			if(bestPossY && bestPossY < upperY)
				upperY = bestPossY
		}
	}
	else if(bestPossY || bestPossY === 0){
		//no actual data (or targets) but do have zones 
		if(yNumberOrder == 'high-to-low'){
			lowerY = bestPossY
			upperY = worstPossY
		}else{
			lowerY = worstPossY
			upperY = bestPossY
		}
	}
	else{
		//no actual data(or targets) or zones
		lowerY = 0
		upperY = 100
	}

	return {lowerY:lowerY, upperY:upperY}
}

export const calcZoneBounds = zones => {
	//bounds - legacy stuff here
	let worstPossY, bestPossY, zoneBounds

	if(zones && zones[0]){
		worstPossY = zones[0].from
		bestPossY = zones[zones.length-1].to
		zoneBounds = [zones[0].from, zones[zones.length-1].to]
	}
	//note - worst and best are legacy - the zones array will replace
	return { worstPossY, bestPossY, zones:zoneBounds }			
}


export const updateChartSizes = (container, widthOveride, heightOveride) =>{
	//NEW SIZES
	let width, height
	if(widthOveride)
		width = widthOveride
	else if(container)
		width = container.getBoundingClientRect().width
	else
		width = 500

	if(heightOveride)
		height = heightOveride
	else if(container)
		height = container.getBoundingClientRect().height
	else
		width = 500
	return calcChartSizes(width, height)
}

