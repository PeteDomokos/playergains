import * as d3 from 'd3'
import { diffInWeeks , addWeeks} from '../TimeHelpers'
import { onlyUnique } from '../../../../util/helpers/ArrayManipulators'


/**
*  
*
**/
function ScatterData(datapoints){
	this.datapoints = datapoints
}

ScatterData.prototype.addOrRemoveTargets = function(keys, measure, allDatapoints){
	//helpers
	const addTargets = () =>{
		const requiredTargets = allDatapoints.filter(d => d.isTarget)
								         .filter(d => keys.includes(d.key))
		const currentTargetIds = this.datapoints.filter(d => d.isTarget)
											  .map(d => d._id)
		const targetsToAdd = requiredTargets.filter(d => !currentTargetIds.includes(d._id))
		this.datapoints = [...this.datapoints, ...targetsToAdd]
	}
	const removeTargets = () =>{
		this.datapoints = this.datapoints.filter(d => !d.isTarget)
	}
	if(measure == 'percent-change-pw'){
		removeTargets()
	}else{
		addTargets()
	}
	return this
}

ScatterData.prototype.filterForKeys = function(keys, allDatapoints){
	if(keys && allDatapoints[0] && allDatapoints[0].key){
		const currentKeys = this.datapoints.map(d => d.key).filter(onlyUnique)
		const keysToAdd = keys.filter(key => !currentKeys.find(k => k === key))
		//retrieve points from mutable store
		const datapointsToAdd = keysToAdd
			.map(key => allDatapoints.filter(d => d.key === key))
			.reduce((a,b) => [...a, ...b], [])

		const keysToRemove = currentKeys.filter(key => !keys.includes(key))
		//remove points from mutable datapoints
		const remainingDatapoints = this.datapoints.filter(d => 
			!keysToRemove.includes(d.key))
		this.datapoints = [...remainingDatapoints, ...datapointsToAdd]
	}
	return this
}

ScatterData.prototype.assignAndFilterDate = function(startDate){
	this.datapoints = this.datapoints
		.map(d =>({ ...d, date:(d.date ? d.date : new Date(d.eventDate)) }))
		.filter(d => !startDate || startDate.getTime() < d.date.getTime())
		.sort((d1, d2) => d1.date - d2.date)
	return this
}

ScatterData.prototype.clearValues = function(){
	this.datapoints = this.datapoints.map(d =>({
		...d,
		xValue:undefined,
		yValue:undefined
	}))
	return this
}

ScatterData.prototype.addPercentagesIfNeeded = function(valueFormat, measure, keyItems){

	if(measure === 'actual'){
		return this
	}

	//helpers
	const getValue = d =>{
		if(d)
			//3 cases (value is directly on d / value is in an object on d, value is in a dv item)
		if(d[valueFormat])
			return d[valueFormat].value || d[valueFormat] 
		return d.dataValues.find(dv => dv.key === valueFormat).value
	}
	//startvalue undefined if no actual values (but may still have a target d)
	const calcPercentChange = (startValue, value) => {
		//helper
		const percentChange = (from, to) =>{
			if(from === 0)
				from += 0.00001
			if(from && to)
				return ((to - from) / from ) * 100
		}
		//if startDate undefined, d must be target with no actual
		//=> use 2% per week from current time (todo - use quadratic so grad slows)
		if(!startValue){
			const weeks = diffInWeeks(new Date(), new Date(d.eventDate))
			return weeks * 2
		}else
			return percentChange(startValue, value)
	}

	const addPercentage = (startValue, d) =>{
		const pcKey = measure == 'percent-change' ? 'pcChange' : 'pcChangeWeekly'
		//3 cases
		if(d[valueFormat] && d[valueFormat].value){
			//value stored in object on d
			if(d[valueFormat][pcKey] || d[valueFormat][pcKey] === 0)
				return d
			else{
				return {
					...d, 
					[valueFormat]:{
						...d[valueFormat], 
						[pcKey]:calcPercentChange(startValue, d[valueFormat].value)
					}
				} 
			}
		}
		else if(d[valueFormat]){
			//value stored directly on d
			if(d[pcKey] || d[pcKey] === 0)
				return d
			else
				return {...d, [pcKey]:calcPercentChange(startValue, d.value)}
		}
		else{
			//values are stored in dataValues array item
			const dv = d.dataValues.find(dv => dv.key === valueFormat)
			if(dv[pcKey] || dv[pcKey] === 0)
				return d
			else{
				const newDataValues = d.dataValues
					.map(dv => ({ ...dv, [pcKey]:calcPercentChange(startValue, dv.value)}))
				return {...d, dataValues:newDataValues} 
			}
		}
	}
	const addPercentagesFromPrevious = ds =>{
		return ds.map((d,i) => {
			//for first value, use same value so it becomes 0%
			const previousValue = i == 0 ? getValue(d) : getValue(ds[i-1])
			return addPercentage(previousValue, d)
		})
	}
	const addPercentagesFromStart = ds =>{
		if(ds.length === 0)
			return []
		const firstDate = d3.min(ds.filter(d => d.key === ds[0].key) , d => d.date)
		const firstD = firstDate ? 
			ds.find(d => d.date.getTime() === firstDate.getTime()) : undefined
		const firstValue = getValue(firstD)

		return ds.map(d => addPercentage(firstValue, d))
	}

	const addPercentages = ds =>{
		if(measure == 'percent-change'){
			return addPercentagesFromStart(ds)
		}
		else
			return addPercentagesFromPrevious(ds)
	}

	const actualDs = this.datapoints.filter(d => !d.projected && !d.isTarget)
	const nonProjectedDs = this.datapoints.filter(d => !d.projected)
	const projectedDs = this.datapoints.filter(d => d.projected)
	var amendedDs
	if(keyItems){
		amendedDs = keyItems
			.map(it =>
				addPercentages(nonProjectedDs.filter(d => d.key === it.key)))
			.reduce((a,b) => [...a,...b])
	}
	else
		amendedDs = addPercentages(nonProjectedDs)

	this.datapoints = [...amendedDs, ...projectedDs]
	return this
}
//doesnt clear the weekly change values, just the total change
ScatterData.prototype.clearPercentages = function(){
	this.datapoints = this.datapoints.map(d =>{
		const newDataValues = d.dataValues.map(dv => ({
			...dv,
			pcChange:'',
			pcChangeWeekly:''
		}))
		return {...d, dataValues:newDataValues}
	})
	return this
}

/**
*   measure and valueFormat are just strings (ie the keys)
*   refresh - Boolean - will be true if valueFormat or measure has changed
**/
ScatterData.prototype.format = function(valueFormat, measure, keyItems){
	//helpers
	//get the object that wraps the value
	const valueWrapper = d =>{
		if(d)
		//3 cases
		if(d[valueFormat] && d[valueFormat].value)
			return d[valueFormat]
		if(d[valueFormat])
			return d
		return d.dataValues.find(dv => dv.key === valueFormat)
	}
	
	const dsToFormat = this.datapoints.filter(d => !d.projected)

	const dsAlreadyFormatted = this.datapoints.filter(d => d.projected)

	const actualDs = this.datapoints.filter(d => !d.isTarget && !d.projected)
	let lastActualDs
	if(!keyItems)
		lastActualDs = [actualDs.slice(-1)]
	else
		lastActualDs = keyItems
			.map(it => actualDs.filter(d => d.key == it.key).slice(-1))
			.reduce((arr1, arr2) => [...arr1, ...arr2], [])

	const isLastActual = d => lastActualDs.find(point => point._id == d._id)
	const newFormattedDs = dsToFormat.map((d,i) => {
		//x - can either be a date or xValue if available, or defaults to numbers
		const xValue = d.date ? d.date : d.xValue ? d.xValue : i+1
		const yValueWrapper = valueWrapper(d)
		let yValue
		if(measure === 'actual')
			yValue = yValueWrapper.value
		else if(measure == 'percent-change')
			yValue = yValueWrapper.pcChange
		else
			yValue = yValueWrapper.pcChangeWeekly
		return {
			...d, 
			yValue:+yValue, //coerce with +
			xValue:xValue,
			isLastActual:isLastActual(d) ? true : false,
		}
	})
	//get rid of any erroneous data
	.filter(d => (d.xValue || d.xValue === 0) && (d.yValue || d.yValue === 0))

	this.datapoints = [...dsAlreadyFormatted, ...newFormattedDs]
	return this

}

/**
*
**/

ScatterData.prototype.addProjections = function(projector, itemKeys, bounds, measure){
	if(projector && measure !== 'percent-change-pw'){
		//helper
		const alreadyHasProjections = data => data.filter(d => d.projected).length > 0

		const nonTargetData = this.datapoints.filter(d => !d.isTarget)
		let projections
		if(itemKeys){
			projections = itemKeys
				.map(it => nonTargetData.filter(d => d.key == it.key))
				.map(itemKeyDs => {
					if(alreadyHasProjections(itemKeyDs)){
					 	return []
					}
					return projector.create(itemKeyDs, bounds)
				})
				.reduce((arr1,arr2) => [...arr1, ...arr2], [])
		}
		else{
			projections = projector.create(nonTargetData, bounds)
		}
		this.datapoints = [...this.datapoints, ...projections]
	}
	return this
}
ScatterData.prototype.removeProjections = function(){
	this.datapoints = this.datapoints.filter(d => !d.projected)
	return this
}

export default ScatterData