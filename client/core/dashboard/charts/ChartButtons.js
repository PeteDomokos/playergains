import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'
import _ from 'lodash'
import Button from '@material-ui/core/Button'

/**
* 
**/
class ChartButtons extends Component{
	constructor(props){
		super(props)
	} 
	static propTypes ={
	    clickHandlers:PropTypes.object,
	    config:PropTypes.object
	}
	render(){
		const {open, setOptionGroupActive, setOptionSelected, chartKey } = this.props
		const { optionGroups, fillScale} = this.props.config

		const optionColour = opt =>{
			if(!opt.selected)
				return 'transparent'
			//check if player
			if(chartKey.includes('players') && opt.surname)
				return fillScale(opt._id)
			//check if dataset
			else (!chartKey.includes('players') && opt.nrParticipants)
				return fillScale(opt.key)
			return 'red'
		}

		const optGroupActive = optionGroups.find(opt => opt.active)

		return(
			<div className ='ctrls-cont'>
				<ul className={'not-ss ctrls '+(open ? 'open':'')}>
					{optionGroups.map(grp =>
						<li className='opt-grp-ctrls' key={'opt-grp-'+grp.key}>
							<Button 
								className={'opt-grp-btn '+(grp.active ? 'active':'')} 
								onClick={() => setOptionGroupActive(grp.key)}>
								{grp.name}</Button>

							{grp.active && <Options options={grp.options}
											setSelected={setOptionSelected(grp.key)}
											colour={optionColour}/>}	
						</li>)}
				</ul>
				<div className={'not-ls not-ms ctrls '+(open ? 'open':'')}>
					<ul className='opt-grps'>
						{optionGroups.map(grp =>
							<li className='opt-grp-btn-cont' key={'opt-grp-'+grp.key}>
								<Button 
									className={'opt-grp-btn '+(grp.active ? 'active':'')} 
									onClick={() => setOptionGroupActive(grp.key)}>
									{grp.name}</Button>
							</li>)}
					</ul>
					{optGroupActive && <Options options={optGroupActive.options}
										setSelected={setOptionSelected(optGroupActive.key)}
										colour={optionColour} />}	
				</div>
			</div>
		)
	}
}

ChartButtons.defaultProps = {
	optionGroups:{}
}

const Options = ({options, setSelected, colour}) =>
	<ul className={'opts '+(open ? 'open':'')} >
		{options.map(opt =>
			<li key={'opt-'+opt.key} className='opt-btn-cont' 
				onClick={() => setSelected(opt.key)}>
				<Button className='opt-btn' 
					style={{backgroundColor:colour(opt)}}>
					{opt.name}</Button>
			</li>)}
	</ul>


export default ChartButtons



