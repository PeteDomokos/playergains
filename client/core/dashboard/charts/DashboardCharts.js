import React, {Component} from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import * as d3 from 'd3'
import IconButton from '@material-ui/core/IconButton'
import Settings from '@material-ui/icons/Settings';
//general
import ChartButtons from './ChartButtons'
import { activeGroupsChanged, findChanged, initialiseOptions, chartChanged, datasetsChanged, findSelected } from './OptionGroupHelpers'
//scatter
import scatter from './scatter/Scatter'
import updateScatterCharts from './scatter/UpdateChart'
import { updateChartSizes, updateValueOptions } from './scatter/ChartHelpers'

/**
* A Scatter or BeeSwarm Chart which shows the progress over time for 
* the scores of players in datasets
**/

class DashboardCharts extends Component{
	constructor(props){
		super(props)
		this.initChartValues = this.initChartValues.bind(this)
		this.setOptionsShown = this.setOptionsShown.bind(this)
		this.setOptionGroupActive = this.setOptionGroupActive.bind(this)
		this.setOptionSelected = this.setOptionSelected.bind(this)
		this.state = {
			myChart:scatter(),
			initValuesSet:false,
			chartRendered:false,
			chartStateUpdated:false,
			latestPrimaryChange:'',
			optionsAreShown:false,
			moreOptionsShown:false,
			optionGroups:[],
			optionsDisabled:false,
			currentPopup:'',
			fillScales:{},
			widthOveride:'',
			heightOveride:''
		}
	} 
	static propTypes ={
	    item: PropTypes.object
	}
	/**
    * Calls function that initialises state values based on props, and sets a timeout for window size changes
    **/
	componentDidMount(){

		this.initChartValues()
		//WINDOW CHANGES - sizes will change
		window.addEventListener('resize', () =>{
			setTimeout(() =>{
				console.log('processing resize....client height',this['cont-'+0].getBoundingClientRect().height )
				this.setState({widthOveride:'', heightOveride:''})
			}, 600)
		})
	}
	/**
    * Determines latest svg sizes, and passes them along with other info about state changes to render methods
    **/
	componentDidUpdate(prevProps, prevState){
		const { myChart, optionGroups, widthOveride, heightOveride, fillScales } = this.state
		//check if an optionGroups became active, as no need to update chart
		if(activeGroupsChanged(optionGroups, prevState.optionGroups))
			return
		//optionsGroupActive hasn't changed, so it must be a chart-related change
		const changed = findChanged(optionGroups, prevState.optionGroups)
		const chart = findSelected('chart', optionGroups).key

		//helper
		const updateCharts = () =>{
			const sizes = updateChartSizes(this['cont-'+0], widthOveride, heightOveride, chart)
			//get the svg classes
			const datasets = findSelected('datasets', optionGroups)
			const svgs = datasets.map((dSet,i) => 'svg.svg-'+i)

			updateScatterCharts(svgs, myChart, optionGroups, fillScales, sizes, changed)
			//ease in svg
			d3.selectAll('svg.svg').style('opacity',1)
		}

		if(changed === 'chart' || (chart === 'compare' && changed === 'datasets'))
			setTimeout(() =>{
				updateCharts()
			},500)
		else{
			updateCharts()
		}
	}
	/**
	*  Removes chart from DOM and removes resize listener
	**/
	componentWillUnmount(){
		d3.selectAll('svg.svg').selectAll("*").remove()
		window.removeEventListener('resize', () => {})
	}
	/**
	*  Sets or resets state to initial values based on props 
	*  Except optionsDisabled, which is handled in onAnimationChange()
	**/
	initChartValues(){
		const { group, charts, measures, init } = this.props
		//INIT VALUES
		let initChartValues = {}

		//set selected values for all options
		const initialisedOptionGroups = initialiseOptions(group, charts, measures, init)
		initChartValues.optionGroups = initialisedOptionGroups

		initChartValues.fillScales = {
			players:d3.scaleOrdinal()
				.domain(group.players.map(p => p._id))
				.range(["blue", "red", "yellow", "orange", "navy","green"]),
			datasets:d3.scaleOrdinal()
				.domain(group.datasets.map(d => d.key))
				.range(["blue", "red", "yellow", "orange", "navy","green"])
		}
		//reset if needed
		initChartValues.optionsAreShown = false
		initChartValues.moreOptionsShown = false
		initChartValues.optionsRendered = false
		initChartValues.chartRendered = false
		initChartValues.initValuesSet = true
		this.setState(initChartValues)
	}
    /**
    * Switches the flag on and off to reveal the option controls, 
    **/
	setOptionsShown(){
		//overide height or width because transition of options bar takes time
		//not small-screen, so options are down the side
		if(window.innerWidth > 500){
			const currW = this['cont-'+0].getBoundingClientRect().width
			this.setState(prevState =>{
				return {
					optionsAreShown:!prevState.optionsAreShown,
					widthOveride:prevState.optionsAreShown ? currW + 130 : currW - 130
				}
			})
		}else{
			//small-screen options are at bottom
			const currH = this['cont-'+0].getBoundingClientRect().height
			//overide width because transition of options bar takes time
			this.setState(prevState =>{
				return {
					optionsAreShown:!prevState.optionsAreShown,
					heightOveride:prevState.optionsAreShown ? currH + 110 : currH - 110
				}
			})

		}
	}
    /**
    * Updates OptionGroups active property
    * - only 1 group can be active at one time
    **/
	setOptionGroupActive(key){

		if(!this.state.optionsDisabled){
			this.setState(prevState =>{
				return {
					optionGroups:prevState.optionGroups.map(g =>({
						...g, active:(g.key == key ? true:false)})),
					widthOveride:'',
					heightOveride:''
				}
			})
		}
	}
   /**
   	*  Processes the click of option. 
   	*
    */
	setOptionSelected(grpKey){
		const { optionGroups, myChart, optionsDisabled } = this.state
		const update = newState => this.setState(newState)
		return function(optKey){
			if(!optionsDisabled){
				//only 1 group active at one time but this may change
				const optionGroup = optionGroups.find(grp => grp.key == grpKey)
				const clickedOption = optionGroup.options.find(opt => opt.key === optKey)
				const optionsSelected = optionGroup.options.filter(opt => opt.selected)

				if(clickedOption.selected && optionsSelected.length == 1){
					alert('Cannot remove last option. Choose another option first.')
					return
				}
				let newOptions, sortedNewOptions
				if(['datasets', 'players'].includes(optionGroup.key)){
					newOptions = optionGroup.options.map(opt =>{
						if(opt.key === optKey)
							return {...opt, selected:!opt.selected}
						else
							return opt
					})
				}else{
					//limit to one option by de-selecting current
					newOptions = optionGroup.options.map(opt =>{
						if(opt.key === optKey){
							//can only select, cannot deselect
							if(!opt.selected){
								return {...opt, selected:true}
							}
							else{
								return opt
							}
						}
						//all other options will be deselected
						return {...opt, selected:false}
					})
				}
				//if dataset added, put it at front
				if(grpKey == 'datasets'){
					const datasetClicked = newOptions.find(opt => opt.key == optKey)
					const otherDatasets = newOptions.filter(opt => opt.key != optKey)
					if(datasetClicked.selected)
						sortedNewOptions = [datasetClicked, ...otherDatasets]
				}
				if(!sortedNewOptions)
					sortedNewOptions = newOptions

				//add options to optionGroup
				const newOptionGroups = optionGroups
					.map(grp => (grp.active ? {...grp, options:sortedNewOptions} : grp))

				update({
					optionGroups:newOptionGroups,
					widthOveride:'',
					heightOveride:''
				})
			}
		}
	}

	render(){
		const { optionGroups, fillScales, optionsAreShown } = this.state
		const chartKey = findSelected('chart', optionGroups).key || ''
	
		let optionsConfig = {
			optionGroups:this.state.optionGroups,	 
			fillScale:chartKey.includes('players') ? fillScales.players : fillScales.datasets
		}
		return(
			<div className='svg-wrapper'>
				<IconButton className={'show-ctrls'} onClick={this.setOptionsShown}> 
					<Settings/>
   				</IconButton>

				<ChartButtons 
					open={optionsAreShown}
					config={optionsConfig} 
					setOptionGroupActive={this.setOptionGroupActive} 
					setOptionSelected={this.setOptionSelected}
					chartKey={chartKey} />
				<div className='svgs-outer-cont'>
					{chartKey.includes('players') ? 
						<div className='svgs-inner-cont'>
							{findSelected('datasets', optionGroups).map((dataset, i) =>{
								const contClassName = 'svg-cont svg-cont-'+i +
										(chartKey.includes('beeSwarm') ? ' beeSwarm' : ' scatter')
										+(this.state.optionsAreShown ? ' short':'')

								const svgClassName = 'svg svg-'+i +' '+chartKey+' '+dataset.key
								const refValue = 'cont-'+i
								return(
									<div className={contClassName} key={'svg-cont-'+i}
										ref={svgContainer => this[refValue] = svgContainer} >
									
										<svg className={svgClassName} 
											style={{opacity:0, transition:'opacity 200ms ease-in 0ms'}}>
										</svg>
									</div>)}
							)}
						</div>
						:
						<div className='svgs-inner-cont'>
							{findSelected('players', optionGroups).map((player, i) =>{
								const contClassName = 'svg-cont svg-cont-'+i +
										(chartKey.includes('beeSwarm') ? ' beeSwarm' : ' scatter')
										+(this.state.optionsAreShown ? ' short':'')
								const svgClassName = 'svg svg-'+i +' '+chartKey+' '+player._id
								const refValue = 'cont-'+i
								return(
									<div className={contClassName} key={'svg-cont-'+i}
										ref={svgContainer => this[refValue] = svgContainer} >
									
										<svg className={svgClassName}
											style={{opacity:0, transition:'opacity 200ms ease-in 0ms'}}>
										</svg>
									</div>)}
							)}
						</div>
					}

				</div>
			</div>
		)
	}
}

DashboardCharts.defaultProps = {
}

export default DashboardCharts


