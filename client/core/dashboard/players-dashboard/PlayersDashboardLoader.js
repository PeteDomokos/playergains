import React, { useEffect }  from 'react'
import PlayersDashboard from './PlayersDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'

/**
*
**/
const PlayersDashboardLoader = ({dashboard, loading, setFilter, LoadDatapoints}) => {
	console.log("PlayersDashboardLoader")
	useEffect(() => {
		if(!loading){
			//todo - check grouos themselevs are all loaded
			//...
			//if groups are loaded, now load datapoints....
			const datasets = flattenedDatasets(dashboard.groups)
				.filter(dataset => dashboard.datasets.find(d => d._id === dataset._id))

			const unloadedDatasets = datasets.filter(d => !d.datapoints)
			if(unloadedDatasets.length > 0){
				console.log("Loader loading datapoints...")
				//todo - send playerIds as an arg so only their points are loaded
				loadDatapoints(unloadedDatasets)
			}
		}
	}, [])
	return (
		<PlayersDashboard dashboard={dashboard} setFilter={setFilter} loading={loading} />)
}

export default PlayersDashboardLoader