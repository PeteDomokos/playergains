import React, { useEffect }  from 'react'
import { mergedDatasets, playerDatasets, areSame} from '../../datasets/DatasetHelpers'
import { commonGroupIds, flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const PlayersDashboard = ({dashboard, loading, setFilter}) => {
	console.log("PlayersDashboard dashboard", dashboard)
	
	const playersWithDatasets = dashboard.players.map(player =>{
		//need only the datasets that are selected
		//console.log("playerDatasets for "+player.firstName, playerDatasets(player, dashboard.groups))
		const requiredDatasets = playerDatasets(player, dashboard.groups)
			.filter(dataset => dashboard.datasets.find(d => areSame([dataset, d])))
		//console.log("requiredDatasets for "+player.firstName, requiredDatasets)
		return {...player, datasets:requiredDatasets} 
	})
	console.log("playersWithDatasets", playersWithDatasets)

	/*
	//filter the datasets selected
	const allDatasets = flattenedDatasets(groups)
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	//merge standard sets
		//todo - check if we still need this now, and if so, we shouldnt need to separate custom out 
	const mergedStandard = mergedDatasets(allDatasets.filter(d => d.isStandard))
	//only keep custom sets from common groups
	const commonGroups = commonGroupIds(players)
		.map(id => groups.find(g => g._id === id))

	const customDatasetsFromCommonGroups = 
		flattenedDatasets(commonGroups).filter(d => !d.isStandard)
		
	//put together and filter to ones selected
	const datasets = 
		[...mergedStandard, ...customDatasetsFromCommonGroups]
			.filter(dataset => dashboard.datasets
				.find(d => d._id === dataset._id))
	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature
	*/

	return (
		<div style={{margin:30, backgrouncolor:'white'}}>
		Players Dashboard
		</div>)
}

export default PlayersDashboard