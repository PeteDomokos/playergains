import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import PlayersDashboardLoader from './PlayersDashboardLoader'
import { filterUniqueByProperty } from '../../../util/helpers/ArrayManipulators'
import { getPlayersGroups } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("PlayersDashboardContainer")
	const uniquePlayersGroups = 
			getPlayersGroups(state.dashboard.players, state.storedItems.groups)

	const loadedDashboard = {...state.dashboard, groups:uniquePlayersGroups}
	return({
		dashboard: loadedDashboard,
		loading:state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	loadDatapoints(groupId, datasetId){
		dispatch(fetchDatapoints(groupId, datasetId))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const PlayersDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(PlayersDashboardLoader)

export default PlayersDashboardContainer