import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'

/**
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
**/
class PlayerTests extends Component{
	constructor(props){
		super(props)
		this.state = {
			
		}
	}
	static propTypes ={
	    item: PropTypes.object,
	 }
	static defaultProps = {
  	}
  	/**
	* Desc ...
	**/
	componentDidMount(){
		console.log("CDM PlayerTests")
	}
	/**
  	* Desc ... 
  	**/
	render(){
		let { id } = this.props.item
		return(
			<div className='player-tests' style={{color:'white', margin:30}}>
				PlayerTests Component not implemented yet
				<br/>
				 - view info and videos about all available tests 
				<br/>
				 - select a test from a bank of tests to assign to a group
				<br/>
				- create a custom test and assign it to a group
			</div>
		)
	}
}

export default PlayerTests