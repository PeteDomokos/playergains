import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'

/**
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
**/
class AddPlayerTest extends Component{
	constructor(props){
		super(props)
		this.state = {
			
		}
	}
	static propTypes ={
	    item: PropTypes.object,
	 }
	static defaultProps = {
  	}
	componentDidMount(){
		console.log("CDM AddPlayerTest")
	}
	render(){
		let { id } = this.props.item
		return(
			<div className='add-player-tests'>
				AddPlayerTest component not implemented yet
				<br/>
				 - select a test from a bank of tests to assign to a group
				<br/>
				- create a custom test and assign it to a group
			</div>
		)
	}
}

export default AddPlayerTest