import React, { useEffect }  from 'react';
import GroupDashboard from './GroupDashboard';
//import { flattenedDatasets } from '../../../groups/GroupHelpers';
/**
*
**/
const GroupDashboardLoader = ({dashboard, loading, setFilter, loadGroup, loadDatapoints}) => {
	const { group, datasets } = dashboard
	const requiredDatasets = (group && datasets) ? 
		datasets.map(d => datasets.find(dset => dset._id === d._id)) :
		group.datasets

	useEffect(() => {
		//for now, no parents so just one group
		//const unloadedGroups = dashboard.groups.filter(g => !g.datasets);
		if(!group.datasets){
			loadGroup(dashboard.group._id);
		}
		else{
			//group is loaded - check datapoints
			const unloadedDatasets = requiredDatasets.filter(d => !d.datapoints);
			if(unloadedDatasets.length != 0){
				loadDatapoints(unloadedDatasets, groupId);
			}
		}
	}, []);
	return (
		<GroupDashboard group={{...group, datasets:requiredDatasets}}
			setFilter={setFilter} loading={loading} />)
}

export default GroupDashboardLoader