import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { HashRouter, Route, Switch, Redirect, withRouter } from 'react-router-dom'
import { Links } from '../../../util/LinksAndRoutes'
//children
//import { DashboardGateway } from '../../util/components/Gateways'
//import { DashboardMenu } from './DashboardMenus'
//import ProgressSVG from './charts/progress/ProgressSVG'
//import CompareSVG from './charts/compare/CompareSVG'
//import PlayerTests from './player-tests/PlayerTests'
//import TestData from './test-data/TestData'
//helpers and constants
//import { TestDataConstants } from '../../constants/TestDataConstants'
import auth from '../../../auth/auth-helper'

/**
* Handles retrieval of players data list from server 
* Passes playersdata onto the required chart components 
**/
/*
TODO - put this into container and handle server data via store)
TODO - for large screen sizes, render more than one chart at a time ie a proper data dashboard
*/
export class GroupDashboard extends Component{
  	constructor(props){
		super(props)
		this.state ={
			players:[],
			user:'',
			redirectToSignin: false
		}
	}
	
	render(){
    	if (this.state.redirectToSignin)
     		return <Redirect to='/signin'/>
     	//if (this.props.history.location.pathname == '/dashboard')
     		//return <Redirect to='/dashboard/progress'/>
     	const { user, group, dashboard, setFilter, loading } = this.props
     	console.log("Dashboard props", this.props)
     	//NOTE - GROUP FILTER WILL BE LINKS TO URLS
     	//THE OTHER FILTERS WILL BE BUTTONS THAT CALL SETFILTER FUNCTION
     	//dont need subgroup, just make all group Select components include subgroups,
     	// - make a reusable GroupSelect component with a includeSubgroups boolean prop

     	//todo - decide how to implement
     	const filtersChosen =  true
		return(

			<div style={{margin:30, padding:30, backgroundColor:'white', height:300}}>
				{!filtersChosen ? 
		          <DashboardGateway 
		          	groups={availableGroups} withSubgroups loadingGroup={loading.group}/>
		          :
				  <div>dashboard</div>
				}
			</div>
		)
	}
}
GroupDashboard.defaultProps = {
}
export default GroupDashboard

//export default withRouter(Dashboard)
