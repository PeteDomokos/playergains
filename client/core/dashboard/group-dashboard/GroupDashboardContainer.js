import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import GroupDashboardLoader from './GroupDashboardLoader'
import { getGroupParents } from "../../../groups/GroupHelpers"

import { fetchGroup } from '../../../actions/Groups'

/**
*   state.dashboard will itself contain the fully loaded group 
*   (and parents), which are loaded in here from storedItems.groups
*
*   the group (not parents) will also contain all required details
*   for it's players, eg photo, position,... in group.players

**/

const mapStateToProps = (state, ownProps) => {
	console.log('GDC state', state)
	const { groups, datasets } = state.dashboard
	//GROUP
	//group may be defined via dashboard selection or may be via params
	//only 1 selection for groupDashboard, and no parents for now so group will be [0][0]
	const groupId = groups[0] ? 
		state.dashboard.groups[0][0]['_id'] : ownProps.match.params.groupId

	console.log('groupId', groupId)

	const loadedGroup = state.storedItems.groups.find(g => g._id === groupId) || {_id:groupId}
	console.log('loadedGroup', loadedGroup)

	//copy loaded items into dashboard
	const loadedDashboard = {
		...state.dashboard, 
		group:loadedGroup
	}
	return({
		dashboard: loadedDashboard,
		loading:state.asyncProcesses.loading.group || state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	loadDatapoints(datasets, groupId){
		//dispatch(fetchDatapoints(datasets, groupId))
	},
	loadGroup(groupId){
		dispatch(fetchGroup(groupId))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const GroupDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupDashboardLoader)

export default GroupDashboardContainer