import React  from 'react'
import { Route, Switch }from 'react-router-dom'
//children
import DashboardCharts from '../charts/DashboardCharts'
//helpers
import { flattenedDatasets } from '../../../groups/GroupHelpers'

/**
*
**/

//for now, pass group through as there are no other dashboard options
//also no parent groups (parent daatsets will be merged into group.datasets)
const GroupDashboard = ({group, loading, setFilter}) => {
	console.log("GroupDashboard dashboard:", group)

	const availableMeasures = [
		{name:'Actual Scores', key:'actual'}, 
		{name:'% Change (total)', key:'percent-change'}, 
		{name:'% Change (weekly)', key:'percent-change-pw'}
		]
	//note - we are determing which chart is selected here,
	const charts = [
		{name:'Players Progress', key:'players-scatter'},
		{name:'Datasets Progress', key:'datasets-scatter'},
		{name:'Compare Players', key:'players-beeSwarm'},
		{name:'Compare Datasets', key:'datasets-beeSwarm'}
		]
	//todo - base default init on this
	const init = {
		chart:charts.find(chart => chart.key == 'players-scatter'),
		players:group.players ? [group.players.find(p => p.firstName === 'Denilson')] : undefined,
	}

	return (
		<div className='dashboard'>
			{(loading || !group.datasets) ?
				<div style={{color:'white', margin:30}}>
					loading data....
				</div>
				:
				<div>
					<DashboardCharts group={group} charts={charts}
						measures={availableMeasures} init={init} />
				</div>
			}
		</div>)
}
GroupDashboard.defaultProps = {
}

export default GroupDashboard