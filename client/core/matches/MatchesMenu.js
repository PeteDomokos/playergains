import React from 'react'
/**
*
**/
export const MatchesMenu = ({availableMatches}) => 
	<form id="matchAndPlayerSelection">
		<select name="matchSelection">
			{availableMatches.map(m => 
				<option key={m.id}>{m.id}</option>
			)}
		</select>
	</form>

MatchesMenu.defaultProps = {
	availableMatches:[]
}