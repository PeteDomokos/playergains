import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Route, Switch }from 'react-router-dom'
//styles 
import './assets/styles/main/main.css'
//children
import PageTemplateContainer from './PageTemplateContainer'
import HomeContainer from './core/containers/HomeContainer'
import About from './core/About'
import Contact from './core/Contact'
import SigninContainer from './auth/containers/SigninContainer'
import Signup from './user/Signup'
import UserRouter from './user/UserRouter'
import GroupContainer from './groups/containers/GroupContainer'
import CreateGroupContainer from './groups/containers/CreateGroupContainer'
import AddDatasetContainer from './core/datasets/add-dataset/AddDatasetContainer'
import AddDatapointContainer from './core/datasets/add-datapoint/AddDatapointContainer'
import DashboardRouter from './core/dashboard/DashboardRouter'
//import Groups from './groups/Groups'
//import Players from './players/Players'
//import Datasets from './core/datasets/Datasets'


/**
*  NOTE -  /uName is the users public profile page eg that clubs might look at
*  not their own personal home page there own personal home page is just '/' 
*  and whatever else, with teh fact tehy are authenticated
*  stored in teh store - no need to show in teh url that just confuses
**/

//todo - swap id params for unique name params, and get rid of things like /group
//because if its the group name then it will be obvious.
//for user, we have user name, for group, we have group-name but need to find a way to make it unique
class MainRouter extends Component {
  render() {
    return(
      <PageTemplateContainer>
        <Switch>
          <Route exact path="/" component={HomeContainer}/>
          <Route path="/about" component={About}/>
          <Route path="/contact" component={Contact}/>
          <Route path="/signin" component={SigninContainer}/>
          <Route path="/signup" component={Signup}/>

          <Route path="/user" component={UserRouter}/>
          <Route path="/group/:groupId" component={GroupContainer}/>

          <Route path="/groups/new" component={CreateGroupContainer}/>
          <Route path="/datapoints/new" component={AddDatapointContainer} />
          <Route path="/datapoints/new" component={AddDatasetContainer} />

          <Route path="/dashboard" component={DashboardRouter} />
          {/**<Route path="/groups" component={Groups}/>
          <Route path="/players" component={Players}/>
          <Route path="/datasets" component={Datasets}/>**/}
        </Switch>
      </PageTemplateContainer>)
  } 
}

export default MainRouter