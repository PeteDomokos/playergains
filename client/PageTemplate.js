import React, { Component, useState } from 'react'
import { render } from "react-dom"
import { withRouter} from 'react-router-dom'
import ReactPlayer from 'react-player'
import { Link } from 'react-router-dom'
import { slide as ElasticMenu } from 'react-burger-menu'
//material-ui
import IconButton from '@material-ui/core/IconButton'
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
//video
import VideoPlayer from './util/components/VideoPlayer'

import { MainMenuManager } from './core/Menus'
import { MainMenu, SubMenu } from './core/Menus'
import logo from './assets/images/logo.png'
import auth from './auth/auth-helper'
//Constants and Data
import homeSections from './constants/HomeSections'

//todo - pass in section as a param instead of using this method
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let sectionName = (history) =>{
	if(history.location.pathname == '/')
		return 'home'
	if(history.location.pathname.startsWith('/about'))
		return 'about'
	if(history.location.pathname.startsWith('/education'))
		return 'education'
	if(history.location.pathname.startsWith('/dashboard'))
		return 'dashboard'
	if(history.location.pathname.startsWith('/contact'))
		return 'contact'
	if(history.location.pathname.startsWith('/signin'))
		return 'signin'
	if(history.location.pathname.startsWith('/signup'))
		return 'signup'
	return ''
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let showHeader = (sectionName) => {
	if(["dashboard"].includes(sectionName))
		return false
	return true
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let showLogo = (sectionName) => {
	if(["dashboard"].includes(sectionName))
		return false
	return true
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let subMenuToShow = sectionName =>{
	if(["dashboard"].includes(sectionName))
		return 'dashboard'
	return undefined
}

const videoURL = sectionName =>{
	if(sectionName === 'dashboard'){
		return homeSections.find(sect => sect.title == 'Dashboard').content.videoURL
	}else{
		return homeSections.find(sect => sect.title == 'Team Set-Up').content.videoURL
	}
}
/**
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
* @return {any} props value
**/
export const PageTemplate = withRouter(({children, history, match}) =>{
	//check if user is signed in
	const userObj = auth.isAuthenticated()
   	const user = userObj ? userObj.user : undefined

   	//state
   	const [selectedVideoURL, selectVideo] = useState('')
   	const handleSelectVideo = () =>{
   		if(selectedVideoURL){
	      selectVideo('');
	    }else{
	      selectVideo(videoURL(sectionName(history)));
	    }
	    console.log('selectedVideoURL changed to ', selectedVideoURL)
   	}

	return(
		/*todo - move menu into here instead of menu-manager, 
		so if mobile then menu is not part of header*/
	<section className={"page"}>
		<section className={'header '+(showHeader(sectionName(history)) ? 'full-height':'zero-height')}>
			<Link to="/">
				<div className={'not-ls logo-cont image-cont '
					+(showLogo(sectionName(history)) ? 'full-height':'zero-height')}>
            		<img src={logo} />
          		</div>
          		<div className='not-ss not-ms logo-cont image-cont full-height'>
            		<img src={logo} />
          		</div>
        	</Link>
	    	<div className='not-ss not-ms menus-cont'>
	      		<MainMenu/>
	      		{/**subMenuToShow(sectionName(history)) && 
	      			<SubMenu menuName={subMenuToShow(sectionName(history))}/>**/}
	    	</div>
		</section>
		<div className={'not-ls '+(history.location.pathname.includes('skills-and-fitness') ? 'dark':'light')}>
	        <div className='bm-burger-button'></div>
	        <ElasticMenu right width={150}>
	          <MainMenu elastic/>
	        </ElasticMenu>
	    </div>
		<div className={'page-background-cont '
			+(showHeader(sectionName(history)) ? 'with-header ':'without-header ')
			+sectionName(history)} >
        </div>
        <section className='content'>
			{user && <VideoLink history={history} onSelect={handleSelectVideo} />}
			{selectedVideoURL && 
				<div style={{width:'100%', display:'flex', justifyContent:'center'}}>
		            <VideoPlayer url={selectedVideoURL} 
		                       handleCancel={handleSelectVideo}
		                       playing={true}/>
		         </div>}
			{children}
		</section>
		<section className='footer'>footer</section>
	</section>
	)}
)

const VideoLink = ({onSelect, history}) => 
	<IconButton onClick={onSelect} className={'vidLink vidLink-'+sectionName(history)}>
    	<HelpOutlineIcon className='vidLinkIcon' />
    </IconButton>


