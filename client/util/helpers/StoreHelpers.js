import { onlyUnique, filterUniqueByProperty } from './ArrayManipulators'
/**
*
**/
export const players = users => {
	if(!users)
		return undefined
	//todo - only select registered players
	return users.filter(u => true)//u.playerInfo.isRegistered)
}
export const coaches = users => {
	if(!users)
		return undefined
	return users.filter(u => u.coachInfo.isRegistered)
}

export const userGroupIds = shallowUser =>{
	if(!shallowUser)
		return undefined

	const { adminGroups, groupsFollowing, groupsViewed, playerInfo, coachInfo } = shallowUser
	const groupIds = [
		...adminGroups, ...groupsFollowing, ...groupsViewed,
		...playerInfo.groups, ...coachInfo.groups
		]
	return groupIds.filter(onlyUnique)
}
//some array elements returned may be undefined if group not in groups
export const userGroupsFromIds = (shallowUser, groups) =>{
	if(!shallowUser || !groups)
		return undefined
	const groupIds = userGroupIds(shallowUser) 
	return groupIds.map(id => groups.find(g => g._id === id))
}
export const mergedUserGroups = user =>{
	if(!user)
		return undefined
	const { adminGroups, groupsFollowing, groupsViewed, playerInfo, coachInfo } = user
	const groups = [
		...adminGroups, ...groupsFollowing, ...groupsViewed,
		...playerInfo.groups, ...coachInfo.groups]
	return filterUniqueByProperty("_id", groups)
}