import React, { Component, useState } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
import { SummarySection, selectorStyles, getName } from './Common'
import { renderActionButton } from '../Buttons'

//todo - make the selects order the players with usersPlayers first

//todo - deal with preselections - may be array of playerIds or maybe array of players, 
//and may be coming through as undefined - alert user, and then render as blank

//todo - implement the active and non active style and onclick functions
const PlayersSelector = 
  ({players, requiredNrPlayers, preselections, history, onSave, loading, classes, active, onActivate}) =>{
  //state 
  //each selection is an array of playerIds, for player, player.subselection, etc...
  const initSelections = preselections ? preselections : 
    requiredNrPlayers ? Array.from(Array(requiredNrPlayers), () => []) : [[]]
  const [selections, setSelections] = useState(initSelections)
  
  //players are in position j=0. subselections of that player may follow
  const playerSelections = selections.map(sel => sel[0])
  //console.log("playerSelections", playerSelections)
  const availablePlayers = players
    .filter(p => !playerSelections.find(player => player && player._id == p._id))
  console.log("availablePlayers", availablePlayers)

  //ready if at least one player is selected
  const selectionsReady = requiredNrPlayers ? 
    (selections.length == requiredNrPlayers) : selections[0].length >= 1

  const onSelectPlayer = i => j => event => {
    //console.log("onSelectPlayer selections before change: ", selections)
    //i is the playerSelector nr, j is the player/subplayer/subplayer... nr, 
    const playerId = event.target.value
    let updatedSelections = selections
    //if no value, remove player and any selected subplayers
    if(!playerId){
      const updatedSelection = selections[i].slice(0,j)
      updatedSelections[i] = updatedSelection 
    }
    else{
      updatedSelections = selections
      updatedSelections[i][j] = players.find(g => g._id == playerId)
    }
    //use spread to ensure update
    setSelections([...updatedSelections])
  } 
  const onAddAnotherPlayer = () =>{
    setSelections([...selections, []])
  }
  const onRemovePlayer = i => {
    const updatedSelections = 
      [...selections.slice(0, i), ...selections.slice(i+1, selections.length)]
    setSelections(updatedSelections)
  }
  const onSubmit = () =>{
    //todo - check two players are not the same
    const path = history.location.pathname
    onSave(selections, path+'.players')
    //push
    /*if(selections.length == 1){
      const selection = selections[0]
      //note - selection must have at least one player selected
      //we want the last player incase its a subplayer
      const selectedPlayer = selection[selection.length-1]
      history.push(path+"/player/"+selectedPlayer._id)
    }else
      history.push(path+"/players")*/
  }
  return(
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
          <Typography 
            type="headline" component="h1" className={classes.cardHeader}>
            {selections.length > 1 ? "Choose Players" : "Choose Player"}
          </Typography>
          <div className={classes.cardBody}>
            {selections.map((selection, i) => 
              <PlayerSelector i={i} selection={selection} players={availablePlayers}
                onSelect={onSelectPlayer(i)} onRemove={() => onRemovePlayer(i)}
                classes={classes} key={'player-select-'+i}/>)}

            {selectionsReady && 
              (!requiredNrPlayers && selections.length <= 4 ? 
              renderActionButton({
                icon:'add-box', 
                onClick:() =>onAddAnotherPlayer(),
                style:{margin:30, marginTop:15},
                options:{fontSize:'large'}})
              :
              <div style={{margin:30, color:'red'}}>
                You can choose up to 5</div>)}
          </div>
      </CardContent>

    {selectionsReady && 
      <SummarySection selections={selections} onSubmit={onSubmit} 
        classes={classes} className={classes.cardFooter}/>}
    </Card>
    )
}
PlayersSelector.defaultProps = {
  usersPlayers:[],
  otherPlayers:[]
}


export const PlayerSelector = ({i, selection, players, onSelect, onRemove, classes}) =>
  <div className={classes.selector}>
      {renderPlayerSelector(selection, players, i, 0, onSelect, classes)}
    {i != 0 && 
      <div onClick={onRemove} className={classes.removeButton}>Remove</div>}
  </div>

PlayerSelector.defaultProps = {

}

//todo - merge with renderPlayerSelector, but instead of subplayers, 
//we could have any key as a sub-selection
//for now, this only renders with j == 0 ie for selecting player name
const renderPlayerSelector = (selection, players, i, j, onSelect, classes) =>{
  console.log("renderPlayerSelector players", players)
  //re-add the player in this selection if they exist so they show up in Select
  const reqPlayers = selection[0] ? [...players, selection[0]] : players
  //for now, we dont have subselections 
  //j == 0 ? parentPlayers(players) : subplayersOf(selection[j-1]._id, players) 
    return(
      <div>
        <div className={classes.selectSection}>
         {j != 0 &&
          <Typography className={classes.subtitle}
            type="headline" component="h2" className={classes.title}>
          Sub-selection (optional)
         </Typography>}
         <Select 
            className={classes.select} className={classes.select}
            value={selection[j] ? selection[j]._id : ""}
            labelId="label" id={"select-player-"+i+"-"+j} 
            onChange={onSelect(j)}
            className={classes.select} >
            {reqPlayers.map(player =>
              <MenuItem key={"select-player-"+i+"-"+j+"-"+player._id} value={player._id}>
                {getName(player)}</MenuItem>)}
            {j != 0 &&
              <MenuItem key={"select-player-"+i+"-"+j+"-"+"none"} value={""}>
                None</MenuItem>}
          </Select>
        </div>
        {/**selection[j] && selection[j].subplayers.length != 0 &&
          renderPlayerSelector(selection, players, i, j+1, onSelect, classes)**/}
      </div>
      )
}

PlayerSelector.defaultProps = {
  playerSelectionArray:[]
}

export default withStyles(selectorStyles)(PlayersSelector)



