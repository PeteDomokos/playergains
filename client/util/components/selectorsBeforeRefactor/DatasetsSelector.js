import React, { Component, useState, useEffect } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
import ProfileCollection from '../profile/ProfileCollection'
import { SummarySection, selectorStyles, getName } from './Common'
import { renderActionButton } from '../Buttons'
//helpers
import { groupsAreSiblings, commonGroupIds } from '../../../groups/GroupHelpers'
import { datasetNamePair, merge } from '../../../core/datasets/DatasetHelpers'

const getDatasets = (players, groups, parents) =>{
  const groupsDatasets = groups.map(g => g.datasets).reduce((a,b) => [...a, ...b], [])
  const parentsDatasets = parents.map(g => g.datasets).reduce((a,b) => [...a, ...b], [])
  //seperate into merged standard sets, and custom sets
  const standard = merge(
      [...groupsDatasets.filter(d => d.isStandard), 
      ...parentsDatasets.filter(d => d.isStandard)])
  const custom = [...groupsDatasets.filter(d => !d.isStandard), 
                  ...parentsDatasets.filter(d => !d.isStandard)]
  //if a single player or group is selected, then custom sets are required too
  if(players.length == 1 || groups.length == 1)
    return [...standard, ...custom]
  //if groups selected and all groups come from a common parent, 
  //then custom groups are required too
  if(!players && groupsAreSiblings(groups))
    return [...standard, ...custom]
  //include the custom groups from any common groups
  if(players.length > 1){
    //check - commonGroupIds method just returns groupIds
    const commonGroups = commonGroupIds(players)
      .map(id => groups.find(g => g._id === id))
    const commonDatasets = commonGroups.map(g => g.datasets)
                                       .reduce((a,b) => [...a, ...b], [])

    return [...standard, ...commonDatasets.filter(d => !d.isStandard)]
  }
  //otherwise several groups are selected so only standard datasets required
  return standard
}

//tdo - worko out why scroll bar renders at bottom here, but in other cases it renders at top

//if dashboard.players, then players have been selected. Otherwise, groups have been selected
const DatasetsSelector = 
  ({dashboard, groups, parents, path, onSave, loading, classes}) =>{
   useEffect(() => {
      window.scrollTo(0, 0)
    }, [])

  const datasetsWithoutNamePairs = getDatasets(dashboard.players, groups, parents)
  //namePair ensures that if necc thse nr of particpants is displayed too
  const datasets = datasetsWithoutNamePairs
    .map(d => {return {...d, namePair:datasetNamePair(d, datasetsWithoutNamePairs)} })
  //state 
  //selections is an array of datasets
  const initSelections = []//dashboard.datasets ? dashboard.datasets : []
  const [selections, setSelections] = useState(initSelections)

  //adds or removes dataset
  const onSelect = dataset => {
    //use spread to ensure update

    //todo - when merging datasets, give each one a mergedId and use this
    //mergedId={datasetIds:Array, id:String}
    //but do the merging in the dashbard itself after all datapoints have been loaded
    //in the dashboard loader 
    if(selections.find(d => d._id === dataset._id))
      setSelections(selections.filter(d => d._id !== dataset._id))
    else
      setSelections([...selections, dataset])
  } 

  const onSubmit = () =>{
    onSave([{path:path+'.datasets', value:selections}])
  }

  const actions = dataset => {
    let label, colour 
    if(selections.find(d => d._id === dataset._id)){
      label = "Remove"
      colour = "red"
    }else{
      label = "Add"
      colour = "blue"
    }
    return [{label:label, onClick:() => onSelect(dataset), 
            style:{backgroundColor:colour, border:'none'},
            id:dataset._id}]
  }
  const itemStyle =  dataset => {
    let backgroundColour
    if(selections.find(d => d._id === dataset._id)){
      backgroundColour = "blue"
    }else{
      backgroundColour = "aqua"
    }
    return {backgroundColor:backgroundColour}
  }
  return(
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
         <ProfileCollection format='grid' title='Chosen' singleRow
            itemType='dataset' items={selections} itemStyle={itemStyle}
            emptyMesg='No datasets chosen yet' />
          <div className={classes.actions}>
            {selections.length != datasets.length &&
              <Button color="primary" variant="contained" onClick={() =>setSelections(datasets)} 
                className={classes.submit}>Select All</Button>}
            {selections.length != 0 &&
              <Button color="primary" variant="contained" onClick={() =>setSelections([])} 
                className={classes.submit}>Delete All</Button>}
            {selections.length != 0 && 
              <Button color="primary" variant="contained" onClick={() =>onSubmit(selections)} 
              className={classes.submit}>Go</Button>}
          </div>
          <ProfileCollection format='list' title='Add or Remove' 
            itemType='dataset' items={datasets} itemStyle={itemStyle}
            onClickItem={onSelect} actions={actions}
            emptyMesg='No datasets added yet' />
      </CardContent>
    </Card>
    )
}
DatasetsSelector.defaultProps = {
  usersGroups:[],
  otherGroups:[]
}


export const DatasetSelector = ({i, selection, groups, onSelect, onRemove, classes}) =>
     <div className={classes.selector}>
        {renderDatasetSelector(selection, groups, i, 0, onSelect, classes)}
      {i != 0 && 
        <div onClick={onRemove} className={classes.removeButton}>Remove</div>}
    </div>

DatasetSelector.defaultProps = {
  groupSelectionArray:[]
}

//helper
const subgroupsOf = (parentId, groups) =>
  groups.filter(g => g.parent == parentId)

const parentGroups = groups => 
  groups.filter(g => !g.parent) 

//todo - check it works for subgroups of subgroups
const renderDatasetSelector = (selection, groups, i, j, onSelect, classes) =>{
  const reqGroups = j == 0 ? 
    parentGroups(groups) : subgroupsOf(selection[j-1]._id, groups) 

    return(
      <div>
        <div className={classes.selectSection}>
         {j != 0 &&
          <Typography className={classes.subtitle}
            type="headline" component="h2" className={classes.title}>
          Subgroup (optional)
         </Typography>}
         <Select 
            className={classes.select} className={classes.select}
            value={selection[j] ? selection[j]._id : ""}
            labelId="label" id={"select-group-"+i+"-"+j} 
            onChange={onSelect(j)}
            className={classes.select} >
            {reqGroups.map(group =>
              <MenuItem key={"select-group-"+i+"-"+j+"-"+group._id} value={group._id}>
                {getName(group)}</MenuItem>)}
            {j != 0 &&
              <MenuItem key={"select-group-"+i+"-"+j+"-"+"none"} value={""}>
                None</MenuItem>}
          </Select>
        </div>
        {selection[j] && selection[j].subgroups.length != 0 &&
          renderGroupSelector(selection, groups, i, j+1, onSelect, classes)}
      </div>
      )
}

export default withStyles(selectorStyles)(DatasetsSelector)