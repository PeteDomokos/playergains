import React, { useEffect }  from 'react'
import GroupsSelector from './GroupsSelector'
import PlayersSelector from './PlayersSelector'
import DatasetsSelector from './DatasetsSelector'
/**
*
**/

//todo - add a buffer mechanism for fetching groups and players, and a way of determining 
//if more need to be fetched, as store.otherGroups and otherPlayers will not be ''
export const GroupsSelectorLoader = 
	({storedGroups, requiredNrGroups, selections, history, onSave, onLoad, loading}) => {
	useEffect(() => {
		//TODO!!!!!!!
		//note storedGroups already available if user logged in.
		//if user not logged in, then there may be no storedGroups 
		//todo - deal with which groups are needed - it should be all users groups 
		//(ie groupsFollowing, playerinfo.groups etc - are all of these going to be stored
		//when user logs in? And which groups should be loaded if user not logged in - maybe none
		//maybe redirect to login then
		if(!storedGroups && !loading){
			console.log("GroupSelectorLoader loading groups...")
			onLoad()
		}
		//todo - deal with selections - may be array of groupIds or maybe array of groups, 
		//attempt should be made to load them
		//and may be coming through as undefined - alert user, and then render as blank
	}, [])
	return (
		<GroupsSelector storedGroups={storedGroups} 
			history={history} requiredNrGroups={requiredNrGroups} preselections={selections}
			onSave={onSave} loading={loading}/>)
}


export const PlayersSelectorLoader = 
	({players, requiredNrPlayers, selections, history, onSave, onLoad, loading, active, onActivate}) => {
	useEffect(() => {
		//TODO - work this thorugh
		//note players already available if user logged in.
		//if user not logged in, then there may be no players 
		if(!players && !loading){
			console.log("PlayerSelectorLoader loading players...")
			onLoad()
		}
		//deal with preselected (see groups above)...
	}, [])
	return (
		<PlayersSelector players={players} 
			preselections={selections} history={history} onSave={onSave} 
			requiredNrPlayers={requiredNrPlayers} loading={loading} active={active} onActivate={onActivate} />)
}

export const DatasetsSelectorLoader = 
	({dashboard, groups, parents, path, onSave, onLoad, loading}) => {
		console.log("DatasetsSelectorLoader groups", groups)
	useEffect(() => {
		//if players and if any groups not loaded, load them from server
		const unloadedGroups = groups.filter(g => !g.datasets)
		const unloadedParents = parents.filter(g => !g.datasets)
		const unloaded = [...unloadedGroups, ...unloadedParents]
		if(unloaded.length != 0 && !loading){
			console.log("PlayerSelectorLoader loading players...")
			onLoad(unloaded.map(g => g._id))
		}
	}, [])
	return (
		<DatasetsSelector 
			dashboard={dashboard} groups={groups} parents={parents} path={path} 
			onSave={onSave} loading={loading} />)
}

