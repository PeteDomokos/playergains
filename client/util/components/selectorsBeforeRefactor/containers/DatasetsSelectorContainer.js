import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroups } from '../../../../actions/Groups'
import { saveSelections } from '../../../../actions/Dashboard'

import { DatasetsSelectorLoader } from '../Loaders'
import { filterUniqueByProperty } from '../../../helpers/ArrayManipulators'
import { getParents } from '../../../../groups/GroupHelpers'


const mapStateToProps = (state, ownProps) => {
	console.log("DatasetsSelectorContainer state", state)
	let groups
	if(state.dashboard.groups)
		groups = state.dashboard.groups
	else{
		//players must be selected instead
		//todo - remove the .groups check, as user groups is laoded when user is loaded
		//but need to check where else user is checked for before changing
		//some groups will be undefined
		const playersGroups = state.dashboard.players
			.map(p => p.playerInfo.groups) //groups not defined for all players, only for user logged in
			//anyway we  only want the groups played for. also need to change player to playerInfo
			.reduce((arr1, arr2) => [...arr1, ...arr2])
			.map(groupId =>{
				const storedGroup = state.storedItems.groups.find(group => group._id === groupId)
				if(storedGroup)
					return storedGroup
				return g })

		console.log("playersGroups ", playersGroups)
		//remove duplications of groups
		groups = filterUniqueByProperty("_id", playersGroups)
		console.log("uniqueGroups ", groups)
	}
	//get unique parents
	const parents = getParents(groups, state.storedItems.groups)
	return({
		dashboard:state.dashboard,
		//groups either the selected ones or the ones the players play for
		groups:groups,
		parents:parents,
		path:ownProps.history.location.pathname,
		loading:state.asyncProcesses.loading.datasets
	})
}
const mapDispatchToProps = dispatch => ({
	//path - current url path before push, to determine store selections location
	onSave(selections){
		console.log("DatasetsSelectorContainer onSave selections:", selections)
		dispatch(saveSelections(selections))
	},
	onLoad(groupIds){
		dispatch({groupIds:fetchGroups(groupIds)})
	}
})

//wrap all 4 sections in the same container for now.
const DatasetsSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DatasetsSelectorLoader)

export default DatasetsSelectorContainer

