import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchPlayers } from '../../../../actions/PlayersAndCoaches'
import { saveSelections } from '../../../../actions/Dashboard'
import { players } from '../../../helpers/StoreHelpers'

import { PlayersSelectorLoader } from '../Loaders'


const mapStateToProps = (state, ownProps) => {
	return({
		//todo - pass through params.playerId and ownProps.selectedPlayers even if undefined
		//usersPlayers includes playersViewed, playersFollowed, and all players in adminGroups

		//todo - make a method to get userPlayers from players
		players:players(state.storedItems.users),
		selections:state.dashboard.players,
		requiredNrPlayers:ownProps.nrPlayers, //may be undefined
		loading:state.asyncProcesses.loading.players,
		history: ownProps.history,
		active:ownProps.active,
		onActivate:ownProps.onActivate
	})
}
const mapDispatchToProps = dispatch => ({
	onLoad(){
		console.log("Playercontainer.onLoad")
		dispatch(fetchPlayers)
	},
	//path - current url path before push, to determine store selections location
	onSave(selections, path){
		console.log("PlayerSelectorContainer onSave selections:", selections)
		dispatch(saveSelections(selections, path))
	}
})

//wrap all 4 sections in the same container for now.
const PlayersSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(PlayersSelectorLoader)

export default PlayersSelectorContainer

