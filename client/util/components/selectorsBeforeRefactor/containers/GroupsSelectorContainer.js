import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroups } from '../../../../actions/Groups'
import { saveSelections } from '../../../../actions/Dashboard'

import { GroupsSelectorLoader } from '../Loaders'


const mapStateToProps = (state, ownProps) => {
	return({
		//todo - pass through params.groupId and ownProps.selectedGroups even if undefined
		storedGroups:state.storedItems.groups,
		selections:state.dashboard.groups,
		requiredNrGroups:ownProps.nrGroups, //may be undefined
		loading:state.asyncProcesses.loading.groups,
		history: ownProps.history
	})
}
const mapDispatchToProps = dispatch => ({
	onLoad(){
		console.log("Groupcontainer.onLoad")
		dispatch(fetchGroups())
	},
	//path - current url path before push, to determine store selections location
	onSave(selectionsWrapper){
		console.log("GroupSelectorContainer onSave selections:", selectionsWrapper)
		dispatch(saveSelections(selectionsWrapper))
	}
})

//wrap all 4 sections in the same container for now.
const GroupsSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupsSelectorLoader)

export default GroupsSelectorContainer

