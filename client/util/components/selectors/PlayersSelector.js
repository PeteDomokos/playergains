import React, { Component, useState } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
import { SummarySection, selectorStyles, getName } from './Common'
import { renderActionButton } from '../Buttons'

//todo - make the selects order the players with usersPlayers first

//todo - deal with preselections - may be array of playerIds or maybe array of players, 
//and may be coming through as undefined - alert user, and then render as blank

//todo - implement the active and non active style and onclick functions
const PlayersSelector = 
  ({players, requiredNrPlayers, preselections, history, onSave, loading, classes, active, onActivate}) =>{
  //state 
  //each selection is an array of playerIds, for player, player.subselection, etc...
  const initSelections = preselections ? preselections : 
    requiredNrPlayers ?  Array.from(Array(requiredNrPlayers), () => '') : ['']
  const [selections, setSelections] = useState(initSelections)
  
  //players are in position j=0. subselections of that player may follow
  const availablePlayers = players
    .filter(p => !selections.find(player => player._id == p._id))

  //ready if at least one player is selected
  const selectionsReady = requiredNrPlayers ? 
    (selections.length == requiredNrPlayers) : 
    selections.filter(sel => sel !== '').length >= 1

  const onSelectPlayer = i => event => {
    const playerId = event.target.value
    if(selections.find(p => p._id == playerId))
      alert("This player is already selected.")
    //if no value, remove player
    /*if(!playerId){} not needed*/
    else{
      const updatedSelections = selections
      updatedSelections[i] = players.find(g => g._id == playerId)
      //use spread to ensure update
      setSelections([...updatedSelections])
    }
  } 
  const onAddAnotherPlayer = () =>{
    setSelections([...selections, ''])
  }
  const onRemovePlayer = i => {
    const updatedSelections = 
      [...selections.slice(0, i), ...selections.slice(i+1, selections.length)]
    setSelections(updatedSelections)
  }
  const onSubmit = () =>{
    alert('Player Dashboard is not available yet. Select a group above.')
    //onSave(selections, history.location.pathname +'.players')
  }

  return(
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
          <Typography 
            type="headline" component="h1" className={classes.cardHeader}>
            {selections.length > 1 ? "Choose Players" : "Choose Player"}
          </Typography>
          <div className={classes.cardBody}>
            {selections.map((selection, i) => 
              <PlayerSelector i={i} selection={selection} players={availablePlayers}
                onSelect={onSelectPlayer(i)} onRemove={() => onRemovePlayer(i)}
                classes={classes} key={'player-select-'+i}/>)}

            {selectionsReady && 
              (!requiredNrPlayers && selections.length <= 4 ? 
              renderActionButton({
                icon:'add-box', 
                onClick:() =>onAddAnotherPlayer(),
                style:{margin:30, marginTop:15},
                options:{fontSize:'large'}})
              :
              <div style={{margin:30, color:'red'}}>
                You can choose up to 5</div>)}
          </div>
      </CardContent>

    {selectionsReady && 
      <SummarySection entity='player' selections={selections} onSubmit={onSubmit} 
        classes={classes} className={classes.cardFooter}/>}
    </Card>
    )
}
PlayersSelector.defaultProps = {
  usersPlayers:[],
  otherPlayers:[]
}


export const PlayerSelector = ({i, selection, players, onSelect, onRemove, classes}) =>
  <div className={classes.selector}>
      {renderPlayerSelector(selection, players, i, onSelect, classes)}
    {i != 0 && 
      <div onClick={onRemove} className={classes.removeButton}>Remove</div>}
  </div>

PlayerSelector.defaultProps = {

}

//todo - merge with renderPlayerSelector, but instead of subplayers, 
//we could have any key as a sub-selection
//for now, this only renders with j == 0 ie for selecting player name
const renderPlayerSelector = (selection, players, i, onSelect, classes) =>{
  //re-add the player in this selection if they exist so they show up in Select
  const reqPlayers = selection ? [...players, selection] : players
    return(
      <div>
        <div className={classes.selectSection}>
         <Select 
            className={classes.select} className={classes.select}
            value={selection ? selection._id : ""}
            labelId="label" id={"select-player-"+i} 
            onChange={onSelect}
            className={classes.select} >
            {reqPlayers.map(player =>
              <MenuItem key={"select-player-"+i+"-"+player._id} value={player._id}>
                {getName(player)}</MenuItem>)}
          </Select>
        </div>
      </div>
      )
}

PlayerSelector.defaultProps = {
  playerSelectionArray:[]
}

export default withStyles(selectorStyles)(PlayersSelector)



