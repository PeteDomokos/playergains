 import React, { useEffect }  from 'react'
import GroupsSelector from './GroupsSelector'
import PlayersSelector from './PlayersSelector'
import DatasetsSelector from './DatasetsSelector'
/**
*
**/

//todo - add a buffer mechanism for fetching groups and players, and a way of determining 
//if more need to be fetched, as store.otherGroups and otherPlayers will not be ''
export const GroupsSelectorLoader = 
	({groups, requiredNrGroups, selections, history, onSave, onLoad, loading}) => {
	useEffect(() => {
		if(selections.length != 0){

			//todo - if selections includes any groups, or parents (in any j pos)
			//that are not fully loaded ie stubs ie {_id:...}, then need to 
			//load those groups, and save them in selections ie call saveSelections
			//so also need to pass throught mehtod for saveSelections to here 
		}

	}, [])
	return (
		<GroupsSelector groups={groups} 
			history={history} requiredNrGroups={requiredNrGroups} preselections={selections}
			onSave={onSave} loading={loading}/>)
}


export const PlayersSelectorLoader = 
	({players, requiredNrPlayers, selections, loading, history, onSave, onLoad, active, onActivate}) => {
	useEffect(() => {
		//for now, use length < 2 because the user themself will be loaded already
		if((!players || players.length < 2) && !loading){
			onLoad()
			//todo - work out which players whould be present here and load only them,
			//if not already loaded
			//so need to decide how to check
			//note players already available if user logged in.
			//if user not logged in, then there may be no players 

			//todo - deal with preselections
		}
	}, [])
	return(
		<PlayersSelector players={players} preselections={selections} history={history} 
			onSave={onSave} requiredNrPlayers={requiredNrPlayers} loading={loading}   
			  active={active} onActivate={onActivate} />)
}

export const DatasetsSelectorLoader = 
	({dashboard, loading, path, onSave, onLoad}) => {
	useEffect(() => {
		//if players selected, groups will be flat already
		const flatGroups = dashboard.players ? dashboard.groups :
			dashboard.groups.reduce((arr1, arr2) => [...arr1, ...arr2], [])
			
		const unloaded = dashboard.groups.filter(g => !g.datasets)

		if(unloaded.length != 0 && !loading){
			onLoad(unloaded.map(g => g._id))
		}

		//if players and if any groups not loaded, load them from server
	}, [])
	return (
		<DatasetsSelector 
			dashboard={dashboard} path={path} 
			onSave={onSave} loading={loading} />)
}

