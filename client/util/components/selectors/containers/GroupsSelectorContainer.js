import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroups } from '../../../../actions/Groups'
import { saveSelections } from '../../../../actions/Dashboard'
import { userGroupsFromIds } from '../../../../util/helpers/StoreHelpers'

import { GroupsSelectorLoader } from '../Loaders'


const mapStateToProps = (state, ownProps) => {
	return({
		groups:userGroupsFromIds(state.user, state.storedItems.groups),
		selections:state.dashboard.groups,
		requiredNrGroups:ownProps.nrGroups, //may be undefined
		loading:state.asyncProcesses.loading.groups,
		history: ownProps.history
	})
}
const mapDispatchToProps = dispatch => ({
	onLoad(groupIds){
		dispatch(fetchGroups(groupIds))
	},
	//path - current url path before push, to determine store selections location
	/*onSave(selectionsWrapper){
		console.log("GroupSelectorContainer onSave selections:", selectionsWrapper)
		dispatch(saveSelections(selectionsWrapper))
	}*/
	onSave(selections, path){
		dispatch(saveSelections(selections, path))
	}
})

//wrap all 4 sections in the same container for now.
const GroupsSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupsSelectorLoader)

export default GroupsSelectorContainer

