import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroups } from '../../../../actions/Groups'
import { saveSelections } from '../../../../actions/Dashboard'

import { DatasetsSelectorLoader } from '../Loaders'
import { filterUniqueByProperty } from '../../../helpers/ArrayManipulators'
import { getGroupParents, getPlayersGroups } from '../../../../groups/GroupHelpers'

const mapStateToProps = (state, ownProps) => {
	let loadedDashboard 
	//case 1: players have been selected
	if(state.dashboard.players){
		const uniquePlayersGroups = 
			getPlayersGroups(state.dashboard.players, state.storedItems.groups)
		loadedDashboard = {...state.dashboard, groups:uniquePlayersGroups}
	}else{
		//case 2: groups have been selected because no players selected
		const loadedDashboardGroups = state.dashboard.groups.map(arr =>
			arr.map(group => state.storedItems.groups.find(g => g._id === group._id)))
		loadedDashboard = {...state.dashboard, groups:loadedDashboardGroups}
	}
	return({
		dashboard:loadedDashboard,
		path:ownProps.history.location.pathname,
		loading:state.asyncProcesses.loading.datasets
	})
}
const mapDispatchToProps = dispatch => ({
	//path - current url path before push, to determine store selections location
	onSave(selections, path){
		dispatch(saveSelections(selections, path))
	},
	onLoad(groupIds){
		dispatch(fetchGroups({groupIds:groupIds}))
	}
})

//wrap all 4 sections in the same container for now.
const DatasetsSelectorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DatasetsSelectorLoader)

export default DatasetsSelectorContainer

