import React, { Component, useState } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//linked and child components
//helpers

export const selectorStyles = theme => ({
  root: theme.mixins.gutters({
    //padding: theme.spacing.unit * 3,
  }),
  //cardContent contains teh header and body
  //todo - nest these
  //todo - check root, header, body, footer - are these classNames already used my materialUi stylesheets?
  cardContent:{
    justifyContent:'flex-start', 
    marginBottom:20,
    display:'flex',
    flexDirection:'column'
  },
  cardHeader: {
    //margin: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 2}px`,
    //color: theme.palette.protectedTitle
  },
  cardBody:{
    display:'flex', 
    justifyContent:'flex-start',
    flexWrap:'wrap'
  },
  cardFooter:{
    //summary section
  },
  subtitle:{
    fontSize:12
  },
  selector:{
    display:"flex", 
    flexDirection:"column", 
    width:200, 
    alignItems:'flex-start'
  },
  removeButton: {
    margin:10, 
    marginTop:0, 
    alignSelf:"flex-end", 
    color:'red',
    "&:hover":{
      cursor:'pointer'
    }
  }, 
  selectSection: {
    margin:20,
    marginBottom:30
  },
  select: {
    width:150, 
    marginBottom:0
  },
  actions:{
    display:'flex',
    justifyContent:'center'
  },
  submit:{
    //minWidth:80,
    //maxWidth:160,
    width:160,
    alignSelf:'center',
    margin:20
  }

})

//todo - use withStyles here for css-in-js instead of this

//helpers
//const playerName = player => player.firstName + " " +item.surname
export const getName = item =>{
  if(!item) 
    return '................'
  if(item.firstName && item.surname)
    return item.firstName + " " +item.surname
  if(item.name)
    return item.name
  else return ''
}
const summarySt = {display:'flex', flexDirection:'column', alignItems:'center'}
const namesSectionSt = {display:'flex', justifyContent:'center', margin:10}
const nameAndVersusContainerSt = {display:'flex', margin:10, marginRight:0, marginLeft:0}
const nameSt = {margin:5, fontSize:16}
const versusSt = {margin:5, fontSize:12}
const namePartSt = j =>{
  return {
      marginLeft:5,//j*5, 
      color:j == 0 ? 'blue': 'red'
    }
}

const groupName = selection =>{
  console.log('group selection', selection)
  if(!selection || !selection[0]){ 
    return (
      <div style={nameSt}>
        <div style={namePartSt(0)}>&#63;</div>
      </div>
      )
  }else{
    return(
      <div style={nameSt}>
        {selection.map((group,j) =>
            <div style={namePartSt(j)} key={'name-'+j}>
              {j != 0 &&<span>&#8594;</span>}
              {getName(selection[j])}</div>)}
      </div>
      )
  }
}

const playerName = selection =>{
  console.log('player selection', selection)
  if(!selection){ 
    return (
      <div style={nameSt}>
        <div style={namePartSt(0)}>&#63;</div>
      </div>
      )
  }else{
    return(
      <div style={nameSt}>
        <div style={namePartSt(0)}>
          {selection.firstName} {selection.surname}
        </div>
      </div>
      )
  }
}

export const SummarySection = ({entity, selections, onSubmit, classes}) =>
  <div style={summarySt}>
    <div style={namesSectionSt}>
      {selections.map((selection,i) =>
        <div style={nameAndVersusContainerSt} key={'summary-name'+i}>
          <span style={versusSt}>{i == 0 ? "": " vs "}</span>
          {entity == 'group' ? groupName(selection) : playerName(selection)}
        </div>)}
    </div>
    <CardActions className={classes.actions}>
      <Button color="primary" variant="contained" onClick={() =>onSubmit(selections)} 
        className={classes.submit}>Go</Button>
    </CardActions>
  </div>

SummarySection.defaultProps = {
  selectedGroups:[]
}

