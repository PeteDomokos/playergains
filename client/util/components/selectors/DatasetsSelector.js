import React, { Component, useState, useEffect } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
import ProfileCollection from '../profile/ProfileCollection'
import { SummarySection, selectorStyles, getName } from './Common'
import { renderActionButton } from '../Buttons'
//helpers
import { groupsAreSiblings, commonGroupIds } from '../../../groups/GroupHelpers'
import { datasetNamePair, merge, isCommon, playerDatasets } from '../../../core/datasets/DatasetHelpers'

const getDatasets = dashboard =>{
    //check if group.datasets not loaded yet for all
    if(dashboard.players){
      //check loaded
      const unloaded = dashboard.groups.filter(g => !g.datasets)
      if(unloaded.length != 0)
        return []
      //attach datasets propery to each player
      //put in all datasets for each group that player plays for 
      //( no need to do parents because each player plays for parents too if they play for a group)
      //(when loading datapoinnts in Dashboard loader, we only load points for the players selected)
      //get all common datasets between players (common test is same as for groups)
      const playersWithDatasets = dashboard.players.map(player =>{
        //note: as players are selected, groups will be flat, children and parents
        return {...player, datasets:playerDatasets(player, dashboard.groups)} })


      //note - players here act as groups, each one having datasets
      const commonSets = playersWithDatasets[0].datasets
        .filter(d => isCommon(d, playersWithDatasets))

      return commonSets

    } else {
      //check loaded
      const unloaded = dashboard.groups.reduce((arr1, arr2) => [...arr1, ...arr2], [])
                                       .filter(g => !g.datasets)
      if(unloaded.length != 0)
        return []
      //groups selected only
      //for each group, first we flat-merge all ancestors datasets with their own
      //todo - make this a helper method
      const groupsWithInheritedDatasets = dashboard.groups.map(selectionArray =>{
        const group = selectionArray[selectionArray.length-1]
        const allDatasets = selectionArray.map(g => g.datasets)
                                          .reduce((a,b) => [...a, ...b], [])

        //todo - remove datapoints for players that are not in the subgroup - but 
        //need to do that after datapoints have been loaded - maybe could tell server to 
        //only send datapoints for certain players
        return {...group, datasets:allDatasets}
      })
      //return the common datasets
      const commonSets = groupsWithInheritedDatasets[0].datasets
        .filter(d => isCommon(d, groupsWithInheritedDatasets))


      return commonSets

      /*const groups = groupsWithInheritedDatasets.map(group =>{
        const commonDatasetsInThisGroup = group.datasets
          .filter(dataset => commonSets.find(d => d._id === dataset._id))
      })
      console.log("groups", groups)*/

      //todo - think about do I need to merge the standard datasets here, or 
      //leave them split into the groups. also same for custom
    }
}

//tdo - worko out why scroll bar renders at bottom here, but in other cases it renders at top

//if dashboard.players, then players have been selected. Otherwise, groups have been selected
const DatasetsSelector = 
  ({dashboard, path, loading, onSave, classes}) =>{
   useEffect(() => {
      window.scrollTo(0, 0)
    }, [])

  /*need to sort the getDatasets method
  but also for when players are selected, the groups pf all players will 
  be stored and fully loaded in dashboard.groups*/

  const datasetsWithoutNamePairs = getDatasets(dashboard)
  //namePair ensures that if necc thse nr of particpants is displayed too
  const datasets = datasetsWithoutNamePairs
    .map(d => {return {...d, namePair:datasetNamePair(d, datasetsWithoutNamePairs)} })
  //state 
  //selections is an array of datasets
  const initSelections = []//dashboard.datasets ? dashboard.datasets : []
  const [selections, setSelections] = useState(initSelections)

  //adds or removes dataset
  const onSelect = dataset => {
    //use spread to ensure update

    //todo - when merging datasets, give each one a mergedId and use this
    //mergedId={datasetIds:Array, id:String}
    //but do the merging in the dashbard itself after all datapoints have been loaded
    //in the dashboard loader 
    if(selections.find(d => d._id === dataset._id))
      setSelections(selections.filter(d => d._id !== dataset._id))
    else
      setSelections([...selections, dataset])
  } 

  const onSubmit = () =>
    onSave(selections, path+'.datasets')

  const actions = dataset => {
    let label, colour 
    if(selections.find(d => d._id === dataset._id)){
      label = "Remove"
      colour = "red"
    }else{
      label = "Add"
      colour = "blue"
    }
    return [{label:label, onClick:() => onSelect(dataset), 
            style:{backgroundColor:colour, border:'none'},
            id:dataset._id}]
  }
  const itemStyle =  dataset => {
    let backgroundColour
    if(selections.find(d => d._id === dataset._id)){
      backgroundColour = "blue"
    }else{
      backgroundColour = "aqua"
    }
    return {backgroundColor:backgroundColour}
  }
  return(
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
         <ProfileCollection format='grid' title='Chosen' singleRow
            itemType='dataset' items={selections} itemStyle={itemStyle}
            emptyMesg='No datasets chosen yet' />
          <div className={classes.actions}>
            {selections.length != datasets.length &&
              <Button color="primary" variant="contained" onClick={() =>setSelections(datasets)} 
                className={classes.submit}>Select All</Button>}
            {selections.length != 0 &&
              <Button color="primary" variant="contained" onClick={() =>setSelections([])} 
                className={classes.submit}>Delete All</Button>}
            {selections.length != 0 && 
              <Button color="primary" variant="contained" onClick={() =>onSubmit(selections)} 
              className={classes.submit}>Go</Button>}
          </div>
          <ProfileCollection format='list' title='Add or Remove' 
            itemType='dataset' items={datasets} itemStyle={itemStyle}
            onClickItem={onSelect} actions={actions}
            emptyMesg='No datasets added yet' />
      </CardContent>
    </Card>
    )
}
DatasetsSelector.defaultProps = {
  usersGroups:[],
  otherGroups:[]
}


export const DatasetSelector = ({i, selection, groups, onSelect, onRemove, classes}) =>
     <div className={classes.selector}>
        {renderDatasetSelector(selection, groups, i, 0, onSelect, classes)}
      {i != 0 && 
        <div onClick={onRemove} className={classes.removeButton}>Remove</div>}
    </div>

DatasetSelector.defaultProps = {
  groupSelectionArray:[]
}

//helper
const subgroupsOf = (parentId, groups) =>
  groups.filter(g => g.parent == parentId)

const parentGroups = groups => 
  groups.filter(g => !g.parent) 

//todo - check it works for subgroups of subgroups
const renderDatasetSelector = (selection, groups, i, j, onSelect, classes) =>{
  const reqGroups = j == 0 ? 
    parentGroups(groups) : subgroupsOf(selection[j-1]._id, groups) 

    return(
      <div>
        <div className={classes.selectSection}>
         {j != 0 &&
          <Typography className={classes.subtitle}
            type="headline" component="h2" className={classes.title}>
          Subgroup (optional)
         </Typography>}
         <Select 
            className={classes.select} className={classes.select}
            value={selection[j] ? selection[j]._id : ""}
            labelId="label" id={"select-group-"+i+"-"+j} 
            onChange={onSelect(j)}
            className={classes.select} >
            {reqGroups.map(group =>
              <MenuItem key={"select-group-"+i+"-"+j+"-"+group._id} value={group._id}>
                {getName(group)}</MenuItem>)}
            {j != 0 &&
              <MenuItem key={"select-group-"+i+"-"+j+"-"+"none"} value={""}>
                None</MenuItem>}
          </Select>
        </div>
        {selection[j] && selection[j].subgroups.length != 0 &&
          renderGroupSelector(selection, groups, i, j+1, onSelect, classes)}
      </div>
      )
}

export default withStyles(selectorStyles)(DatasetsSelector)