import React, { Component, useState } from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'

//linked and child components
import { SummarySection, selectorStyles, getName } from './Common'
import { renderActionButton } from '../Buttons'
//helpers
import { filterUniqueByProperty } from '../../helpers/ArrayManipulators'

//todo - make the selects order the groups with usersGroups under a heading then other groups,
//but all in one select

//todo - redirect to signin, which also loads all the groups connected to user
//user is not given option to select groups that they dont have some connection to, 
//even if its just following, or viewed. Otherwise there would be too many groups

//helper
const selectorFormat = (group, parents) =>{
  //return array [..., grandparent, parent, group]
}
const GroupsSelector = 
  ({groups, preselections, requiredNrGroups, loading, history, onSave, classes}) =>{
  //s tate 
  //each selection is an array of groupIds, for group, subgroup, subgroup of subgroup, etc
  const initSelections = preselections ? preselections : 
    requiredNrGroups ? Array.from(Array(requiredNrGroups), () => []) : [[]]
  const [selections, setSelections] = useState(initSelections)

  const selectionsReady = requiredNrGroups ? 
    (selections.length == requiredNrGroups) : selections[0].length >= 1

  const onSelectGroup = i => j => event => {
    //console.log("onSelectGroup selections before change: ", selections)
    //i is the groupSelector nr, j is the group/subgroup/subgroup... nr, 
    const groupId = event.target.value
    let updatedSelections = selections
    //if no value, remove group and any selected subgroups
    if(!groupId){
      const updatedSelection = selections[i].slice(0,j)
      updatedSelections[i] = updatedSelection 
    }
    else{
      updatedSelections = selections
      updatedSelections[i][j] = groups.find(g => g._id == groupId)
    }
    //use spread to ensure update
    setSelections([...updatedSelections])
  } 
  const onAddAnotherGroup = () =>{
    setSelections([...selections, []])
  }
  const onRemoveGroup = i => {
    const updatedSelections = 
      [...selections.slice(0, i), ...selections.slice(i+1, selections.length)]
    setSelections(updatedSelections)
  }
  const onSubmit = () =>{
    //todo - check two groups are not the same
    //just take the final groups in each array
    //const selectedGroups = selections.map(sel => sel[sel.length-1])
    //onSave([{path:pathPrefix+'.groups', value:selectedGroups}])
    console.log('selections', selections)
    if(selections.length > 1){
      alert('Multiple-Group Dashboard is not available yet. Select only 1 group.')
    }else{
      onSave(selections, history.location.pathname +'.groups')
    }

    //push
    /*if(selections.length == 1){
      const selection = selections[0]
      //note - selection must have at least one group selected
      //we want the last group incase its a subgroup
      const selectedGroup = selection[selection.length-1]
      history.push(path+"/group/"+selectedGroup._id)
    }else
      history.push(path+"/groups")*/
  }
  return(
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
          <Typography 
            type="headline" component="h1" className={classes.cardHeader}>
            {selections.length > 1 ? "Choose Groups" : "Choose Group"}
          </Typography>
          <div className={classes.cardBody}>
            {selections.map((selection, i) => 
              <GroupSelector i={i} selection={selection} groups={groups}
                onSelect={onSelectGroup(i)} onRemove={() => onRemoveGroup(i)}
                classes={classes} key={'group-select-'+i}/>)}

            {selectionsReady && 
              (!requiredNrGroups && selections.length <= 4 ? 
              renderActionButton({
                icon:'add-box', 
                onClick:() =>onAddAnotherGroup(),
                style:{margin:30, marginTop:15},
                options:{fontSize:'large'}})
              :
              <div style={{margin:30, color:'red'}}>
                You can choose up to 5</div>)}
          </div>
      </CardContent>

    {selectionsReady && 
      <SummarySection entity='group' selections={selections} onSubmit={onSubmit} 
        classes={classes} className={classes.cardFooter}/>}
    </Card>
    )
}
GroupsSelector.defaultProps = {
  usersGroups:[],
  otherGroups:[]
}


export const GroupSelector = ({i, selection, groups, onSelect, onRemove, classes}) =>
     <div className={classes.selector}>
        {renderGroupSelector(selection, groups, i, 0, onSelect, classes)}
      {i != 0 && 
        <div onClick={onRemove} className={classes.removeButton}>Remove</div>}
    </div>

GroupSelector.defaultProps = {
  groupSelectionArray:[]
}

//helper
const subgroupsOf = (parentId, groups) =>
  groups.filter(g => g.parent == parentId)

const parentGroups = groups => 
  groups.filter(g => !g.parent) 

//todo - check it works for subgroups of subgroups
const renderGroupSelector = (selection, groups, i, j, onSelect, classes) =>{
  const reqGroups = j == 0 ? 
    parentGroups(groups) : subgroupsOf(selection[j-1]._id, groups) 

    return(
      <div>
        <div className={classes.selectSection}>
         {j != 0 &&
          <Typography className={classes.subtitle}
            type="headline" component="h2" className={classes.title}>
          Subgroup (optional)
         </Typography>}
         <Select 
            className={classes.select} className={classes.select}
            value={selection[j] ? selection[j]._id : ""}
            labelId="label" id={"select-group-"+i+"-"+j} 
            onChange={onSelect(j)}
            className={classes.select} >
            {reqGroups.map(group =>
              <MenuItem key={"select-group-"+i+"-"+j+"-"+group._id} value={group._id}>
                {getName(group)}</MenuItem>)}
            {j != 0 &&
              <MenuItem key={"select-group-"+i+"-"+j+"-"+"none"} value={""}>
                None</MenuItem>}
          </Select>
        </div>
        {selection[j] && selection[j].subgroups.length != 0 &&
          renderGroupSelector(selection, groups, i, j+1, onSelect, classes)}
      </div>
      )
}

export default withStyles(selectorStyles)(GroupsSelector)