import React, { Component, useEffect } from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
//child components
import UserProfile from '../../../user/UserProfile'
import Profile from './Profile'
import ProfileCard from './ProfileCard'
import ProfileStrip from './ProfileStrip'
//helpers
import auth from '../../../auth/auth-helper'

//note - clickHandler and component name kept general so component can be reused
//TODO - STYLING USING WITH STYLES, AND USE MEDIA QUERIES. FOR SS, LIST ITEMS SHOULD TAKE UP FULL WIDTH
const ProfileCollection = 
  ({format, title, itemType, items, selectedItems, onClickItem, actions, style, itemStyle, emptyMesg, singleRow}) =>{
  const titleStyle = {marginTop:'2vh', marginBottom:'2vh'}
  return(
    <div>
     <Typography style={{marginTop:'2vh', marginBottom:'2vh'}} 
      type="headline" component="h1" className=''>{title}</Typography>
      {format == 'list' ?
        <ProfileList itemType={itemType} items={items} 
          selectedItems={selectedItems} itemStyle={itemStyle}
          onClickItem={onClickItem} actions={actions} emptyMesg={emptyMesg}/>
      :
        <ProfileGrid itemType={itemType} items={items} selectedItems={selectedItems} 
          itemStyle={itemStyle} onClickItem={onClickItem} actions={actions} 
          emptyMesg={emptyMesg} singleRow={singleRow} />
      }
    </div>
)}
ProfileCollection.defaultProps = {
  items:[],
  selecteditems:[]
}
//todo - replace scroll with a react scroll. currently, it renders at the bottom
const ProfileList = ({itemType, items, selectedItems, onClickItem, actions, style, itemStyle, emptyMesg}) =>{
    const propsStyle = style ? style :{}
    const listStyle = {...propsStyle, margin:'5vh', marginTop:'2vh', marginBottom:'2vh', padding:'2vw',
      border:'solid',borderColor:'blue',borderWidth:'thin', overflow:'auto', maxHeight:'40vh'}
    return(
      <div>
        {items.length > 0 ?
          <div style={listStyle}>
            {items.map(item =>
              <div key={'item-'+item._id} style={{margin:10}}>
                <ProfileStrip  item={item} itemType={itemType} minimal 
                  style={itemStyle ? itemStyle(item) : {}}
                  onClick={onClickItem} actions={actions ? actions(item) :[]}/>
              </div>)}
          </div>
          :
          <div style={listStyle}>{emptyMesg}</div>
        }
      </div>
    )
}
  
const ProfileGrid = ({itemType, items, selectedItems, onClickItem, actions, style, itemStyle, emptyMesg, singleRow}) =>{
  const propsStyle = style ? style :{}
  let gridHeight = singleRow ? 160 : 'initial'
  if(actions)
    gridHeight = gridHeight+25
  const gridStyle = {...propsStyle, height:gridHeight, display:'flex', margin:'5vh', 
    marginTop:'1.5vh', marginBottom:'1.5vh', padding:'1vw',
    border:'solid',borderColor:'blue',borderWidth:'thin', overflow:'auto'}
  return(
    <div>
      {items.length > 0 ?
        <div style={gridStyle}>
          {items.map(item =>{
            return(
            <div key={'item-'+item._id} style={{margin:5}}>
              <ProfileCard item={item} itemType={itemType} minimal
                style={itemStyle ? itemStyle(item) : {}}
                onClick={onClickItem} actions={actions ? actions(item) :[]}
                key={'items-'+item._id}/>
            </div>)}
            )}
        </div>
        :
        <div style={gridStyle}>{emptyMesg}</div>
      }
    </div>
)}


  export default ProfileCollection

