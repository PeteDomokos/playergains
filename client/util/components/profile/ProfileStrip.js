import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import DeleteIcon from '@material-ui/icons/Delete'
//styles
import {withStyles} from '@material-ui/core/styles'
//children
import { ProfileActions, AdditionalItems } from './Common'

const styles = theme => ({
  root: theme.mixins.gutters({
    minHeight:70,
    maxWidth:400,
    margin: 'auto',
    padding: theme.spacing(1),
    marginTop: theme.spacing(0),
    backgroundColor:'aqua'
  }),
  listItemAvatar: {
    //hover - cursor:pointer
  },
  listItemText:{
    '&:hover':{
      cursor:'pointer',
      textDecoration:'underline'
    }
  },
  mainItems:{
    display:'flex',
    flexWrap:'wrap'
  }
})

const ProfileStrip = 
  ({item, itemType, onClick, actions, selected, minimal, style, classes}) =>{
  const { name, firstName, surname, namePair, desc, _id, created } = item
  const photoUrl = 
      itemType &&_id ? `/api/${itemType}/photo/${_id}?${new Date().getTime()}` : ''
  const primaryText = namePair ? namePair[0] :
    surname ? firstName+" "+surname : name
  const secondaryText = namePair ? namePair[1] : desc
  return(
    <Paper className={classes.root} elevation={4} style={style}>
      <List dense>

        <ListItem className={classes.mainItems}>
          {itemType !== 'dataset' &&
          <ListItemAvatar className={classes.listItemAvatar} onClick={() => onClick(item)}>
            <Avatar src={photoUrl} className={classes.bigAvatar}/>
          </ListItemAvatar>}
          <ListItemText className={classes.listItemText} onClick={() => onClick(item)}
            primary={primaryText} secondary={secondaryText}/>
          {actions && <ProfileActions actions={actions}/>}
        </ListItem>

        {!minimal && 
          <AdditionalItems item={item}/> }
      </List>
    </Paper>
  )
}

ProfileStrip.propTypes = {
}
ProfileStrip.defaultProps = {
}

export default withStyles(styles)(ProfileStrip)
