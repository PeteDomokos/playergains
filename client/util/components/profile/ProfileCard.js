import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'
//children
import { ProfileActions, AdditionalItems } from './Common'

const styles = theme => ({
  root: theme.mixins.gutters({
    width:50,
    backgroundColor:'aqua'
  }),
   list: {
    height:'100%',
  },
  listItem:{
    height:'100%',
    flexDirection:'column',
    justifyContent:'space-between',
    alignItems:'center'
  },
  listItemElement:{
    //TODO -- INJECT COMMON PROPERTIES INTO BOTH CLASSES BELOW
  },
  listItemAvatar:{
    height:45,
    width:45,
  },
  listItemText:{
    flex:'20% 1 1',
  },
})
const ProfileCard = 
  ({item, itemType, onClick, actions, selected, minimal, style, classes}) =>{
  const { name, firstName, surname, namePair, desc, _id, created } = item
  const photoUrl = 
      itemType &&_id ? `/api/${itemType}/photo/${_id}?${new Date().getTime()}` : ''
  //todo - put cardHeight into styles above, using a function
  const cardHeight = actions ? 115 : 100

  const primaryText = namePair ? namePair[0] :
    surname ? firstName+" "+surname : name

  const secondaryText = namePair ? namePair[1] : desc

  return(
    <Paper elevation={4} className={classes.root}
      style={{height:cardHeight, ...style}}>
      <List dense className={classes.list}>
        <ListItem className={classes.listItem}>
          {itemType !== 'dataset' &&
          <ListItemAvatar className={classes.listItemAvatar} onClick={() => onClick(item)}>
            <Avatar src={photoUrl} className={classes.bigAvatar}/>
          </ListItemAvatar>}
          <ListItemText className={classes.listItemText}
            onClick={() => onClick(item)}
            primary={primaryText} secondary={secondaryText}/>
        </ListItem>
        {actions && <ProfileActions actions={actions}/>}
        {!minimal && 
          <AdditionalItems item={item}/> }
      </List>
    </Paper>
  )
}

ProfileCard.propTypes = {
}
ProfileCard.defaultProps = {
}


export default withStyles(styles)(ProfileCard)

/*
<Paper elevation={4} 
      style={{backgroundColor:'aqua', width:80, height:cardHeight}}>
      <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
        <Avatar src={photoUrl} />
        {surname ?
          <div>
            <h5 style={{margin:5, marginBottom:0}}>{firstName}</h5>
            <h5>{surname}</h5>
          </div>
          :
          <div>
             <h5 style={{margin:5, marginBottom:0}}>{name}</h5>
          </div>
        }
      </div>
    </Paper>
*/