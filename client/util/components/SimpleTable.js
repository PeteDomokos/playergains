import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    width: '80vw',
    minWidth:500
  },
  headerCell:{
    color:'blue'
  },
  cell:{
  },
  firstCell:{
  }
});

//todo - make minWidth a function of nrHeadings - see material-ui passing arg into styles

//todo - if value is date, need to apply .toLocaleDateString()
export default function SimpleTable({headings, rows}) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
           {headings.map((head,i) =>
            <TableCell align={i == 0 ? "left" : "center"}
                       className={classes.headerCell}
                       key={'head-'+i}>{head.name} </TableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={'row-'+row._id}>

              <TableCell component="th" scope="row" className={classes.firstCell}>
                {row[headings[0].key]}
              </TableCell>
              {headings.slice(1).map(head =>
                <TableCell component="td" align="center" key={'cell-'+head.key} className={classes.cell} >
                {row[head.key]}</TableCell>)}

            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
