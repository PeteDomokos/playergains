import React, {Component} from 'react'
import PropTypes from 'prop-types'
//material-ui
import IconButton from '@material-ui/core/IconButton'
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';
//video
import ReactPlayer from 'react-player'


const VideoPlayer = ({url, handleCancel, playing}) =>{
  const wrapperStyle = {
    position:'relative', paddingTop:'56.25%', width:'100%'
  }
  const playerStyle = {
    position:'absolute',
    top:0,
    left:0
  }
  const closeStyle={
    //position:'absolute',
    //bottom:-60,
    //right:-60,
    alignSelf:'flex-end',
    height:40,
    width:40,
    color:'#ffa530',
    backgroundColor:'black',
    padding:0
  }
  return(
    <div style={{display:'flex', flexDirection:'column', width:'70%', alignItems:'center'}}>
      <div style={wrapperStyle}>
        <ReactPlayer
          style={playerStyle}
          url={url}
          width='100%'
          height='100%'
          playing={playing}
          controls
        />
      </div>
      <IconButton style={closeStyle} onClick={() => handleCancel('')}>
        <CancelIcon style={{height:'100%', width:'100%', margin:0}}/>
      </IconButton>
    </div>
  )
}

export default VideoPlayer
