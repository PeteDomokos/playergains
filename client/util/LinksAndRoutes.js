export const Links = {
	linkToHome(){
		return '/'
	},
	linkToAbout(){
		return '/about'
	},
	linkToContact(){
		return '/contact'
	},
	linkToResources(){
		return '/resources'
	},
	linkToJoin(){
		return '/join'
	},
	linkToUserProfile(uName){
		return '/' +uName
	},
	linkToFeature(featureName){
		return '/' +featureName
	}
}

//Todo - implement option params too for the last three
export const Routes = {
	routeToHome() { return '/'},
	routeToAnyFeature() {return this.routeToHome() +':feature'},
	routeToFeature(feature) {return this.routeToHome() +feature}
}



