import React, {Component} from 'react'

//helpers
import auth from './../auth/auth-helper'

const Signout = ({onSignout, signingOut, history, isActive}) =>
   <li className='main-menu-item other' style={{color:'white'}}
      onClick={() => onSignout(history)}>
      Sign out</li>

export default Signout
