import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { signin } from '../../actions/Auth'
import Signin from '../Signin'

const mapStateToProps = state => ({
	signingIn:state.asyncProcesses.signingIn
})
const mapDispatchToProps = dispatch => ({
	onSignin(user, history){
		dispatch(signin(user, history))
	}
})

const SigninContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(Signin)

export default withRouter(SigninContainer)