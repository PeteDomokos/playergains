

const auth = jest.genMockFromModule('../auth-helper')

auth.isAuthenticated = () => ({
	user:{
		firstName:'User1FirstName',
		surname:'User1Surname',
		_id:'User1Id',
		groups:[],
		player:mockPlayer
	}
})

export default auth
