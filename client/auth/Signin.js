import React, {Component} from 'react'
import {Redirect, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import { withStyles } from '@material-ui/core/styles'
//helpers
import auth from './../auth/auth-helper'

const styles = theme => ({
  card: {
    maxWidth: '600px',
    width:'80vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300
  },
  submit: {
    marginBottom: theme.spacing(2)
  }
})

class Signin extends Component {
  state = {
      email: '',
      password: '',
      error: ''
  }
  clickGuestSubmit = () =>{
    this.props.onSignin(
        {email: 'deo@y.z', password:'dddddd'}, 
        this.props.history)
  }
  clickSubmit = () =>{
    let user = {
        email: this.state.email || undefined,
        password: this.state.password || undefined
    }
    const error = auth.signinError(user)
    if(error)
      this.setState({error:entry})
    else
      this.props.onSignin(user, this.props.history)
  }
  handleChange = name => event => {
    this.setState({[name]: event.target.value})
  }
  render() {
    const { signingIn, history, classes } =this.props
    /*default to home if location undefined
     const {from} = this.props.location.state || {
      from: {pathname: '/' }}
     const {redirectToReferrer} = this.state
      if (redirectToReferrer) {
        return (<Redirect to={from}/>)
      }
    //TODO - find out why location undefined for redirect- or use solution below 
    -ie pass the loaction into this component from the referrer

    I fixed my problem. the "redirectToSignIn" function can be like this
    redirectToSignin: function(nextState, replace) {
        if (!isSignedIn()) {
            replace({
                pathname: '/signin',
                state: {
                    nextPathname: nextState.location.pathname
                }
            });
        }
    },
    and after successfully sign in
        this.context.router.replace(this.props.location.state.nextPathname);*/
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2" className={classes.title}>
            Sign In
          </Typography>
          <TextField id="email" type="email" label="Email" className={classes.textField} 
            value={this.state.email} onChange={this.handleChange('email')} margin="normal"/><br/>
          <TextField id="password" type="password" label="Password" className={classes.textField} 
            value={this.state.password} onChange={this.handleChange('password')} margin="normal"/>
          <br/> {
            this.state.error && (<Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {this.state.error}
            </Typography>)
          }
        </CardContent>
        <CardActions style={{display:'flex', flexDirection:'column'}}>
          <Button style={{margin:10}} color="primary" variant="contained" 
            onClick={this.clickSubmit} className={classes.submit}>Submit</Button>
          <Button color="primary" variant="contained" onClick={this.clickGuestSubmit} 
            className={classes.submit}>Demo Sign-in</Button>
        </CardActions>
      </Card>
    )
  }
}

Signin.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(withRouter(Signin))
