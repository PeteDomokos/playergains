import React, {Component} from 'react'
import {Redirect, Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import { Dialog, DialogActions, DialogContent, DialogContentText, 
  DialogTitle} from '@material-ui/core'
//icons
import DeleteIcon from '@material-ui/icons/Delete'

class DeleteGroup extends Component {
  render() {
    //history is passed to action creator to push after delete
    const { id, deleting, open, error, openDelete, openDialog, 
      deleteGroup, closeDialog, history } = this.props

    return (<span style={{padding:0}}>
      <IconButton aria-label="Delete" onClick={openDialog} color="secondary">
        <DeleteIcon/>
      </IconButton>

      <Dialog open={open}>
        <DialogTitle>{"Delete Group"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete this group and all it's data?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={() => deleteGroup(id, history)} color="secondary" autoFocus="autoFocus">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </span>)
  }
}
DeleteGroup.propTypes = {
  id: PropTypes.string.isRequired
}
export default withRouter(DeleteGroup)
