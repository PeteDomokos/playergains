import React, { useEffect }  from 'react'
import Group from './Group'
/**
*
**/
const GroupLoader = ({group, loading, onLoad}) => {

	useEffect(() => {
		//if no datasets, then group has not been fully loaded
		if(!group.datasets && !loading){
			onLoad(group._id)
		}
	}, [])
	return (
		<Group group={group} />
		)
}

export default GroupLoader