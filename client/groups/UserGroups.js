import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import { List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
//icons
import ArrowForward from '@material-ui/icons/ArrowForward'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'

//helpers

import { renderActionButton } from '../util/components/Buttons'

const styles = theme => ({
  root: theme.mixins.gutters({
    padding: theme.spacing(1),
    margin: theme.spacing(2),
    marginTop: 0,
    marginBottom: theme.spacing(6),
    minWidth:270,
    height:130
  }),
  header:{
    height:60,
    display:'flex',
    justifyContent:'space-between'
  },
  title: {
    margin: `${theme.spacing(0)}px 0 ${theme.spacing(0)}px`,
    color: theme.palette.openTitle,
    display:'flex',
    alignItems:'center'
  },
  actions:{
    margin: `${theme.spacing(0)}px 0 ${theme.spacing(0)}px`
  },
  item:{
    height:55
  }
})

class UserGroups extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    /**so when we click this link to group, what prompts teh fetch call? is it in GroupLoader ? or 
      should it happen somewhere else? we need to check that group is not already in store, either under the 
      group key, or under user but with the deeper version having replaced the shallow version
      because evry time we save group, we should save it under user.groups too, so we dont have to keep 
      gping back to server if user goes off teh page and then returns.

      anyway, need to sort out all teh signing out stuff to make sur store is kept up to date
    **/
    const { classes, user, groups } = this.props
    const { adminGroups, groupsFollowing, groupsViewed, playerInfo, coachInfo } = this.props.user

    const admin = adminGroups.map(id => groups.find(g => g._id === id))
    const playerGroups = playerInfo.groups.map(id => groups.find(g => g._id === id))
    const coachGroups = coachInfo.groups.map(id => groups.find(g => g._id === id))
    const following = groupsFollowing.map(id => groups.find(g => g._id === id))
    const viewed = groupsViewed.map(id => groups.find(g => g._id === id))

    const addGroupAction = {icon:"add-group", link:"/groups/new"}
    return (
      <Paper className={classes.root} elevation={4}>
        <div className={classes.header} >
          <Typography type="title" className={classes.title}>
            Groups
          </Typography>
          <div className={classes.actions}>
            {renderActionButton(addGroupAction)}</div>
        </div>
        <List dense>
         {
            admin.map(group => {
              const photoUrl = `/api/group/photo/${group._id}?${new Date().getTime()}`
              return( 
                <Link to={"/group/" + group._id} key={group.name}>
                  <ListItem button className={classes.item}>
                    <ListItemAvatar>
                      <Avatar src={photoUrl}>
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={group.name} style={{marginRight:10}}/>
                    <ListItemSecondaryAction>
                    <IconButton>
                        <ArrowForward/>
                    </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
               </Link>
              )
            })
          }
        </List>
      </Paper>
    )
  }
}

UserGroups.propTypes = {
  classes: PropTypes.object.isRequired
}
UserGroups.defaultProps = {
  user:{
    adminGroups:[],
    groupsFollowing:[],
    groupsViewed:[],
    playerInfo:{
      groups:[]
    },
    coachInfo:{
      groups:[]
    }
  },
  groups:[]
}

export default withStyles(styles)(UserGroups)
