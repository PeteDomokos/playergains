import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroup, deleteDataset } from '../../actions/Groups'
import { resetStatus } from '../../actions/Common'
import { allGroups } from '../../util/helpers/StoreHelpers'

import GroupSummary from '../GroupSummary'

const mapStateToProps = (state, ownProps) => {
	const { groupId } = ownProps.match.params
	const group = state.storedItems.groups.find(g => g._id === groupId) || {_id:groupId}
	return({
		group: group,
		loading:state.asyncProcesses.loading.group,

		//todo? change to deleting.group.dataset
		deletingDataset:state.asyncProcesses.updating.group.datasets,
		history:ownProps.history,
	})
}
const mapDispatchToProps = dispatch => ({
	//methods for setting dashboard and pushing to it
	onSaveSelections(selections){
		//save group
	},
	handleDeleteDataset(groupId, datasetId){
		dispatch(deleteDataset(groupId, datasetId))
	},
	resetStatus(){
		dispatch(resetStatus('updating.group.datasets'))
	}
})

//wrap all 4 sections in the same container for now.
const GroupSummaryContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupSummary)

export default GroupSummaryContainer

