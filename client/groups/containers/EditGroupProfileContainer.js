import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { updateGroup } from '../../actions/Groups'

import EditGroupProfile from '../EditGroupProfile'

const mapStateToProps = (state, ownProps) => {
	const { groupId } = ownProps.match.params
	const group = state.storedItems.groups.find(g => g._id === groupId) 
	return({
		group:group,
		updating:state.asyncProcesses.updating.group,
		error:''
	})
}
const mapDispatchToProps = dispatch => ({
	onUpdate(id, formData, history){
		dispatch(updateGroup(id, formData, history))
	}
})

//wrap all 4 sections in the same container for now.
const EditGroupProfileContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(EditGroupProfile)

export default EditGroupProfileContainer

