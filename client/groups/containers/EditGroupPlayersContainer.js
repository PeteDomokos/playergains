import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchEligiblePlayers } from '../../actions/PlayersAndCoaches'
import { fetchGroup, addPlayer, removePlayer } from '../../actions/Groups'
import { playersFromGroupNotInOtherGroup } from '../GroupHelpers'
import { players } from '../../util/helpers/StoreHelpers'

import EditGroupPlayersLoader from '../EditGroupPlayersLoader'

const mapStateToProps = (state, ownProps) => {
	console.log("EditGroupPlayersContainer state", state)
	//todo - filter players so it is only thos taht match teh groups criteria
	//this filter is also applied on server, but the store just stores all players under available
	//so they may have been mixed in with players not available to this particular group
	const { groupId } = ownProps.match.params
	const group = state.storedItems.groups.find(g => g._id === groupId)
	const groupPlayerIds = group.players.map(p => p._id)
	console.log("groupPlayerIds", groupPlayerIds)
	//available
	let eligiblePlayers
	//if group has parent, we get the players from this group if it is loaded
	//if not, we get directly from group.eligiblePlayers
	if(group.parent){
		const parentGroup = state.storedItems.groups.find(g => g._id === group.parent)
		eligiblePlayers = parentGroup ? parentGroup.players : undefined
	}else{
		eligiblePlayers = group.eligiblePlayers
	}
	const availablePlayers = eligiblePlayers ? 
		eligiblePlayers.filter(player => !groupPlayerIds.includes(player._id)) :
		undefined
	return({
		group:group,
		availablePlayers:availablePlayers,
		loading:state.asyncProcesses.loading.players
	})
}
const mapDispatchToProps = dispatch => ({
	onLoad(group){
		if(group.parent)
			dispatch(fetchGroup(group.parent))
		else
			dispatch(fetchEligiblePlayers(group))
	},
	//note - a player is just a user we call player instead. The user contains playerInfo object. 
	handleAdd(player, groupId){
		console.log("adding player")
		dispatch(addPlayer(player, groupId))
	},
	handleRemove(player, groupId){
		console.log("removing player", player)
		dispatch(removePlayer(player, groupId))
	}
})

//wrap all 4 sections in the same container for now.
const EditGroupPlayersContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(EditGroupPlayersLoader)

export default EditGroupPlayersContainer

