import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { openDialog, closeDialog } from '../../actions/Common'
import { deleteGroup } from '../../actions/Groups'

import DeleteGroup from '../DeleteGroup'


const mapStateToProps = (state, ownProps) => {
	return{
		id:ownProps.id,
		deleting:state.asyncProcesses.deleting.group,
		open:state.dialogs.deleteGroup,
		error:''
	}
}
const mapDispatchToProps = dispatch => ({
	openDialog(){
		console.log("openDeleteGroupDialog called...")
		dispatch(openDialog('deleteGroup'))
	},
	deleteGroup(groupId, history){
		dispatch(deleteGroup(groupId, history))
	},
	closeDialog(){
		dispatch(closeDialog('deleteGroup'))
	}
})

//wrap all 4 sections in the same container for now.
const DeleteGroupContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DeleteGroup)

export default DeleteGroupContainer

