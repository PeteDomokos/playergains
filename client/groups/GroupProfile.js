import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import { List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'

//child components
import DeleteGroupContainer from './containers/DeleteGroupContainer'
//helpers
import auth from './../auth/auth-helper'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(0),
    marginTop: theme.spacing(0),
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  },
  additional:{
    padding:0,//theme.spacing(1),
    display:'flex',
    justifyContent:'space-between'
  },
  date:{
    flex:'65% 1 1',
    margin:0//theme.spacing(1)
  },
  update:{
    flex:'60px 0 0',
    margin:0,//theme.spacing(1)
    padding:0
  },
  updateBtn:{
    padding: theme.spacing(1)
  }
})


class GroupProfile extends Component {
  constructor({match}) {
    super()
    this.state = {
      redirectToSignin: false
    }
  }
  render() {
    const { classes, group, authToUpdate, shortVersion } = this.props
    const photoUrl = group._id ? `/api/group/photo/${group._id}?${new Date().getTime()}` : ''

    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>

    return (
        <Paper className={classes.root +' group'} elevation={4}>
          <List dense>

            <ListItem>
              <ListItemAvatar>
                <Avatar src={photoUrl} className={classes.bigAvatar}/>
              </ListItemAvatar>
              <ListItemText primary={group.name} secondary={group.desc}/> 
            </ListItem>
            <Divider/>

            {!shortVersion && 
              <AdditionalItems group={group} authToUpdate={authToUpdate} classes={classes} />}

          </List>
        </Paper>
    )
  }
}
GroupProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  group: PropTypes.object.isRequired
}
GroupProfile.defaultProps = {
  group:{
    name:'', desc:'', _id:''
  }
}

const UpdateActions = ({groupId, classes}) =>
  <ListItem className={classes.update}>
    <Link to={"/group/"+groupId+"/edit"}>
      <IconButton aria-label="Edit" color="primary" className={classes.updateBtn}>
        <Edit/>
      </IconButton>
    </Link>
    <DeleteGroupContainer id={groupId}/>
  </ListItem>

const AdditionalItems = ({group, authToUpdate, classes}) =>
  <List dense className={classes.additional}>
    <ListItem className={classes.date}>
      <ListItemText 
        primary={"Created: " +(new Date(group.created)).toDateString()}/>
    </ListItem>

     {authToUpdate &&
      <UpdateActions groupId={group._id} classes={classes} />}
  </List>

export default withStyles(styles)(GroupProfile)










