import React, { useEffect }  from 'react'
import EditGroupPlayers from './EditGroupPlayers'
/**
*
**/
const EditGroupPlayersLoader 
	=({group, availablePlayers, onLoad, loading, handleAdd, handleRemove}) => {
	console.log("EditGroupPlayersLoader group", group)
	console.log("EditGroupPlayersLoader availablePlayers", availablePlayers)
	useEffect(() => {
		//if no availablePlayers array, then need to either load parent to get parent players,
		//or load all available players from db
		if(!availablePlayers){
			console.log("EditGroupPlayersLoader loading available players...")
			onLoad(group)
		}
	}, [])
	return (
		<EditGroupPlayers
			groupId={group._id}
			players={group.players} availablePlayers={availablePlayers} 
			loading={loading} 
			handleAdd={player => () => handleAdd(player, group._id)} 
			handleRemove={player => () => handleRemove(player, group._id)}/>
		)
}

export default EditGroupPlayersLoader