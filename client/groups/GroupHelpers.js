import { onlyUnique, filterUniqueByProperty } from '../util/helpers/ArrayManipulators'


//todo - check player.groups is just a list of ids
export const commonGroupIds = players =>{
  //its enough just to check all groups of first player
  const groupsToCheck = players[0] ? players[0].playerInfo.groups : []
  //helper
  const getCommon = (commonSoFar, idToCheck, count) =>{
    if(!idToCheck)
      return commonSoFar
    //check group
    const playersInGroup = players.filter(p => p.playerInfo.groups.includes(idToCheck))
    const groupIsCommon = playersInGroup.length === players.length
    if(groupIsCommon)
      return getCommon([...commonSoFar, idToCheck], groupsToCheck[count+1], count+1)
    return getCommon(commonSoFar, groupsToCheck[count+1], count+1)
  }
  return getCommon([], groupsToCheck[0], 0)
}
//helpers for finding group
export const findGroup = (id, groups) => groups.find(g => g._id === id)

/**
*  Returns all of a users groups and subgroups flattened
**/
//todo - finish this
export const flattenGroups = groups =>{
  if(!groups || groups.length == 0)
    return []
  //if subgroups not contained, just return the top level.
  if(!groups[0].subgroups)
    return groups
  //return groups.map(grp => grp.subgroups).reduce((subgroups1, subgroups2) => )
  //need a recurive method
}

export const flattenedDatasets = groups =>
  groups.map(p => p.datasets)
    .reduce((sets1, sets2) => [...sets1, ...sets2], [])

export const getParents = (groups, allGroups) =>{
  const allParents = groups
    .map(g => getGroupParents(g, allGroups))
    .reduce((a,b) => [...a, ...b], [])
  //remove duplicates
  return filterUniqueByProperty("_id", allParents)
}
//returns a list of groups, containing teh parent its parents all the way
//up to teh root group. If a group is not available in allGroups, it 
//returns an object containing the groupId. (This group can then 
//be loaded from server later) 
export const getGroupParents = (group, allGroups) =>{
  if(!group || !allGroups)
    return undefined
 
  const addParent = (parents, group) => {
    if(!group.parent)
      return parents
    else{
      let parent
      const loadedParentGroup = allGroups.find(g => group.parent == g._id)
      if(loadedParentGroup)
        parent = loadedParentGroup
      else
        parent = {_id:group.parent}
      return addParent([...parents, parent], parent)
    }
  }
  return addParent([], group)
}

export const getPlayerGroups = (player, groups) =>
  player.playerInfo.groups.map(groupId =>
    groups.find(g => g._id === groupId))

export const getPlayersGroups = (players, storedGroups) =>{
  //dashboard.groups will be the groups (and their parents) that the 
  //selected players play for - all groups, even if some players are not in one of them
  const playersGroups = players
    .map(p => p.playerInfo.groups) //groups not defined for all players, only for user logged in
    //anyway we  only want the groups played for. also need to change player to playerInfo
    .reduce((arr1, arr2) => [...arr1, ...arr2])
    .map(groupId =>{
      const storedGroup = storedGroups.find(group => group._id === groupId)
      if(storedGroup)
        return storedGroup
      return {_id: groupId} })

  //remove duplications of groups
  //todo - chain with the above by using filter instead
  return filterUniqueByProperty("_id", playersGroups)
}


export const groupsAreSiblings = groups => 
  groups.map(g => g.parent).filter(onlyUnique).length == 1


export const playerFromGroup = (playerId, group) =>{
	if(!group || !group.players)
		return false
	return group.players.find(p => p._id === playerId)
}

export const playersFromGroupNotInOtherGroup = (group, otherGroup) =>{
	if(!group || !group.players)
		return undefined
	return group.players.filter(p => !playerFromGroup(p._id, otherGroup))
}

export const isACommonParentOf = (parentToCheck, groups, allGroups) =>{
  return groups
    .filter(g => isAParentOf(parentToCheck, g, allGroups))
    .length == groups.length
}
//returns true if parentToCheck is a parent of group, or grandparent and so on...
//note - if a parent is not stored in allGroups, this method returns undefined
export const isAParentOf = (parentToCheck, group, allGroups) =>{
  const checkParentOf = group => {
    if(!group.parent)
      return false
    if(group.parent === parentToCheck._id)
      return true
    const parentGroup = allGroups.find(g => g._id === group.parent)
    if(!parentGroup)
      return undefined
    return checkParentOf(parentGroup)
  }
  checkParentOf(group)
}
