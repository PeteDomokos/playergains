import React, {Component} from 'react'
import {Link, Redirect, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import { Card, CardActions, CardContent } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import Avatar from '@material-ui/core/Avatar'
//icons
import Publish from '@material-ui/icons/Publish'
//styles
import {withStyles} from '@material-ui/core/styles'
//helpers
import auth from './../auth/auth-helper'

const styles = theme => ({
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  title: {
    margin: theme.spacing(2),
    color: theme.palette.protectedTitle
  },
  error: {
    verticalAlign: 'middle'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  bigAvatar: {
    width: 60,
    height: 60,
    margin: 'auto'
  },
  input: {
    display: 'none'
  },
  filename:{
    marginLeft:'10px'
  }
})

//private route - only available to group admin
class EditGroupProfile extends Component {
  constructor({match}) {
    super()
    this.state = {
      group:{name:'', desc:'', _id:'', admin:[], players:[]},
      error:'' //could be an error with what is entered whic can be caught 
      //here in a helper method
    }
    this.match = match
  }
  componentDidMount = () => {
    window.scrollTo(0, 0)
    this.groupData = new FormData()
    //set current group details into state so they can be updated
    this.setState({group:this.props.group})
  }
  /* we may need this as this.groupData is not yet available when rendered so cannot reference
  it within render()

  clickSubmit = () => {
    this.props.onUpdate(this.groupData)
  }*/
  handleChange = name => event => {
    const value = name === 'photo'
      ? event.target.files[0]
      : event.target.value
    this.groupData.set(name, value)
    let updatedGroup = {...this.state.group, [name]: value }
    this.setState({ group:updatedGroup })
  }
  render() {
    const {classes, updating, error, onUpdate, history} = this.props
    //group loaded into state in componentDidMount
    const { group } = this.state
    //if(this.state.redirectToGroup)
      //return (<Redirect to={'/group/' + group._id}/>)

    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2" className={classes.title}>
            Edit Group
          </Typography>
          <UpdatePhoto 
            photo={this.state.photo} 
            id={this.state.group._id}
            handleChange={this.handleChange} 
            classes={classes}/>
          <UpdateTextFields
            group={this.state.group} 
            handleChange={this.handleChange} 
            classes={classes}/>

          {this.state.error && (<Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {this.state.error}
            </Typography>)}
        </CardContent>
        <CardActions>
          <Button color="primary" variant="contained" 
            onClick={() =>onUpdate(group._id, this.groupData, history)} 
            className={classes.submit}>Submit</Button>
        </CardActions>
      </Card>
    )
  }
}

EditGroupProfile.defaultProps = {
}

const UpdatePhoto = ({photo, id, handleChange, classes}) =>{
  const url = id
    ? `/api/group/photo/${id}?${new Date().getTime()}`
    : '/api/groups/defaultphoto'

  return(
    <React.Fragment>
      <Avatar src={url} className={classes.bigAvatar}/><br/>
      <input accept="image/*" onChange={handleChange('photo')} className={classes.input} id="icon-button-file" type="file" />
      <label htmlFor="icon-button-file">
        <Button variant="contained" color="default" component="span">
          Upload
          <Publish/>
        </Button>
      </label> <span className={classes.filename}>{photo ? photo.name : ''}</span><br/>
    </React.Fragment>
  )
}

const UpdateTextFields = ({group, handleChange, classes}) =>
   <React.Fragment>
      <TextField id="name" label="name" className={classes.textField} value={group.name} 
            onChange={handleChange('name')} margin="normal"/><br/>
      <TextField id="desc" label="desc" className={classes.textField} value={group.desc} 
            onChange={handleChange('desc')} margin="normal"/><br/>
    </React.Fragment>


EditGroupProfile.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(withRouter(EditGroupProfile))
