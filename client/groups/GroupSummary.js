import React, {Component, useState} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import GroupProfile from './GroupProfile'
import ProfileCollection from '../util/components/profile/ProfileCollection'
import { DialogWrapper } from '../util/components/Dialog'
//helpers
import { datasetNamePair } from '../core/datasets/DatasetHelpers'
import auth from './../auth/auth-helper'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(3),
    marginTop: theme.spacing(2),
    marginBottom: 300// theme.spacing(10)
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  }
})

class GroupSummary extends Component {
  render() {
    const {classes, group, handleDeleteDataset, loading, deletingDataset, 
      history, onSaveSelections, resetStatus} = this.props
    const jwt = auth.isAuthenticated()
    //note - admin is an array of user objects with id, firstName and surname
    const authToUpdate = jwt && group.admin && 
      (group.admin.map(user => user._id).includes(jwt.user._id) || 
        jwt.user.isSystemAdmin)

    const datasetsWithNamePairs = group.datasets ?
      group.datasets.map(d => {return {...d, namePair:datasetNamePair(d, group.datasets)} })
      : []
    //format users if they are only ids
    const admin = group.admin[0] && group.admin[0]._id ? 
      group.admin : group.admin.map(user => {return{_id:user}})
    const players = group.players[0] && group.players[0]._id ? 
      group.players : group.players.map(p => {return{_id:p}})
    const coaches = group.coaches[0] && group.coaches[0]._id ? 
      group.coaches : group.coaches.map(c => {return{_id:c}})

    //by default, if no dataset specified, it will be all datasets for the group,
    //including inherited
    const renderDashboard = datasetId =>{
      //get groups parent ids
      const groupSelections = groupIdWithParentIds(group)
      const parents = groupParents()
      const datasetSelections = [datasetId] || groupDatasetsIncludingInherited(group, parents)
      onSaveSelections([
        {path:'dashboard.groups', value:[]}, 
        {path:'dashboard.datasets', value:[]}
        ])
      //push to dashboard
      history.push("/dashboard")
    }

    return (
      <Paper className={classes.root +' group'} elevation={4}>
        <GroupProfile group={group} authToUpdate={authToUpdate} />
        <GroupAdmin adminUsers={admin}/>
        <CoachesSummary coaches={coaches} groupId={group._id}/>
        <PlayersSummary players={players} groupId={group._id}/>
        <DatasetsSummary datasets={datasetsWithNamePairs} groupId={group._id} 
          handleDelete={handleDeleteDataset} deleting={deletingDataset} 
          history={history} renderDashboard={renderDashboard} resetStatus={resetStatus}/>
      </Paper>
      )
  }
}

GroupSummary.propTypes = {
  classes: PropTypes.object.isRequired,
  group: PropTypes.object.isRequired
}

GroupSummary.defaultProps = {
}

const GroupAdmin = ({adminUsers}) =>{
  const titleStyle = {marginTop:'2vh', marginBottom:'2vh'}
  const listStyle = {margin:'5vh', marginTop:'2vh', marginBottom:'2vh'}
  return(
    <div className='admin-users users'>
      <h4 style={titleStyle}>Admin</h4>
      <div style={listStyle}>
        {adminUsers.map(u =>
          <div className='user-list-item' key={'admin'+u._id}>{u.username}</div>)
        }
      </div>
    </div>
)}
GroupAdmin.defaultProps = {
  adminUsers:[]
}

const CoachesSummary = ({coaches, groupId}) =>
  <div style={{margin:30, marginLeft:0, marginRight:0}}>
    <ProfileCollection format='grid' title='Coaches' itemType='user' items={coaches} 
      emptyMesg='No coaches in group yet' />
     <Link to={"/group/"+groupId+'/coaches/edit'}>
      <button className='btn btn-primary'>
       {coaches.length == 0 ? 'Add Coaches' : 'Edit Coaches'}</button>
    </Link>
  </div>

CoachesSummary.defaultProps = {
  coaches:[]
}

const PlayersSummary = ({title, players, groupId}) =>{
  return(
  <div style={{margin:30, marginLeft:0, marginRight:0}}>
    <ProfileCollection format='grid' title='Players' itemType='user' items={players} 
      emptyMesg='No players in group yet' />
    <Link to={"/group/"+groupId+'/players/edit'}>
      <button className='btn btn-primary'>
       {players.length == 0 ? 'Add Players' : 'Edit Players'}</button>
    </Link>
  </div>)
}

PlayersSummary.defaultProps = {
  players:[]
}

const DatasetsSummary = ({datasets, groupId, history, handleDelete, deleting, resetStatus, renderDashboard}) =>{
  //for some reason, can't add condition to initValue, so need to use setDialogOpen
  const [dialogOpen, setDialogOpen] = useState(false)
  if(deleting && !dialogOpen)
    setDialogOpen(true)

  if(deleting.complete || deleting.error){
    setTimeout(() =>{
      resetStatus()
      //component doesnt update so need to manually reset dialogOpen
      setDialogOpen(false)
    },1000)
  }

  const onDelete = datasetId =>
    handleDelete(groupId, datasetId)

  const dialogButtonsForDelete = datasetId => [
    {label:'Cancel', id:'cancel'},
    {label:'Confirm', onClick:() => onDelete(datasetId), id:'confirm'}
  ]
  const actions = dataset => {
    return [
      {icon:'data', label:'view data',
        onClick:() => renderDashboard(groupId, dataset._id),
        id:dataset._id},
      {icon:'add', label:'add data', 
        link:"/group/"+groupId+'/datasets/'+dataset._id+"/datapoints/new",
        id:dataset._id},
      //link to DeleteDataset component
      {
        icon:'delete', label:'delete', 
        dialog:{
          title:'Delete',
          mesg:'Are you sure you want to delete dataset and all it\'s data?',
          buttons:dialogButtonsForDelete(dataset._id)
        },
        id:dataset._id
      }
    ]
  }

  const onClickDataset = dataset => {
    history.push("/group/"+groupId+'/datasets/'+dataset._id)
  }
  //temp mock names
  const _datasets = datasets.map(dset =>{
    if(dset.name === '5-metre Acceleration'){
      return {...dset, name:'5m Max Acceleration Test'}
    }else if(dset.name === 'Best Acceleration'){
      return {...dset, name:'Top Acceleration in Match'}
    }
    return dset
  })
  return(
    <div style={{margin:30, marginLeft:0, marginRight:0}}>
      <ProfileCollection format='list' title='Datasets' itemType='dataset' items={datasets} 
        onClickItem={onClickDataset} actions={actions} emptyMesg='No datasets added yet' />
      <Link to={"/group/"+groupId+'/datasets/new'}>
        <button className='btn btn-primary'>Add Dataset</button>
      </Link>

        <DialogWrapper 
          open={dialogOpen} 
          title='Delete Dataset'
          mesg={
            deleting.pending ?  'Deleting. Please Wait...' : 
            deleting.error ? deleting.error : 'Dataset has been deleted.'} />
    </div>
    )
}

DatasetsSummary.defaultProps = {
  datasets:[]
}

export default withStyles(styles)(GroupSummary)