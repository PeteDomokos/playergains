import C from './constants/ConstantsForStore'
import _ from 'lodash'
import * as cloneDeep from 'lodash/cloneDeep'
import { InitialState } from './constants/InitialStateForStore'
import { userGroupIds, mergedUserGroups, userGroupsFromIds } from './util/helpers/StoreHelpers'
import { filterUniqueByProperty } from './util/helpers/ArrayManipulators'

//helper
const shallowUser = user =>{
	const { adminGroups, groupsFollowing, groupsViewed, 
		playerInfo, coachInfo } = user
	return {
		...user,
		adminGroups:adminGroups.map(g => g._id),
		groupsFollowing:groupsFollowing.map(g => g._id),
		groupsViewed:groupsViewed.map(g => g._id),
		playerInfo:{
			...playerInfo, 
			groups:playerInfo.groups.map(g => g._id)
		},
		coachInfo:{
			...coachInfo,
			groups:coachInfo.groups.map(g => g._id)
		}
	}
}

//user just stores the ids of groups. These ids dont ever change,
//so they can only be added or removed in user
//anything else about the groups are stored and updated in storedItems.groups
export const user= (state={}, action) =>{
	switch(action.type){
		case C.SIGN_IN:
			return shallowUser(action.user)
		case C.SAVE_USER:{
			//check we are not updatng another user (eg system admin)
			if(action.userIsSignedIn)
				return shallowUser(action.user)
			return state
		}
		case C.SIGN_OUT:{
			return InitialState.user
		}
		case C.SAVE_NEW_GROUP:{
			//saves a newly created group into adminGroups
			//ADD_GROUP will add an existing groupId into a specific location eg groupsFollowing
			return {
				...state, 
				adminGroups:[...state.adminGroups, action.group._id]}
		}
		case C.DELETE_GROUP:{
			//deletes the group from every location 
			//(note - REMOVE_GROUP will remove from only one location)
			return {
				...state, 
				adminGroups:state.adminGroups.filter(groupId  => groupId !== action.id),
				groupsFollowing:state.groupsFollowing.filter(groupId  => groupId !== action.id),
				groupsViewed:state.groupsViewed.filter(groupId  => groupId !== action.id),
				playerInfo:{
					...state.playerInfo, 
					groups:state.playerInfo.groups.filter(groupId  => groupId !== action.id)
				},
				coachInfo:{
					...state.coachInfo, 
					groups:state.coachInfo.groups.filter(groupId  => groupId !== action.id)
				}
			}
		}
		case C.ADD_PLAYER:{
			if(action.player._id === state._id){
				//add group to users playerInfo
				const _playerInfo = {
					...state.playerInfo, 
					groups:[...state.playerInfo.groups, action.groupId]
				}
				return {...state, playerInfo:_playerInfo}
			}
			return state
		}
		case C.REMOVE_PLAYER:{
			if(action.player._id === state._id){
				//remove group from users playerInfo
				const _playerInfo = {
					...state.playerInfo, 
					groups:state.playerInfo.groups.filter(g => g._id === action.groupId)
				}
				return {...state, playerInfo:_playerInfo}
			}
			return state
		}
		default:{
			return state
		}
	}
}

export const storedItems = (state={}, action) =>{
	switch(action.type){
		case C.SIGN_IN:{
			return {...state, groups:groups(state.groups, action)}
		}
		case C.SAVE_USER:{
			//saves all the users groups into groups too, and here it only saves shallow
			//WARNING - IT DOESNT SAVE USERS GROUPS WHEN SAVING MULTIPLE USERS
			//user ie groups are just ids
			const otherUsers = state.users.filter(user => user._id === action.user._id)
			const users = [...otherUsers, shallowUser(action.user)]

			//add groups to storedItems if its the signed in user
			return {
				...state, users:users, groups:groups(state.groups, action)
			}
		}
		case C.SAVE_USERS:{
			//WARNING - USERS' GROUPS ARE NOT SAVED TO STORE HERE AS ITS A
			//LIST OF MANY USERS, SO DOESNT EVEN INCLUDE THEIR GROUPS
			const usersToSaveIds = action.users.map(user => user._id)
			const otherUsers = state.users.filter(user => !usersToSaveIds.includes(user._id))
			return {...state, users:[...otherUsers, ...action.users]}
		}
		/*
		for now, only the signed in uer can update themselves
		todo - enable admin user to update here
		case C.UPDATE_USER:{
			return {...state, users:users(state.users, action)}
		}
		case C.DELETE_USER:{
			return {...state, users:groups(state.users, action)}
		}*/
		case C.SAVE_GROUP:{
			return {
				...state, groups:groups(state.groups, action)
			}
		}
		case C.SAVE_GROUPS:{
			return {
				...state, groups:groups(state.groups, action)
			}
		}
		case C.SAVE_NEW_GROUP:{
			return {...state, groups:groups(state.groups, action)}
		}
		case C.DELETE_GROUP:{
			return {...state, groups:groups(state.groups, action)}
		}
		case C.SAVE_ELIGIBLE_PLAYERS:{
			return {...state, groups:state.groups.map(g => group(g, action))}
		}
		case C.ADD_PLAYER:{
			return {
				...state, 
				groups:groups(state.groups, action),
				//add group to user.playerInfo for this player if stored
				users:state.users.map(u => user(u,action))
			}
		}
		case C.REMOVE_PLAYER:{
			return {
				...state, 
				groups:groups(state.groups, action),
				//remove group from user.playerInfo for this player if stored
				users:state.users.map(u => user(u, action))
			}
		}
		default:{
			return state
		}
	}
}

const groups = (state={}, action) =>{
	switch(action.type){
		case C.SIGN_IN:{
			//some groups may already be stored eg if a user was viewing them before signing in
			const userGroups = mergedUserGroups(action.user)
			return filterUniqueByProperty("_id", 
				[...state, ...userGroups])
		}
		case C.SAVE_USER:{
			//saves the users groups
			//some groups may already be stored eg if a user was viewing them before signing in
			//or if another user is signed in and they are connected to the group too
			const userGroups = mergedUserGroups(action.user)
			return filterUniqueByProperty("_id", 
				[...state, ...userGroups])
		}
		case C.SAVE_GROUP:{
			//removes any old version and adds new version of group
			const otherGroups = state.filter(g => g._id !== action.group._id)
			return [...otherGroups, action.group]
		}
		case C.SAVE_GROUPS:{
			//removes any old version and adds new version of group
			const otherGroups = state.filter(group => 
				!action.groups.find(g => g._id === group._id))
			return [...otherGroups, ...action.groups]
		}
		case C.SAVE_NEW_GROUP:{
			return [...state, action.group]
		}
		case C.UPDATE_GROUP:{
			//updates any existing version of group, or adds if new
			const groupToUpdate = state.find(g => g._id === action.group._id) || {}
			const otherGroups = state.filter(g => g._id !== action.group._id)
			return [...otherGroups, {...groupToUpdate, ...action.group}]
		}
		case C.DELETE_GROUP:{
			return state.filter(g => g._id !== action.id)
		}
		case C.ADD_PLAYER:{
			const groupToUpdate = state.find(g => g._id === action.groupId)
			const otherGroups = state.filter(g => g._id !== action.groupId)
			return [...otherGroups, group(groupToUpdate, action)]
		}
		case C.REMOVE_PLAYER:{
			const groupToUpdate = state.find(g => g._id === action.groupId)
			const otherGroups = state.filter(g => g._id !== action.groupId)
			return [...otherGroups, group(groupToUpdate, action)]
		}
		default:
			return state
	}

}
const group = (state={}, action) =>{
	switch(action.type){
		//todo - change to update group
		//note - no need to check group as it was done in groups reducer above
		case C.SAVE_ELIGIBLE_PLAYERS:{
			if(action.groupId !== state._id)
				return state 
			return {...state, eligiblePlayers:action.players}
		}
		case C.ADD_PLAYER:{
			const otherPlayers = state.players.filter(p => p._id !== action.player._id)
			return {...state, players:[...otherPlayers, action.player]}
		}
		case C.REMOVE_PLAYER:{
			const otherPlayers = state.players.filter(p => p._id !== action.player._id)
			return {...state, players:otherPlayers}
		}
		default:
			return state
	}

}

//todo - clear available when component that needs it is finished
export const available = (state={}, action) =>{
	switch(action.type){
		case C.SIGN_OUT:{
			//todo - impl this return to init state
			return InitialState.available
		}
		/*case C.SAVE_AVAILABLE:
			return {...state, [action.entity]:action.value}*/
		case C.SAVE_ELIGIBLE_PLAYERS:
			return {...state, players:action.players}
		case C.SAVE_GROUPS:
			return {...state, groups:action.groups}
		default:
			return state
	}
}


export const dashboard= (state={}, action) =>{
	const { type, path, value } = action
	
	switch(type){
		case C.SIGN_OUT:{
			//todo - impl this return to init state
			return InitialState.dashboard
		}
		case C.SET_DASHBOARD_FILTER:{
			//if(type == subgroup)
				//reset players, player
				//if(!inherit) reset datasets
			return {
				state, 
				filters:{...state.filters, [path]:value}
			}
		}
		//selections is an array of {} with a path and a value
		/*case C.SAVE_SELECTIONS:{
			const dashboardUpdates = action.selections
				.filter(sel => sel.path.includes("dashboard"))
				.map(sel => {
					return{
						path:sel.path.replace("dashboard.", ""),
						value:sel.value
					}
				})
 			//add each selection update to the new state object
			let _state = cloneDeep(state)
			dashboardUpdates.forEach(update =>{
				_state[update.path] = update.value
			})
			return _state
		}*/

		case C.SAVE_SELECTIONS:{
			//todo - swicth all this around so we have a selections reducer which routes the path
			if(action.path.includes("dashboard")){
				const innerPath = action.path.replace("dashboard.", "")
				//for now, innerPath is 1 level, but when we have subpaths then use lodash _.set

				//todo - refactor - in store have selected:{groups:false, datasets:false}
				if(action.path.includes("datasets"))
					return {
						...state,
						datasetsSelected:true,
						[innerPath]:action.selections
					}
				else
					return {
						...state,
						selected:true,
						[innerPath]:action.selections
					}

			}
		}
		case C.START_CHANGING_SELECTIONS:{
			return {
				...state,
				selected:false
			}
		}
		default:
			return state
	}
}
//todo - look into whether we can use the same groups reducer for otherGroups instead,
//and user for otherPlayers

//todo - generalise action to just FETCH_START

export const asyncProcesses= (state={}, action) =>{
	const { type, path, value } = action
	switch(type){
		case C.ERROR:{
			let _state = cloneDeep(state)
			_.set(_state, path, {error:value})
			return _state
		}
		case C.SIGN_OUT:{
			//todo - impl this return to init state
			return InitialState.asyncProcesses
		}
		case C.START:{
			let _state = cloneDeep(state)
			_.set(_state, path, {pending:true})
			return _state
		}
		case C.END:{
			let _state = cloneDeep(state)
			if(path.includes('updating')){
				_.set(_state, path, {complete:true})
			}
			//if loading, then no need for the referring components to receive 
			//completion message as they will receive the data via the store anyway
			//todo - decide how to handle open being set to true and error messages
			//else if(path.includes('creating'))
				//_.set(_state, path, {complete:true})
			else
				_.set(_state, path, false)
			return _state			
		}
		case C.RESET_STATUS:{
			let _state = cloneDeep(state)
			_.set(_state, path, false)
			return _state
		}
		case C.SET_DASHBOARD_FILTER:{
			//if(type == subgroup)
				//reset players, player
				//if(!inherit) reset datasets
			return {
				state, 
				dashboardFilters:{...state.dashboardFilters, [path]:value}
			}
		}
		default:
			return state
	}
}

//NOT NEEDED NOW
//todo - instead of a boolean, use a count so that dialogs can haev multiple stages
//may use that for question answer tracking
export const dialogs = (state={}, action) =>{
	const { type, path, value } = action
	switch(type){
		case C.ERROR:{
		}
		case C.SIGN_OUT:{
			//todo - impl this return to init state
			return InitialState.dialogs
		}
		case C.OPEN_DIALOG:{
			let _state = cloneDeep(state)
			_.set(_state, path, true)
			return _state
		}
		case C.CLOSE_DIALOG:{
			let _state = cloneDeep(state)
			_.set(_state, path, false)
			return _state			
		}
		//automatically close dialog upon deletion
		case C.DELETE_GROUP:{
			let _state = cloneDeep(state)
			_.set(_state, "deleteGroup", false)
			return _state			
		}
		default:
			return state
	}
}

/*
//TODO throw an error if uName and pw are incorrect format. This can be caught in error middleware (see redux middleware tutorial on you tube) 
export const user= (state={}, action) =>{
	switch(action.type){
		case C.LOGIN:
			return action.user
		case C.DEMO_LOGIN:
			return users.find((u => u.uName == "Brian" && u.pw == "p"))
		default:
			return state
	}
}

export const profiles = (state={}, action) =>{
	switch(action.type){
		default:
			return state
	}
}

export const tracker = (state={}, action) =>{
	switch(action.type){
		case C.SET_TRACKER:
			return action.tracker
		case C.UPDATE_FEATURE_ACTIVE:
			return {...state, featureActive: action.featureName}
		case C.UPDATE_REP_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.UPDATE_REPORT_SECT_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.UPDATE_REPORT_ACT_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.OVERRIDE_Q_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		default:
			return state
		}
}
export const reportTracker = (state={}, action) =>{
	switch(action.type){
		case C.UPDATE_REP_ACTIVE:
			return {repActive: action.repId, sectActive: 0, actActive: 0, qActiveOverride: -1}
		case C.UPDATE_REPORT_SECT_ACTIVE:
			return {...state, sectActive: action.sectNr, actActive: 0, qActiveOverride: -1}
		case C.UPDATE_REPORT_ACT_ACTIVE:
			return {...state, actActive: action.actNr, qActiveOverride: -1}
		case C.OVERRIDE_Q_ACTIVE:
			return {...state, qActiveOverride: action.qNr}
	}
}

export const reports = (state={}, action) =>{
	switch(action.type){
		case C.SET_REPORTS:{
			return action.reports
		}
		case C.SAVE_PART_SEC_ANSWER:{
			return state.map(rep => report(rep, action))
		}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return state.map(rep => report(rep, action))
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return state.map(rep => report(rep, action))
		}
		case C.UPDATE_PART_ACTIVE:{
			return state.map(rep => report(rep, action))
		}
		case C.Q_MOVE_ON:
			return state.map(rep => report(rep, action))
		default:
			return state
	}
}
export const report = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_PART_SEC_ANSWER:
			return (state.id !== Number(action.repId)) ?
				state
				:
				{...state, userAnswers: state.userAnswers.map(uAns  => userAnswers(uAns, action))}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.id !== action.repTracker.repActive) ?
				//state
				//:
				//{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return (state.id !== action.repId) ?
				state
				:
				{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		}
		case C.UPDATE_PART_ACTIVE:
			return (state.id !== Number(action.repId)) ?
				state
				:
				{...state, partsActive: state.partsActive.map(partAct  => partsActive(partAct, action))}
		
		case C.Q_MOVE_ON:
			return (state.id !== action.repTracker.repActive) ?
				state
				:
				{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		default:
			return state
	}
}
export const partsActive = (state={}, action) =>{
	switch(action.type){
		case C.UPDATE_PART_ACTIVE:{
			if(state.sectNr !== action.sectNr)
				return state
			if(state.actNr !== action.actNr)
				return state
			if(state.qNr !== action.qNr)
				return state
			else
				return {...state, nr: action.partNrActive}
		}
		default:
			return state
	}
}
export const userAnswers = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_PART_SEC_ANSWER:{
			if(state.sectNr !== action.sectNr)
				return state
			if(state.actNr !== action.actNr)
				return state
			if(state.qNr !== action.qNr)
				return state
			if(state.partNr !== action.partNr)
				return state
			if(state.ansSecNr !== action.ansSecNr)
				return state
			else
				return {...state, ans: action.answer}
		}
		default:
			return state
	}
}
export const reportSect = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return (state.nr !== action.sectNr) ?
				state
				:
				{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.nr !== action.repTracker.sectActive) ?
				//state
				//:
				//{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		case C.Q_MOVE_ON:
			return (state.nr !== action.repTracker.sectActive) ?
				state
				:
				{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		default:
			return state
	}
}
export const reportSectAct = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:
			return (state.nr !== action.actNr) ?
				state
				:
				{...state, intro: reportSectActIntro(state.intro, action)}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.nr !== action.repTracker.actActive) ?
				//state
				//:
				//{...state, introStatus:"completed"}
		case C.Q_MOVE_ON:
			return (state.nr !== action.repTracker.actActive) ?
				state
				:
				{...state, qs:state.qs.map(q => reportSectActQ(q, action))}
		default:
			return state
	}
}
//TODO !!!!!!!!!!!!!!!!!!!!!!!!!! - get rid of status here- make it part of userAnswers in report
export const reportSectActIntro = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:
			return {...state, status:"completed"}
		default:
			return state
	}
}
export const reportSectActQ = (state={}, action) =>{
	switch(action.type){
		case C.Q_MOVE_ON:
			return (state.nr !== action.qNr) ?
				state
				:
				{...state, movedOn:true}
		default:
			return state
	}
}

export const reportSectActQPart= (state={}, action) =>{
	switch(action.type){
		default:
			return state
		}
}

export const reportSectActQPartSec= (state={}, action) =>{
	switch(action.type){
		default:
			return state
		}
}
*/

