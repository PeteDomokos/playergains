import C from '../constants/ConstantsForStore'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'
import auth from './../auth/auth-helper'

export const signin = (user, history, redirectTo) => dispatch =>{
	fetchThenDispatch(dispatch, 
		'signingIn',
		{
			url: '/auth/signin/',
			method:'POST',
			body: JSON.stringify(user),
			//credentials:'include' ?????
			processor: data => {
				//save to session storage
				console.log("actions signin data", data)
				const userCredentials = {email:data.user.email, _id:data.user._id, isSystemAdmin:data.user.isSystemAdmin}
				const jwt = {...data, user:userCredentials}
				auth.authenticate(jwt, () => {
				console.log('redirectTo', redirectTo)
				  //return to referrer ---todo use location from

				  //todo - have another param which determines where to push to
				  //eg demo dashboard will push to dashboard/group/groupId
				  console.log('pushing.........')
				  if(!redirectTo  || redirectTo == 'user-home'){
				  	history.push("/")
				  }else if(redirectTo == 'demoDashboard'){
				  	console.log('pushing to demo dashboard')
				  	//todo - get groupId from store
				  	history.push('/dashboard/group/5f00fe200af43e502f3cf857')
				  }else{
				  	history.push(redirectTo)
				  }
		        })
		        //save to store
		        return {
					type:C.SIGN_IN, user:data.user
				}
			}
		})
}

export const signout = history => dispatch =>{
	fetchThenDispatch(dispatch, 
		'signingOut',
		{
			url: '/auth/signout/',
			headers:{},
			processor: data => {
				//clear sessionStorage and cookies
				if (typeof window !== "undefined")
      				sessionStorage.removeItem('jwt')
      			document.cookie = "t=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
      			//return to home
				history.push("/")
				//clear user in store
				return {type:C.SIGN_OUT}
			}
		})
}