import C from '../constants/ConstantsForStore'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'

//todo
//options are for filtering te request. can be playerIds, or groupIds, as group that
//contains the dataset may be a parent
export const fetchDatapoints = (datasets, filters) => dispatch => {
	const { subgroupIds, playerIds } = filters
	//if datasetIds is just one id, put it into array
	const datasetIdsArray = Array.isArray(datasets) ? datasets : [datasets]

	//decide - 1 api call or one for each dataset?
	//and if one call. can i do a get request with a query eg id includes....

	//ans - impl as below - id2, id2 etc for datasetIds
	//datasets may reference multiple groups, so filter and make separate api call for each

	//step 1  - sort by groupId

	//step 2 - api call
	//const url='/api/group/'+groupId+'/datasets?ids=id1&ids=id2&ids=id3&ids=id4&ids=id5'
	/*fetchThenDispatch(dispatch, 
		'loading.datapoints', 
		{
			url: '/api/.................................................' +id, 
			requireAuth:true,
			processor: data => {
				return {
					type:C.SAVE_DATAPOINTS, groupId:groupId, 
					datasetIds:datasetIdsArray, value:data
				}
			}
		})*/
}

export const setDashboardFilter = (path, value) => dispatch => (
	{
		type: C.SET_DASHBOARD_FILTER,
		path:path,
		value:value
	}
)

/**export const saveSelections = selections => dispatch => {
	const formattedSelections = selections.map(sel =>{
		//get rid of first '/' and replace others with .
		const formattedPath = sel.path.charAt(0) === '/' ? 
			sel.path.replace("/", ".").slice(1) : sel.path.replace("/", ".")
		return {
			path:formattedPath,
			value:sel.value
		}
	})
	
	console.log("formattedSelections", formattedSelections)
	dispatch({
		type: C.SAVE_SELECTIONS,
		selections:formattedSelections
	})
}**/

export const saveSelections = (selections, path) => dispatch => {
	console.log("action...saveSelections path:", path)
	const formattedPath = path.charAt(0) === '/' ? 
		path.replace("/", ".").slice(1) : path.replace("/", ".")
	console.log("formattedPath", formattedPath)
	dispatch({
		type: C.SAVE_SELECTIONS,
		path:formattedPath,
		selections:selections
	})
}
