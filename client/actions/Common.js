import C from '../constants/ConstantsForStore'
import auth from '../auth/auth-helper'

//HELPER METHODS
export const status = resp =>{
	if (resp.status != 200)
		return Promise.reject(new Error(resp.statusText))
	return Promise.resolve(resp)
}
export const parseResponse = resp => {
	console.log('response...', resp)
	return resp.json()
}
export const logError = (dispatch, err, path) =>{
	//todo - process other types of error, and retrieve mesg from server
	console.error("There is an action error!!!", err)
	const mesg = 'You are signed in as Demo Coach, so you cannot make changes. Signout and then sign up to create your own account.'
	const error = { type:'demoUser', mesg:mesg }
	dispatch({type:C.ERROR, value:error, path:path})
}
//higher-order actions
export const fetchStart = path => (
	{
		type: C.START,
		path:path
	}
)
export const fetchEnd = path => (
	{
		type: C.END,
		path:path
	}
)
//async
/**
*	fetches data from server and dispatched it to the store, 
*   Warning - server must return an action object.
*
*   Fetch Defaults: method GET, content-type json, no authentication
*/
export const fetchThenDispatch = (dispatch, asyncProcessesPath, options, responseProcessor) => {
	dispatch(fetchStart(asyncProcessesPath))

	const { method, url, headers, body, requireAuth, processor, errorHandler } = options
	const handleError = errorHandler ? errorHandler : logError
	const nullAction = {type:C.NO_ACTION}
	const processResponse = processor ? processor : function(data){return nullAction}

	const requiredHeaders = headers ? headers : {
		'Accept': 'application/json', 
     	'Content-Type': 'application/json'
	} 
	if(requireAuth){
		const jwt = auth.isAuthenticated()
		requiredHeaders['Authorization'] ='Bearer '+jwt.token
	}
	const settings = {
		method: method ? method : 'GET',
		headers: requiredHeaders,
		body:body //maybe undefined
	}
	
	fetch(url, settings)
	  .then(status)
	  .then(parseResponse)
	  .then(data =>{
	  	console.log("Response from server: ", data)
	  	return data
	  })
	  .then(processResponse)
	  .then(data =>{
	  	console.log("dispatching data:", data)
	  	dispatch(data)
	  })
	  .then(data =>{
	  	dispatch(fetchEnd(asyncProcessesPath))
	  })
	  //todo - put an error action into the catch, or an optional one, inc dispatch fetch end
	  .catch(err => logError(dispatch, err, asyncProcessesPath))
}

export const resetStatus = path => (
	{
		type:C.RESET_STATUS,
		path:path
	}
)

export const openDialog = path => {
	console.log("actions.Dialog() path", path)
	return {
		type:C.OPEN_DIALOG,
		path:path
	}
}

//todo - impl deleteUser and genralise the openDialog and closeDialog methods into Common file
export const closeDialog = path => {
	console.log("actions.closeDialog() path", path)
	return {
		type:C.CLOSE_DIALOG,
		path:path
	}
}
