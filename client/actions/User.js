import C from '../constants/ConstantsForStore'
import { filterUniqueByProperty } from '../util/helpers/ArrayManipulators'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'
import auth from './../auth/auth-helper'
import { signout } from './Auth.js'

//todo - error handler must be passed through to remove user from session storge if not auth on server

//todo - remove authorisation requirement so that this can be fetched and displayed to other users
//or maintain it but at server only return non-sensitive info rather than full user
export const fetchUser = id => dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.user',
		{
			url: '/api/user/'+id, 
			requireAuth:true,
			processor: data => {
				const jwt = auth.isAuthenticated()
				//may be reloading signed in user
				const userIsSignedIn = jwt ? jwt.user._id === data._id : false
				return {
					type:C.SAVE_USER, user:data, userIsSignedIn:userIsSignedIn
				}
			}
		}) 
}
export const fetchUsers = id => dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.user',
		{
			url: '/api/users/', 
			requireAuth:true,
			processor: data => {
				return { type:C.SAVE_USERS, users:data }
			}
		}) 
}

//returns the action to be dispatched that will save the user
//export const saveUser = user =>{
	//gather all groups and players  into one array for client-side manipulation
	//Any temporary saving and retrieving of data on client side happens in here
	//with specific group and player arrays within user used as references only
	/*
	TODO - make this a helper method accessible from anywhere in app
	const groups = [
		...user.adminGroups, 
		...user.groupsFollowing,
		...user.groupsViewed, 
		...user.playerInfo.groups,
		...user.coachInfo.groups]
	const filteredGroups = filterUniqueByProperty('_id', groups)
	const players = [
	//todo - add playersFollowing and playersViewed properties to user.model
		...user.adminGroups.map(g => g.players),   
		...user.playerInfo.groups.map(g => g.players), 
		...user.coachInfo.groups.map(g => g.players)]
	const filteredPlayers = filterUniqueByProperty('_id', players)
	const userWithGroupsAndPlayers = {...user, groups:filteredGroups, players:filteredPlayers}*/
	//return {
		//type:C.SAVE_USER, user:userWithGroupsAndPlayers
	//}

//}

export const updateUser = (id, formData, history) => dispatch => {
	fetchThenDispatch(dispatch, 
		'updating.user',
		{
			url: '/api/user/'+id,
			method: 'PUT',
			headers:{
	        	'Accept': 'application/json'
	      	},
			body:formData, //not stringify as its a formidable object
			requireAuth:true,
			processor: data => {
				history.push("/")
				const jwt = auth.isAuthenticated()
				const userIsSignedIn = jwt ? jwt.user._id === data._id : false
				return {
					type:C.SAVE_USER, user:data, userIsSignedIn:userIsSignedIn
				}
			}
		})
}
export const deleteUser = (id, history) => dispatch => {
	console.log("actions.deleteUser()")
	//deleting ok, but DELETE_USER action not impl in reducer
	//and also need to clear session storage 
	fetchThenDispatch(dispatch, 
		'deleting.user',
		{
			url: '/api/user/'+id,
			method: 'DELETE',
			requireAuth:true,
			processor: data => signout(history)
		})
}