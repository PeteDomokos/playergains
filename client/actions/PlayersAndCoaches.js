import C from '../constants/ConstantsForStore'
import { filterUniqueByProperty } from '../util/helpers/ArrayManipulators'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'

//to fetch player
export const fetchPlayer = userId => dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.player',
		{
			url: '/api/player/'+userId, 
			requireAuth:true,
			processor: data => {return {type:C.SAVE_PLAYER, value:data}}
		})
}


//to fetch coach
export const fetchCoach = userId => dispatch => {
	fetchThenDispatch(dispatch,
		'loading.coach',  
		{
			url: '/api/coach/'+userId, 
			requireAuth:true,
			processor: data => {return {type:C.SAVE_COACH, value:data}}
		})
}

//available players if no parent
//todo later - server will filter for things like age group, gender etc

//saved in the main users store
//todo - filter out any users already stored there
export const fetchEligiblePlayers = group => dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.players', 
		{
			url: '/api/players/eligible/bygroup/' +group._id, 
			requireAuth:true,
			processor: data => {
				return {
					type:C.SAVE_ELIGIBLE_PLAYERS, players:data, groupId:group._id
				}
			}
		})
}

//fetches all public player summaries
//todo - implement privacy options for players' data

//todo - buffer these so only fetch a batch at at time. Also then will need a way of ensuring
//loader still requests more as store will have groups stored

//saved in the main users store
//todo - filter out any users already stored there
export const fetchPlayers = dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.players',
		{
			url: '/api/players', 
			requireAuth:false,
			processor: data => {return {type:C.SAVE_USERS, users:data}}
		})
}
