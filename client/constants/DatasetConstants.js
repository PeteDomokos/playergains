export const DatapointOptions = {
	surfaces:['grass', 'synthetic', 'concrete', 'other']
}


/**
* this stores the values that will be copied into datasets as they are created
**/
export const standardDatasets = [
	{
		category:'Fitness', name:'5-metre Acceleration', nrParticipants:1, 
		key:'fiveMetreAcceleration',
		desc:'Straight sprint from standing start. Acc = Final Speed/Time. This score is used as a marker for comparing sprints in matches',
	    valueFormats:[
	    	{key:'accel', name:'Accel', unit:'M/S2', isMain:true}
	    	], 
	},
	{
		category:'Fitness', name:'1-min Jumps', nrParticipants:1, 
		key:'oneMinJumps',
		desc:'Tests local muscular endurance. Repeated hurdle jumps with 10kg weight vest',
	    valueFormats:[
	    	{key:'total', name:'Total', unit:'reps', isMain:true}
	    	], 
	},
	{
		category:'Fitness', name:'Speed Endurance', nrParticipants:1, 
		key:'speedEndurance',
		desc:'Best score is 0. Tests players % decrease in speed over 7 repeated 35m sprints with 30sec rest.',
	    valueFormats:[
	    	{key:'decrease', name:'Decrease (%)', unit:'%', isMain:true, numberOrder:'high-to-low'}
	    	], 
	},
	{
		category:'Match', name:'Nr of Sprints', nrParticipants:1, 
		key:'nrOfSprints',
		desc:'A sprint is defined as a 5-metre movement where their acceleration is +90% of their off-pitch 5-metre acceleration test score. Can also be broken into phases of match. Note: A lower bar for defining sprints - eg +80% of max - would increase the number of sprints recorded', 
	    valueFormats:[
	    	{key:'total', name:'Total', unit:'sprints', isMain:true}
	    	], 
	},
	{
		category:'Match', name:'Best Acceleration', nrParticipants:1, 
		key:'bestAcceleration',
		desc:'This is the mean average of the players top 5 5-metre sprints in the match',
	    valueFormats:[
	    	{key:'accel', name:'Accel', unit:'M/S2', isMain:true}
	    	], 
	},
	{
		category:'Skills', name:'Dribble', nrParticipants:1, 
		key:'dribble',
	    valueFormats:[
	    	{key:'time', name:'Time', unit:'Seconds', isMain:true, numberOrder:'high-to-low'}
	    	], 
	},
	{
		category:'Technique', name:'Long Passes', nrParticipants:1, 
		key:'longPasses',
	    valueFormats:[
	    	{key:'left', name:'Left Foot'}, 
	    	{key:'right', name:'Right Foot'}, 
	    	{key:'total', name:'Total', isMain:true, formula:'left+right'}]
	},
	{
		category:'Skills', name:'Passing Ladder', nrParticipants:1, 
		key:'passingLadder',
	    valueFormats:[
		   	{key:'time', name:'Time', unit:'Seconds', numberOrder:'high-to-low'},
		   	{key:'score', name:'Score', isMain:true},
		   	{key:'100Score', name:'100Score', formula:'(points/time)*600'}]
	},
	{
		category:'Fitness', name:'Shuttles', nrParticipants:1, 
		key:'shuttles',
	    valueFormats:[
	    	{key:'time', name:'Time', unit:'Seconds', isMain:true, numberOrder:'high-to-low'}
	    	]
	},
	{
	  	category:'Skills', name:'Switchplay', nrParticipants:1, 
	  	key:'switchplay',
	   	valueFormats:[
	   		{key:'score', name:'Score', isMain:true}]
	},
	{
		category:'Fitness', name:'T-Test', nrParticipants:1, 
		key:'tTest', 
	    valueFormats:[
	    	{key:'time', name:'Time', unit:'Seconds', isMain:true, numberOrder:'high-to-low'}]
	},
	{
		category:'Skills', name:'Touch', nrParticipants:1, 
		key:'touch',
	    valueFormats:[
	    	{key:'score', name:'Score', isMain:true}]
	},
	{
	  	category:'Skills', name:'Touch', nrParticipants:2, 
	  	key:'touch2P',
	    valueFormats:[
	    	{key:'score', name:'Score', isMain:true}, 
	    	{key:'100score', name:'100Score', formula:'Points'}]
	},
	{
		category:'Skills', name:'Touch', nrParticipants:3, 
		key:'touch3P',
	    valueFormats:[
	    	{key:'score', name:'Score', isMain:true}]
	},
	//todo - calculate distance as a formula from level (or a function)
	{
		category:'Fitness', name:'Yoyo L2', nrParticipants:1, 
		key:'yoyoL2', 
	    valueFormats:[
	    	{key:'distance', name:'Distance', unit:'Metres', isMain:true}, {name:'Level'}]
	}
]
/*
* Zones are not copied into datasets, as they may change over time, so stored here.
* Scores
*/
export const datasetZones = {
	fiveMetreAcceleration:{
		accel:[  
			{id:'social', name:'Social', nr:1, from:2, to:3}, 
			{id:'amateur', name:'Amateur', nr:2, from:3, to:4}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:4, to:5}, 
			{id:'pro', name:'Pro', nr:4, from:5, to:5.5}, 
			{id:'elite', name:'Elite', nr:5, from:5.5, to:6}, 
			{id:'worldClass', name:'World Class', nr:6, from:6, to:8}
		]
	},
	oneMinJumps:{
		total:[  
			{id:'social', name:'Social', nr:1, from:0, to:20}, 
			{id:'amateur', name:'Amateur', nr:2, from:20, to:35}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:35, to:45}, 
			{id:'pro', name:'Pro', nr:4, from:45, to:60}, 
			{id:'elite', name:'Elite', nr:5, from:60, to:75}, 
			{id:'worldClass', name:'World Class', nr:6, from:75, to:90}
		]
	},
	speedEndurance:{
		decrease:[  
			{id:'social', name:'Social', nr:1, from:40, to:30}, 
			{id:'amateur', name:'Amateur', nr:2, from:30, to:20}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:20, to:10}, 
			{id:'pro', name:'Pro', nr:4, from:10, to:5}, 
			{id:'elite', name:'Elite', nr:5, from:5, to:2.5}, 
			{id:'worldClass', name:'World Class', nr:6, from:2.5, to:0}
		]
	},
	nrOfSprints:{
		total:[  
			{id:'social', name:'Social', nr:1, from:0, to:20}, 
			{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:55}, 
			{id:'pro', name:'Pro', nr:4, from:55, to:65}, 
			{id:'elite', name:'Elite', nr:5, from:65, to:75}, 
			{id:'worldClass', name:'World Class', nr:6, from:75, to:80}
		]
	},
	bestAcceleration:{
		accel:[  
			{id:'social', name:'Social', nr:1, from:2, to:3}, 
			{id:'amateur', name:'Amateur', nr:2, from:3, to:4}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:4, to:5}, 
			{id:'pro', name:'Pro', nr:4, from:5, to:5.5}, 
			{id:'elite', name:'Elite', nr:5, from:5.5, to:6}, 
			{id:'worldClass', name:'World Class', nr:6, from:6, to:8}
		]
	},
	dribble:{
		time:[  
			{id:'social', name:'Social', nr:1, from:30, to:10.5}, 
			{id:'amateur', name:'Amateur', nr:2, from:10.5, to:9}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:9, to:7.5}, 
			{id:'pro', name:'Pro', nr:4, from:7.5, to:6}, 
			{id:'elite', name:'Elite', nr:5, from:6, to:4.5}, 
			{id:'worldClass', name:'World Class', nr:6, from:4.5, to:3}
		]
	},
	longPasses:{
		score:[  
			{id:'social', name:'Social', nr:1, from:30, to:10.5}, 
			{id:'amateur', name:'Amateur', nr:2, from:10.5, to:9}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:9, to:7.5}, 
			{id:'pro', name:'Pro', nr:4, from:7.5, to:6}, 
			{id:'elite', name:'Elite', nr:5, from:6, to:4.5}, 
			{id:'worldClass', name:'World Class', nr:6, from:4.5, to:3}
		]
	},
	passingLadder:{
		score:[  
			{id:'social', name:'Social', nr:1, from:0, to:20}, 
			{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:60}, 
			{id:'pro', name:'Pro', nr:4, from:60, to:75}, 
			{id:'elite', name:'Elite', nr:5, from:75, to:90}, 
			{id:'worldClass', name:'World Class', nr:6, from:90, to:100}
		]
	},
	shuttles:{
		time:[  
			{id:'social', name:'Social', nr:1, from:15, to:13}, 
			{id:'amateur', name:'Amateur', nr:2, from:13, to:11}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:11, to:9.5}, 
			{id:'pro', name:'Pro', nr:4, from:9.5, to:8.5}, 
			{id:'elite', name:'Elite', nr:5, from:8.5, to:7.5}, 
			{id:'worldClass', name:'World Class', nr:6, from:7.5, to:7}
		]
	},
	switchplay:{
		score:[
			{id:'social', name:'Social', nr:1, from:0, to:20}, 
			{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:60}, 
			{id:'pro', name:'Pro', nr:4, from:60, to:75}, 
			{id:'elite', name:'Elite', nr:5, from:75, to:90}, 
			{id:'worldClass', name:'World Class', nr:6, from:90, to:100}
		]
	},
	touch:{
		score:[ 
			{id:'social', name:'Social', nr:1, from:0, to:20}, 
			{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:60}, 
			{id:'pro', name:'Pro', nr:4, from:60, to:75}, 
			{id:'elite', name:'Elite', nr:5, from:75, to:90}, 
			{id:'worldClass', name:'World Class', nr:6, from:90, to:100}
		]
	},
	tTest:{
		time:[ 
			{id:'social', name:'Social', nr:1, from:25, to:14}, 
			{id:'amateur', name:'Amateur', nr:2, from:14, to:12.5}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:12.5, to:11}, 
			{id:'pro', name:'Pro', nr:4, from:11, to:10}, 
			{id:'elite', name:'Elite', nr:5, from:10, to:9}, 
			{id:'worldClass', name:'World Class', nr:6, from:9, to:5}
		]
	},	
	yoyoL2:{
		distance:[
			{id:'social', name:'Social', nr:1, from:1000, to:1600}, 
			{id:'amateur', name:'Amateur', nr:2, from:1600, to:1800}, 
			{id:'semiPro', name:'Semi-Pro', nr:3, from:1800, to:2100}, 
			{id:'pro', name:'Pro', nr:4, from:2100, to:2300}, 
			{id:'elite', name:'Elite', nr:5, from:2300, to:2500}, 
			{id:'worldClass', name:'World Class', nr:6, from:2500, to:3000}
		]
	}


}


export const TestDataConstants ={
	targets:[{weekNr:12, tTest:20, yoyo:20, shuttles:20, touch:20, switchplay:20, dribble:20}],
	coachTargets:{
			skills:{touch:75, switchplay:30, dribble:6},
	    	fitness:{tTest:12, yoyo:2000, shuttles:8},
	    	games:{wins:6, points:15, keepUps:10}
    },
    zones:{ tTest:[ {id:'social', name:'Social', nr:1, from:25, to:14}, 
					{id:'amateur', name:'Amateur', nr:2, from:14, to:12.5}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:12.5, to:11}, 
					{id:'pro', name:'Pro', nr:4, from:11, to:10}, 
					{id:'elite', name:'Elite', nr:5, from:10, to:9}, 
					{id:'worldClass', name:'World Class', nr:6, from:9, to:5}],
					
			yoyo:[  {id:'social', name:'Social', nr:1, from:1000, to:1600}, 
					{id:'amateur', name:'Amateur', nr:2, from:1600, to:1800}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:1800, to:2100}, 
					{id:'pro', name:'Pro', nr:4, from:2100, to:2300}, 
					{id:'elite', name:'Elite', nr:5, from:2300, to:2500}, 
					{id:'worldClass', name:'World Class', nr:6, from:2500, to:3000}],
			shuttles:[  
					{id:'social', name:'Social', nr:1, from:20, to:13}, 
					{id:'amateur', name:'Amateur', nr:2, from:13, to:11}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:11, to:9.5}, 
					{id:'pro', name:'Pro', nr:4, from:9.5, to:8.5}, 
					{id:'elite', name:'Elite', nr:5, from:8.5, to:7.5}, 
					{id:'worldClass', name:'World Class', nr:6, from:7.5, to:0}],

			touch:[ {id:'social', name:'Social', nr:1, from:0, to:20}, 
					{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:60}, 
					{id:'pro', name:'Pro', nr:4, from:60, to:75}, 
					{id:'elite', name:'Elite', nr:5, from:75, to:90}, 
					{id:'worldClass', name:'World Class', nr:6, from:90, to:100}],
					
			switchplay:[  {id:'social', name:'Social', nr:1, from:0, to:20}, 
					{id:'amateur', name:'Amateur', nr:2, from:20, to:40}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:40, to:60}, 
					{id:'pro', name:'Pro', nr:4, from:60, to:75}, 
					{id:'elite', name:'Elite', nr:5, from:75, to:90}, 
					{id:'worldClass', name:'World Class', nr:6, from:90, to:100}],
			dribble:[  
					{id:'social', name:'Social', nr:1, from:30, to:10.5}, 
					{id:'amateur', name:'Amateur', nr:2, from:10.5, to:9}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, from:9, to:7.5}, 
					{id:'pro', name:'Pro', nr:4, from:7.5, to:6}, 
					{id:'elite', name:'Elite', nr:5, from:6, to:4.5}, 
					{id:'worldClass', name:'World Class', nr:6, from:4.5, to:3}]
	},	
	keyLabels:{weekNr:'Weeks', tTest:['T-Test', '(agility)'], yoyo:['YoYo', '(CV)'], 
			   shuttles:['Shuttles', '(agility)'], touch:'Touch', switchplay:'Switchplay', 
			   dribble:'Dribble', wins:'Wins', points:'Points', keepUps:'Keep Ups'},
	measurements:{weekNr:"Time", tTest:"Time", yoyo:"Distance", shuttles:"Time", touch:"Points", switchplay:"Points", 
		dribble:"Time", wins:"Wins", points:"Points", keepUps:"Total"
	},
	titles:{weekNr:{full:'Week Number', short:'Week', vShort:'Week'},
	     tTest:{full:'Agility T-Test', short:'Agility T-Test', vShort:'Ttest'}, 
	     yoyo:{full:'Yoyo Endurance Test L2', short:'Yoyo Endurance', vShort:'Yoyo'},
		 shuttles:{full:'Shuttle Runs', short:'Shuttle Runs', vShort:'Shut'},
		 touch:{full:'Touch Test', short:'Touch Test', vShort:'Touch'},
		 switchplay:{full:'Switchplay Passing Test', short:'Switchplay Passing', vShort:'Switch'},
		 dribble:{full:'Timed Dribble Test', short:'Dribble Test', vShort:'Drib'},
		 wins:{full:'Number of Wins', short:'Wins', vShort:'Wins'},
		 points:{full:'Number of Points', short:'Points', vShort:'Pts'},
		 keepUps:{full:'Number of Kick-Ups', short:'Kick-Ups', vShort:'Kicks'}
	},
	//unit may be blank
	units:{
		weekNr:{full:"weeks", short:"wks", symbol:""},
		tTest:{full:"seconds", short:"secs", symbol:"s"},
		yoyo:{full:"metres", short:"mtrs", symbol:"m"},
		shuttles:{full:"seconds", short:"secs", symbol:"s"}, 
		touch:{full:"points", short:"pts", symbol:""}, 
		switchplay:{full:"points", short:"pts", symbol:""}, 
		dribble:{full:"seconds", short:"secs", symbol:"s"}, 
		wins:{full:"wins", short:"wins", symbol:""}, 
		points:{full:"points", short:"pts", symbol:""},
		keepUps:{full:"kick-ups", short:"kicks", symbol:""},
	},
	keyNumberOrders:{weekNr:'low-to-high', tTest:'high-to-low', yoyo:'low-to-high',
					shuttles:'high-to-low', touch:'low-to-high', switchplay:'low-to-high',
					dribble:'high-to-low', wins:'low-to-high', points:'low-to-high', keepUps:'low-to-high'}, 
    dispType:'scatter',
    initSelectedOption:'fitness', 
    initXKey:'weekNr', 
    initYKey:'tTest',
    buttons:{
    	//todo - give each option a label
    	skills:[{key:'touch', butLabel:'Touch', type:'skill'}, {key:'switchplay', butLabel:'Switchplay', type:'skill'}, 
    		 {key:'dribble', butLabel:'Dribble', type:'skill'}],
    	fitness:[{key:'tTest', butLabel:'T-Test', type:'fitness'}, {key:'yoyo', butLabel:'YoYo', type:'fitness'}, 
    		 {key:'shuttles', butLabel:'Shuttles', type:'fitness'}],
    	//games:[{key:'wins', butLabel:'Wins'}, {key:'points', butLabel:'Points'}, 
    		 //{key:'keepUps', butLabel:'Keep Ups'}],
    	//players default to []
    	players:[]
    },
    titleLines:'My Progress', 
    xMaxes:{'weekNr':12}
}