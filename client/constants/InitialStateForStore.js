export const InitialState = {
	user:'',
	//players, coaches etc start as '' so loaders can tell the difference between it being 
	//saved but empty (ie server call not required, and not being saved (ie server call required)
	storedItems:{
		users:[],
		groups:[]
	},
	//todo -remove available
	available:{
		players:'',
		coaches:'',
		groups:'',
	},
	dashboard:{
		groups:'',
		players:'',
		datasets:'',
		selected:false,
		datasetsSelected:false
	},
	//get rid of otherItems - groups should be stored under available, or in user.groups
	asyncProcesses:{
		signingIn:false,
		signingOut:false,
		loading:{
			groups:false,
			group:false,
			users:false,
			user:false,
			players:false,
			player:false,
			coaches:false,
			coach:false,
			datapoints:false
		},
		//updating can be false or {pending:true} or {complete:'completion mesg'} or {error:'error here'}
		//so user can check using eg updating.group.players.e
		updating:{
			group:{
				players:false,
				datasets:false
			},
			dataset:{
				datapoints:false
			}
		},
		deleting:{
			user:false,
			group:false,
			dataset:false,
			datapoint:false
		},
		creating:{
			user:false,
			group:false,
			dataset:false,
			datapoint:false
		}
	},
	dialogs:{
		createUser:false,
		deleteUser:false,
		createGroup:false,
		deleteGroup:false
	}
}