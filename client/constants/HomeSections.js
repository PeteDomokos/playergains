
const homeSections = [
  {
    title:'Team Set-Up', 
    id:'user-home', 
    mesg:"Use football to make learning more fun and meaningful.",
    content:{
      heading:'Team Set-Up', 
      subheading:['Create a team. Add players.', 'Choose the data to record.'],
      desc:'An exciting learning tool which combines maths, English and football. Use stats to analyse your performance, and write a report.', 
      buttonText:'Video',
      videoURL:'https://youtu.be/aNC9XoJJMro'

    }
  },
  {
    title:'Dashboard', 
    id:'demoDashboard', 
    mesg:"Compare your skills and fitness to the pros.",
    content:{
      heading:'Dashboard', 
      subheading:['Compare players\' data.', 'Track progress. Set targets.'],
      desc:'A range of fun tests. Compare your scores to players of different ages, positions and levels. Set yourself SMART targets and track your progress',
      buttonText:'Video',
      videoURL:'https://www.youtube.com/watch?v=0JyzmFALuW8'
    }
  }
]

export default homeSections
