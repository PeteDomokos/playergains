import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import {withStyles} from 'material-ui/styles'
import Paper from 'material-ui/Paper'
//child components
//helpers
import auth from './../auth/auth-helper'

const styles = theme => ({
  root: theme.mixins.gutters({
    width: '75vw',
    maxWidth:400,
    margin: 'auto',
    padding: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit * 0,
  }),
  title: {
    margin: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 2}px`,
    color: theme.palette.protectedTitle
  }
})

const Player = ({format, title, players, selectedPlayers, handleClick, emptyMesg}) =>{
  const titleStyle = {marginTop:'2vh', marginBottom:'2vh'}
  const listStyle = {margin:'5vh', marginTop:'2vh', marginBottom:'2vh', padding:'2vw',
    border:'solid',borderColor:'black',borderWidth:'thin'}
  return(
    <div>
      <h4 style={{marginTop:'2vh', marginBottom:'2vh'}}>Player</h4>
    </div>
)}
Player.defaultProps = {
}

export default withStyles(styles)(Player)

