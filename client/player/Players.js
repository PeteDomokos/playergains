/*import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import {withStyles} from 'material-ui/styles'
import Paper from 'material-ui/Paper'
//child components
import UserProfile from './../user/UserProfile'
import PlayerCard from './PlayerCard'
//helpers
import auth from './../auth/auth-helper'
import { readGroup, addPlayer, removePlayer, updateGroupFields } from './api-group.js'
import { listPlayers } from './../user/api-user.js'

//note - clickHandler and component name kept general so component can be reused
const Players = ({format, title, players, selectedPlayers, handleClick, emptyMesg}) =>{
  const titleStyle = {marginTop:'2vh', marginBottom:'2vh'}
  const listStyle = {margin:'5vh', marginTop:'2vh', marginBottom:'2vh', padding:'2vw',
    border:'solid',borderColor:'black',borderWidth:'thin'}
  return(
    <div>
      <h4 style={{marginTop:'2vh', marginBottom:'2vh'}}>{title}</h4>
      {format == 'list' ?
        <PlayerList players={players} selectedPlayers={selectedPlayers}
          handleClick={handleClick} emptyMesg={emptyMesg}/>
      :
        <PlayerGrid players={players} selectedPlayers={selectedPlayers}
          handleClick={handleClick} emptyMesg={emptyMesg} singleRow />
      }
    </div>
)}
Players.defaultProps = {
  players:[],
  selectedPlayers:[]
}
const PlayerList = ({players, selectedPlayers, handleClick, emptyMesg}) =>{
    const listStyle = {margin:'5vh', marginTop:'2vh', marginBottom:'2vh', padding:'2vw',
      border:'solid',borderColor:'black',borderWidth:'thin'}
    return(
      <div>
        {players.length > 0 ?
          <div style={listStyle}>
            {players.map(player =>
              <UserProfile player user={player} shortVersion 
                selected={selectedPlayers.map(p => p._id).includes(player._id)}
              onClickFunc={() => handleClick(player)} key={'players-'+player._id}/>)}
          </div>
          :
          <div style={listStyle}>{emptyMesg}</div>
        }
      </div>
    )
}
  
const PlayerGrid = ({players, selectedPlayers, handleClick, emptyMesg, singleRow}) =>{
  const gridHeight = singleRow ? 90 : 'auto'
  const gridStyle = {height:gridHeight, display:'flex', margin:'5vh', marginTop:'2vh', marginBottom:'2vh', padding:'2vw',
    border:'solid',borderColor:'black',borderWidth:'thin'}
  return(
    <div>
      {players.length > 0 ?
        <div style={gridStyle}>
          {players.map(p =>
            <PlayerCard user={p} clickHandler={() => handleClick(p)} key={'players-'+p._id}/>)}
        </div>
        :
        <div style={gridStyle}>{emptyMesg}</div>
      }
    </div>
)}

export default Players

*/