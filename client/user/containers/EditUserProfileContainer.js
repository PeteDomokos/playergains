import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { updateUser } from '../../actions/User'

import EditUserProfile from '../EditUserProfile'

const mapStateToProps = (state, ownProps) => {
	return({
		user:state.user,
		updating:state.asyncProcesses.updating.user,
		error:''
	})
}
const mapDispatchToProps = dispatch => ({
	onUpdate(id, formData, history){
		dispatch(updateUser(id, formData, history))
	}
})

//wrap all 4 sections in the same container for now.
const EditUserProfileContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(EditUserProfile)

export default EditUserProfileContainer

