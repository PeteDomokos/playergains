import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchUser } from '../../actions/User'

import User from '../User'

//ownProps must have match.params.userId or a userId passed as props
const mapStateToProps = (state, ownProps) => {
	const userId = ownProps.userId  || ownProps.match.params.userId
	const users = [...state.users, state.user]
	const user = users.find(user => user._id === userId)
	return{
		user:user,
		loading:asyncProcesses.loading.user
	}
}
const mapDispatchToProps = dispatch => ({
	onLoad(userId){
		dispatch(fetchUser(userId))
	}
})

//wrap all 4 sections in the same container for now.
const UserContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(User)

export default UserContainer

