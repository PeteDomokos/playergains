import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { openDialog, closeDialog } from '../../actions/Common'
import { deleteUser } from '../../actions/User'

import DeleteUser from '../DeleteUser'


const mapStateToProps = (state, ownProps) => {
	return{
		//id can be passed in or otherwise its teh logged in user from store
		id:ownProps.id || state.user._id,
		deleting:state.asyncProcesses.deleting.user,
		open:state.dialogs.deleteUser,
		error:''
	}
}
const mapDispatchToProps = dispatch => ({
	openDialog(){
		console.log("openDeleteUserDialog called...")
		dispatch(openDialog('deleteUser'))
	},
	deleteUser(userId, history){
		dispatch(deleteUser(userId, history))
	},
	closeDialog(){
		dispatch(closeDialog('deleteUser'))
	}
})

//wrap all 4 sections in the same container for now.
const DeleteUserContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DeleteUser)

export default DeleteUserContainer

