import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton'
//icons
import ArrowForward from '@material-ui/icons/ArrowForward'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'
//helpers
import auth from './../auth/auth-helper'

//todo - styling - a diff color for the currently selected group
const styles = theme => ({
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  title: {
    margin: theme.spacing(2),
    color: theme.palette.protectedTitle
  },
  error: {
    verticalAlign: 'middle'
  },
  textField: {
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    width: 300
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  bigAvatar: {
    width: 60,
    height: 60,
    margin: 'auto'
  },
  input: {
    display: 'none'
  },
  filename:{
    marginLeft:'10px'
  }
})

class UpdateMainGroup extends Component {
  constructor() {
    super()
    this.state = {
      selectingGroup:false,
    }
  }
  onSelectGroup = () =>{
    this.setState({selectingGroup:true})
  }
  setGroup = (g) =>{
    this.props.onSelect(g)
    this.setState({selectingGroup:false})
  }
  render() {
    const {group, availableGroups, classes} = this.props
    const photoUrl = group._id
                 ? `/api/users/photo/${group._id}?${new Date().getTime()}`
                 : '/api/users/defaultphoto'

    return (
      <div>
        <Typography type="headline" component="h2" >
            Main Group: {group.name}
          </Typography>
          <div>
            {this.state.selectingGroup ?
              <div className='groups'>
                <div className='top-line' style={{display:'flex', justifyContent:'center'}}>
                  <h6 className='user-list-item'>Click to select</h6>
                  <div onClick={() => {this.setState({selectingMainGroup:false})}}>X</div>
                </div>
                <List dense> 
                  {availableGroups.map(g =>
                    <ListItem button key={'available'+g._id} onClick={() => this.setGroup(g)}>
                      <ListItemAvatar>
                        <Avatar>
                          <Person/>
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText primary={g.name}/>
                      <ListItemSecondaryAction>
                      <IconButton>
                          <ArrowForward/>
                      </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  )}
                </List>
              </div>
              :
              <Button className={classes.selectBtn} onClick={this.onSelectGroup} 
                  color="primary" autoFocus="autoFocus" variant="contained">
                 Select Main Group
              </Button>
             }
          </div> 
      </div>
    )
  }
}

UpdateMainGroup.propTypes = {
  classes: PropTypes.object.isRequired
}
UpdateMainGroup.defaultProps = {
  group:{name:''}
}

export default withStyles(styles)(UpdateMainGroup)
