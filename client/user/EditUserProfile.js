import React, {Component} from 'react'
import {Redirect, Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import { Card, CardActions, CardContent } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import Avatar from '@material-ui/core/Avatar'
//icons
import Publish from '@material-ui/icons/Publish'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import UpdateMainGroup from '../user/UpdateMainGroup'
//helpers
import auth from './../auth/auth-helper'
import { filterUniqueByProperty } from '../util/helpers/ArrayManipulators'

const styles = theme => ({
  card: {
    maxWidth: '600px',
    width:'80vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  title: {
    margin: theme.spacing(2),
    color: theme.palette.protectedTitle
  },
  error: {
    verticalAlign: 'middle'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  bigAvatar: {
    width: 60,
    height: 60,
    margin: 'auto'
  },
  input: {
    display: 'none'
  },
  filename:{
    marginLeft:'10px'
  }
})

class EditUserProfile extends Component {
  constructor({match}) {
    super()
    this.state = {
       user: {username:'', firstName:'', surname:'', email:'', _id:'', mainGroup:'', photo:'',
          adminGroups:[], groupsFollowing:[], playerInfo:{groups:[]}, coachInfo:{groups:[]}},
      error: ''
    }
    this.match = match
  }
  componentDidMount = () => {
    window.scrollTo(0, 0)
    this.userData = new FormData()
    this.setState({user:this.props.user})
  }
  handleChange = key => event => {
    const value = key === 'photo'
      ? event.target.files[0]
      : event.target.value
    this.userData.set(key, value)
    let updatedUser = {...this.state.user, [key]: value }
    this.setState({ user:updatedUser })
  }
  updateMainGroup = (group) =>{
    console.log("setting main group")
    //todo: use state => function(state) format
    this.userData.set('mainGroup', group._id)
    let updatedUser = {...this.state.user, mainGroup: group._id }
    this.setState({ user:updatedUser })
  }
  render() {
    const { updating, error, onUpdate, history, classes} = this.props
    //user is saved into state after mounting
    const { user } = this.state
    const allUsersGroups = [
      ...user.adminGroups, 
      ...user.playerInfo.groups, 
      ...user.coachInfo.groups,
      ...user.groupsFollowing
    ]
    const availableGroups = filterUniqueByProperty('_id', allUsersGroups)
    //not mainGroup in user is just the id
    const mainGroup = availableGroups.find(g => g._id === this.state.user.mainGroup)
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2" className={classes.title}>
            Edit Profile
          </Typography>
          <UpdatePhoto 
            photo={this.state.photo} 
            id={this.state.user._id} 
            handleChange={this.handleChange}
            classes={classes}/>
          <UpdateTextFields 
            user={this.state.user} 
            handleChange={this.handleChange} 
            classes={classes}/>
           
          <UpdateMainGroup
            group={mainGroup} 
            availableGroups={availableGroups}
            onSelect={this.updateMainGroup} /><br/>
          {
            this.state.error && (<Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {this.state.error}
            </Typography>)
          }
        </CardContent>
        <CardActions>
          <Button color="primary" variant="contained" 
            onClick={() =>onUpdate(user._id, this.userData, history)} 
            className={classes.submit}>Submit</Button>
        </CardActions>
      </Card>
    )
  }
}
EditUserProfile.propTypes = {
  classes: PropTypes.object.isRequired
}
EditUserProfile.defaultProps = {
}

//todo - sort out photo name - what is it?
const UpdatePhoto = ({photo, id, classes, handleChange}) =>{
  const url = id ? `/api/user/photo/${id}?${new Date().getTime()}`
    : '/api/users/defaultphoto'

  return(
    <React.Fragment>
      <Avatar src={url} className={classes.bigAvatar}/><br/>
      <input accept="image/*" onChange={handleChange('photo')} className={classes.input} id="icon-button-file" type="file" />
      <label htmlFor="icon-button-file">
        <Button variant="contained" color="default" component="span">
          Upload
          <Publish/>
        </Button>
      </label> <span className={classes.filename}>{photo ? photo.name : ''}</span><br/>
    </React.Fragment>
  )
}

const UpdateTextFields = ({user, handleChange, classes}) =>
  <React.Fragment>
    <TextField id="username" label="username" className={classes.textField} 
      value={user.username} onChange={handleChange('username')} margin="normal"/><br/>
    <TextField id="firstName" label="firstName" className={classes.textField} 
      value={user.firstName} onChange={handleChange('firstName')} margin="normal"/><br/>
    <TextField id="surname" label="surname" className={classes.textField} 
      value={user.surname} onChange={handleChange('surname')} margin="normal"/><br/>

    <TextField id="email" type="email" label="Email" className={classes.textField} 
      value={user.email} onChange={handleChange('email')} margin="normal"/><br/>
    <TextField id="password" type="password" label="Password" className={classes.textField} 
      value={user.password} onChange={handleChange('password')} margin="normal"/>
  </React.Fragment>


export default withStyles(styles)(withRouter(EditUserProfile))
