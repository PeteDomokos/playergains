import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'

//child components
import UserProfile from './UserProfile'
//helpers
import auth from './../auth/auth-helper'

//note - User component required the full user, not just the summary details 
//which are sufficient for UserProfile.
//However, if this user is not signed in, then some sensitive info will not be sent by server
//so User component must handle non-existence of stuff. This will also be affected by the Users
//settings - is theri profile public, for example. But even if its private, 
//we still display a page, but just hide the info and say theis profile is private.
//or we just dont display it on the list that is clicked, or ust dont allow teh link to go through
//and put a pricate label on them
class User extends Component {
  componentDidMount = () => {
    window.scrollTo(0, 0)
  }
  render() {
    const {user, loading, classes} = this.props
    const userIsAuthorisedToUpdate = auth.isAuthenticated() &&
      (auth.isAuthenticated().user._id == user._id || 
        auth.isAuthenticated().user.isSystemAdmin)
      //todo - add stuff like users stats, posts etc 
    return (
        <UserProfile user={user}/>
    )
  }
}
User.propTypes = {
  classes: PropTypes.object.isRequired
}
User.defaultProps = {
}

export default User
