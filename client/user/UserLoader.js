import React, { useEffect }  from 'react'
import User from './User'
/**
*
**/
//note - User comoponent required the full user, not just teh summary details 
//which are sufficient for UserProfile
const UserLoader = ({user, loading, onLoad}) => {

	useEffect(() => {
		//if no adminGroups, then user has not been fully loaded
		if(!user.adminGroups && !loading){
			console.log("UserLoader loading user...")
			onLoad(user._id)
		}
	}, [])
	return (
		<User user={user} />
		)
}

export default UserLoader