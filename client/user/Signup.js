import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Card, CardActions, CardContent} from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography' 
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Icon from '@material-ui/core/Icon'
//icons
import Publish from '@material-ui/icons/Publish'
import ArrowForward from '@material-ui/icons/ArrowForward'
import Person from '@material-ui/icons/Person'
//styles
import { withStyles } from '@material-ui/core/styles'
//child components
import { DialogWrapper } from '../util/components/Dialog'

//helper api (note - api accessed directly for signup rather than through redux)
const create = (user) => {
  console.log("client api-user create()")
  return fetch('/api/users/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: user
    })
    .then((response) => {
      return response.json()
    }).catch((err) => console.log(err))
}

const styles = theme => ({
  card: {
    maxWidth: '600px',
    width:'80vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '60vw'
  },
  submit: {
    margin: 'auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  additionalFields:{
  }
})

class Signup extends Component {
  state = {
      username:'',
      firstName: '',
      surname: '',
      photo:'',
      email: '',
      password: '',
      registerAsPlayer:false,
      registerAsCoach:false,
      open: false,
      error: '',
  }
  componentDidMount(){
    window.scrollTo(0, 0)
    this.userData = new FormData()
  }

  handleChange = name => event => {
    const value = name === 'photo'
      ? event.target.files[0] : event.target.value
      
    this.userData.set(name, value)
    this.setState({[name]: value})
  }
  playerRegisterToggle = () =>{
    this.userData.set('registerAsPlayer', !this.state.registerAsPlayer)
    this.setState({registerAsPlayer:!this.state.registerAsPlayer})
  }
  coachRegisterToggle = () =>{
    this.userData.set('registerAsCoach', !this.state.registerAsCoach)
    this.setState({registerAsCoach:!this.state.registerAsCoach})
  }

  clickSubmit = () => {
    create(this.userData).then((data) => {
      if (data.error) {
        console.log("client Signup error", data)
        this.setState({error: data.error})
      } else {
        console.log("client Signup signed up!", data)
        this.setState({error: '', open: true})
      }
    })
  }
  render() {
    const {classes} = this.props
    const playerMessg = this.state.registerAsPlayer ? "Cancel player" : 
      this.state.registerAsCoach ? 'I am also a player' : 'I am a player'
    const coachMessg = this.state.registerAsCoach ? "Cancel coach" : 
      this.state.registerAsPlayer ? 'I am also a coach' : 'I am a coach'
    const userValues = {
      username:this.state.username,
      firstName:this.state.firstname,
      surname:this.state.surname,
      email:this.state.email,
      password:this.state.password,
      photo:this.state.photo
    }
    return (<div>
      <Card className={classes.card}>
        <CardContent>
          <UserCardContent values={userValues} handleChange={this.handleChange} classes={classes}/>
          {/**this.state.player && <PlayerCardContent values={this.state.player} handleChange={this.handleChange} classes={classes} />}
          {this.state.coach && <CoachCardContent values={this.state.coach} handleChange={this.handleChange} classes={classes}/>**/}
          
          {this.state.error && <Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {this.state.error}</Typography>}
        </CardContent>
        <CardActions>
          <Button className={classes.selectBtn} onClick={this.playerRegisterToggle} 
            color="primary" variant="outlined">
            {playerMessg}</Button>
          <Button className={classes.selectBtn} onClick={this.coachRegisterToggle} 
            color="primary" variant="outlined">
            {coachMessg}</Button>
          {(this.state.registerAsPlayer || this.state.registerAsCoach) &&
              <Button color="primary" variant="contained" onClick={this.clickSubmit} className={classes.submit}>
                Submit</Button>}
        </CardActions>
      </Card>
      <DialogWrapper 
        open={this.state.open} 
        title='New Account' 
        mesg='New account successfully created.' 
        buttons={[{label:'Sign in', link:'/signin'}]}/>
    </div>)
  }
}
Signup.defaultProps = {
}

//todo - use shorter syntax for Fragment but text editor not recognising it
const UserCardContent = ({values, handleChange, classes}) => 
  <React.Fragment>
    <Typography type="headline" component="h2" className={classes.title}>
      Sign Up
    </Typography>
    <TextField id="username" label="Username" className={classes.textField} value={values.username} 
        onChange={handleChange('username')} margin="normal"/><br/>
    <TextField id="firstName" label="First Name" className={classes.textField} value={values.firstName} 
        onChange={handleChange('firstName')} margin="normal"/><br/>
    <TextField id="surname" label="Surname(s)" className={classes.textField} value={values.surname} 
        onChange={handleChange('surname')} margin="normal"/><br/>
    <TextField id="email" type="email" label="Email" className={classes.textField} value={values.email} 
      onChange={handleChange('email')} margin="normal"/><br/>
    <TextField id="password" type="password" label="Password" className={classes.textField} 
      value={values.password} onChange={handleChange('password')} margin="normal"/><br/><br/>
    <input accept="image/*" onChange={handleChange('photo')} className={classes.input} 
      id="icon-button-file" type="file" style={{display:'none'}} />
    <label htmlFor="icon-button-file">
      <Button variant="text" color="default" component="span">Upload Photo
        <Publish/>
      </Button>
    </label> 
    <span className={classes.filename}>{values.photo ? values.photo.name : ''}</span>
  </React.Fragment>

const PlayerCardContent = ({values, handleChange, classes}) =>
  <React.Fragment>
    <TextField id="desc" multiline rows="2" type="desc" label="Player description" className={classes.textField} 
      value={values.description} onChange={handleChange('desc')} margin="normal"/><br/>
  </React.Fragment>

const CoachCardContent = ({values, handleChange, classes}) =>
  <React.Fragment>
    <TextField id="desc" multiline rows="2" type="desc" label="Coach description" className={classes.textField} 
      value={values.description} onChange={handleChange('desc')} margin="normal"/><br/>
  </React.Fragment>
  

Signup.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Signup)
