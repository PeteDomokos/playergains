import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//@material-ui/core
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
    ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import DeleteUserContainer from './containers/DeleteUserContainer'
//helpers
import auth from './../auth/auth-helper'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(0),
    marginTop: theme.spacing(0),
  }),
  //todo - use media queries to make display direction dependent on screen size
  //ms - row, otherwise column
  //also can use display:none for the date part of additonal items for ms
  list:{
    display:'flex',
    flexDirection:'row',
    border:'solid',
    borderColor:'pink'
  },
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  },
  additional:{
    padding:0,//theme.spacing(1),
    display:'flex',
    justifyContent:'space-between'
  },
  date:{
    flex:'65% 1 1',
    margin:0//theme.spacing(1)
  },
  update:{
    flex:'60px 0 0',
    margin:0,//theme.spacing(1)
    padding:0
  },
  updateBtn:{
    padding: theme.spacing(1)
  }
})


//note - Just the summary details are sufficient for UserProfile, not the full user
//this must be passed as props from another component 
//(User Profile itself is not connected directly to store via a container+loader)
class UserProfile extends Component {
  componentDidMount = () => {
    window.scrollTo(0, 0)
  }
  render() {
    const {user, classes} = this.props
    let backgroundColour = this.props.selected ? 'blue':''
    let profileStyle = {padding:'2%', backgroundColor:backgroundColour}
    const photoUrl = user._id
              ? '/api/user/photo/' +user._id +'?'+new Date().getTime()
              : '/api/users/defaultphoto'

    //if(this.state.redirectToSignin)
      //return <Redirect to='/signin'/>
    const authToUpdate = auth.isAuthenticated() &&
      (auth.isAuthenticated().user._id == user._id || 
        auth.isAuthenticated().user.isSystemAdmin)
    return (
      <Paper onClick={this.props.onClickFunc ? this.props.onClickFunc : () => {}} 
        className={classes.root} style={profileStyle} elevation={4}>
        <List dense className={classes.list}>
          <ListItem>

            <ListItemAvatar>
              <Avatar src={photoUrl} className={classes.bigAvatar}/>
            </ListItemAvatar>
            <ListItemText primary={user.firstName +' '+user.surname} 
                          secondary={user.email}/>
          </ListItem>

          <Divider/>

          {!this.props.shortVersion && 
            <AdditionalItems user={user} authToUpdate={authToUpdate} classes={classes}/> }
        </List>
      </Paper>
    )
  }
}
UserProfile.propTypes = {
  classes: PropTypes.object.isRequired
}
UserProfile.defaultProps = {
  user:{firstName:'', surname:'', email:''}
}

const UpdateActions = ({userId, classes}) =>
  <ListItem className={classes.update}>
    <Link to={"/user/" +userId+"/edit"}>
      <IconButton aria-label="Edit" color="primary">
        <Edit/>
      </IconButton>
    </Link>
    <DeleteUserContainer/>
  </ListItem>

const AdditionalItems = ({user, authToUpdate, classes}) =>
  <List dense className={classes.additional}>
    {/**<ListItem className={classes.date}>
      {user.created &&
        <ListItemText primary={"Joined: " + (new Date(user.created)).toDateString()}/>}
    </ListItem>**/}

    {authToUpdate &&
      <UpdateActions userId={user._id} classes={classes} />}
  </List>

/*const AdditionalItems = ({user, authToUpdate, classes}) =>
  <List dense className={classes.additional}>
    <ListItem className={classes.date}>
      {user.created &&
        <ListItemText primary={"Joined: " + (new Date(user.created)).toDateString()}/>}
    </ListItem>

    {authToUpdate &&
      <UpdateActions userId={user._id} classes={classes} />}
  </List>*/


export default withStyles(styles)(UserProfile)
