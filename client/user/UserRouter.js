import React, {Component} from 'react'
import { Route, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//children
import User from './User'
import EditUserProfileContainer from './containers/EditUserProfileContainer'

//todo - Route /user should redirect to "/signin" or to "/user/:userId" if signed in
const UserRouter = () =>
  <Switch>
    <Route path="/user/:userId/edit" component={EditUserProfileContainer}/>
    <Route path="/user/:userId" component={User}/>
  </Switch>

export default UserRouter