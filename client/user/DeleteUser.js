import React, {Component} from 'react'
import {Redirect, Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import { DialogWrapper } from '../util/components/Dialog'
//icons
import DeleteIcon from '@material-ui/icons/Delete'

class DeleteUser extends Component {
  render() {
    //history is passed to action creator to push after delete
    const { id, deleting, open, error, openDelete, openDialog, 
      deleteUser, closeDialog, history } = this.props

    const buttons =[
      {label:"Cancel", onClick:closeDialog},
      {label:"Confirm", onClick:() => deleteUser(id, history)}]

    return (<span>
      <IconButton aria-label="Delete" onClick={openDialog} color="secondary">
        <DeleteIcon/>
      </IconButton>
      <DialogWrapper open={open} title="Delete Account"
        mesg="Confirm to delete your account." buttons={buttons}/>
    </span>)
  }
}
DeleteUser.propTypes = {
  id: PropTypes.string.isRequired
}
export default withRouter(DeleteUser)
