import { mockPlayer } from './PlayerMocks'
//normal case
export const mockUser = {
	firstName:'User1FirstName',
	surname:'User1Surname',
	_id:'User1Id',
	groups:[],
	player:mockPlayer
}

//todo - edge case - missing data
export const mockPlayersWithMissingData = {}