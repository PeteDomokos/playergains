import { mockPlayers, mockPlayersWithMissingData } from './PlayerMocks'
import { TestDataConstants } from '../client/constants/TestDataConstants'

//normal case
export const mockItem = {
	id:'mock-item',
	players:mockPlayers,
	buttons:mockButtons,
	...TestDataConstants
}
//edge case - missing test data
export const mockItemWithMissingTestData = {
	id:'mock-item',
	players:mockPlayersWithMissingData,
	buttons:mockButtons,
	...TestDataConstants
}

const mockButtons = {
	skills:[{key:'touch', butLabel:'Touch'}, {key:'switchplay', butLabel:'Switchplay'}, {key:'dribble', butLabel:'Dribble'}],
	fitness:[{key:'tTest', butLabel:'T-Test'}, {key:'yoyo', butLabel:'YoYo'}, {key:'shuttles', butLabel:'Shuttles'}], 
	players:[{key:'player1', butLabel:'Player1'}, {key:'player2', butLabel:'Player2'}, {key:'player3', butLabel:'Player3'}]
}
