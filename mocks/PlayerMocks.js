import { normalData } from './TestDataMocks'
import deepFreeze from 'deep-freeze'

export const mockPlayer =
	{
		firstName:'player1',
		skills:normalData.player1.skills,
		fitness:normalData.player1.fitness,
		_id:1,
		startDate: normalData.startDate
	}

//normal case
export const mockPlayers = [
	{
		firstName:'player1',
		skills:normalData.player1.skills,
		fitness:normalData.player1.fitness,
		_id:1,
		startDate: normalData.startDate
	},
	{
		firstName:'player2',
		skills:normalData.player2.skills,
		fitness:normalData.player2.fitness,
		_id:2,
		startDate: normalData.startDate
	},
	{
		firstName:'player1',
		skills:normalData.player3.skills,
		fitness:normalData.player3.fitness,
		_id:3,
		startDate: normalData.startDate
	}
]

//todo - edge case - missing data
export const mockPlayersWithMissingData = {}
