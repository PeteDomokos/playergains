# Player Gains #

This app is in further development but has been parked since July 2020.

This app is designed primarily for use on mobile phones. A version of the app is being used in user trials at present within football clubs. The latest version can be viewed at 
	https://playergains.herokuapp.com/ 
(Update - 30th January: Currently the database is offline as the app is parked - however, this will be resolved shortly to allow viewers to see the app)
	
Demonstration videos are available here:

https://youtu.be/aNC9XoJJMro

https://youtu.be/0JyzmFALuW8


## Introduction ##

Player Gains allows sports coaches and players to enter and track their performance data over time, and to compare with other players. 
Users sign up, create groups of players from the database of available players. They can also create subgroups from an existing group. 

Users can select tests from the system, or they can create their own custom tests.

Users then start to enter data. 

Users can then view their data from a variety of perspectives, including tracking their own progress, or comparing their scores to other players, or to the expected standards for their age and level.

## Technologies ##

This app uses a MERN stack, with d3.js.

**React** – the front-end is built with reactjs, with a mobile-first approach. The front end is being re-implemented to utilise more recent React 16 functionality such as hooks.

**Redux** – Redux is built into the system, and was originally used for storing user details. However, these are now stored in session storage. The redux store will in the next iteration handle the data for the charts. This data is currently handled by the dashboard component and passed to the charts. However, the charts will still maintain local state related to user interaction.

**D3.js** – D3 functionality is embedded within React components, hence utilising the react life-cycles. Main charts are an interactive Scatter chart (for looking at how players’ data has changed over time) and a beeswarm plot using force-directed network (for comparing players’ best scores against one another). Initially, d3.js usage was contained within a shell inside a react component. In the current cycle, D3-driven elements are now being fully integrated with react components, with state managed through the life-cycles. In the next phase, some state will be managed via the redux store.

**Node/Express** – this server set-up matches the speed requirements of the system. There is currently no usage of complex systems such as integrating data from multiple web service sources, and so there seemed little to gain from using something such as Java or Scala and the Play framework, which was considered. It is anticipated that real-time data from users mobile devices such as smart watches will be added as an option, and a Node/Express solution should provide the necessary speed, especially when coupled with d3 on the client side for processing the data. 

**MongoDB** – a no sql database solution is deemed appropriate because users have the potential to reate their own sports tests which will have their own properties. This means that freedom needs to be afforded to the system to evolve in response to the users requirements. 

**Styling and responsiveness**– external stylesheets have been used, except in some cases where elements have been borrowed from MaterialUI. Some stylesheets contain styling for all screen sizes and devices, and other sheets are specifically for certain cases. Screen sizes have been split into three options, corresponding to mobile phone, tablet and laptop/desktop. Some react components are only rendered for specific screen sizes. This is handled predominantly via classNames and css properties than via inline media queries.

## Design Methodology ##

The app has used a wizard-of-oz methodology, involving several iterations of evaluate – develop – trial, with each iteration facilitating more and more independent usage of the app by users (ie users requiring less support from observers) or inbuilt prompts.
The approach used in the technical development to date can be described as Experiment-driven development but in order to achieve the necessary robustness, this app is now being re-implemented with test-driven development. Following trials that are currently taking place, the app will be implemented for ReactNative.



