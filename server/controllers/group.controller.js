import formidable from 'formidable'
import fs from 'fs'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import { addRefToGroupArray, addRefToUserArray,
  removeRefFromGroupArray, removeRefFromUserArray } from './../helpers/dbQueries'
import Group from '../models/group.model'
import User from '../models/user.model'
//import Player from '../models/player.model'
import profileImage from './../../client/assets/images/profilepic.png'
/**
 * Load group and append to req.
 */
export const groupById = (req, res, next, id) => {
  Group.findById(id)
    .populate('admin', 'firstName surname')
    //recall players are just users
    .populate('players', 'firstName surname photo')
    .exec((err, group) => {
    	// ||group makes it log even when error = null
        if(err){
          console.log("groupById error....", err)
          return res.status('400').json({error: 
            errorHandler.getErrorMessage(err)
          })
        }
      	req.group = group
      	next()
    })
}
/**
*   Parses the incoming request form, and calls function to create and save user
* 
**/
export const create = (req, res, next) => {
  console.log("grpCtrl.create")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
  	if(err){
      console.log("grpCtrl.create() form parse error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
  	createAndReturnGroup(req, res, fields, files)
  })
}
/**
*   Creates and saves a user to the database
*   @params   fields - the properties for the user, including required fields: 
*             username, firstName, surname, email, hashed_password.  
**/
export const createAndReturnGroup = (req, res, fields, files) =>{
  console.log("createAndReturnGroup fields:", fields)
  const group = new Group(fields)
  //photo
  if(files.photo){
    group.photo.data = fs.readFileSync(files.photo.path)
    group.photo.contentType = files.photo.type
  }
  //save
  group.save((err, val) =>{
    if(err){
      console.log("createGroup group save error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("createGroup group saved")
    //add group ref into each admin user
    group.admin.forEach(userId =>{
      addRefToUserArray(userId, 'adminGroups', group._id)
    })
    //add group ref to parent group
    if(group.parent)
      addRefToGroupArray(group.parent, 'subgroups', group._id)
    
    //need to add ref props eg for group admin so client can go straight to home page from new group
    return res.status(200).json(val)
  })
}

/**
* @params req - the incoming request with the user attached
**/
export const read = (req, res) => {
  console.log("read group...params", req.params)
  if(req.params.groupId)
    return res.json(req.group)
  else{
    //multiple groups
    console.log("reading multiple groups")
    Group.find()
    .populate('players', 'firstName surname photo')
    .populate('subgroups', 'name _id desc subgroups')
    .exec((err, groups) => {
      if(err){
        console.log("read error")
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      }
      console.log("read returning")
      return res.status(200).json(groups)
    })
  }
}
export const list = (req, res) => {
  console.log("list groups")
  Group.find()
    .populate('subgroups', 'name _id desc subgroups')
    .exec((err, groups) => {
      if(err){
        console.log("list error")
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      }
      console.log("list returning")
      return res.status(200).json(groups)
    })
}
export const listUserAdminGroups = (req, res) => {
  console.log("list user admin groups userId", req.user._id)
  //need the query operator which returns all groups whose admin field array contains req.group._id 
  Group.find({admin:{ $in:[req.user._id]}}, (err, value) =>{
    if(err){
      console.log("list group by admin user error", err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("list by admin user returning")
    return res.status(200).json(value)
  })
}

export const update = (req,res) =>{
  console.log("groupCtrl.update")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
    if(err){
      console.log("groupCtrl.update() form parse error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("update form parsed no errors...")
    updateAndReturnGroup(req, res, fields, files)
  })
}
export const updateAndReturnGroup = (req, res, fields, files) =>{
  console.log("updateAndReturnGroup fields", fields)
  let group = req.group
  group = _.extend(group, fields)
  group.updated = Date.now()
  if(files.photo){
    group.photo.data = fs.readFileSync(files.photo.path)
    group.photo.contentType = files.photo.type
  }
  group.save((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("updated and saved group:", group.name)
    res.json(group)
  })
}
//todo - merge with update
export const updateFields = (req,res) =>{
  console.log("groupCtrl.updateFields")
  let fields = req.body
  let group = req.group
  group = _.extend(group, fields)
  group.save((err, result) => {
    if (err) {
      console.log("updated error!", errorHandler.getErrorMessage(err))
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("updated and saved group")
    res.json(group)
  })
}

/**
* @params req - the incoming request with the user attached
**/
export const remove = (req, res) => {
  console.log("group remove name: ", req.group.name)
  const { group } = req
  Group.deleteOne({_id:group._id}, (err, value) =>{
    if(err){
      console.log("removal error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("removed group val", value)
    //remove refs

    //todo - make these separate methods called from router!!!!!!!!!!!!!!!!!!!
    if(group.parent){
      removeRefFromGroupArray(group.parent, 'subgroups', group._id)
    }

    group.admin.forEach(userId =>{
      removeRefFromUserArray(userId, 'adminGroups', group._id)
    })

    return res.status(200).json(value)
  })
}

export const removeAll = (req, res) => {
  Group.deleteMany({}, (err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}

export const addPlayer = (req, res) => {
  console.log("grpCrtl.addPlayer")
  const { playerId, groupId } = req.body

  Group.findByIdAndUpdate(groupId, {$push: {'players': playerId}}, {new: true})
    .exec((err, result) => {
      if(err)
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      console.log("added player to group:", result)
      return res.status(200).json(result)
    }) 
}

export const removePlayer = (req, res) => {
  console.log("grpCrtl.addPlayer")
  const { playerId, groupId } = req.body

  Group.findByIdAndUpdate(groupId, {$pull: {'players': playerId}}, {new: true})
    .exec((err, result) => {
      if(err)
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      console.log("removed player from group")
      return res.status(200).json(result)
    })

}
export const addDataset = (req, res) =>{
  console.log("addDataset: ", req.body)
  const { group } = req
  group.datasets.push(req.body)
  group.save((err, group) => {
    if (err) {
      console.log("updatedgroup save error!", errorHandler.getErrorMessage(err))
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)})
    }
    console.log("updated and saved group")
    //for now, whole group is returned and updated in store
    res.json(group)
  })
}

export const deleteDataset = (req, res) =>{
  console.log("deleteDataset...")
  const { group } = req
  console.log('group datasets  size before update', group.datasets.length)
  const { datasetId } = req.params
  const updatedDatasets = group.datasets.filter(d => !d._id.equals(datasetId))
  group.datasets = updatedDatasets
  console.log('updatedGroup datasets size', group.datasets.length)
  //group.datasets.push(req.body)
  group.save((err, group) => {
    if (err) {
      console.log("updatedgroup save error!", errorHandler.getErrorMessage(err))
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)})
    }
    console.log("updated and saved group")
    //for now, whole group is returned and updated in store
    res.json(group)
  })
}
export const updateDataset = (req, res) =>{
  console.log("updateDataset()...")
  const { group } = req
  console.log('current datasets length', group.datasets.length)
  //remove old dataset
  const _datasets = group.datasets.filter(d => !d._id.equals(req.body._id))
  console.log('other datasets length', _datasets.length)
  //add updated
  _datasets.push(req.body)
  console.log('new datasets length', _datasets.length)
  group.datasets = _datasets
  group.save((err, group) => {
    if (err) {
      console.log("updatedgroup save error!", errorHandler.getErrorMessage(err))
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)})
    }
    console.log("updated and saved group")
    //for now, whole group is returned and updated in store
    res.json(group)
  })
}

export const addDatapoint = (req, res) =>{
  console.log("addDatapoint to datasetId", req.params.datasetId)
  console.log("datapoint...", req.body)
  const { group } = req
  const { datasetId } = req.params
  const dataset = group.datasets.find(d => d._id.equals(datasetId))
  console.log("datapoints length before push", dataset.datapoints.length)
  dataset.datapoints.push(req.body) 
  console.log("datapoints length after push", dataset.datapoints.length)
  const otherDatasets = group.datasets.filter(d => !d._id.equals(datasetId))
  const _datasets = [...otherDatasets, dataset]
  group.datasets = _datasets
  console.log("updatedDatasetDatapoints")
  group.save((err, updatedGroup) => {
    if (err) {
      console.log("updatedgroup save error!", errorHandler.getErrorMessage(err))
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)})
    }
    console.log("updated and saved group")
    res.json(updatedGroup)
  })
}


/**
*
*  
**/
export const photo = (req, res, next) => {
  //console.log("group photo")
  if(req.group && req.group.photo.data){
    res.set("Content-Type", req.group.photo.contentType)
    return res.send(req.group.photo.data)
  }
  next()
}
/**
*
*  
**/
export const defaultPhoto = (req, res) => {
  //console.log("group default photo")
  return res.sendFile(process.cwd()+profileImage)
}

export default {
  create,
  groupById,
  read,
  list,
  remove,
  update,
  updateFields,
  photo,
  defaultPhoto,
  addPlayer, 
  removePlayer,
  listUserAdminGroups,
  addDataset,
  deleteDataset,
  updateDataset,
  addDatapoint
}
