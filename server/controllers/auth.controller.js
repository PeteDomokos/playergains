import User from '../models/user.model'
import Group from '../models/group.model'
import jwt from 'jsonwebtoken'
import expressJwt from 'express-jwt'
import config from './../../config/config'

const signin = (req, res) => {
  console.log("authCtrl.signin() req.body:", req.body)
  User.findOne({"email": req.body.email})
    .populate('adminGroups', 'name _id desc players parent admin coaches subgroups')
    .populate('groupsFollowing', 'name _id desc groupType players parent admin coaches subgroups')
    .populate({ path: 'player.groups', select: 'name _id desc groupType players parent admin coaches subgroups' })
    .populate({ path: 'coach.groups', select: 'name _id desc groupType players parent admin coaches subgroups' })
    .exec((err, user) => {
    if (err || !user)
      return res.status('401').json({
        error: "User not found"
      })

    if (!user.authenticate(req.body.password)) {
      return res.status('401').send({
        error: "Email and password don't match."
      })
    }

    const token = jwt.sign({
      _id: user._id
    }, config.jwtSecret)

    res.cookie("t", token, {
      expire: new Date() + 9999
    })

    return res.json({
      token,
      user: user
    })

  })
}

const signout = (req, res) => {
  console.log("sign out")
  res.clearCookie("t")
  return res.status('200').json({
    message: "signed out"
  })
}

/**
*   Adds auth object to req, containing the credentials of the signed in user
*
*  if no user signed in, this throws an error
**/
const requireSignin = expressJwt({
  secret: config.jwtSecret,
  userProperty: 'auth'
})

/**
*   Checks that the user signed in (stored in auth) is either systemAdmin 
*   or is the user being updated (ie req.profile)
*
*  if no user signed in, this throws an error
**/
//todo -use this instead o fhardcoding admin id. But need to work out how to 
//use as its async
const userIsSystemAdmin = (id) => {
  console.log("userIsSystemAdmin()...")
   User.findById(id).exec((err, user) => {
    if(err){
      console.log("userIsSystemAdmin()...error")
      return false
    }
    else{
      console.log("userIsSystemAdmin()...true")
      return true
    }
  })
}
//NOT AWAITING!
/*const hasAuthorization = async (req, res, next) => {
  console.log("hasAuth()...req.user", req.user)
  console.log("hasAuth()...req.auth", req.auth)
  const authorized = req.user && req.auth && req.user._id == req.auth._id 
  if(authorized)
    next()
  else{
    console.log("finding by id")
    await User.findById(id).exec().then(result =>{
      console.log("result", result)
      return true
    })
  }
}*/

const hasAuthorization = (req, res, next) => {
  //put temp block on changes
  let userIsDemo = false
  //todo - just check for includes('Demo')
  if(req.user && req.user.firstName == 'Demo'){
    userIsDemo = true;
  //todo - change name to Demo Attacking Players and just check for includes('Demo')
  }else if(req.group && req.group.name == 'Attacking Players'){
    userIsDemo = true
  }

  //temp
  userIsDemo = true;

  const authorized = req.user && req.auth && req.user._id == req.auth._id 
    && !userIsDemo

  console.log('authorized? ', authorized)

  if(authorized){
    next()
  }
  else{
    if(userIsDemo){
      console.log('!!!!!!demo')
      return res.status('400').json({
          message: "This is a demo user so you cannot save changes."
        })
    }else{
      User.findById(req.auth.id).exec((err, user) => {
        if(err){
          console.log("userIsSystemAdmin()...error")
          return res.status('403').json({
            error: "User not authorized or system admin"
          })
          //return false
        }
        else{
          console.log("userIsSystemAdmin()...true")
          next()
          //return true
        }
      })
    }
  }
}

/**
*   Checks that the user signed in (stored in auth) is either systemAdmin 
*   or is in admin list of the group being updated (ie req.group)
*
*  if no user signed in, this throws an error
**/
const hasGroupAuthorization = (req, res, next) => {
  const authorized = req.group && req.auth && 
      (req.group.admin.includes(req.auth.id) || req.auth.isSystemAdmin === true) 
      
  if (!(authorized)) {
    return res.status('403').json({
      error: "User is not authorized"
    })
  }
  next()
}
/**
*   Checks that the user signed in (stored in auth) is either systemAdmin 
*   or is in admin list of the player being updated (ie req.player)
*
*  if no user signed in, this throws an error
**/
const hasPlayerAuthorization = (req, res, next) => {
  const authorized = req.player && req.auth && 
      (req.player.admin.includes(req.auth.id) || req.auth.isSystemAdmin === true) 

  if (!(authorized)) {
    return res.status('403').json({
      error: "User is not authorized"
    })
  }
  next()
}

export default {
  signin,
  signout,
  requireSignin,
  hasAuthorization,
}
