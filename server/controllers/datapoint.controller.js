import errorHandler from './../helpers/dbErrorHandler'

/**
 * Load datapoint and append to req.
 */
export const datapointByID = (req, res, next, id) => {
  Datapoint.findById(id)
    //.populate()
      .exec((err, datapoint) => {
    if (err || !datapoint)
      return res.status('400').json({
        error: "datapoint not found"
      })
    req.datapoint = datapoint
    next()
  })
}

//note - playerId is actually the userId - we dont need to use the player Id,
//but need to consider, is there a case for using playerId
//seeing as we have PlayerSchema embedded within user

/**
*      Returns all datapoints belonging to a given player
*      
*      This does not return datapoints that reference a group of players,
*      even if that group includes this player
**/
export const datapointsByPlayerID = (req, res, next, id) => {
  //need to search datapoints for those whose player that is referenced has this id
  /*Datapoint.find({player:id???????????})
    //.populate()
      .exec((err, datapoint) => {
    if (err || !datapoint)
      return res.status('400').json({
        error: "datapoint not found"
      })
    req.datapoints = datapoints
    next()
  })*/
}
/**
*      Returns all datapoints for all players in a given group
*
*      Does not return datapoints that belong to the group (ie group datapoints)
**/
export const allPlayerDatapointsByGroupID = (req, res, next, id) => {
  //need to search datapoints for those whose player that is referenced has this id
  /*Datapoint.find({player:id???????????})
    //.populate()
      .exec((err, datapoint) => {
    if (err || !datapoint)
      return res.status('400').json({
        error: "datapoint not found"
      })
    req.datapoints = datapoints
    next()
  })*/
}
/**
*   Parses the incoming request form, and calls function to create and save datapoint
* 
**/
export const create = (req, res, next) => {
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
  	if(err)
      return res.status(400).json({error: err})
  	createAndReturnDatapoint(res, fields, files)
  })
}

/**
*   Creates and saves a datapoint to the database
*   @params   fields - the properties for the datapoint, including required fields: 
*             datapointname, firstName, surname, email, hashed_password.  
**/
export const createAndReturnDatapoint = (res, fields, files) =>{
  const datapoint = new datapoint(fields)
  Datapoint.save(datapoint, (err, val) =>{
    if(err){
      return res.status(400).json({error: err})
    }
    return res.status(200).json(val)
  })
}

/**
* @params req - the incoming request with the datapoint attached.
*               
**/
const readDatapoint = (req, res) => {
  req.profile.hashed_password = undefined
  req.profile.salt = undefined
  return res.json(req.datapoint)
}
/**
* @params req - the incoming request with the datapoints attached. These could be for one player or several
*               
**/
const readDatapoints = (req, res) => {
  req.profile.hashed_password = undefined
  req.profile.salt = undefined
  return res.json(req.datapoints)
}


export const list = (req, res) => {
  Datapoint.find((err, value) =>{
    if(err)
      return res.status(400).json({error: err})
    return res.status(200).json(value)
  })
}

/**
* @params req - the incoming request with the datapoint attached
**/
export const remove = (req, res) => {
  Datapoint.deleteMany({_id:req.datapoint._id}, (err, res) =>{
    if(err)
      return res.status(400).json({error: err})
    return res.status(200).json(res)
  })
}

export const removeAll = (req, res) => {
  Datapoint.deleteMany({}, (err, value) =>{
    if(err)
      return res.status(400).json({error: err})
    return res.status(200).json(value)
  })
}

