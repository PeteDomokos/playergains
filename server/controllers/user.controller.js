 import formidable from 'formidable'
import fs from 'fs'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import User from '../models/user.model'
import Group from '../models/group.model'
import profileImage from './../../client/assets/images/profilepic.png'

/**
 * Load user and append to req.
 */
export const userById = (req, res, next, id) => {
  //console.log("userById req.params", req.params)
  //console.log("userById req.url", req.url)
  User.findById(id)
    .populate('adminGroups', 'name _id desc players parent admin coaches subgroups')
    .populate('groupsFollowing', 'name _id desc groupType players parent admin coaches subgroups')
    .populate({ path: 'playerInfo.groups', select: 'name _id desc groupType players parent admin coaches subgroups' })
    .populate({ path: 'coachInfo.groups', select: 'name _id desc groupType players parent admin coaches subgroups' })
    .exec((err, user) => {
    if(err || !user){
      console.log("userById error: ", err)
      return res.status('400').json({error: "User not found"})
    }
    //console.log("userById found:", user.username)
    req.user = user
    next()
  })
}
/**
*   Parses the incoming request form, and calls function to create and save user
* 
**/
export const create = (req, res, next) => {
  console.log("userCtrl...create req.body", req.body)
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
  	if(err){
      console.log("userCtrl.create() form parse error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("form parsed...")
  	createAndReturnUser(res, fields, files)
  })
}

/**
*   Creates and saves a user to the database
*   @params   fields - the properties for the user, including required fields: 
*             username, firstName, surname, email, hashed_password.  
**/
export const createAndReturnUser = (res, fields, files) =>{
  console.log("userCtrl...createUser fields", fields)
  //fields
  const {registerAsPlayer, registerAsCoach, ...basicUserFields} = fields
  const playerInfo = registerAsPlayer ? {registered:true} : {}
  const coachInfo = registerAsCoach ? {registered:true} : {}
  const userFields = { ...basicUserFields, playerInfo, coachInfo }
  const user = new User(userFields)
  //photo
  if(files.photo){
      user.photo.data = fs.readFileSync(files.photo.path)
      user.photo.contentType = files.photo.type
    }
  //save
  user.save((err, val) =>{
    if(err){
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    return res.status(200).json(val)
  })
}

/**
* @params req - the incoming request with the user attached
**/
export const read = (req, res) => {
  console.log("userCtrl.read:", req.user.username)
  req.user.hashed_password = undefined
  req.user.salt = undefined
  return res.json(req.user)
 
}

export const list = (req, res) => {
  console.log("user list")
  User.find((err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}
export const listPlayers = (req, res) => {
  User.find({player:{registered:true}},(err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}
export const listCoaches = (req, res) => {
  User.find({coach:{registered:true}},(err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}
export const update = (req,res) =>{
  console.log("userCtrl.update")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  console.log("parsing form")
  form.parse(req, (err, fields, files) =>{
    console.log("update form parsed...")
    if(err){
      console.log("userCtrl.update() form parse error")
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("update form parsed no errors...")
    updateAndReturnUser(req, res, fields, files)
  })
}
export const updateAndReturnUser = (req, res, fields, files) =>{
  console.log("updateAndReturnUser fields", fields)
  let { user } = req
  user = _.extend(user, fields)
  user.updated = Date.now()
  if(files.photo){
    user.photo.data = fs.readFileSync(files.photo.path)
    user.photo.contentType = files.photo.type
  }
  user.save((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    user.hashed_password = undefined
    user.salt = undefined
    res.json(user)
  })
}

/**
* @params req - the incoming request with the user a ttached
**/
export const remove = (req, res) => {
  console.log("remove() user", req.user._id)
  User.deleteOne({_id:req.user._id}, (err, val) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(val)
  })
}

export const removeAllExceptAdmin = (req, res) => {
  console.log("removeAllExceptAdmin()")
  User.deleteMany({isSystemAdmin:false}, (err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}

export const photo = (req, res, next) => {
  //console.log("photo()")
  if(req.user && req.user.photo.data){
    res.set("Content-Type", req.user.photo.contentType)
    return res.send(req.user.photo.data)
  }
  next()
}

export const defaultPhoto = (req, res) =>{
  //console.log("defaultphoto().............................")
  return res.sendFile(process.cwd()+profileImage)
}


export const addGroupToPlayer = (req, res, next) => {
  console.log("addGroupToPlayer body:", req.body)
  const { playerId, groupId } = req.body

  //todo - change player to playerInfo
  User.findById(playerId)
    .exec((err, player) => {
      if(err)
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      //add group ref to player
      console.log("found player...groups before push:", player.playerInfo.groups)
      player.playerInfo.groups.push(groupId)
      console.log("found player...groups after push:", player.playerInfo.groups)
      player.save((err, result) => {
        if(err){
          console.log("updated player save error...")
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          })
        }
        console.log("updated player save error...")
        next()
        }) 
    })
}

export const removeGroupFromPlayer = (req, res, next) => {
  console.log("removeGroupFromPlayer body:", req.body)
  const { playerId, groupId } = req.body

  //todo - change player to playerInfo
  User.findById(playerId)
    .exec((err, player) => {
      if(err)
        return res.status(400).json({error: 
          errorHandler.getErrorMessage(err)
        })
      //add group ref to player
      console.log("groups length before filter:", player.playerInfo.groups.length)
      const _groups = player.playerInfo.groups.filter(g => !g.equals(groupId))
      player.playerInfo.groups = _groups
      console.log("after:", player.playerInfo.groups.length)
      
      player.save((err, result) => {
        if(err){
          console.log("updated player save error...")
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          })
        }
        next()
      })
    })
 
}

export default {
  create,
  userById,
  read,
  list,
  listCoaches,
  listPlayers,
  remove,
  removeAllExceptAdmin,
  update,
  photo,
  defaultPhoto,
  addGroupToPlayer,
  removeGroupFromPlayer
}


/*
//PROMISES async
 async function test(){
    const result = await new Promise((resolve, reject) =>{
      setTimeout(() => resolve("Hello world"), 1000)
    })
    
    console.log(result)    //prints 2nd
    console.log("Goodbye!") //prints 3rd as it awaits
  }
  //test()
  //console.log("and good luck!") //prints first as it is not within test() so it doesnt await

*/

