import formidable from 'formidable'
import fs from 'fs'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import Dataset from '../models/dataset.model'
import User from '../models/user.model'
import Group from '../models/group.model'

/**
 * warning: Group must be already appended to req.
 */
export const datasetById = (req, res, next, id) => {
  if(!req.group){
    console.log("error no group")
    return res.status('400').json({error: "server error"})
  }
  //note === doesnt work only ==
  const requiredDataset = req.group.datasets.find(d => d._id == id)
  if(!requiredDataset){
    console.log("error cant find dataset")
    return res.status('400').json({
        error: "Dataset not found"
      })
  }
  console.log("Dataset found. name: ", requiredDataset.name)
  req.dataset = requiredDataset
  next()
}
/**
*   Parses the incoming request form, and calls function to create and save user
* 
**/

//todo - create validator that checks dataset deosnt exist already
export const create = (req, res, next) => {
  console.log("datasetCtrl.create dataset props", req.body)
  const dataset = {...req.body, group:req.group._id}
  //cant crfeate new as its not model const dataset = new Dataset(req.dataset)
  //console.log("dataset", dataset)

  //todo
  //note - we are getting group but we already have it from groupById method
  //as groupId is a param. so we just need to update it manually and save
  Group.findByIdAndUpdate(req.group._id, {$push: {datasets: dataset}}, {new:true})
    .exec((err, group) => {
      if (err || !group){
        console.log("find group error", err)
        return res.status('400').json({
          error: "Group not found"
        })
      }
      console.log("found and updated group", group)
      return res.status(200).json(group)
    })
   /*Group.findById(req.group._id).exec((err, group) => {
      if (err || !group){
        console.log("find group error", err)
        return res.status('400').json({
          error: "Group not found"
        })
      }
      group.datasets.push(req.body)
      console.log("group after push", group)
      group.save((err, val) =>{
          console.log("save group error")
          if (err || !group)
            return res.status('400').json({
              error: "Group not found"
            })
          console.log("saved group")
          return res.status(200).json(group)
       })
    })
    */
}
//todo - create validator that checks dataset deosnt exist already
//if eventDate is within a given time range eg 5 mins, send user a 
//warning, saying - you have already created... are you sure you want to create
//another datapoint for so soon afetr the previous?
//they may want to do that, but they may just have pressed save twice!!!
//need to think about this.
export const createDatapoint = (req, res) =>{
  //console.log("createDatapoint group name:", req.group.name)
 console.log("createDatapoint dataset id:", req.dataset._id)
 console.log("datasets.length before update (should stay same): ", req.group.datasets.length)
 console.log("createDatapoint datapoint:", req.body)
  //need to find out how to update.  
  const datapoint = req.body
  //todo - check we neeneed toObject() 
  const group = req.group.toObject()
  const dataset = req.dataset.toObject()
  const datasetWithAddedDatapoint = {...dataset, datapoints:[...req.dataset.datapoints, datapoint]}
  console.log("datasetWithAddedDatapoint: ", datasetWithAddedDatapoint)
  //remove old dataset
  const otherGroupDatasets = req.group.datasets.filter(d => d._id != req.dataset._id)
  //add updated dataset
  const updatedGroupDatasets = [...otherGroupDatasets, datasetWithAddedDatapoint]
  //todo - try group.update({datasets:updatededGroupDatasets}) so noneed to search collection
  Group.findByIdAndUpdate(req.group._id, { $set: {datasets:updatedGroupDatasets} }, 
    {new:true}, (err, updatedGroup) =>{
      if (err || !updatedGroup){
          console.log("find group error", err)
          return res.status('400').json({
            error: "Group not found"
          })
        }
      //check new ds not added
      console.log("group updated. datasets.length still: ", updatedGroup.datasets.length)
      return res.status(200).json(updatedGroup)
  })
  
  /*todo - try this way, using req.grou instead of searching db unneccesarily
  group.save((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("updated and saved group:", req.group.name)
    return res.status(200).json(result)
  })*/
}
/**
* @params req - the incoming request with the user attached
**/
export const read = (req, res) => {
  console.log("datasetCtrl.read")
  return res.json(req.dataset)
 
}
export const listUserDatasets= (req, res) => {
  console.log("dataset list")
  Dataset.find((err, value) =>{
    if(err){
      console.log("dataset list error", err)
      console.log("dataset list error mesg", errorHandler.getErrorMessage(err))
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("dataset list returning", value)
    return res.status(200).json(value)
  })
}


export const listAllDatasets = (req, res) => {
  console.log("dataset list")
  Dataset.find((err, value) =>{
    if(err){
      console.log("dataset list error", err)
      console.log("dataset list error mesg", errorHandler.getErrorMessage(err))
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("dataset list returning", value)
    return res.status(200).json(value)
  })
}

/**
* @params req - the incoming request with the user a ttached
**/
export const remove = (req, res) => {
  console.log("remove() dataset", req.dataset._id)
  Dataset.deleteOne({_id:req.dataset._id}, (err, val) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(val)
  })
}
/*

export const removeAll = (req, res) => {
  console.log("removeAllExceptAdmin()")
  User.deleteMany({isSystemAdmin:false}, (err, value) =>{
    if(err)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
    return res.status(200).json(value)
  })
}
*/

