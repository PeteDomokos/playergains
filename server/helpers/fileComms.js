'use strict'

/**
 * read file
 */
const readLocalFile = (path) => {
    //console.log("readlocalFile path:", path)
    const fs = require('fs')
    return new Promise(function(resolve,reject){
        fs.readFile(path, function(err,data){
            if(err){
                console.log('err:',err)
                return reject(err)
            }
            else{
                const dataStr = data.toString('utf8')
                let dataArr = dataStr.split("\n").slice(1).filter(line => line.length > 1)
      
                //remove \r from end of each string
                let dataArrFormatted = dataArr
                  .map(line => line.replace('\r', ''))
                  .map(line => line.replace(/ , /g, ","))
                  .map(line => line.replace(/ ,/g, ","))
                  .map(line => line.replace(/, /g, ","))
                  //split array of strings into array of arrays
                  .map(line => line.split(","))
                  //remove last char of each array entry if its ' '
                  .map(arr => arr.map(el => {
                    if(el[el.length-1] == ' ')
                      return el.substring(0,el.length-1)
                    return el
                    })
                  )
                //console.log("readLocalFile returning data from path: ",path)
                /*parse csv
                //const parse = require('csv-parse')
                //const assert = require('assert')

                let parsedData = parse(dataStr, {
                    comment: '#'
                }, function(err, output){
                    //console.log("output: ", output)
                    //console.log("output length: ", output.length)
                })
               // return dataStr.slice(nrLinesToDrop)
               */
               return resolve(dataArrFormatted) 
            }
        })
    })
}

/**
 * 
 */
const isWindows = true
const configPath = (path) => {
    return path
     //if (systemIsWindows) {
           // let windowsPath = path.replace("""/""", """\""")
           // windowsPath
        //}
        //else path
}
//todo - handle players undefined or length < 3
const formatFromFile = (str, players) => {
  let punctuatedStr = str.replace(/_C/g, ",")
  if(!players || players < 2)
    return punctuatedStr
  let puncStrWith2Players = punctuatedStr.replace(/p1/g, players[0].persInfo.firstName)
                                         .replace(/p2/g, players[1].persInfo.firstName)
  if(players.length < 3)
    return puncStrWith2Players
  return puncStrWith2Players.replace(/p3/g, players[2].persInfo.firstName)
  /*const playerNames = players.map(p => p.persInfo.firstName)
    return str.replace("_C", ", ").
           replace("p1", playerNames(0)).
             replace("p2", playerNames(1)).
             replace("p3", playerNames(2)).
             replace("p4", playerNames(3)).
             replace("p5", playerNames(4))*/
}
    
export default {readLocalFile, configPath, formatFromFile}
