'use strict'


//starts at week 0, but if user goes back to previous week
// its -1, then -2, then -3...
const calculateWeek = (date, startingDate) =>{
  if(date - startingDate >= 0)
    return weeksElapsed(date, startingDate)
  else
    return weeksElapsed(date, startingDate) - 1
}

const weeksElapsed = (date, startingDate) =>{
  //console.log("calculating weeksElapsed...")
  //convert time to milli if necc
  let startMil = 
   typeof startingDate == 'number' ? startingDate : startingDate.getTime()
  let requiredMil = 
    typeof date == 'number' ? date : date.getTime()
  // get elapsed time since start, in milliseconds
  var mil = requiredMil - startMil
  //calculate the elapsed time in weeks, days, hours, mins and secs
  var seconds = (mil / 1000) | 0;
  mil -= seconds * 1000;

  var minutes = (seconds / 60) | 0;
  seconds -= minutes * 60;

  var hours = (minutes / 60) | 0;
  minutes -= hours * 60;
  var days = (hours / 24) | 0;
  hours -= days * 24;

  var weeks = (days / 7) | 0;
  days -= weeks * 7;
 // console.log("returning weeks:", weeks)

  return weeks

}

export default {calculateWeek}