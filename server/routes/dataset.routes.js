import express from 'express'
import { datasetById, create, createDatapoint, read, listUserDatasets, 
	listAllDatasets, remove, removeAll } 
	from '../controllers/dataset.controller'
import { groupById } from '../controllers/group.controller'
import { userById } from '../controllers/group.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()
//todo -check user signed in is system admin
//router.route('/api/datasets')
 // .get(listAllDatasets)

//todo -check user signed in is part of group admin
router.route('/api/datasets/bygroup/:groupId/:datasetId')
  //.get(read)
  //.delete(remove)
  .put(createDatapoint)

//todo -check user signed in is part of group admin
router.route('/api/datasets/bygroup/:groupId')
  .post(create)
  .get(read)

router.route('/api/datasets/byuser/:userId')
  .get(listUserDatasets)


router.param('groupId', groupById)
router.param('datasetId', datasetById)

export default router
