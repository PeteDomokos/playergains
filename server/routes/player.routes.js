import express from 'express'
import playerCtrl from '../controllers/player.controller'
import { groupById } from '../controllers/group.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

router.route('/api/players')
  .get(playerCtrl.list)
  //.post(playerCtrl.create) 
router.route('/api/players/bygroup/:groupId')
  .get(playerCtrl.list)
router.route('/api/players/eligible/bygroup/:groupId')
  .get(playerCtrl.listEligibleByGroup)

//router.route('/api/player-snapshots')
 // .get(playerCtrl.listSnapshots)

router.route('/api/players/photo/:playerId')
  .get(playerCtrl.photo, playerCtrl.defaultPhoto)
router.route('/api/players/defaultphoto')
  .get(playerCtrl.defaultPhoto)

/*
router.route('/api/players/writeDataFromServer')
  .get(playerCtrl.writeDataFromServer)
router.route('/api/players/createPlayersFromServer')
  .get(playerCtrl.createPlayersFromServer)

router.route('/api/players/:playerId')
  .delete(playerCtrl.remove)
  .put(playerCtrl.update)

router.route('/api/tests/:playerId/:cat/:test')
  .get(playerCtrl.listTests)
  .delete(playerCtrl.deleteTests)

router.route('/api/tests/:playerId/:cat/:test/:testId')
  .delete(playerCtrl.deleteTest)

router.route('/api/players/:playerId/:cat/:test/:week')
  .get(playerCtrl.readScore)
  */

router.param('groupId', groupById)

export default router
