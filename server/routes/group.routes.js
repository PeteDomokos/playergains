import express from 'express'
import groupCtrl from '../controllers/group.controller'
import { userById, addGroupToPlayer, removeGroupFromPlayer } from '../controllers/user.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

//temp until impl query string of group Ids so we know which groups we want
//for now just load all groups from server in full
router.route('/api/full-groups')
  .get(groupCtrl.read)

router.route('/api/groups')
  .get(groupCtrl.list)
  .post(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.create)

router.route('/api/group/players/add')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, addGroupToPlayer, groupCtrl.addPlayer)

router.route('/api/group/players/remove')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, removeGroupFromPlayer, groupCtrl.removePlayer)

router.route('/api/groups/by-admin-user/:userId')
  .get(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.listUserAdminGroups)

//todo - merge this with the other update method
router.route('/api/groups/update-fields/:groupId')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.updateFields)

router.route('/api/group/photo/:groupId')
  .get(groupCtrl.photo, groupCtrl.defaultPhoto)
router.route('/api/group/defaultphoto')
  .get(groupCtrl.defaultPhoto)
  
router.route('/api/groups/:groupId')
  .get(groupCtrl.read)
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.update)
  .delete(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.remove)

router.route('/api/group/:groupId/datasets/add')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.addDataset)

router.route('/api/group/:groupId/dataset/:datasetId')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.updateDataset)
  .delete(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.deleteDataset)

router.route('/api/group/:groupId/dataset/:datasetId/datapoints/add')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.addDatapoint)

router.param('groupId', groupCtrl.groupById)
router.param('userId', userById)

export default router
