import express from 'express'
import coachCtrl from '../controllers/coach.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

//router.param('coachId', coachCtrl.coachByID)

export default router
