import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'

export default new mongoose.Schema({
  name:{
    type:String,
    required:'Name is required'
  },
  key:{
    type:String,
    required:'Key is required'
  },
  dataType:{
    type:String,
    default:'number'
  },
  numberOrder: {
    type:String,
    default:'low-to-high'
  },
  formula:String,
  unit:String,
  isMain:Boolean
})

