import mongoose from 'mongoose'
import DatasetSchema from './dataset.model'



/*
  TODO - certain groups can act as 'root' groups. This means they appear in any flat search of groups, 
  even if they are a subgroup. Any group without a parent is automatically a root group.
  But a subgroup can be anabled to act as a root. So I will add an 'isRoot' property.
  This enables large organisations to have several roots, eg MUFC Senior, MUFC Junior, etc
  Otherwise the DashboardSelectors and such like would be chaotic, it would take too long to filter down to 
  teh subgroup required eg MUFC -> Juniors -> Under-12 -> defenders. Better to be able to do it in 2 steps,
  just MUFC U-12s -> defenders. So I will also add a 'rootName' property which would be an 
  intehgration of the levels eg MUFC U-12s. 
  Defaults - isRoot = parent ? true : false , rootName = name
*/
const GroupSchema = new mongoose.Schema({ 
  name:{
    type: String,
    trim: true,
    required: 'Group name is required'
  },
  desc:String,
  groupType:String,
  photo:{
    data:Buffer,
    contentType:String
  },
  admin:[{type:mongoose.Schema.ObjectId, ref:'User'}],
  coaches:[{type:mongoose.Schema.ObjectId, ref:'Coach'}],
  players:[{type:mongoose.Schema.ObjectId, ref:'User'}],
  subgroups:[{type:mongoose.Schema.ObjectId, ref:'Group'}],
  parent:{type:mongoose.Schema.ObjectId, ref:'Group'},
  datasets:[DatasetSchema],
  //would be better if groups are embedded as subdocuments
  // subgroups:[this],
  followers:[{type:mongoose.Schema.ObjectId, ref:'User'}],
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  //todo - false by default, only true if created by parent group admin and they select public
  isPublic: {
    type: Boolean,
    default: true
  }
})

//module.exports = {
export default mongoose.model('Group', GroupSchema)
