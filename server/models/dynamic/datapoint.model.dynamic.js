/*import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'

const datapointSchema = (entries) => {
  let schemaProperties = {
    values:[{name:String, value:Number}],
    //normally datapoint is attached to a player (eg the player who performed the test)
    player:{
        type:mongoose.Schema.ObjectId,
        ref:'User'
    },
    //could be attached to more than one player 
    players:[{
        type:mongoose.Schema.ObjectId,
        ref:'User'
    }],
    //can be attached to a group
    group:{
        type:mongoose.Schema.ObjectId,
        ref:'Group'
    },
    //a datapoint can also be attached to a coach
    coach:{
        type:mongoose.Schema.ObjectId,
        ref:'Coach'
    },
    created: {
      type: Date,
      default: Date.now
    },
    eventDate:{
      type: Date,
      default: Date.now
    }
  }

entries.forEach(entry =>{
   //mesg undefined if not required
  let requiredMesg = entry.isRequired ? 
    schemaProperties[entry.name] +' is required' : undefined

  if(entry.type === 'string')
    schemaProperties[entry.name] = {
      type:String,
      required:requiredMesg
    }
  else if(entry.type === 'boolean')
    schemaProperties[entry.name] = {
      type:Boolean,
      required:requiredMesg
    }
  else
    schemaProperties[entry.name] = {
      type:Number,
      required:requiredMesg
    }
})
  const DatapointSchema = new mongoose.Schema(schemaProperties)

export default mongoose.model('Datapoint', DatapointSchema)*/
