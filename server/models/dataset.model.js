import mongoose from 'mongoose'
import crypto from 'crypto'
import DatapointSchema from './datapoint.model'
import DataValueFormatSchema from './datavalue-format.model'
import UserSchema from './user.model'
import GroupSchema from './group.model'

/*
todo - use classes
class DataValueFormat {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}
*/

export default new mongoose.Schema({
  isStandard:Boolean,
  visibleTo:[String],
  category:{
    type: String,
    trim: true,
    required: 'Category is required'
  },
  name: {
    type: String,
    trim: true,
    required: 'Name is required'
  },
  key: {
    type: String,
    trim: true,
    required: 'Key is required'
  },
  desc:String,
  nrParticipants: {
    type:Number,
    default:1
  },
  dataValueFormats:[DataValueFormatSchema],
  datapoints:[DatapointSchema],
  createdBy:{type:mongoose.Schema.ObjectId, ref:'User'},
  group:{type:mongoose.Schema.ObjectId, ref:'Group'}
})

//export default mongoose.model('Dataset', DatasetSchema)
