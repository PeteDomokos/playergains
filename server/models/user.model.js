import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'

const UserSchema = new mongoose.Schema({
  username:{
    type: String,
    trim: true,
    required: 'Username is required'
  },
  firstName: {
    type: String,
    trim: true,
    required: 'First name is required'
  },
  surname: {
    type: String,
    trim: true,
    required: 'Surname is required'
  },
  photo:{
    data:Buffer,
    contentType:String
  },
  email: {
    type: String,
    trim: true,
    unique: 'User --- Email already exists',
    match: [/.+\@.+\..+/, 'Please fill a valid email address'],
    required: 'Email is required'
  },
  hashed_password: {
    type: String,
    required: "Password is required"
  },
  salt: String,
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  //A player with no current teams is still a player provided
  //this field is not empty
  //ie Object.entries(obj).length === 0 && obj.constructor === Object
  //todo - change to playerInfo
  playerInfo:{
    type: PlayerSchema,
    default:{}
  },
  //todo - change to coachInfo
  coachInfo:{
    type: CoachSchema,
    default:{}
  },
  //all groups for which this user is in group.admin
  adminGroups:[{type:mongoose.Schema.ObjectId, ref:'Group'}],
  groupsFollowing:[{type:mongoose.Schema.ObjectId, ref:'Group'}],
  groupsViewed:[{type:mongoose.Schema.ObjectId, ref:'Group'}],
  //mainGroup can be admin, following or a player or coach group
  mainGroup:{type:mongoose.Schema.ObjectId,ref:'Group'}, 
  isSystemAdmin:{
    type:Boolean,
    default:false
  }
})

UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function() {
    return this._password
  })

UserSchema.path('hashed_password').validate(function(v) {
  if (this._password && this._password.length < 6) {
    this.invalidate('password', 'Password must be at least 6 characters.')
  }
  if (this.isNew && !this._password) {
    this.invalidate('password', 'Password is required')
  }
}, null)

UserSchema.methods = {
  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashed_password
  },
  encryptPassword: function(password) {
    if (!password) return ''
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex')
    } catch (err) {
      return ''
    }
  },
  makeSalt: function() {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  }
}

export default mongoose.model('User', UserSchema)
