import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'
import DataValueSchema from './datavalue.model'

/*
todo - use classes
class DataValue {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}

also need flexibility so value doesnt have to be number
*/
export default new mongoose.Schema({
  //normally a datapoint is attached to a player
  players:[{
      type:mongoose.Schema.ObjectId,
      ref:'User'
  }],
  //names of values are read from dataset
  dataValues:[DataValueSchema],
  eventDate:{
    type: Date,
    default: Date.now
  },
  location:String,
  surface:String,
  notes:String,
  creationDate: {
    type: Date,
    default: Date.now
  },
  createdBy:{
    type:mongoose.Schema.ObjectId,
    ref:'User'
  },
  isProjection:Boolean,
  isTarget:Boolean
})

/*
ADD LATER
  //a datapoint can be attached to a group instead of an individual player (eg a group test)
  group:{
      type:mongoose.Schema.ObjectId,
      ref:'Group'
  },
  //a datapoint can also be attached to a coach (eg number of hours worked)
  coach:{
      type:mongoose.Schema.ObjectId,
      ref:'Coach'
  },
*/