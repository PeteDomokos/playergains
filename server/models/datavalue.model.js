import mongoose from 'mongoose'
import crypto from 'crypto'

export default new mongoose.Schema({
  name:{
    type:String,
    required:'Name is required'
  },
  key:{
    type:String,
    required:'Key is required'
  },
  value:{
    type:Number,
    required:'Value is required'
  }
})

