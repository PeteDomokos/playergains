import mongoose from 'mongoose'
/**
*
*/
//const PlayerSchema = new mongoose.Schema({
export default new mongoose.Schema({
  registered:{
    type:Boolean,
    default:false
  },
  mainGroup:{
      type:mongoose.Schema.ObjectId,
      ref:'Group'
  },
  groups:[{
    type:mongoose.Schema.ObjectId,
    ref:'Group'
  }],
  positions:[String],
  dob:Date,
  desc:String,
  startDate:{
    type:Date,
    default:Date.now
  },
  admin:[{type:mongoose.Schema.ObjectId, ref:'User'}]
})

//export default mongoose.model('Player', PlayerSchema)

