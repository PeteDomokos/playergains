//year, month, day, hour, minute, second, millisecond
//note: month from 0 to 11, hours produces 2 less than what we put in constructor
let week0Date = new Date("2019-07-02T18:55:52.815Z") //jul
let week1Date = new Date(2019,6,9,20,24,0,0) //jul
let week3Date = new Date(2019,6,24,15,24,0,0) //jul
let week5Date = new Date(2019,7,7,15,24,0,0) //aug
let week7Date = new Date(2019,7,21,15,24,0,0) //aug

const breham ={
    id:'5db70842bde0f61dd051ada8',
    /*test:{category:'fitness', test:'tTest', 
      data:{date:Date.now, time:13.5, points:95}
    }*/
    tests:[
    //touch
    {category:'skills', test:'touch', 
      data:{date:week0Date, left:25, right:15, advanced:5, points:45}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week0Date, reps:7.5, points:13}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week0Date, time:6.9}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week0Date, distance:1650}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week0Date, time:13.5}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week0Date, time:9.9}
    }
  ]
 }





  export default breham

