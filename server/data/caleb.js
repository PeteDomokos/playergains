//year, month, day, hour, minute, second, millisecond
//note: month from 0 to 11, hours produces 2 less than what we put in constructor
let week0Date = new Date("2019-07-02T18:55:52.815Z") //jul
let week1Date = new Date(2019,6,9,20,24,0,0) //jul
let week3Date = new Date(2019,6,24,15,24,0,0) //jul
let week5Date = new Date(2019,7,7,15,24,0,0) //aug
let week7Date = new Date(2019,7,21,15,24,0,0) //aug

const caleb = {
  id:'5db6e51cf9b232271083d282',
  
  /*test: {category:'fitness', test:'tTest', 
      data:{date:Date.now, time:11.8, points:95}
    }*/
   tests:[
    //1st test-----------------------
    //touch
    {category:'skills', test:'touch', 
      data:{date:week0Date, points:65}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week0Date, reps:6.5, points:15}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week0Date, time:10.9}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week0Date, distance:1760}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week0Date, time:13.9}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week0Date, time:11.2}
    },
    //2nd test-----------------------
    //touch
    {category:'skills', test:'touch', 
      data:{date:week1Date, points:70}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week1Date, reps:7, points:16}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week1Date, time:10.5}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week1Date, distance:1800}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week1Date, time:13.8}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week1Date, time:10.9}
    },
    //3rd test-----------------------
    //touch
    {category:'skills', test:'touch', 
      data:{date:week3Date, points:60}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week3Date, reps:7, points:19}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week3Date, time:10.5}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week3Date, distance:1840}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week3Date, time:13.3}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week3Date, time:10.9}
    },
    //4th test-----------------------
    //touch
    {category:'skills', test:'touch', 
      data:{date:week5Date, points:75}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week5Date, reps:7.5, points:20}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week5Date, time:10.1}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week5Date, distance:1880}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week5Date, time:13.0}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week5Date, time:10.4}
    },
    //5th test-----------------------
    //touch
    {category:'skills', test:'touch', 
      data:{date:week7Date, points:75}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week7Date, reps:7.5, points:19}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week7Date, time:10.0}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week7Date, distance:1920}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week7Date, time:13.0}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week7Date, time:10.4}
    }
  ]
}

export default caleb